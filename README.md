# 🎾 Setmatch 🎾 - An inverted index for real-time matching of boolean expressions

Setmatch is a library that keeps an index of boolean expression trees in memory, and matches them against incoming
documents. It's designed to scale up to hundreds of thousands (potentially millions) trees indexed in memory, matching
against hundreds of  thousands of documents per second. This problem frequently comes up in ad-tech (bidding on 
impressions) as well as social media analytics.

## [Documentation and a better explanation is now hosted on GitLab Pages - Click Here](https://burningmime.gitlab.io/setmatch/)
