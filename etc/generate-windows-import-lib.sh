#!/usr/bin/env bash

# Copyright 2019-2020 Robert William Fraser IV.
#
# This file is part of setmatch.
#
# setmatch is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# setmatch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

set -eo pipefail

scriptFile=$(readlink -f "${BASH_SOURCE[0]}")
scriptDir=$(cd -P "$(dirname "$scriptFile")" >/dev/null && pwd)
cd "$scriptDir"

exportsFile="libsetmatch.exports"
defFile="libsetmatch.def"
libFile="libsetmatch.lib"

echo "LIBRARY libsetmatch" >"$defFile"
echo "EXPORTS" >>"$defFile"
while read -r line; do
    # not exposing any JNI functions in the export lib (java has its own way of finding them)
    if [[ ("$line" != _Java*) && ("$line" != _JNI*) ]]; then
        # the ${line:1} strips off the first character, which is always an underscore because...  fuck if i know why
        # osx linker wants extra underscores
        echo "    ${line:1}" >>"$defFile"
    fi
done <"$exportsFile"

# On my system there's no llvm-dlltool, just an llvm-dlltool-6.0 and llvm-dlltool-8
# There are better ways to find the executable by scanning the path, but they generate a bunch of errors and take a
# long time on my system, so just doing this for now. Can adjust later if needed.
dllToolExe=$(ls /usr/bin/llvm-dlltool-* | tail -n1)
"$dllToolExe" -m "i386:x86-64" -d "$defFile" -l "$libFile"

