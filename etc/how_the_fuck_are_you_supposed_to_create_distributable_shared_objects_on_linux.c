// This file is in the public domain because it is so awful a hack that I give up all copyright to it.

#ifndef ENABLE_GLIBC_HACK
#error "This file is only needed for a very specific build situation"
#endif

//#include <time.h>

// This problem is so disgustingly awful, if I try to describe it in a comment, I might throw up.
// Just take some pepto bismol, have a bucket next to you in case, then visit this stack overflow link:
// https://stackoverflow.com/questions/4032373/linking-against-an-old-version-of-libc-to-provide-greater-application-coverage
// https://gist.github.com/nicky-zs/7541169

// declare private aliases to the wrapped functions
double __exp_2_2_5(double);
//int __clock_gettime_2_2_5(clockid_t, struct timespec*);
//void* __memcpy_2_2_5(void*, const void*, size_t);

// attach those private aliases to the correct symbols
// this "assembly" just causes the assembler to emit a linker directive
asm(".symver __exp_2_2_5, exp@GLIBC_2.2.5");
//asm(".symver __clock_gettime_2_2_5, clock_gettime@GLIBC_2.17");
//asm(".symver __memcpy_2_2_5, memcpy@GLIBC_2.2.5");

// create the wrapper functions themselves
double __wrap_exp(double a) { return __exp_2_2_5(a); }
//int __wrap_clock_gettime(clockid_t a, struct timespec* b) { return __clock_gettime_2_2_5(a, b); }
//void* __wrap_memcpy(void* dest, const void* src, size_t n) { return __memcpy_2_2_5(dest, src, n); }

// TODO clock_gettime moved; need to find way to link to the one in librt. others older than that commented out

// HOW TO FIND/FIX
// ===================================
// 1. build on newer linux inside docker:
//     ./build.sh clean
//     docker/build.sh release -static
// 2. get a list of versioned functions:
//     objdump -p bin/release/libsetmatch.so
// 3. find things that look suspiciously high (eg 2.14, 2.29, etc)
// 4. find out what the functions are:
//     objdump -T bin/release/libsetmatch.so | grep '2\.18'
// 5. find better versions, eg:
//      objdump -T /lib/x86_64-linux-gnu/libm.so.6 | grep exp
// 6. Look up function signatures
// 7. Create dummy functions:
//      double __exp_2_2_5(double);
//      asm(".symver __exp_2_2_5, exp@GLIBC_2.2.5");
// 8. Create wrapper functions to call the dummies:
//      double __wrap_exp(double a) { return __exp_2_2_5(a); }
// 9. Ensure this file is *not* built with link-time optimization
// 10. Add the wrappers to the linker arguments:
//      -Wl,--wrap=exp
// 11. Repeat until the drugs wear off
//
// Ideally, this process would be automated somehow, but I don't want to go there yet. There's a script in the StackOverflow
// link that does that, which could be adapted.
//
// Hopefully, this doesn't introduce any ABI incompatabilities because of caller expectations (*especially* important when dealing with the gcc builtins)
