# TODO

- Use tickets instead of a readme file for this (doing it this way is easier while I'm still the only one working on it)

## Before release
 
- Fix negation (again...)
- Java needs a new example (all the docs, really, should get a pass)
- Change binary rule format to incorporate sub-version number
 
## Long term

- Linux thing
   - clock_gettime is the main concern; others are taken care of already
       - It's in a different .so file, so the assembly hack won't work
       - The next step is either to compile on modern linux, then link on an old version (likely impossible)
         or to look into the ELF binary format for object files and maybe modify the actual file itself.
   - If i use glibc 2.30 that might have fixed the tls bug in libdl
       - FUCK LINUX MINT! HOW LONG DOES IT TAKE TO UPGRADE GLIBC?
       - This also means the game I bought a month ago still won't play because it requires 2.28
       - There's something seriously, fundamentally wrong with glibc versioning that's been around for 40+ years
         and apparently no one wants to talk about it. This shit just doesn't happen on Windows. But somehow the Linux
         community has decided a 1-year-old OS is too old to build software for... oh, and by the way your distro
         is on "long-term-support" mode, so you better reinstall if you feel like running new software.
- Track memory usage per database (need to rethink this; probably will need to use jemalloc anyway)
- Graph optimization (needs new algorithm)
- Merge and simplify OR expressions with ranges
- Hook up to espresso logic minimizer to further reduce our SOP form
- Retrieve rule as JSON

## Would be nice

- Maybe a phrase-only mode?
- C/C++ docs should link back to the main project page instead of being their own thing.
- CODE CLEANUP: Everywhere that uses "int" should be changed to use int32_t, uint32_t, size_t, int64_t, or a specialized wrapper type
- Look into trimming excess memory after many removals
- Get build script working on OSX (oh hai Catalina thanks for switching to zsh and breaking everyone's scripts)
- Rewrite negation to use contrapositives
- Boolean tests should be improved
- Profiling (need line-level data; function-level is kinda useless because of all the inlining and rearranging of code)
- Get together a performance suite
- Hierarchical and grouped predicates
- Functions to write/read entire index to a file.
- JSON parser could have nicer errors that actually point to where the problem is and some of the chars around it.
- Generate version string with git commit hash, etc.

## Release procedure

1. "docker/build.sh"
2. Remove "-SNAPSHOT" in POM
3. Update java api docs to reference new version
4. Update version in doxygen file
5. Update version in common.hpp
6. Remove the "(in progress)" from changelog
7. Go to Java dir and "mvn deploy -P release" (takes waaaaay longer than it should)
8. Commit to master
9. Merge to release
10. Download artifacts from GitLab
11. Create GitLab tag and upload the artifacts back
12. Bump to next SNAPSHOT version in doxygen, common, and POM
