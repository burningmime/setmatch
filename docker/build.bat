@echo off & setlocal
rem Copyright 2019-2020 Robert William Fraser IV.
rem
rem This file is part of setmatch.
rem
rem setmatch is free software: you can redistribute it and/or modify
rem it under the terms of the GNU Lesser General Public License as published by
rem the Free Software Foundation, either version 3 of the License, or
rem (at your option) any later version.
rem
rem setmatch is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU Lesser General Public License for more details.
rem
rem You should have received a copy of the GNU Lesser General Public License
rem along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

rem change to script directory (allows the script to be executed from anywhere)
for %%i in ("%~dp0..") do set "folder=%%~fi"
pushd %folder%

rem Build the docker image.
set imagename="registry.gitlab.com/burningmime/setmatch/build:0.8.1"
copy /y java\pom.xml docker\pom.xml || goto End
docker build -t %imagename% docker || goto End

rem Run the actual build. Pass arguments along to the linux build script if any were given, otherwise assume full build.
rem This is repetitive but I don't really know enough about batch files to fix it and don't care to learn until this
rem gets a lot worse than it is now.
if [%1] == [] goto NoArgs
docker run --rm -v "%folder%:/build-context" %imagename% bash -c "/build-context/build.sh %*%"
goto End
:NoArgs
docker run --rm -v "%folder%:/build-context" %imagename% bash -c "/build-context/build.sh full"

rem Return to directory where script was invoked
:End
popd
