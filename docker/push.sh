#!/usr/bin/env bash

# Copyright 2019-2020 Robert William Fraser IV.
#
# This file is part of setmatch.
#
# setmatch is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# setmatch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

set -eo pipefail
imageName="registry.gitlab.com/burningmime/setmatch/build:0.8.1"
if grep -qE "(Microsoft|WSL)" /proc/version &>/dev/null; then dockerCmd="docker.exe"; else dockerCmd="docker"; fi
$dockerCmd login registry.gitlab.com
$dockerCmd push "$imageName"
