#!/usr/bin/env bash

# Copyright 2019-2020 Robert William Fraser IV.
#
# This file is part of setmatch.
#
# setmatch is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# setmatch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

set -eo pipefail
scriptFile=$(readlink -f "${BASH_SOURCE[0]}")
scriptDir=$(cd -P "$(dirname "$scriptFile")" >/dev/null && pwd)

# Must run pelican from this directory
cd "$scriptDir"

# Clean out whatever was here before
rm -rf output
rm -rf pages

# Actually run it
pelican -s pelicanconf.py

# Pelican generates some junk, so go ahead and remove all but the necessary bits
cp -ar output/pages pages
mv pages/setmatch.html pages/index.html
rm -rf output
cp "m.css/css/m-dark.compiled.css" pages/

# Yes, I know using regex to edit HTML is a terrible and evil thing; cthulhu fhtagn
for file in pages/*.html; do
    # Fix some of the URLs
    sed -i 's|href="/pages/|href="|g' "$file"
    sed -i 's|href="/|href="|g' "$file"
    sed -i 's|href="\.\./"|href="index.html"|g' "$file"

    # Remove the "title" attribute from latex-generated SVGs
    sed -i 's|^<title>$|<!--|' "$file"
    sed -i 's|^</title>$|-->|' "$file"

    # if TikZ is ever needed, the stroke property will be wrong
    # this is a very hacky way to do it, while the "right" way is to modify the CSS
    # sed -i "s|stroke='#000000'|stroke='#dcdcdc'|g" "$file"
done

# One of the filter lists on uBlock Origin hides stuff with the class "m-warning"
for file in pages/*; do
    sed -i 's|m-warning|m-mellow-yellow|g' "$file"
done

# Manually adding this style so table of contents has a transparent background
echo ".m-note.m-toc { background-color: rgba(0,0,0,0.5); }" >>pages/m-dark.compiled.css

# Copy title banner
cp background.jpg pages/
