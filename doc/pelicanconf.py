#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Copyright 2019-2020 Robert William Fraser IV.
#
# This file is part of setmatch.
#
# setmatch is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# setmatch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import unicode_literals

AUTHOR = 'Bobby Fraser'
SITENAME = 'setmatch'
#SITEURL = 'https://burningmime.gitlab.io/setmatch'

# These are all the defaults that Pelican gives
PATH = 'content'
TIMEZONE = 'America/New_York'
DEFAULT_LANG = 'en'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
DEFAULT_PAGINATION = False

# Need this because we're not actually using the whole Pelican directory structure
RELATIVE_URLS = True

# m.css theme
THEME = 'm.css/pelican-theme'
THEME_STATIC_DIR = 'static'
DIRECT_TEMPLATES = ['index']
M_CSS_FILES = ['https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i%7CSource+Code+Pro:400,400i,600', 'm-dark.compiled.css']
M_THEME_COLOR = '#22272e'
PLUGIN_PATHS = ['m.css/pelican-plugins']
PLUGINS = ['m.htmlsanity', 'm.components', 'm.code', 'm.dot', 'm.math', 'm.plots', 'm.images']
#M_HTMLSANITY_HYPHENATION = True
M_DOT_FONT = 'Source Sans Pro'
M_DOT_FONT_SIZE = 16.0
M_MATH_RENDER_AS_CODE = False
M_PLOTS_FONT = 'Source Sans Pro'
M_DISABLE_SOCIAL_META_TAGS = True
M_IMAGES_REQUIRE_ALT_TEXT = False

# Footer at bottom of page
M_FINE_PRINT = """
| © 2019 Robert William Fraser IV. Distributed under `LGPL 3.0 <https://www.gnu.org/licenses/lgpl.html>`__.
| Documentation generated with `Pelican <https://getpelican.com>`__  and `m.css <https://mcss.mosra.cz>`__.
| Header image © iStockphoto.com / monsitj. All rights reserved.
"""

# need to add some additional stuff to the latex preamble so our figures render correctly
import sys
sys.path.insert(0, 'm.css/pelican-plugins')
import latex2svgextra
latex2svgextra.params["preamble"] += r"""
\usepackage{array,graphicx}
\usepackage{booktabs}
\usepackage{pifont}
"""

# The top navigation bar
M_LINKS_NAVBAR1 = [
    ('Java', 'java-api.html', 'java-api', [
        ('Overview', 'java-api.html', 'java-api'),
        ('API Docs', 'javadoc/com/burningmime/setmatch/package-summary.html', ''),
        ('Example', 'https://gitlab.com/burningmime/setmatch/blob/release/java/src/test/java/com/burningmime/setmatch/examples/JavaExample.java', '')]),
    ('C/C++', 'c-and-c-apis.html', 'c-and-c-apis', [
        ('Overview', 'c-and-c-apis.html', 'c-and-c-apis'),
        ('JSON Format', 'json-format.html', 'json-format'),
        ('C++ API Docs', 'doxygen/namespacesetmatch.html', ''),
        ('C API Docs', 'doxygen/setmatch_8h.html', ''),
        ('C++ Example', 'https://gitlab.com/burningmime/setmatch/blob/release/src/public/cpp_api_example.cpp', ''),
        ('C Example', 'https://gitlab.com/burningmime/setmatch/blob/release/src/public/c_api_example.c', '')]),
    ('Misc', '', '', [
        ('Changelog', 'changelog.html', 'changelog'),
        ('Building From Source', 'building.html', 'building'),
        ('Implementation Overview', 'implementation-overview.html', 'implementation-overview'),
        ('Setmatch on GitLab', 'https://gitlab.com/burningmime/setmatch', ''),
        ('License', 'https://www.gnu.org/licenses/lgpl-3.0.en.html', '')
    ])]
