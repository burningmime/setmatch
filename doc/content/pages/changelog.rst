..
    Copyright 2019-2020 Robert William Fraser IV.

    This file is part of setmatch.

    setmatch is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    setmatch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

Changelog
=========

:cover: background.jpg

.. contents::
    :class: m-note m-toc

0.9
---

0.8.2
~~~~~

- **Breaking change to Java API:** Numeric methods on ``RuleNode`` moved to ``TermNode`` for a
  more "fluent" style API. For example, ``RuleNode.greaterThan("x", 15)`` would now be written
  as ``RuleNode.term("x").greaterThan(15)``.
- **Breaking changes to Java API:** Tokens are now constructed using the ``Token.of()`` methods
  instead of the (rather awkward) ``Token.literal()`` and ``Token.number()`` methods.
- Rewrote the Java examples, and cleaned up documentation.
- Small predicate lists are stored as actual lists instead of compressed bitsets. This is a win
  for highly sparse data sets with many columns. Dense rows (ie a single subrule referencing hundreds
  of predicates) will still use the bitset.
- The phrase trie has been optimized to support long phrases with few common sub-phrases.

0.8
---

0.8.1
~~~~~

- Fix Linux... kinda. It should run on some modern distros. I'd like to fix it to run on, say, Ubuntu 14,
  or RHEL, but I don't have the knowledge or ability to hack the linker and object file formats, which I
  believe would be nessescary to change the referenced library.
- Remove ``setmatch_matchArrayS()`` from C API
- Few bug fixes

0.8.0
~~~~~

- **Breaking changes to Java API:** ``RuleNode.equalTo()`` and ``RuleNode.notEqualTo()``
  functions take an int instead of a double (to avoid floating point issues).
- **New functions in C API:** ``setmatch_matchArrayN()`` and ``setmatch_matchArrayS()``
  functions added so you don't need to pass a callback function.
- Fixed a few cases where errors didn't have error code.
- Many, many, internal changes.
- Also a few bug fixes.

0.7
---

0.7.2
~~~~~

- Fix major bug when removing numeric/range predicates.

0.7.1
~~~~~

- Rules consisting only of negated predicates are supported
- Breaking change to Java API: ``compileRule(String)`` changed to ``compileRuleJson(String)``.

0.7.0
~~~~~

- Numeric predicates! A very early implementation of these which is possibly buggy and subject to change.
- Major bugfix with multi-predicate negation
- **Breaking change:** Both Java and C++ APIs now throw specialized exceptions instead of ``RuntimeException`` and ``std::runtime_error``.
  Java API throws ``com.burningmime.setmatch.SetmatchException``. C++ API throws ``setmatch::SetmatchError``.
- **Breaking change to Java API:** Instead of using overloading for all the versions off ``addRule``, it now has
  3 methods with different names: ``addRuleCompiled``, ``addRuleJson`` and ``addRule``.

  + This should make it clearer what's going on, and since some of the node methods accept plain Strings, reduce
    confusion from that front.

- **Deprecated:** ``debugGetRuleAsString()`` is deprecated in all APIs.

  + It was designed for debugging use, but is arbitrarily truncated to 2000 characters and has implementation-defined
    handling of some characters (for example, it transforms ``\r\n`` into ``\n``, and transforms tabs into spaces).
    This method will be replaced with something more useful and well-speced.

- Most error messages are prefixed by a 4-digit code. These are still undocumented for now and might change soon. There
  are still some errors without the code.

0.6
---

0.6.3
~~~~~

- Hotfix for bug with Java. Apparently Java can return either "x86_64" or "amd64" for the "os.arch" property depending
  on its mood and/or the current phase of the moon.

0.6.2
~~~~~

- Switched from AVX2 to AVX
- Stripped libraries to reduce JAR size.

0.6.1
~~~~~

- Improved performance on Windows (and probably OSX).
- Minor bugfix with failed assertions.

0.6.0
~~~~~

- Initial OSX implementation. Completely untested.
- Fixed a major bug with negation.

0.5
---

- **Breaking changes to C++ API:** ``compileRuleJson()`` is no longer a static member function, but a free function. Just
  replace all calls to ``RuleDB::compileRuleJson()`` with ``compileRuleJson()``.
- Fixed missing copy/move constructors and assignment operators in C++ API.
- Reduced memory usage of Aho-Corasick Trie
- Internal changes to support numbers.

0.4
---

- **Breaking change to Java API:** match() now accepts Token objects as well as strings. These token objects will
  eventually be used to represent other types of data, such as numeric data. To support this change, match() now accepts
  a collection or array of objects (expected to be a mix of Strings and Tokens).

  + Existing code that used a string array may replace ``db.match(stringArray)`` with ``db.match(Arrays.asList(stringArray))``.

- **Breaking change to C/C++ API:** any tokens that start with the ``{`` character must now escaped with an extra ``{``
  at the beginning (for example, ``{hello}{world}`` must be written ``{{hello}{world}``).
- Windows DLL isn't crashing anymore.
- Improved performance on databases with few distinct predicates (under 256) but many permutations, as might come up
  in some scientific computing or machine learning tasks.
- Better handling of binary data and unicode.

0.3
---

* New website
* Documentation for C/C++ APIs
* Small changes to C++ API
* Roughly 25-30% speedup in match performance by rearranging some data and code for cache efficiency.

0.2
---

* Added C and C++ APIs
* Breaking changes to binary rule format
* Changes to Java API:

  - Added RuleNode overloads that take Collections and Streams
  - Added debugGetRuleAsString()
  - Added containsRule()
  - addRule() now returns a boolean indicating if a rule was replaced
  - Added pollMetrics()
  - Removed debugPrintMetrics()

* Increased limit on unique predicates per rule to 1024
* Increased limit on token length
* Added continuous integration

0.1
---

* Initial release
