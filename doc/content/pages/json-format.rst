..
    Copyright 2019-2020 Robert William Fraser IV.

    This file is part of setmatch.

    setmatch is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    setmatch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

JSON Format
===========

:cover: background.jpg

The C and C++ APIs require that you pass the rule in JSON (and it's an option for the Java API). The encoding is fairly
straightforward. Here's a kind of EBNF-like description, which probably isn't very clear:

.. code:: ebnf

    Rule       ::= { "rule" : Node }
    Node       ::= AndNode | OrNode | NotNode | PhraseNode | RangeNode
    AndNode    ::= { "and" : [ Node+ ] }
    OrNode     ::= { "or" : [ Node+ ] }
    NotNode    ::= { "not" : Node }
    PhraseNode ::= { "phrase" : [ String+ ] }
    RangeNode  ::= { "range" : RangeData }
    RangeData  ::= { "term" : String, "min" : Number, "max" : Number, "kind" : RangeKind }
    RangeKind  ::= "INCLUSIVE" | "MIN_INCLUSIVE" | "MAX_INCLUSIVE" | "EXCLUSIVE"

It might be clearer with an example. Here's a boolean expression tree:

.. digraph::

    AND1[label="AND" shape=ellipse]
    OR1[label="OR" shape=ellipse]
    OR2[label="OR" shape=ellipse]
    AND2[label="AND" shape=ellipse]
    AND3[label="AND" shape=ellipse]
    OR3[label="OR" shape=ellipse]
    AND4[label="AND" shape=ellipse]
    NOT1[label="NOT" shape=ellipse]

    P1[label="coffee" shape=box]
    P2[label="tea" shape=box]
    P3[label="eggs" shape=box]
    P4[label="bacon" shape=box]
    P5[label="sausage" shape=box]
    P6[label="cereal" shape=box]
    P7[label="fresh fruit" shape=box]
    P8[label="toast" shape=box]
    P9[label="gluten" shape=box]

    AND1 -> {OR1 OR2 AND4}
    OR1 -> {P1 P2}
    OR2 -> {AND2 AND3}
    AND2 -> {P3 OR3}
    OR3 -> {P4 P5}
    AND3 -> {P6 P7}
    AND4 -> {P8 NOT1}
    NOT1 -> P9

The java code for that would be:

.. code:: java

    db.addRule(id,
        RuleNode.and(
            RuleNode.or("coffee", "tea"),
            RuleNode.or(
                RuleNode.and("eggs", RuleNode.or("sausage", "bacon")),
                RuleNode.and("cereal", RuleNode.phrase("fresh", "fruit"))),
            RuleNode.and("toast", RuleNode.not("gluten"))));

And the generated JSON:

.. code:: json

    { "rule":
        { "and": [
            { "or": [{ "phrase": ["coffee"] }, { "phrase": ["tea"] }]},
            { "or": [
                { "and": [{ "phrase": ["eggs"] }, { "or": [{ "phrase": ["sausage"] }, { "phrase": ["bacon"] }]}]},
                { "and": [{ "phrase": ["cereal"] }, { "phrase": ["fresh", "fruit"] }]}]},
            { "and": [ { "phrase": ["toast"] }, { "not": { "phrase": ["gluten"] }}]}]}}

That's a lot of closing brackets, but it shows the basic recursive nature. Here's an example involving ranges:

.. code:: java

    RuleNode.or(
        RuleNode.greaterThan("value", 10),
        RuleNode.lessThanOrEqualTo("value", -22.5),
        RuleNode.equalTo("value", 0.0000032),
        RuleNode.between("value", 4, 6, Interval.MAX_INCLUSIVE));

.. code:: json

    { "rule":
        { "or": [
            { "range": { "term": "value", "kind": "EXCLUSIVE", "min": 10.0 }},
            { "range": { "term": "value", "kind": "INCLUSIVE", "max": -22.5 }},
            { "range": { "term": "value", "kind": "INCLUSIVE", "min": 3.2E-6, "max":3.2E-6 }},
            { "range": { "term": "value", "kind": "MAX_INCLUSIVE", "min": 4.0, "max": 6.0 }}]}}

- The "equal to" range has both max and min set the same
- The "greater than" and "less than" ranges don't have a maximum or minimum element (respectively)
- It's just a JSON number, so scientific notation is supported

.. note-warning::

    Note that single tokens must be wrapped into a ``"phrase"`` node. ``{"and":["x","y"]}`` is not legal, you must use
    ``{"and":[{"phrase":["x"]},{"phrase":["y"]}]}``.

.. note-info::

    The root node can have other members besides ``"rule"`` if you want to store extra info (ie the ID)
    in the JSON. These will be silently ignored. Nodes other than the root must follow the schema exactly.

.. note-danger::

    I pity the fool who must write these by hand. For C++, you might want to look into `RapidJSON <https://github.com/Tencent/rapidjson>`__.
    There's a `test helper <https://gitlab.com/burningmime/setmatch/blob/release/main/src/test/test_rule_enc.cpp>`__
    which shows it in use for simple 2-level forms.
