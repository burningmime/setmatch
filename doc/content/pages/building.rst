..
    Copyright 2019-2020 Robert William Fraser IV.

    This file is part of setmatch.

    setmatch is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    setmatch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

Building
========

:cover: background.jpg

.. contents::
    :class: m-note m-toc

Easy mode: Building in Docker
-----------------------------

There are two scripts in the Docker directory,
`build.sh <https://gitlab.com/burningmime/setmatch/blob/release/docker/build.sh>`__
and `build.bat <https://gitlab.com/burningmime/setmatch/blob/release/docker/build.bat>`__ that will do this for you
without any fuss. Once it's done, look in the ``artifacts`` directory under the root.

However, be warned that this is a *very* slow process. The first time you run it can take up to an hour to pull and build
all the dependencies, and subsequent runs might take ~15 minutes. If you plan to actually work on setmatch itself, you'll
probably want to get it building locally.

If you're going to submit a merge request, please run this to make sure the build and tests work.

Dependencies
------------

- CMake 3.10 (or higher)
- g++ 7 (or higher)
- Boost 1.65 (or higher)
- JDK 8 (or higher, either OpenJDK or Oracle should be fine)
- Maven 3.0 (or higher)
- Valgrind (required for some tests)
- lcov (optional; for generating code coverage reports)

Quick Start for Ubuntu 16+
--------------------------

.. code:: bash

    sudo apt-get update
    sudo apt-get install -y git default-jdk maven cmake make g++ libboost-all-dev valgrind lcov
    git clone https://gitlab.com/burningmime/setmatch.git
    chmod u+x setmatch/build.sh
    setmatch/build.sh debug
    setmatch/build.sh release
    setmatch/build.sh java

Build script
------------

The ``build.sh`` script wraps calls to maven, cmake, unit tests, etc.

::

    USAGE: build.sh build_type [options ...]

    Build types:
        debug             = Build debug library and run unit tests
        release           = Build release library and run unit tests
        java              = Build java wrapper (requires you build release lib first)
        docs              = Build documentation
        clean             = Remove all build artifacts
        full              = Full build

    Options for debug/release builds:
        -no-tests     = Skip unit tests
        -no-valgrind  = Skip valgrind tests (implied by -no-tests)
        -test-chaos   = Include chaos test
        -test=*       = For manual specification of test cases (see boost docs)
        -mingw        = Cross-compile for Windows using MinGW
        -osxcross     = Cross-compile for Mac OSX using osxcross
        -static       = Static link boost and libstdc++ (implied by -osxcross and -mingw)
        -coverage     = Generate code coverage (requires lcov, only working on gcc7)
        --            = All subsequent arguments passed to cmake

    Options for Java builds:
        -no-tests     = Skip unit tests
        --            = All subsequent arguments passed to maven


Linking static Boost into the shared libraries
----------------------------------------------

A quick note about the *-static* option. This will pass ``-DBoost_USE_STATIC_LIBS=ON`` to cmake, which passes it onto
`FindBoost <https://cmake.org/cmake/help/latest/module/FindBoost.html>`__. This causes it to link the boost libraries
statically - which normally will fail the build with some strange linker error like
``relocation R_X86_64_PC32 against symbol _ZTVNsomething can not be used when making a shared object; recompile with -fPIC``.
So if you want to do this, you need to build boost yourself, something like:

.. code:: bash

    wget https://dl.bintray.com/boostorg/release/1.67.0/source/boost_1_69_0.tar.gz
    tar xzf boost_1_69_0.tar.gz
    cd boost_1_69_0
    ./bootstrap.sh
    ./b2 --with-atomic --with-chrono --with-date_time --with-system --with-thread --with-filesystem --with-test cxxflags=-fPIC stage
    sudo ./b2 --with-atomic --with-chrono --with-date_time --with-system --with-thread --with-filesystem --with-test cxxflags=-fPIC install
    cd ..
    sudo rm -rf boost_1_69_0
    setmatch/build.sh release -static

The only reason to go through all that effort is if you want to build the library locally and then run it on a different
machine without boost installed (such as inside a docker container). Note this also causes libstdc++ to be linked
statically (but not glibc).

Cross-compiling for Windows
---------------------------

Everything works inside WSL, so on modern versions of Windows, just pull up the MS Store, install Ubuntu, and follow the
above instructions. `Here are more instructions if using the CLion IDE <https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html>`__.
If you want native Windows binaries, you have to cross-compile, which can be done from within WSL:

.. code:: bash

    sudo apt-get install -y g++-mingw-w64-x86-64

    wget https://dl.bintray.com/boostorg/release/1.67.0/source/boost_1_69_0.tar.gz
    tar xzf boost_1_69_0.tar.gz
    cd boost_1_69_0
    ./bootstrap.sh
    echo "using gcc : mingw : x86_64-w64-mingw32-g++-posix ;" > user-config.jam
    sudo ./b2 --prefix=/usr/x86_64-w64-mingw32/ \
            --with-atomic --with-chrono --with-date_time --with-system --with-thread --with-filesystem --with-test \
            --user-config=user-config.jam toolset=gcc-mingw target-os=windows address-model=64 threading=multi install
    cd ..
    sudo rm -rf boost_1_69_0
    
    setmatch/build.sh release -mingw

OSX cross-compile, documentation, and troubleshooting
-----------------------------------------------------

`Check the Dockerfile. <https://gitlab.com/burningmime/setmatch/blob/release/docker/Dockerfile>`__

No, really. The Docker environment is used for continuous integration, meaning that all the dependencies are kept up
to date and working. The documentation building, in particular, has gigabytes of dependencies (like LaTeX, Python, and
some fonts) that are normally not needed. osxcross is even worse.
