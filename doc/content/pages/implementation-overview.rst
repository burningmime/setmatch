..
    Copyright 2019-2020 Robert William Fraser IV.

    This file is part of setmatch.

    setmatch is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    setmatch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

Implementation Overview
=======================

:cover: background.jpg

.. contents::
    :class: m-note m-toc

Introduction
------------

The original problem Setmatch was designed to solve was a problem of social media analytics. Imagine hundreds of thousands
of social media messages (ie Twitter or Weibo) were passing through your servers every second, and you needed to look
for certain combinations of phrases -- and there were thousands of these combinations.

Setmatch was born from the question: "What if there wasn't hardware to throw at it, and it had to be done as efficiently
and scalably as possible?"

Looking into it more, the problem of matching a large volume of incoming documents against a large set of boolean expressions
seems to come up in the ad-tech industry, specifically in designing `real time bidders <https://en.wikipedia.org/wiki/Real-time_bidding>`__.
I came across `this paper <https://link.springer.com/chapter/10.1007/978-3-319-55705-2_23>`__. The essential idea is to
use a compressed sparse bit matrix to enable using bitwise operators for matching.

.. note-info::

    There are a number of differences between the algorithm described there and the one implemented in setmatch. For
    example, that algorithm does not allow elements to be added or removed at runtime, and uses a different compression
    scheme. Further, setmatch contains a number of algorithmic innovations designed to improve cache usage and exploit
    the AVX instruction set.

Setmatch is implemented in C++ with a Java shim. I chose C++ to allow it to be usable from many platforms. It shouldn't
interfere with the runtime or garbage collector of Python, .NET, Go, node.js, Ruby, or whatever the flavor of the month
is. Being able to directly control the memory layout is a nice bonus, because I've gotten 20%+ speedups just by playing
with cache alignment.

Converting input expressions
----------------------------

Boolean expressions are generally expressed in some form of tree. For example, a hotel might offer the following
continental breakfast:

* Coffee or tea
* Your choice of:

  - Eggs with bacon or sausage
  - Cereal with fresh fruit

* Gluten-free toast

Programmers like to be exact with things, so they might write that as ``(coffee OR tea) AND ((eggs AND (bacon OR sausage)) OR (cereal AND "fresh fruit")) AND (toast AND (NOT gluten))``,
which would decompose into the following tree:

.. digraph::

    AND1[label="AND" shape=ellipse]
    OR1[label="OR" shape=ellipse]
    OR2[label="OR" shape=ellipse]
    AND2[label="AND" shape=ellipse]
    AND3[label="AND" shape=ellipse]
    OR3[label="OR" shape=ellipse]
    AND4[label="AND" shape=ellipse]
    NOT1[label="NOT" shape=ellipse]

    P1[label="coffee" shape=box]
    P2[label="tea" shape=box]
    P3[label="eggs" shape=box]
    P4[label="bacon" shape=box]
    P5[label="sausage" shape=box]
    P6[label="cereal" shape=box]
    P7[label="fresh fruit" shape=box]
    P8[label="toast" shape=box]
    P9[label="gluten" shape=box]

    AND1 -> {OR1 OR2 AND4}
    OR1 -> {P1 P2}
    OR2 -> {AND2 AND3}
    AND2 -> {P3 OR3}
    OR3 -> {P4 P5}
    AND3 -> {P6 P7}
    AND4 -> {P8 NOT1}
    NOT1 -> P9

This is the level at which the setmatch API works. For example, to use the Java API to insert that, you can write:

.. code:: java

    db.addRule(id,
        RuleNode.and(
            RuleNode.or("coffee", "tea"),
            RuleNode.or(
                RuleNode.and("eggs", RuleNode.or("sausage", "bacon")),
                RuleNode.and("cereal", RuleNode.phrase("fresh", "fruit"))),
            RuleNode.and("toast", RuleNode.not("gluten"))));

However, that's not a very good format for indexing and retrieval. So the first step is to transform that input into
`Disjunctive Normal Form <https://en.wikipedia.org/wiki/Canonical_normal_form>`__ (aka sum of products form, for those
with a digital logic background). We flatten out ORs inside of ORs and ANDs inside of ANDS, push down negations to the
lowest level using DeMorgan's law, then distribute. This does end up turning perfectly innocent expressions into
monstrosities because of the distribution, which can be :math:`O(2^{N})` in the worst case. For example, the above
gets transformed into 6 minterms:

::

    coffee, eggs, sausage, toast, ~gluten
    coffee, eggs, bacon, toast, ~gluten
    coffee, cereal, "fresh fruit", toast, ~gluten
    tea, eggs, sausage, toast, ~gluten
    tea, eggs, bacon, toast, ~gluten
    tea, cereal, "fresh fruit", toast, ~gluten

Because of how the database shares redundant data, and just how fast the bitwise operations are, that's rarely a
performance concern, although it could be a memory concern for very large databases.

.. note-info::

    For complex expressions that end up exploding into tens of thousands of minterms, setmatch currently just
    throws an exception. This is to prevent user-provided data from DOSing you. Most sane expressions will not push it to
    the limit.

Once we have the minterms, there are some additional simplifications that can be done. Right now, it just eliminates
duplicates and minterms fully covered by others (ie ``tea AND cereal AND "fresh fruit"`` is fully covered by
``tea AND "fresh fruit"``, so we can eliminate the cereal requirement altogether), as well as eliminating expressions that
are always false (ie ``tea AND NOT tea``).

.. note-default::

    There are many more simplifications that could be done.
    `Quine-McCluskey <https://en.wikipedia.org/wiki/Quine%E2%80%93McCluskey_algorithm>`__ will get the best possible expression.
    `Espresso <https://en.wikipedia.org/wiki/Espresso_heuristic_logic_minimizer>`__ is a good heretic algorithm that would work
    for large expressions. But, for now, it occasionally generates some non-optimal expressions.

Henceforth, these minterms will be referred to as **subrules**. The individual variables (``coffee``, ``tea``, etc) will be
called **predicates**.

Indexing and matching
---------------------

Let's step away from the breakfast table now, and move to a more relevant example: an advertising targeting system.
Consider the following ad campaigns:

.. class:: m-table

    ======================= ================== ==========================
    Campaign                Target Ages        Target Interests
    ======================= ================== ==========================
    Hockey news             (any)              sports AND news
    Hockey video game       (any)              sports AND video-games
    Ninja video game        18-25              video-games
    Watch hockey online     18-25 OR 25-35     sports
    Health insurance        35-50              (any)
    ======================= ================== ==========================

Following the above procedure, we transform the input rules and come up with this matrix (note that "Watch Hockey
Online" has been split into two subrules):

.. math::

    \newcommand*\rot{\rotatebox{90}}
    {\def\arraystretch{1}\tabcolsep=3pt
    \begin{tabular}{l llllll}
        & \rot{18-25} & \rot{25-35} & \rot{35-40} & \rot{sports} & \rot{video-games} & \rot{news} \\
        \cmidrule{2-7}
        Hockey news                            & 0 & 0 & 0 & 1 & 0 & 1 \\
        Hockey video game                      & 0 & 0 & 0 & 1 & 1 & 0 \\
        Ninja video game                       & 1 & 0 & 0 & 0 & 1 & 0 \\
        Watch hockey online\textsubscript{(1)} & 1 & 0 & 0 & 1 & 0 & 0 \\
        Watch hockey online\textsubscript{(2)} & 0 & 1 & 0 & 1 & 0 & 0 \\
        Health insurance                       & 0 & 0 & 1 & 0 & 0 & 0 \\
    \end{tabular}}

When we encounter an incoming document, we first go through each token in the document and look it up in a
`really fast hashtable <https://probablydance.com/2017/02/26/i-wrote-the-fastest-hashtable/>`__. If we find it, we now
have the index of the predicate and a list of subrules that reference that predicate (either as a negative or a positive).
We set the corresponding bit in the predicate bitset, and also go through the predicate's subrules to get our candidate set.

Then a match is a simple bitwise and. For example, if someone comes along who is 30 years old and enjoys sports and news,
we could go ahead and create a bitset for that, and btwise and it across all the columns.

.. math::

    \newcommand*\rot{\rotatebox{90}}
    \newcommand*\OK{\ding{51}}
    {\def\arraystretch{1}\tabcolsep=3pt
    \begin{tabular}{l llllll|l}
        & \rot{18-25} & \rot{25-35} & \rot{35-40} & \rot{sports} & \rot{video-games} & \rot{news} & \rot{Matched?} \\
        \cmidrule{2-8}
        User profile                           & 0 & 1 & 0 & 1 & 0 & 1 & \\
        \cmidrule{2-8}
        Hockey news                            & 0 & 0 & 0 & 1 & 0 & 1 & \OK \\
        Hockey video game                      & 0 & 0 & 0 & 1 & 1 & 0 & \\
        Ninja video game                       & 1 & 0 & 0 & 0 & 1 & 0 & \\
        Watch hockey online\textsubscript{(1)} & 1 & 0 & 0 & 1 & 0 & 0 & \\
        Watch hockey online\textsubscript{(2)} & 0 & 1 & 0 & 1 & 0 & 0 & \OK \\
        Health insurance                       & 0 & 0 & 1 & 0 & 0 & 0 & \\
    \end{tabular}}

From a very high level, that's how you can look at the setmatch algorithm. However, when the number of rows and columns
goes into the hundreds of thousands, keeping that whole matrix around and processing every row becomes a big problem.
As you can guess, we don't actually store the entire uncompressed matrix, nor do we process every single row.

Compression with blocksets
~~~~~~~~~~~~~~~~~~~~~~~~~~

As the index gets larger, we assume that there will be an increasing number of distinct predicates (columns in the
above matrix). At some point, storing the entire matrix in memory doesn't make much sense. The first thing to notice
is that (with most data sets), this matrix is very sparse. That is, there are very few 1s and a lot of 0s. Pretend for
now the 1s are distributed fairly evenly throughout the matrix.

.. note-default::

    There are ways to reorganize the matrix so that 1s are well-distributed. Originally, I was using the
    `Louvian Method <https://sites.google.com/site/findcommunities/>`__ to sort by community, but while that slightly
    improved compression, it wasn't particularly effective, and I'm not convinced that improving compression will actually
    lead to fewer false positives. So the infrastructure for reindexing is still in the code, but it's currently disabled
    except in some tests. Future versions might have some newfangled algorithms that give better results.

We create a 256-bit "blockset" that is a summary of the actual data. If there are any 1s in a block, then the blockset
will contain a 1-bit in that position, otherwise it will contain a 0 bit. Each bit of the blockset represents 1/256th
of all the bits. To illustrate, pretend we had only 16 bits, and the blocks were of size 4. If our bitset was
``1001 0000 0100 0000``, then we would generate the blockset ``1010``.

.. note-info::

    Actually, we don't use all the blocks. The last few blocks are often empty. We resize whenever :math:`2^{16}` predicates
    are added. Resizing requires that all the blocksets be re-built (since each block is now 256 bits larger) and also
    re-hashed. This process can be relatively slow. Future options would be to speculatively resize in the background
    if the index gets ~80% full, and to allow users to specify an initial block size to pre-allocate.

When matching, we do a bitwise and with the blockset first, and only go into the actual matrix bits once that passes.
This is astoundingly effective at filtering out subrules:

.. note-default::

    .. plot:: Average per call to match()
        :type: barh
        :labels:
            Index size
            Eliminated by hash table
            Eliminated by blockset match (fast)
            Eliminated by bitset match (slow)
            Actual matches
        :labels_extra:
            118360
            99698.8
            18661.2
            95.7
            4.8
        :units: subrules
        :values: 118360 99698.8 18661.2 95.7 4.8
        :colors: info warning success danger default

    This shows the process a match() operation eliminating candidates when matching the test data against itself.
    "Eliminated by hash table" refers to subrules that never even make it into the candidate set because none of
    their predicates match. Of the ~16% that do make it in, 99.4% get eliminated by the blockset match, leaving only
    the last 0.6% that require looking at the uncompressed data.

This is especially important for cache efficiency. These blocksets are 32 bytes each (2 to a cache line, and they
are aligned), meaning a lot of them can fit in the processor cache. On a machine with 6MB of shared L3 cache, you can
fit nearly 200k blocksets there without going to main memory.

.. note-success::

    The AVX instruction `VPTEST <https://software.intel.com/en-us/node/524147>`__ can be done with ~3 cycles
    latency on Haswell and later according to `these tables <https://www.agner.org/optimize/instruction_tables.pdf>`__.
    There are also several cycles spent iterating through a bitset (mask then bsf). The inner loop is incredibly fast
    when data is in cache, and the data is small and cache-aligned, so it's quite fast even when it needs to fetch from
    main memory.

The blockset is also used for some rudimentary compression -- we only store the blocks where there are 1 bits. If we use
an average of 6 blocks per subrule, that means we're only storing 2.3% as much data as we would with the uncompressed
matrix (plus the blocksets themselves and some pointers).

.. note-info::

    Small subrules (currently under 8 elements) are just stored as actual lists with the length embedded in the pointer,
    instead of doing the compressed bitset thing. This reduces memory overhead for indices that are very sparse (ie
    millions of columns but only a few bits per row).

To further reduce memory overhead, subrules are always deduplicated. For example, if we get the rules ``(18-25 OR 25-35) AND sports``
and ``25-35 AND (sports OR news)``, one of the subrules (``25-35 AND sports``) can be shared between these two. To do this,
we keep a hashtable of the subrules which we can use to quickly look up duplicates.

The full match operation in psuedocode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    𝓣 <- input tokens
    𝓟 <- initially empty bitset with size equal to the predicate count
    𝓒 <- intiially empty bitset to store our candidates
    for each token 𝓽 in 𝓣:
        look it up in the hash
        if found:
            𝓹 <- the predicate ID of 𝓽
            add 𝓹 to 𝓟
            for each subrule 𝓼 which references 𝓹:
                add 𝓼 to 𝓒

    for each subrule 𝓼 in 𝓒:
        if 𝓼.blockset ≠ (𝓟.blockset & 𝓼.blockset), where & is the bitwise AND operator:
            remove 𝓼 from 𝓒

    𝓡 <- initially empty set of matched rules
    for each subrule 𝓼 that's left in 𝓒:
        for each block 𝓫_1 in 𝓼:
           if 𝓫_1 ≠ (𝓫_1 & the corresponding block in 𝓟):
              goto next subrule
        𝓷 <- the set of negated predicates in 𝓼
        if 𝓷.blockset = (𝓟.blockset & 𝓷.blockset)
            for each block 𝓫_2 in 𝓷:
                if 𝓫_2 ≠ (𝓫_2 & the corresponding block in 𝓟):
                    break out of negation loop; we're not negated
            if we get here, subrule negation matches, so goto next subrule
        if we get here, the subrule matches; add its parent ID to 𝓡

The phrase trie
---------------

Above, when I said the token lookup was done in a hashtable, I actually lied a bit. To support multi-word phrases, we
need to be a bit smarter. For example, consider how to support both "casino royale" and "royale with cheese". There
is an algorithm which does just that: `Aho-Corasick <https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm>`__.
However, we want to go fast, so this isn't just a standard implementation.

First note that our "alphabet" consists of whole terms, not single characters. Most papers about the algorithm will talk
about matching strings of characters, but just imagine that each character is actually a whole term. Also, the nodes are
stored different ways depending on how many children they have:

-  The majority of nodes do not have any children, so they are stored in a packed 32-byte structure.
-  Nodes which have only a few children use an embedded array of children. For each child, a 4-byte token ID is stored,
   as well as a 6-byte compressed pointer. Because his list is embedded, so it's often on the same cache line as the rest
   of the node. The entire node structure including the embedded list is only 96 bytes, meaning it's only 2-3 cache
   lines large with no need to chase pointers.
-  Nodes with many children (for example, the root node, or common words) switch to a hash table.

Because of all this, nodes occasionally need to be reallocated, and every pointer to those nodes need to be updated.
This is only possible as long as nodes never escape the trie structure itself (and even then is surprisingly tricky).
So all interaction with the trie is done at the phrase level, and the trie itself should be treated as a black box
to external code.

Finally, there are no "dictionary suffix" pointers. These take a lot of extra memory, and are only useful if the trie is
deep and the alphabet is small, which won't be the case when searching for N-grams in text. Since they can always be
computed by following parent pointers, they represent a small optimization for some tries, which is most likely a
pessimization here (but I don't have enough test data to be sure of that).

Supporting add/remove in an Aho-Corasick trie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It supports modification (add/remove) instead of being built all at once from a pre-set dictionary. This isn't mentioned
in any literature about it, and all the open-source implementations I've seen of it have followed in that respect. It
turns out that supporting add and remove operations to an AC automaton is fairly trivial. For deep tries over a small
alphabet, it would not be very time-efficient, but luckily we have a shallow trie over a large alphabet.

We store a hash multimap of each token to every node that uses that token. When we remove a phrase, we start from the
last (bottom-most) node. We remove the pointer to the phrase from this bottom-most node. Then, if it has any children,
we can't delete any more. Otherwise, go through each other node that uses this node as a fail state and recompute
its fail state. Finally, delete the node, then go up to the node's parent and do the same process. We'll eventually reach
either a node that has another phrase output, a child, or the root node.

That's kind of difficult to picture, so consider this trie (stolen from the Wikipedia article). We're going to remove
the string ``caa`` (light gray color), which will require that the fail states in yellow be updated:

.. digraph::

    ROOT[label=""]
    A1[label="a"]
    B1[label="b"]
    C1[label="c" class="m-dim"]
    B2[label="b"]
    A2[label="a"]
    C2[label="c"]
    A3[label="a" class="m-dim"]
    B3[label="b"]
    A4[label="a"]
    A5[label="a" class="m-dim"]

    ROOT -> {A1 B1}
    ROOT -> C1 [class="m-dim"]
    A1 -> B2
    B1 -> {A2 C2}
    C1 -> A3 [class="m-dim"]
    A2 -> B3
    C2 -> A4
    A3 -> A5 [class="m-dim"]

    edge[constraint=false]
    B3 -> B2 [style="dashed"]
    B2 -> B1 [style="dashed"]
    B1 -> ROOT [style="dashed"]
    A4 -> A3 [style="dashed" class="m-warning"]
    A5 -> A1 [style="dashed" class="m-dim"]
    A2 -> A1 [style="dashed"]
    C2 -> C1 [style="dashed" class="m-warning"]
    C1 -> ROOT [style="dashed" class="m-dim"]
    A3 -> A1 [style="dashed" class="m-dim"]
    A1 -> ROOT [style="dashed"]

The result:

.. digraph::

    ROOT[label=""]
    A1[label="a"]
    B1[label="b"]
    B2[label="b"]
    A2[label="a"]
    C2[label="c"]
    B3[label="b"]
    A4[label="a"]

    ROOT -> {A1 B1}
    A1 -> B2
    B1 -> {A2 C2}
    A2 -> B3
    C2 -> A4

    edge[constraint=false]
    B3 -> B2 [style="dashed"]
    B2 -> B1 [style="dashed"]
    B1 -> ROOT [style="dashed"]
    A4 -> A1 [style="dashed" class="m-warning"]
    A2 -> A1 [style="dashed"]
    C2 -> ROOT [style="dashed" class="m-warning"]
    A1 -> ROOT [style="dashed"]

.. note-info::

    Note there are cases where a node's fail state might be updated 2 or more times in the same remove operation, but
    will eventually be correct. This could be avoided by removing everything first, then going back through tokens,
    but the code is complicated enough as it is, and that would only provide a performance benefit in certain awkward
    circumstances, while being a performance hit in the average case. We just assume that strings containing the same
    token more than once are rare.

Adding a node is slightly more difficult, but can make use of the same hash multimap structure. Each time a node is
added, every other node in the trie that uses the same token and is at a lower depth than the added node could need
to have its fail state updated to the new node.

To help picture that, imagine going from the second diagram back to the first diagram. The same two fail states are the ones
who would need to be updated if adding the string ``caa`` back into that trie, and since we recompute every ``c`` and
``a`` node, it's not a problem.

.. note-default::

    We could keep track of the node depths to skip over about half, but right now it's just recomputing the fail state
    for every node that shares the token to save memory. This means that tries where the tokens appear many times will
    be slow to add to, but again that's considered a rare enough situation. It would be a concern if you were trying to
    adapt this technique to something like a DNA sequence or antivirus database where there is a small alphabet.

The time complexity depends on how many nodes share symbols and how deep the trie is, meaning it's worst-case :math:`O(N)`,
but in practice a fairly small number (average-case is roughly :math:`O(log^{2} N)`).

Handling ranges and numeric rules
---------------------------------

Transformation and merging
~~~~~~~~~~~~~~~~~~~~~~~~~~

Numeric predicates (less than, greater than, equal to, and ranges) need special handling. First, all numeric predicates
are normalized to closed intervals (inclusive ranges). This allows them to be easily merged and reasoned about, and keeps
most of the code to deal with them (outside the actual index) the same.

All computations are done with double-precision floating point (it is left up to the user to split the data further
if decimal handling is needed). Infinity/negative infinity are used to signal greater than/less than types of ranges,
and NaN values are used to signal when a range is not present.

The simplification step merges intervals that occur together in minterms (post-simplification "ands"). For example, if
a minterm contains ranges ``[3,5]`` and ``[4,6]``, we can simplify that to ``[4,5]``. We can also use to completely eliminate
some minterms. For example, if we have the expression ``([3,5]|[10,12])&([1,3]|[4,11])``, our initial transformation
converts that to ``([1,3]&[3,5])|([4,11]&[3,5])|([10,12]&[1,3])|([10,12]&[4,11])``. Each of these can be simplified
and one can be completely eliminated. The final minterm set will be ``[3,3]|[4,5]|[10,11]``.

One limitation of the current approach is that it does not properly merge OR expressions. For example, if passed the
rule ``[1,3]|[2,5]``, this should clearly be reduced to ``[1,5]``. However, there's currently no mechanism to do that.
I have some ideas about how to approach that, although there might be some issues with very complex expressions.

Indexing
~~~~~~~~

The indexing is straightforward. A hashtable is kept of all terms that have one or more
associated intervals. For terms that have 8 or fewer intervals, we just do a linear scan of each interval when
encountering a number. This saves needing to allocate and manage large data structures in the case the user has many
terms, each of which only has a few associated intervals.

For a term with many associated intervals, this is instead turned into a "split interval set", which can keep a
hashtable, b-tree, or r-tree depending on what intervals are being stored. This gives the best algorithmic complexity
when there are thousands or millions of different intervals to scan, at the price of a lot more memory. This is the
place where the abstraction of "everything is a closed interval" breaks down. For example, if every range contains only
a single element (equal to), we want to use a hashtable instead of an R-tree.

A possible future optimization would be to use a multi-dimensional R-tree when certain terms are correlated. For example,
if the user is passing geographic ranges (latitude/longitude), the correct solution would be to use a single 2D R-tree.
However, the current implementation uses two separate 1D R-trees.

There is really not much algroithmic innovation w.r.t. interval handling; it's very much an "elbow grease" approach to
improving performance via small micro-optimizations and partially adapting to shape of the user's data.

Other notes
-----------

.. raw:: html

    <details>
        <summary>Everything is done under a read-write lock.</summary>
        <aside class="m-note m-default" style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
           A better solution would be to do what real databases do, and perform writes to a staging buffer then
           integrate this buffer later, with time-bounded pauses on how long the write lock can be held, and some
           lock-free swaps where possible. Someday that might happen.
        </aside>
    </details>
    <details>
        <summary>Some data has been split to reduce cache usage and improve locality.</summary>
        <aside class="m-note m-default" style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            This is similar to techniques used in game engine particle systems when calculating
            thousands of new particle states per frame.
        </aside>
    </details>

References
----------

.. note-dim::

    Tao J., Zhang C., Rao W. (2017) [An Efficient Boolean Expression Index by Compression.](https://link.springer.com/chapter/10.1007/978-3-319-55705-2_23)
    In: Bao Z., Trajcevski G., Chang L., Hua W. (eds) Database Systems for Advanced Applications. DASFAA 2017. Lecture
    Notes in Computer Science, vol 10179. Springer, Cham ;; School of Software Engineering, Tongji University, Shanghai,
    China. DOI: 10.1007/978-3-319-55705-2\_23
