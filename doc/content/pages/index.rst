..
    Copyright 2019-2020 Robert William Fraser IV.

    This file is part of setmatch.

    setmatch is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    setmatch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

Setmatch
========

:cover: background.jpg
:hide_navbar_brand: True
:landing:

    .. raw:: html

        <center style="margin-top:25px;"><h1>Setmatch</h1></center>
        <center style="margin-bottom:25px;"><h2>Inverted index for boolean expression matching</h2></center>


Setmatch is an inverted index for matching boolean expressions against incoming documents, for use in
social media analytics, real-time bidding, and ETL pipelines. Setmatch is extremely fast and scalable, supporting
queries against hundreds of thousands - even millions - of boolean expressions in a few milliseconds or less. It
runs entirely in memory, as a Java or C++ library.

A motivating example
--------------------

Imagine you had the following ad campaigns running:

.. class:: m-table

    ======================= ================== ==========================
    Campaign                Target Ages        Target Interests
    ======================= ================== ==========================
    Football news           (any)              sports OR news
    Ninja video game        18-25              video-games
    Hockey video game       (any)              sports AND video-games
    Organic food            NOT 18-25          nutrition
    Movie tickets           (any)              NOT (sports AND nutrition)
    ======================= ================== ==========================

Then users come along...

.. math::

    \newcommand*\rot{\rotatebox{90}}
    \newcommand*\OK{\ding{51}}
    {\def\arraystretch{1}\tabcolsep=3pt
    \begin{tabular}{lllllll}
        Age & Interests & \rot{Football news} & \rot{Ninja video game} & \rot{Hockey video game} & \rot{Organic food} & \rot{Movie tickets} \\
        \cmidrule{1-7}
        20 & sports, video-games, nutrition & \OK & \OK & \OK &     &     \\
        27 & nutrition, video-games         &     &     &     & \OK & \OK \\
        33 & sports, nutrition              & \OK &     &     & \OK &     \\
        45 & news, video-games              & \OK &     &     &     & \OK \\
    \end{tabular}}

Now imagine you had hundreds of thousands of those campaigns, complex nested expressions, and hundreds of thousands
of users per second.

.. note-info::

    Setmatch isn't just for ad targeting, that's just an easy example. It also works well for social media analytics
    or other NLP applications that need realtime feedback. It can also be used in ETL pipelines and some data analytics
    tasks. Setmatch shines in any situation where you want to run many, many different queries over incoming data.

Get started
-----------

- `Java API <java-api.html>`__
- `C and C++ APIs <c-and-c-apis.html>`__
- `Find out how the algorithm works <implementation-overview.html>`__.

License
-------

`GNU Lesser General Public License 3.0 <https://www.gnu.org/licenses/lgpl-3.0.en.html>`__

Included Libraries
------------------

- `Flat Hash Map <https://probablydance.com/2017/02/26/i-wrote-the-fastest-hashtable/>`__ copyright 2017 Malte Skarupke.
  Distributed under the terms of the `Boost Software License 1.0 <https://www.boost.org/users/license.html>`__
- `RapidJSON <https://github.com/Tencent/rapidjson>`__ copyright 2015 THL A29 Limited, a Tencent company, and Milo Yip.
  Distributed under the terms of the `MIT License and others. <https://github.com/Tencent/rapidjson/blob/release/license.txt>`__
- `Google B-Tree <https://code.google.com/archive/p/cpp-btree/>`__ copyright 2013 Google Inc. Distributed under the
  terms of the `Apache License 2.0 <http://www.apache.org/licenses/LICENSE-2.0>`__.
- `m.css <https://mcss.mosra.cz/>`__ copyright 2017–2019 Vladimír Vondruš. Distributed under the terms of the
  `MIT License <https://github.com/mosra/m.css/blob/release/COPYING>`__.

Help Wanted
-----------

Graph Algorithms
~~~~~~~~~~~~~~~~

I'm interested in finding some good alternative algorithms for optimizing the layout of predicates. You can read more
about the algorithm, and the problem that needs solving, at the `implementation overview <implementation-overview.html>`__
page. If you know C++ and have a better understanding than I of graph algorithms, please reach out (open a ticket, and
we can connect via Skype or something).

