..
    Copyright 2019-2020 Robert William Fraser IV.

    This file is part of setmatch.

    setmatch is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    setmatch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

Java API
========

:cover: background.jpg

.. contents::
    :class: m-note m-toc

Supported Platforms
-------------------

- Linux, Windows, or OSX
- x86-64 (amd64) processor
- 64-bit JVM
- AVX support (most processors made since 2011 have this)

Getting it
----------

The easiest way to get the JAR is from Maven Central:

.. raw:: html

    <table class="m-table"><tbody>
    <tr><td><a href="https://maven.apache.org/">Maven</a></td><td><pre><code>&lt;dependency&gt;
        &lt;groupId&gt;com.burningmime&lt;/groupId&gt;
        &lt;artifactId&gt;setmatch&lt;/artifactId&gt;
        &lt;version&gt;0.8.1&lt;/version&gt;
    &lt;/dependency&gt;</code></pre></td></tr>
    <tr><td><a href="https://gradle.org/">Gradle</a></td><td><pre><code>implementation 'com.burningmime:setmatch:0.8.1'</code></pre></td></tr>
    <tr><td><a href="https://github.com/gradle/kotlin-dsl">Gradle (Kotlin)</a></td><td><pre><code>compile("com.burningmime:setmatch:0.8.1")</code></pre></td></tr>
    <tr><td><a href="https://www.scala-sbt.org/">SBT</a></td><td><pre><code>libraryDependencies += "com.burningmime" % "setmatch" % "0.8.1"</code></pre></td></tr>
    <tr><td><a href="https://ant.apache.org/ivy/">Ivy</a></td><td><pre><code>&lt;dependency org=&quot;com.burningmime&quot; name=&quot;setmatch&quot; rev=&quot;0.8.1&quot;/&gt;</code></pre></td></tr>
    </tbody></table>

Build artifacts are generated on every commit, so if you want an unstable/bleeding-edge build, you can also go to
`the pipelines page <https://gitlab.com/burningmime/setmatch/pipelines>`__ and click on the download icon for the commit
you want. The JAR is in the artifacts archive, under the ``bin`` directory. It depends on `slf4j <https://www.slf4j.org/>`__
and `gson <https://github.com/google/gson>`__, so add those to your classpath however you want.

Finally, if you enjoy frustrating and confusing error messages, you can `build it yourself <building.html>`__.

Example
-------

Here's a simple example of an ad-targeting system. This should be enough to get a feel for how it works:

.. include:: ../../../java/src/test/java/com/burningmime/setmatch/examples/JavaExample.java
    :code: java

Using setmatch for natural language processing
----------------------------------------------

There are some additional things to keep in mind if your documents are actual text instead of a bunch of keywords. If
you're doing any other NLP, you're probably doing some of this already.

-  Tokenize using a language-aware tokenizer. Some languages, notably Asian languages (CJK) do not have spaces between words.
-  Normalize the tokens (ie stemming or lemmatization). `For example <https://stackoverflow.com/questions/1578062/lemmatization-java>`__.
-  Remove stop words ("of", "the", etc)
-  Use ``RuleNode.phrase()`` to match N-Grams. For example, ``RuleNode.phrases("now", "is", "the", "winter", "of", "our", "discontent")``
   (after stopword removal that might be ``RuleNode.phrase("now", "winter", "discontent")``). This uses an Aho-Corasick trie
   internally, so there's almost no performance penalty.
-  Run these steps on the rules you put in and the documents you match. All setmatch does is compare exact strings, so
   it's up to you to do all pre-processing.

Limitations on rule complexity
------------------------------

Click on the arrows for detailed explanations:

.. raw:: html

    <details>
        <summary>1024 UTF-8 bytes per token</summary>
        <aside class="m-note m-default"  style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            Tokens in the document stream can be longer, but obviously they won't match any rules.
        </aside>
    </details>
    <details>
        <summary>256 unique predicates in a single rule (less if using ranges)</summary>
        <aside class="m-note m-default"  style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            Because of some internal design decisions, it's currently quite restricted as to how many different
            predicates can be in a single rule. This limit should be increased someday, but for now exceptions
            will be thrown when a rule uses many different predicates (terms, phrases, or ranges).
            <br/><br/>
            If using ranges, the limit will be lower than 256, and depends on how some of the simplification steps
            are done (and might even depend on the ordering of things). This is clearly not an ideal scenario, but
            is done this way currently to simplify and speed up the common cases where there are under 100 predicates
            per rule.
            <br/><br/>
            You can get around this limitation by splitting the rule in multiple parts, but when generating
            rules programatically and/or based on user input, this is not always straightforward or even possible.
        </aside>
    </details>
    <details>
        <summary>1024 OR clauses after simplification</summary>
        <aside class="m-note m-default" style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            If you have very complex rules, you may receive an exception telling you to split the rule into multiples.
            This will most often happen if you have many ORs inside of ANDs, because the simplification of those is
            exponential in size.
            <br/><br/>
            For example, the rule <code>(a|b)&amp;(c|d)&(e|f)</code>, which looks simple, is
            internally transformed into the monstrosity <code>(a&amp;c&amp;e)|(a&amp;c&amp;f)|(a&amp;d&amp;e)|(a&amp;d&amp;f)|(b&amp;c&amp;e)|(b&amp;c&amp;f)|(b&amp;d&amp;e)|(b&amp;d&amp;f)</code>.
            To stop from running out of memory or entering into an exponential time loop, an error will be thrown if
            your once the rule reaches a certain threshold of complexity.
        </aside>
    </details>
    <br/<br/>

(All these throw exceptions if you try to add a rule which hits one of these limits)

Performance Tips
----------------

Click on the arrows for detailed explanations:

.. raw:: html

    <details>
        <summary>Use only one database object.</summary>
        <aside class="m-note m-default"  style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            Databases are big, heavyweight objects that take a lot of memory, and time to set up or destruct. Having
            more than a few databases is not recommended. Instead of using separate databases, try filtering the results
            when you get them back.
        </aside>
    </details>
    <details>
        <summary>Rules excluded by negation take longer than rules where the positive parts don't match.</summary>
        <aside class="m-note m-default" style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            In some cases, you can take advantage of this by manually doing the negation part. For example, instead of
            making a rule with <code>18-25&amp;(~sports&amp;~technology&amp;~news)</code>, you could make a rule
            like <code>18-25&amp;no-interests</code>.
        </aside>
    </details>
    <details>
        <summary>Try querying from multiple threads.</summary>
        <aside class="m-note m-default" style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            Setmatch is thread-safe and uses read-write locks internally.
        </aside>
    </details>
    <details>
        <summary>For minimum latency, be careful when you add rules.</summary>
        <aside class="m-note m-default" style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            Adding a rule can trigger a resize, which will cause all threads to wait a few milliseconds for it to
            complete. If you add all the rules during startup, then it's not an issue.
        </aside>
    </details>
    <details>
        <summary>
            For faster startup, <a href="javadoc/com/burningmime/setmatch/RuleDB.html#compileRule%28com.burningmime.setmatch.RuleNode%29">
            compile your rules</a> and store these results.
        </summary>
        <aside class="m-note m-default" style="margin-left:2.5em;margin-top:0.5em;margin-bottom:0.5em;">
            For example, you could store them as a BLOB in a database, then each application instance would read the
            database table during startup.
        </aside>
    </details>

API Documentation
-----------------

`See the Javadoc <https://burningmime.gitlab.io/setmatch/javadoc/com/burningmime/setmatch/package-summary.html>`__
