..
    Copyright 2019-2020 Robert William Fraser IV.

    This file is part of setmatch.

    setmatch is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    setmatch is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

C and C++ APIs
==============

:cover: background.jpg

.. contents::
    :class: m-note m-toc

Getting it
----------

ZIP versions of each release can be found on the `GitLab releases page <https://gitlab.com/burningmime/setmatch/releases>`__.

Build artifacts are triggered on every commit, so if you want an unstable/bleeding-edge build, you can also go to
`the pipelines page <https://gitlab.com/burningmime/setmatch/pipelines>`__ and click on the download icon.

Finally, you can always `build it yourself <building.html>`__.

C API
-----

| `Doxygen Reference <https://burningmime.gitlab.io/setmatch/doxygen/setmatch_8h.html>`__
| `Example File <https://gitlab.com/burningmime/setmatch/blob/release/src/public/c_api_example.c>`__

The C API provides a uniform, simplified interface without any of the abstractions provided by the other APIs. Ultimately,
every other API (Java, C++, etc) will call the C API. But it's not very convenient.

Inputs are passed in a `recursive JSON format <json-format.html>`__.

Most of the data exposed by this API is done via pointers to internal, thread-local buffers. This means there's very
little memory management to do on your side unless you need copies of them (and if you *do* need copies, you need to
make them yourself). The only two exceptions to this are the rule DB itself and the compiled rule object, both of which
need to be freed by library if they were created by the library.

There are currently no error codes, just implementation-defined error strings. This may change soon, but for now it's
difficult to determine the cause of any errors without trying to parse strings. Sorry about that!

C++ Wrapper
-----------

| `Doxygen Reference <https://burningmime.gitlab.io/setmatch/doxygen/namespacesetmatch.html>`__
| `Example File <https://gitlab.com/burningmime/setmatch/blob/release/src/public/cpp_api_example.cpp>`__

The C++ API is actually just a header that wraps the C API. It requires a working C++11 compiler and C++11 standard
library, but shouldn't require anything else (ie no need for Boost). It should be compiler-neutral, but I haven't tested
it on many different compilers, so there may be subtle issues I'm not aware of (please open a ticket if you find some!).

Rules are passed in `the same JSON format <json-format.html>`__. If you're extensively using the C++ API and would like
an alternative to the JSON input (similar to the RuleNodes from Java), you can also open a ticket. Right now, I feel like
I'm the only one actually using the C++ API for much, so I've left it bare-bones.

The changes from the C API are:

-  Exceptions are thrown on errors instead of returning error messages. If something fails, a ``std::runtime_exception`` will
   be thrown. You should definitely catch these, especially on compileRule() which can fail for all sorts of unexpected
   reasons if using user input data (for example, if a rule is too complex). While these failures should be rare,
   don't trust your user not to create a rule with thousands of branches -- catch the exception!
-  Most things have RAII semantics.
-  Matching supports input either as a functor, or as a pair of iterators. See the doxygen for more info.

See Also
--------

The Java API docs have more info about usage patterns, almost all of which apply here also:

-  `Using setmatch for natural language processing <java-api.html#using-setmatch-for-natural-language-processing4>`__
-  `Working with ranges and numeric attributes <java-api.html#working-with-ranges-and-numeric-attributes>`__
-  `Limits <java-api.html#limits>`__
-  `Performance Tips <java-api.html#performance-tips>`__
