/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "common.hpp"
#include <math.h>
#include <functional>

namespace burningmime::setmatch
{
    enum class IntervalKind
    {
        INVALID = 0,
        EXCLUSIVE = 1,
        MAX_INCLUSIVE = 2,
        MIN_INCLUSIVE = 3,
        INCLUSIVE = 4,
    };

    class Interval
    {
        // We always store an "closed" interval and do >=/<= checks. Closed intervals are transformed into closed intervals
        // using std::nextafter() plus some magic for handling denormals and infinities. If left and right are both NaN
        // that means this interval does not exist, so this is actually an "optional" type.
        double left;
        double right;

    public:
        inline Interval() : left(NaN), right(NaN) { }

        inline Interval(double left, double right) : left(left), right(right)
        {
            dassert(!std::isnan(left) && !std::isnan(right), "Range cannot have NaN items");
            dassert(!std::isinf(left) || !std::isinf(right), "Range cannot have right or left as infinite");
            dassert(left <= right, "left = %d, right = %d", left, right);
        }

        inline static Interval makeNormalized(double left, double right, IntervalKind kind)
        {
            dassert(!std::isnan(left) && !std::isnan(right), "Range cannot have NaN items");
            dassert(!std::isinf(left) || !std::isinf(right), "Range cannot have right or left as infinite");
            dassert(left <= right, "left = %d, right = %d", left, right);

            left = flushDenormals(left);
            right = flushDenormals(right);

            if(left != right)
            {
                switch(kind)
                {
                    case IntervalKind::EXCLUSIVE:
                        left = nextAfter(left);
                        right = nextBefore(right);
                        break;
                    case IntervalKind::MAX_INCLUSIVE:
                        left = nextAfter(left);
                        break;
                    case IntervalKind::MIN_INCLUSIVE:
                        right = nextBefore(right);
                        break;
                    case IntervalKind::INCLUSIVE:
                        break;
                    default:
                        xassert_false("Invalid interval kind %d", (int) kind);
                }

                // some of the denormal stuff might cause this to happen in the case of 0
                // one option would just be to raise an exception (especially if they asked for an open interval), however
                // this situation is extremely rare and very specific. in general, if they're passing in denormalized
                // doubles, we can no longer guaruntee correct behaviour.
                if(UNLIKELY(right < left))
                {
                    right = left;
                }
            }

            return Interval{left, right};
        }

        inline explicit operator bool() const { return !std::isnan(left); }
        inline static Interval none() { return Interval{}; }
        inline double getLeft() const { return left; }
        inline double getRight() const { return right; }
        inline bool contains(double value) const { return value >= left && value <= right; }
        inline bool operator==(const Interval& other) const { return std::isnan(left) ? std::isnan(other.left) : left == other.left && right == other.right; }
        inline bool operator!=(const Interval& other) const { return !(*this == other); }

        struct Hash
        {
            inline size_t operator()(const Interval& x) const
            {
                size_t hash = std::hash<double>()(x.left);
                hash *= 1099511628211ULL;
                hash ^= std::hash<double>()(x.right);
                return hash;
            }
        };

        static Interval mergeAnd(Interval x, Interval y)
        {
            dassert((bool) x && (bool) y, "Both intervals here should be valid");
            double left = std::max(x.left, y.left);
            double right = std::min(x.right, y.right);
            if(left > right)
                return none();
            else
                return Interval{left, right};
        }
    };

    ASSERT_TYPE_SIZE(Interval, 16);
}

// old merging code... i will remove this on a later commit, but i want it somewhere in the git history in case
// it becomes useful eventually
//
//        enum class MergeResult
//        {
//            MERGED,
//            KEEP_BOTH,
//            ALWAYS_TRUE,
//            ALWAYS_FALSE
//        };
//
//        inline static std::pair<MergeResult, Interval> mergeAnd(Interval x, Interval y)
//        {
//            dassert((bool) x && (bool) y, "Both intervals here should be valid");
//            double left = std::max(x.left, y.left);
//            double right = std::min(x.right, y.right);
//            if(left > right)
//                return {MergeResult::ALWAYS_FALSE, Interval{}};
//            else
//                return {MergeResult::MERGED, Interval{left, right}};
//        }
//
//        inline static std::pair<MergeResult, Interval> mergeOr(Interval x, Interval y)
//        {
//            dassert((bool) x && (bool) y, "Both intervals here should be valid");
//            if(x.right < y.left || y.right < x.left)
//                return {MergeResult::KEEP_BOTH, Interval{}};
//            else if((x.left == negInf && y.right == posInf && x.right <= y.left) ||
//                    (y.left == negInf && x.right == posInf && y.right <= x.left))
//                return {MergeResult::ALWAYS_TRUE, Interval{}};
//            else
//                return {MergeResult::MERGED, Interval{std::min(x.left, y.left), std::max(x.right, y.right)}};
//        }