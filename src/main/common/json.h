/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "common.hpp"
#include "../../thirdparty/rapidjson/document.h"
#include "../../thirdparty/rapidjson/error/en.h"

namespace burningmime::setmatch
{
    typedef const rapidjson::Value &JsonNode;
    #define jsonFormatError(...) setmatchRaise(JSON_FORMAT_ERROR, ##__VA_ARGS__)
    ALWAYS_INLINE inline bool isKey(const char* key, const char* expected) { return !strcmp(key, expected); }
    NEVER_INLINE inline rapidjson::Document parseJson(const char* json_utf8_nt)
    {
        rapidjson::Document document;
        constexpr auto PARSE_FLAGS =
                rapidjson::kParseDefaultFlags |
                rapidjson::kParseTrailingCommasFlag |
                rapidjson::kParseCommentsFlag;

        document.Parse<PARSE_FLAGS>(json_utf8_nt);
        if(document.HasParseError())
        {
            setmatchRaise(JSON_SYNTAX_ERROR, "JSON parse error at offset %d: %s", document.GetErrorOffset(),
                          rapidjson::GetParseError_En(document.GetParseError()));
        }

        if(document.GetType() != rapidjson::kObjectType)
            jsonFormatError("Root JSON node must be an object");

        return document;
    }
}
