/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "common.hpp"
#include <memory>
#include <atomic>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread.hpp>
#include <pthread.h>

namespace burningmime::setmatch
{
    // use this where you need a lock guard
    #define LOCK_GUARD_SCOPE(MUTEX) boost::lock_guard<decltype(MUTEX)> _SETMATCH_UNIQUE_NAME(lockGuardScope){MUTEX}

    /**
     * About as simple a spinlock as you can get.
     */
    class SimpleSpinlock
    {
        std::atomic_flag flag = ATOMIC_FLAG_INIT;
    public:
        void lock() { while(flag.test_and_set(std::memory_order_acquire)) { } }
        void unlock() { flag.clear(std::memory_order_release); }
    };

    class _CrowBase
    {
    protected:
        struct CrowData
        {
            std::atomic<bool> cancelled;
            boost::shared_mutex mutex;
        };
        typedef std::shared_ptr<CrowData> PCrowData;
        _CrowBase() = default; // prevent direct construction
    };

    /**
     * Represents a task that can be asynchronously cancelled. If operator bool returns true, then the task
     * should stop as soon as it is safe to do so. It does not protect the data of the task in any way.
     */
    class CancellationToken : private _CrowBase
    {
        PCrowData data;
        inline explicit CancellationToken(PCrowData _data) : data(std::move(_data)) { }
        friend class CrowMutex;
    public:
        static CancellationToken makeNull() { return CancellationToken(nullptr); }
        ALWAYS_INLINE inline explicit operator bool() { return LIKELY(data != nullptr) && UNLIKELY((bool) data->cancelled); }
        DEFAULT_COPY_CONSTRUCTOR(CancellationToken)
        DEFAULT_MOVE_CONSTRUCTOR(CancellationToken)
    };

    /**
     * A Crow* is a generalization of a read-write mutex that provides better error handling in the event the protected
     * resource is destroyed. It uses a shared (reference-counted) pointer to the mutex and cancellation state. Threads
     * can come along at any point where at least one reference to the crow exists (note not actually the resource) and
     * attempt to lock it in a shared/read-write manner. After the lock is acquired, if the protected resource has been
     * destroyed in another thread, then an exception is raised. It also allows for async cancellation, where you can
     * mark a resource as "about to be destroyed" so other threads can safely stop their operations ASAP without needing
     * to invoke undefined behaviour like thread abort exceptions.
     *
     * (*) It stands for "Cancel, Read, Upgrade, Write". The "U" from upgrade became an "O" because no one ever heard of
     * a "Cruw".
     */
    class CrowMutex : private _CrowBase
    {
        PCrowData data;
    public:
        class ReadLock
        {
            friend class CrowMutex;
            PCrowData data;
            boost::shared_lock<boost::shared_mutex> mainLock;
            explicit inline ReadLock(PCrowData _data) : data(std::move(_data)), mainLock(data->mutex)
            {
                if(data->cancelled)
                    setmatchRaise(RESOURCE_DELETED, "Use of protected resource after it was destroyed");
            }

        public:
            ~ReadLock() = default;
            DISABLE_COPY_CONSTRUCTOR(ReadLock)
        };

        class WriteLock
        {
            friend class CrowMutex;
            PCrowData data;
            boost::unique_lock<boost::shared_mutex> mainLock;
            explicit inline WriteLock(PCrowData _data, bool isDestructor) : data(std::move(_data)), mainLock(data->mutex)
            {
                if(!isDestructor && data->cancelled)
                    setmatchRaise(RESOURCE_DELETED, "Use of protected resource after it was destroyed");
            }

        public:
            ~WriteLock() = default;
            DISABLE_COPY_CONSTRUCTOR(WriteLock)
        };

        class UpgradeLock
        {
            friend class CrowMutex;
            typedef boost::upgrade_to_unique_lock<boost::shared_mutex> UpgradeLockImpl;
            PCrowData data;
            boost::upgrade_lock<boost::shared_mutex> mainLock;
            std::unique_ptr<UpgradeLockImpl> upgradeLock = nullptr;
            explicit inline UpgradeLock(PCrowData _data) : data(std::move(_data)), mainLock(data->mutex)
            {
                if(data->cancelled)
                    setmatchRaise(RESOURCE_DELETED, "Use of protected resource after it was destroyed");
            }

        public:
            void upgrade()
            {
                if(upgradeLock) return;
                upgradeLock = std::make_unique<UpgradeLockImpl>(mainLock);
            }

            ~UpgradeLock() = default;
            DISABLE_COPY_CONSTRUCTOR(UpgradeLock)
        };

        ALWAYS_INLINE bool isCancelled() { return UNLIKELY(data->cancelled); }
        NO_DISCARD CancellationToken token() { return CancellationToken(data); }
        NO_DISCARD ReadLock readLock() { return ReadLock(data); }
        NO_DISCARD WriteLock writeLock() { return WriteLock(data, false); }
        NO_DISCARD UpgradeLock upgradeLock() { return UpgradeLock(data); }
        NO_DISCARD WriteLock cancelAndDestructorLock() { data->cancelled = true; return WriteLock(data, true); }

        inline CrowMutex() : data(std::make_shared<CrowData>()) { }
        DEFAULT_MOVE_CONSTRUCTOR(CrowMutex)
        DISABLE_COPY_CONSTRUCTOR(CrowMutex)
        ~CrowMutex() { data->cancelled = true; }
    };

    /**
     * Wrapper around boost thread-local storage. Using this instead of C++ thread_local to increase compatibility
     * on Linux. Also appears to be slightly faster.
     */
    template<typename T>
    class ThreadLocal
    {
        boost::thread_specific_ptr<T> impl;

        T* get()
        {
            T* ptr = impl.get();
            if(!ptr)
            {
                ptr = new T();
                impl.reset(ptr);
            }
            return ptr;
        }

    public:
        T& operator*() { return *get(); }
        T* operator->() { return get(); }
        explicit operator T*() { return get(); }
        ThreadLocal() = default;
        DISABLE_COPY_CONSTRUCTOR(ThreadLocal);
        DEFAULT_MOVE_CONSTRUCTOR(ThreadLocal);
    };
}
