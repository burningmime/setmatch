/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.hpp"
#include <sstream>
#include <iomanip>
#include <locale>
#include <codecvt>

// i'm quite surprised something like this is not part of boost
// using deprecated codecvt since nothing better is in std. really they shouldn't deprecate things without providing
// an alternative. however, this makes it fail to compile under msvc since microsoft took out codecvt and told everyone
// to use windows-specific api calls.

namespace burningmime::setmatch
{
    typedef std::codecvt_utf8_utf16<char16_t> ConvertFacet;
    typedef std::wstring_convert<ConvertFacet, char16_t> StringConverter;

    std::string utf16to8(const char16_t* inChars, size_t inCount)
    {
        StringConverter stringConverter;
        return stringConverter.to_bytes(inChars, inChars + inCount);
    }

    std::u16string utf8to16(const char* inChars, size_t inCount)
    {
        StringConverter stringConverter;
        return stringConverter.from_bytes(inChars, inChars + inCount);
    }

    // Gets a string suitable for returning to the user or printing out
    // Input and output are assumed to be UTF-8. However, input may contain invalid UTF-8 sequences, embedded nulls,
    // ascii control chars, or just generally random bytes. These need to be properly escaped.
    //
    // WARNING: this conversion is lossy wrt newlines; it should be used for error handling or implementation-defined
    // data only. if we ever get something where the user expects an exact string to be returned, stuff might break
    std::string cleanUtf8(const char* buf, size_t length)
    {
        // this is pretty inefficient (should be copying blocks at a time), but it's fine for error handling purposes
        // IMO, as messy and spaghetti as this is, it's actually cleaner with goto instead of loops, since it's clear what's
        // going on at each point. a "continue" is just a goto anyway

        size_t chi;
        size_t i = 0;
        std::ostringstream ss;
        goto LNext;

    LEscape:
        ++i;
        ss << '\\' << 'x' << std::setfill('0') << std::setw(2) << std::hex << chi;

    LNext:
        // loop termination
        if(i >= length)
            return ss.str();

        // get current character ("char" is signed by default, so need to cast to get unsigned)
        #define CHAR_AT(I) ((const uint8_t*) buf)[I]
        chi = CHAR_AT(i);

        // most common case: ascii char
        if(LIKELY((chi >= ' ' && chi < 0x7F) || chi == '\n'))
        {
            ++i;
            ss << char(chi);
            goto LNext;
        }

        // carriage return
        else if(chi == '\r')
        {
            if(i == (length - 1) || CHAR_AT(i + 1) != '\n')
                goto LEscape;
            else
            {
                // always use \n instead of \r\n, even on windows
                // this might be an issue if the result is written directly to a file, but expect that it will not be
                // since this is used for error/debugging, it's OK if the conversion is a little lossy
                i += 2;
                ss << '\n';
                goto LNext;
            }
        }

        // convert tabs to spaces
        else if(chi == '\t')
        {
            ++i;
            ss << "    ";
            goto LNext;
        }

        // unicode sequences: https://stackoverflow.com/questions/6555015/check-for-invalid-utf8
        else if(chi >= 0xC2 && chi <= 0xDF)
        {
            // all two bytes handled here
            if(i == length - 1)
                goto LEscape;
            else
            {
                size_t chi2 = CHAR_AT(i + 1);
                if(chi2 >= 0x80 && chi2 <= 0xBF)
                {
                    i += 2;
                    ss << char(chi) << char(chi2);
                    goto LNext;
                }
                else
                {
                    goto LEscape;
                }
            }
        }

        // for 3 and 4 byte sequences, the valid range of the second byte depends on what the first byte is
        // there are actually 7 different cases

        #define THREE_BYTES(FIRST_MIN, FIRST_MAX, SECOND_MIN, SECOND_MAX) \
            else if(chi >= FIRST_MIN && chi <= FIRST_MAX) \
            { \
                if(i >= length - 2) \
                { \
                    goto LEscape; \
                } \
                else \
                { \
                    size_t chi2 = CHAR_AT(i + 1); \
                    if(chi2 >= SECOND_MIN && chi2 <= SECOND_MAX) \
                    { \
                        size_t chi3 = CHAR_AT(i + 2); \
                        if(chi3 >= 0x80 && chi3 <= 0xBF) \
                        { \
                            i += 3; \
                            ss << char(chi) << char(chi2) << char(chi3); \
                            goto LNext; \
                        } \
                        else \
                        { \
                            goto LEscape; \
                        } \
                    } \
                    else \
                    { \
                        goto LEscape; \
                    } \
                } \
            }

        #define FOUR_BYTES(FIRST_MIN, FIRST_MAX, SECOND_MIN, SECOND_MAX) \
            else if(chi >= FIRST_MIN && chi <= FIRST_MAX) \
            { \
                if(i >= length - 3) \
                { \
                    goto LEscape; \
                } \
                else \
                { \
                    size_t chi2 = CHAR_AT(i + 1); \
                    if(chi2 >= SECOND_MIN && chi2 <= SECOND_MAX) \
                    { \
                        size_t chi3 = CHAR_AT(i + 2); \
                        if(chi3 >= 0x80 && chi3 <= 0xBF) \
                        { \
                            size_t chi4 = CHAR_AT(i + 3); \
                            if(chi4 >= 0x80 && chi4 <= 0xBF) \
                            { \
                                i += 4; \
                                ss << char(chi) << char(chi2) << char(chi3) << char(chi4); \
                                goto LNext; \
                            } \
                            else \
                            { \
                                goto LEscape; \
                            } \
                        } \
                        else \
                        { \
                            goto LEscape; \
                        } \
                    } \
                    else \
                    { \
                        goto LEscape; \
                    } \
                } \
            }

        THREE_BYTES(0xE0, 0xE0, 0xA0, 0xBF)
        THREE_BYTES(0xE1, 0xEC, 0x80, 0xBF)
        THREE_BYTES(0xED, 0xED, 0x80, 0x9F)
        THREE_BYTES(0xEE, 0xEF, 0x80, 0xBF)
        FOUR_BYTES(0xF0, 0xF0, 0x90, 0xBF)
        FOUR_BYTES(0xF1, 0xF3, 0x80, 0xBF)
        FOUR_BYTES(0xF4, 0xF4, 0x80, 0x8F)

        else
        {
            // everything else is an invalid char and should be escaped
            goto LEscape;
        }

        UNREACHABLE;
        #undef CHAR_AT
        #undef THREE_BYTES
        #undef FOUR_BYTES
    }
}
