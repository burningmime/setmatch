/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "metric.hpp"
#include <cmath>
#include <algorithm>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/weighted_mean.hpp>
namespace ba = boost::accumulators;

// using a separate source file instead of inlining this because boost accumulators takes several seconds to compile
// and makes my IDE shit all over my computer's overtaxed fans

namespace burningmime::setmatch
{
    constexpr double ALPHA = 0.069; // https://www.wolframalpha.com/input/?i=exp(-0.069*x)+from+x%3D0+to+x%3D60 -- can play around w/ number to see decay
    constexpr const size_t DEFAULT_N_SAMPLES = 60;
    constexpr const SystemTime TIME_PER_SAMPLE = fromSeconds(1);

    ExponentialDecayMetric::ExponentialDecayMetric(const char* name)
        : Metric(name), currentSampleEnd(now() + TIME_PER_SAMPLE), samples(DEFAULT_N_SAMPLES)
    {
    }

    struct ExponentialDecayMetric::Private
    {
        static void storeSampleIfReady(ExponentialDecayMetric& m, SystemTime _now)
        {
            if(_now >= m.currentSampleEnd)
            {
                if(m.currentSampleNPoints > 0)
                {
                    double sampleValue = m.currentSampleValue / m.currentSampleNPoints;
                    m.samples.push_back({sampleValue, m.currentSampleEnd});
                }
                m.currentSampleEnd = _now + TIME_PER_SAMPLE;
                m.currentSampleValue = 0;
                m.currentSampleNPoints = 0;
            }
        }
    };

    void ExponentialDecayMetric::push(double value)
    {
        // every second(ish), take the unweighted average of the values we have, and store that in the circular
        // buffer. then, when we want to fetch the sample value, we just calculate the weighted mean of all those
        // samples. this means that if we're getting 20k values per second, we can get a snapshot of a longer
        // period of time, instead of the last 1024 samples or however big our buffer is. it basically decouples
        // the time range we can get a good estimate for from the number of actual values being pushed through
        if(!std::isnan(value) && !std::isinf(value))
        {
            LOCK_GUARD_SCOPE(mutex);
            SystemTime _now = now();
            Private::storeSampleIfReady(*this, _now);
            currentSampleNPoints++;
            currentSampleValue += value;
        }
    }

    double ExponentialDecayMetric::get()
    {
        LOCK_GUARD_SCOPE(mutex);
        SystemTime _now = now();
        bool noValues = true;
        ba::accumulator_set<double, ba::stats<ba::tag::weighted_mean>, double> acc;

        // if we have a full sample ready, push it
        Private::storeSampleIfReady(*this, _now);

        // if we have a partial sample, include that (but don't push it)
        if(currentSampleNPoints > 0)
        {
            noValues = false;
            dassert(currentSampleEnd > _now, "should have stored the sample above");
            double partialValue = currentSampleValue / currentSampleNPoints;
            double partialWeight = std::clamp(toMilliseconds(currentSampleEnd - _now) / toMilliseconds(TIME_PER_SAMPLE), 0.0, 1.0);
            acc(partialValue, ba::weight = partialWeight);
        }

        // add older samples
        for(auto& sample : samples)
        {
            noValues = false;
            double elapsed = toSeconds(_now - sample.t);
            double sampleWeight = std::exp(-ALPHA * elapsed);
            acc(sample.value, ba::weight = sampleWeight);
        }

        return noValues ? 0 : ba::weighted_mean(acc);
    }
}
