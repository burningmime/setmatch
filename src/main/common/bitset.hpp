/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "common.hpp"

namespace burningmime::setmatch
{
    //==================================================================================================================
    // General helper functions for bit manipulation
    //==================================================================================================================

    ALWAYS_INLINE inline int64_t sbit(int shift) { return ((int64_t) 1) << shift; }
    ALWAYS_INLINE inline int64_t nbit(int shift) { return ~sbit(shift); }
    ALWAYS_INLINE inline int64_t nmask(int shift) { return ((int64_t) -1) << shift; }
    ALWAYS_INLINE inline int64_t bmask(int shift) { return ~nmask(shift); }
    ALWAYS_INLINE inline int bsf(int64_t word) { return __builtin_ctzll(word); }

    //==================================================================================================================
    // Main bitscan functions
    //==================================================================================================================

    #define BITSCAN_FIRST(X, W, I) \
    { \
        for(int i = 0; i < W; i++) \
        { \
            int64_t thisWord = (int64_t) I; \
            if(thisWord != 0) \
            { \
                int ret = i * X + bsf(thisWord); \
                return ret; \
            } \
        } \
        return -1; \
    }

    #define BITSCAN_NEXT_PRELUDE(X, W, I) \
        int64_t prev = pprev + 1; \
        if(prev >= W * X) \
        { \
            return -1; \
        } \
        \
        /* for first word we need to mask smaller bits */ \
        /* (note if prev % W == 0, it just "overflows" to the next word and everything works as expected) */ \
        int64_t i = prev / X; \
        int64_t thisWord = (int64_t) I; \
        thisWord &= nmask(prev % X); \
        if(thisWord != 0) \
        { \
            int ret = (int) (i * X + bsf(thisWord)); \
            return ret; \
        }

    #define BITSCAN_NEXT_1(X) \
    { \
        BITSCAN_NEXT_PRELUDE(X, 1, word) \
        return -1; \
    }

    ALWAYS_INLINE inline int bitscanFirst8(int8_t word) BITSCAN_FIRST(8, 1, word)
    ALWAYS_INLINE inline int bitscanNext8(int8_t word, int pprev) BITSCAN_NEXT_1(8)
    ALWAYS_INLINE inline int bitscanFirst16(int16_t word) BITSCAN_FIRST(16, 1, word)
    ALWAYS_INLINE inline int bitscanNext16(int16_t word, int pprev) BITSCAN_NEXT_1(16)
    ALWAYS_INLINE inline int bitscanFirst32(int32_t word) BITSCAN_FIRST(32, 1, word)
    ALWAYS_INLINE inline int bitscanNext32(int32_t word, int pprev) BITSCAN_NEXT_1(32)
    ALWAYS_INLINE inline int bitscanFirst64(int64_t word) BITSCAN_FIRST(64, 1, word)
    ALWAYS_INLINE inline int bitscanNext64(int64_t word, int pprev) BITSCAN_NEXT_1(64)

    ALWAYS_INLINE inline int bitscanFirst64n(int64_t const* __restrict__ words, int nWords) BITSCAN_FIRST(64, nWords, words[i])
    ALWAYS_INLINE inline int bitscanNext64n(int64_t const* __restrict__ words, int nWords, int pprev)
    {
        BITSCAN_NEXT_PRELUDE(64, nWords, words[i]);
        i++;
        for(; i < nWords; i++)
        {
            thisWord = (int64_t) words[i];
            if(thisWord != 0)
                return (int) (i * 64 + bsf(thisWord));
        }
        return -1;
    }

    ALWAYS_INLINE inline int bitscanFirst64bs(int64_t const* __restrict__ words) BITSCAN_FIRST(64, 4, words[i])
    ALWAYS_INLINE inline int bitscanNext64bs(int64_t const* __restrict__ words, int pprev)
    {
        BITSCAN_NEXT_PRELUDE(64, 4, words[i]);
        // manual unroll here because i looked at the disassembly and didn't like what i saw
        switch(i)
        {
            case 0:
                thisWord = words[1];
                if(thisWord != 0)
                    return 1 * 64 + bsf(thisWord);
                FALL_THROUGH;
            case 1:
                thisWord = words[2];
                if(thisWord != 0)
                    return 2 * 64 + bsf(thisWord);
                FALL_THROUGH;
            case 2:
                thisWord = words[3];
                if(thisWord != 0)
                    return 3 * 64 + bsf(thisWord);
                FALL_THROUGH;
            default:
                return -1;
        }
    }

    #undef BITSCAN_FIRST
    #undef BITSCAN_NEXT_PRELUDE
    #undef BITSCAN_NEXT_1

    //==================================================================================================================
    // Simple bitset class that's allocated dynamically and aligned to 32 bytes (for use in AVX vector comparisons).
    // You can specify additional over-alignment if desired.
    //==================================================================================================================

    class BitArray
    {
        int64_t* words = nullptr;
        size_t count = 0;
        int nCapcityWords = 0;
        int nUsedWords = 0;

    public:
        BitArray() = default;

        ALWAYS_INLINE inline bool set(size_t index)
        {
            dassert(index >= 0 && index < count, "index = %" PRIu64 ", count = %" PRIu64, index, count);
            int64_t* words = ASSUME_ALIGNED(this->words, 32);
            int64_t old = words[index / 64];
            int64_t _new = words[index / 64] |= sbit((int) (index % 64));
            return old != _new;
        }

        ALWAYS_INLINE inline bool unset(size_t index)
        {
            dassert(index >= 0 && index < count, "index = %" PRIu64 ", count = %" PRIu64, index, count);
            int64_t* words = ASSUME_ALIGNED(this->words, 32);
            int64_t old = words[index / 64];
            int64_t _new = words[index / 64] &= nbit((int) (index % 64));
            return old != _new;
        }

        ALWAYS_INLINE inline bool test(size_t index) const
        {
            dassert(index >= 0 && index < count, "index = %" PRIu64 ", count = %" PRIu64, index, count);
            int64_t* words = ASSUME_ALIGNED(this->words, 32);
            return (words[index / 64] & sbit((int) (index % 64))) != 0;
        }

        ALWAYS_INLINE inline int first() const
        {
            int64_t* words = ASSUME_ALIGNED(this->words, 32);
            if(UNLIKELY(!words))
                return -1;
            return bitscanFirst64n(words, nUsedWords);
        }

        ALWAYS_INLINE inline int next(int after) const
        {
            int64_t* words = ASSUME_ALIGNED(this->words, 32);
            dassert(after >= 0 && after < (int) count, "after = %d, count = %d", after, (int) count);
            dassert(words, "don't call next() on empty bitset");
            return bitscanNext64n(words, nUsedWords, after);
        }

        ALWAYS_INLINE inline size_t bitCount() const { return count; }
        ALWAYS_INLINE inline size_t byteCount() const { return nUsedWords * 8; }
        ALWAYS_INLINE inline void* data() const { return (void*) words; }
        ALWAYS_INLINE inline size_t hash() const { return nUsedWords == 0 ? 0 : hashBytes(data(), byteCount()); }

        ALWAYS_INLINE inline size_t popcount() const
        {
            int64_t* words = ASSUME_ALIGNED(this->words, 32);
            size_t result = 0;
            size_t nUsedWords = this->nUsedWords;
            for(size_t i = 0; i < nUsedWords; ++i)
                result += __builtin_popcountll(words[i]);
            return result;
        }

        ALWAYS_INLINE inline bool any() const
        {
            int64_t* words = ASSUME_ALIGNED(this->words, 32);
            size_t nUsedWords = this->nUsedWords;
            for(size_t i = 0; i < nUsedWords; ++i)
                if(words[i])
                    return true;
            return false;
        }

        ALWAYS_INLINE inline bool intersects(const BitArray& other) const
        {
            int64_t* mine = ASSUME_ALIGNED(this->words, 32);
            int64_t* theirs = ASSUME_ALIGNED(other.words, 32);
            size_t nUsedWords = std::min(this->nUsedWords, other.nUsedWords);
            for(size_t i = 0; i < nUsedWords; ++i)
                if((mine[i] & theirs[i]) != 0)
                    return true;
            return false;
        }

        ALWAYS_INLINE inline bool isSubsetOf(const BitArray& other) const
        {
            int64_t* mine = ASSUME_ALIGNED(this->words, 32);
            int64_t* theirs = ASSUME_ALIGNED(other.words, 32);
            if(other.count < this->count)
                return false;
            dassert(other.nUsedWords >= this->nUsedWords, "Should be at least as big when count is at least as big");
            size_t nUsedWords = this->nUsedWords;
            for(size_t i = 0; i < nUsedWords; ++i)
            {
                int64_t myWord = mine[i], theirWord = theirs[i];
                if((myWord & theirWord) != myWord)
                    return false;
            }
            return true;
        }

        // adding these to support use in STL set. with const under protest
        ALWAYS_INLINE inline bool operator==(const BitArray& other) const { return count == other.count && !memcmp(words, other.words, (size_t) nUsedWords * 8); }
        ALWAYS_INLINE inline bool operator!=(const BitArray& other) const { return !(*this == other); }
        ALWAYS_INLINE inline bool operator<(const BitArray& other) const { if(other.count < count) return false; else if(other.count > count) return true;
            else return memcmp(words, other.words, (size_t) nUsedWords * 8) < 0; }

    private:
        inline void resizeImpl(size_t newCount, bool clear)
        {
            // size is bigger than it would otherwise be for 2 reasons:
            //   first, we always want to be a multiple of the block size so that unaligned reads don't go into invalid memory
            //     (this only actually happens when blockSize % 256 == 192, but it's best to be careful in all cases)
            //   second, we always want it to be a multiple of 32 bytes for vector purposes
            if(newCount > count)
            {
                count = newCount;
                nUsedWords = (int) ((newCount + 63) / 64);
                size_t minSizeBytes = ((newCount + 7) / 8);
                minSizeBytes = nextMultipleOf(minSizeBytes, 32); // vector alignment
                int minSizeWords = (int) (minSizeBytes / 8);
                if(minSizeWords > nCapcityWords)
                {
                    if(!words)
                    {
                        words = (int64_t*) alignedAlloc(32, (size_t) minSizeWords * 8);
                        memset(words, 0, (size_t) minSizeWords * 8);
                        nCapcityWords = minSizeWords;
                    }
                    else
                    {
                        int newNWords = nCapcityWords;
                        while(newNWords < minSizeWords)
                            newNWords *= 2;
                        if(clear)
                        {
                            alignedFree(words);
                            words = (int64_t*) alignedAlloc(32, (size_t) newNWords * 8);
                            memset(words, 0, (size_t) newNWords * 8);
                            nCapcityWords = newNWords;
                        }
                        else
                        {
                            int64_t* newWords = (int64_t*) alignedAlloc(32, (size_t) newNWords * 8);
                            memcpy(newWords, words, (size_t) nCapcityWords * 8);
                            memset(newWords + nCapcityWords, 0, (size_t) (newNWords - nCapcityWords) * 8);
                            alignedFree(words);
                            words = newWords;
                            nCapcityWords = newNWords;
                        }
                    }
                }
                else if(clear)
                {
                    xassert(words, "should not be null here");
                    memset(ASSUME_ALIGNED(words, 32), 0, (size_t) nUsedWords * 8);
                }
            }
            else if(newCount < count)
            {
                xassert(words, "should not be null here");
                if(newCount == 0)
                {
                    memset(words, 0, (size_t) nUsedWords * 8);
                    count = 0;
                    nUsedWords = 0;
                }
                else
                {
                    if(clear)
                    {
                        memset(ASSUME_ALIGNED(words, 32), 0, (size_t) nUsedWords * 8);
                        count = newCount;
                        nUsedWords = (int) ((count + 63) / 64);
                    }
                    else
                    {
                        // we want to unset all elements greater than count. for example, if we have 1010101 and size=3,
                        // we will shrink it to 1010000 (except it's multiples of 256 for everything). This way, if we
                        // grow back there, those elements will already be zeroed. this also makes our bitscan functions
                        // and iterators work correctly without needing extra size checks
                        int newUsedWords = (int) ((newCount + 63) / 64);
                        dassert(newUsedWords > 0 && newUsedWords <= nUsedWords, "newUsedWords = %d, nUsedWords = %d", (int) newUsedWords, (int) nUsedWords);
                        memset(words + newUsedWords, 0, (size_t) (nUsedWords -  newUsedWords) * 8); // trailing words can be zeroed
                        words[newUsedWords - 1] &= bmask((int) (newCount % 64)); // last word in use must be masked
                        count = newCount;
                        nUsedWords = newUsedWords;
                    }
                }
            }
            else if(clear && words)
            {
                memset(ASSUME_ALIGNED(words, 32), 0, (size_t) nUsedWords * 8);
            }
        }

    public:
        ALWAYS_INLINE inline void clear() { if(words) { memset(ASSUME_ALIGNED(words, 32), 0, (size_t) nUsedWords * 8); } }
        ALWAYS_INLINE inline void clearAndResize(size_t newCount) { resizeImpl(newCount, true); }
        ALWAYS_INLINE inline void resize(size_t newCount) { resizeImpl(newCount, false); }

        ALWAYS_INLINE inline void merge(const BitArray& other)
        {
            if(other.count > count)
                resize(other.count);
            int64_t* mine = ASSUME_ALIGNED(this->words, 32);
            int64_t* theirs = ASSUME_ALIGNED(other.words, 32);
            size_t nUsedWords = std::min(this->nUsedWords, other.nUsedWords);
            for(size_t i = 0; i < nUsedWords; ++i)
                mine[i] |= theirs[i];
        }

        //==============================================================================================================
        // C++ Busywork Part 1 - Iterator
        //==============================================================================================================

        class Iterator
        {
            int64_t* words;
            int current;
            int nWords;

        public:
            ALWAYS_INLINE inline Iterator(const BitArray& bitset, int _current) : words(bitset.words), current(_current), nWords(bitset.nUsedWords) { }
            ALWAYS_INLINE inline Iterator& operator++() { current = bitscanNext64n(words, nWords, current); return *this; }
            ALWAYS_INLINE inline explicit operator bool() const { return current != -1; }
            ALWAYS_INLINE inline int operator*() const { return current; }
            ALWAYS_INLINE inline bool operator==(const Iterator& other) const { return current == other.current; }
            ALWAYS_INLINE inline bool operator!=(const Iterator& other) const { return !(*this == other); }
            // default copy/move constructor/assignment operators
        };

        ALWAYS_INLINE inline Iterator begin() const { return words ? Iterator(*this, first()) : end(); }
        ALWAYS_INLINE inline Iterator end() const { return Iterator(*this, -1); }

        //==============================================================================================================
        // C++ Busywork Part 2 -- "Rule of 5"
        //==============================================================================================================

        ALWAYS_INLINE inline BitArray(const BitArray& copy)
            : count(copy.count), nCapcityWords(copy.nCapcityWords), nUsedWords(copy.nUsedWords)
        {
            if(copy.words && nCapcityWords > 0)
            {
                words = (int64_t*) alignedAlloc(32, (size_t) nCapcityWords * 8);
                memcpy(words, copy.words, (size_t) nCapcityWords * 8);
            }
        }

        ALWAYS_INLINE inline BitArray(BitArray&& move) noexcept
                : words(move.words), count(move.count), nCapcityWords(move.nCapcityWords), nUsedWords(move.nUsedWords)
        {
            move.words = nullptr;
            move.count = 0;
            move.nCapcityWords = 0;
            move.nUsedWords = 0;
        }

        ALWAYS_INLINE inline BitArray& operator=(const BitArray& copy)
        {
            if(words)
                alignedFree(words);
            count = copy.count;
            nCapcityWords = copy.nCapcityWords;
            nUsedWords = copy.nUsedWords;
            if(copy.words && nCapcityWords > 0)
            {
                words = (int64_t*) alignedAlloc(32, (size_t) nCapcityWords * 8);
                memcpy(words, copy.words, (size_t) nCapcityWords * 8);
            }
            else
            {
                words = nullptr;
            }
            return *this;
        }

        ALWAYS_INLINE inline BitArray& operator=(BitArray&& move) noexcept
        {
            if(this != &move)
            {
                if(words)
                    alignedFree(words);
                words = move.words;
                count = move.count;
                nCapcityWords = move.nCapcityWords;
                nUsedWords = move.nUsedWords;
                move.words = nullptr;
                move.count = 0;
                move.nCapcityWords = 0;
                move.nUsedWords = 0;
            }
            return *this;
        }

        ALWAYS_INLINE inline ~BitArray()
        {
            if(words)
            {
                alignedFree(words);
                words = nullptr;
                count = 0;
                nCapcityWords = 0;
                nUsedWords = 0;
            }
        }
    };

    class TombstoneList
    {
        BitArray impl;
        int first = -1;
    public:

        struct FreeIndex
        {
            int index;
            bool isNew;
        };

        inline FreeIndex acquire()
        {
            if(first < 0)
            {
                int index = (int) impl.bitCount();
                impl.resize(impl.bitCount() + 1);
                return {index, true};
            }
            else
            {
                int index = first;
                impl.unset((size_t) index);
                first = impl.next(index);
                return {index, false};
            }
        }

        inline void release(int index)
        {
            xassert(index >= 0 && index < (int) impl.bitCount(), "index %d was out of range (size = %d)", index, (int) impl.bitCount());
            xassert(!impl.test(index), "index %d was already free", index);
            impl.set((size_t) index);
            if(first < 0 || index < first)
                first = index;
        }

        inline void clearAndResize(size_t newSize)
        {
            impl.clearAndResize(newSize);
            first = -1;
        }

        inline bool test(size_t index)
        {
            return impl.test(index);
        }

        // this is copyable and movable, using the above copy/move constructors
        TombstoneList() = default;
        DEFAULT_COPY_CONSTRUCTOR(TombstoneList)
        DEFAULT_MOVE_CONSTRUCTOR(TombstoneList)
    };
}
