/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <atomic>
#include "sync.hpp"
#include <boost/circular_buffer.hpp>

namespace burningmime::setmatch
{
    struct MetricValue
    {
        const char* name;
        double value;
        // WARNING: this struct is reinterpret-cast to other types in the API, therefore be very careful when modifying it
    }; ASSERT_TYPE_SIZE(MetricValue, 16);

    class Metric
    {
        const char* _name;
    protected:
        inline explicit Metric(const char* name_) : _name(name_) { }
    public:
        inline const char* name() { return _name; }
        virtual double get() = 0;
        inline virtual ~Metric() = default;
        DISABLE_COPY_CONSTRUCTOR(Metric)
    };

    class SingleValueMetric : public Metric
    {
        std::atomic<double> value = 0; // we might be able to get away with "volatile" on x64, not really sure, but this seems safest
    public:
        inline explicit SingleValueMetric(const char* name) : Metric(name) { }
        inline ~SingleValueMetric() override = default;
        inline void push(double newValue) { value = newValue; }
        inline double get() override { return value; }
        DISABLE_COPY_CONSTRUCTOR(SingleValueMetric)
    };

    template<typename TProvider>
    class LambdaMetric : public Metric
    {
        TProvider provider;
    public:
        inline LambdaMetric(const char* name, TProvider&& provider) : Metric(name), provider(std::move(provider)) { }
        inline ~LambdaMetric() override = default;
        inline double get() override { return provider(); }
    };

    // constructor deduction is failing for some reason
    // also it suggests using std::forward, which won't work either
    template<typename TProvider>
    inline Metric* newLambdaMetric(const char* name, TProvider&& provider)
    {
        return new LambdaMetric<TProvider>(name, std::move(provider));
    }

    // somewhat kinda inspired by dropwizard metrics, but implementation is totally different (ie not doing any rescaling;
    // just calculating weights on the fly when retrieving value). this might be less efficient than rescaling for some
    // use cases, but it seems fine for now.
    class ExponentialDecayMetric : public Metric
    {
        struct Sample { double value; SystemTime t; };
        struct Private;

        SystemTime currentSampleEnd;
        double currentSampleValue = 0;
        size_t currentSampleNPoints = 0;
        boost::circular_buffer<Sample> samples;
        SimpleSpinlock mutex;

    public:
        explicit ExponentialDecayMetric(const char* name);
        inline ~ExponentialDecayMetric() override = default;
        void push(double value);
        double get() override;
        DISABLE_COPY_CONSTRUCTOR(ExponentialDecayMetric)
    };

    class MetricRegistry
    {
        std::vector<std::unique_ptr<Metric>> metrics;

    public:
        inline MetricRegistry() = default;

        template<typename TMetric> inline TMetric& add(TMetric* metric)
        {
            // insert it at sorted position (since we're iterating much more often than inserting, it doesn't matter
            // that this is O(N); still better to use a vector than a set
            auto it = metrics.begin();
            while(it != metrics.end() && strcasecmp((*it)->name(), metric->name()) < 0) ++it;
            metrics.emplace(it, metric);

            // return reference to it so method can be chained
            return *metric;
        }

        inline auto size() { return metrics.size(); }
        inline auto begin() { return metrics.begin(); }
        inline auto end() { return metrics.end(); }
        inline Metric& operator[](size_t i) { return *metrics[i]; }
        DISABLE_COPY_CONSTRUCTOR(MetricRegistry)
    };

    #define METRIC_TIMER_T(M, N, T) MetricTimer<decltype(M->N)> N{M ? &(M->N) : nullptr, T}
    #define METRIC_TIMER(M, N) METRIC_TIMER_T(M, N, ::burningmime::setmatch::now())
    template<typename _TMetric>
    class MetricTimer
    {
        typedef typename std::remove_reference<_TMetric>::type TMetric;
        TMetric* metric;
        SystemTime start;
    public:
        inline explicit MetricTimer(TMetric* metric, SystemTime start) :  metric(metric), start(start) { }
        inline double elapsed() { return toMilliseconds(now() - start); }
        inline ~MetricTimer() { if(metric) metric->push(elapsed()); }
        DISABLE_COPY_CONSTRUCTOR(MetricTimer)
    };
}
