/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../../thirdparty/hash/flat_hash_map.hpp"

namespace burningmime::setmatch
{
    /**
     * This wrapper is just here in case we ever decide to change the hash table implementation, and to make sure
     * the load factor is set.
     *
     * See https://probablydance.com/2017/02/26/i-wrote-the-fastest-hashtable/ for more info about the hash
     * implementation. Basically, when you insert an element, if it's too far away from its desired position, the table
     * re-hashes. To avoid re-hashing when elements are deleted and then others are inserted (but the size doesn't
     * change), the default implementation uses a max load factor of 0.5, meaning on average the table will be 25-50%
     * full. This seems like a huge waste of memory. Instead, we can just accept the table might re-hash at any insert.
     *
     * Because of the invariant about number of positions off, it actually will re-hash often around 0.7 (at least
     * according to the blog). The max load factor here is a cutoff if hashes are nicely distributed, but usually there
     * will be clumps causing it to hash sooner than 85%.
     */
    template<typename K, typename V, typename H = std::hash<K>, typename E = std::equal_to<K>, typename A = std::allocator<std::pair<K, V>>>
    class HashMap : public ska::flat_hash_map<K, V, H, E, A>
    {
        static constexpr float MAX_LOAD_FACTOR = 0.85f;
    public:
        inline HashMap() { this->max_load_factor(MAX_LOAD_FACTOR); }
    };

    /**
     * @see burningmime::setmatch::HashMap
     */
    template<typename T, typename H = std::hash<T>, typename E = std::equal_to<T>, typename A = std::allocator<T>>
    class HashSet : public ska::flat_hash_set<T, H, E, A>
    {
        static constexpr float MAX_LOAD_FACTOR = 0.85f;
    public:
        inline HashSet() { this->max_load_factor(MAX_LOAD_FACTOR); }
        inline bool contains(const T& item) const { return this->find(item) != this->end(); }
    };
}
