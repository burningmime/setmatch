/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

// there are problems with pragma once, but there are also problems with include guards (like forgetting to rename
// or using too general a name), which are more relevant to me than network file paths or symlinks. using pragma once
// everywhere except public headers
#pragma once

// we want these things in most files, so just go ahead and include them here
// if compilation time becomes a problem, consider using precompiled headers
// at least for now, it's only really a problem with some of the boost libs
#include <cstdint>
#include <cstddef>
#include <cinttypes>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <cmath>

// boost::format pulls in the entire world
// while i'd rather not all this appear in every file; there's not much to do about it if we want
// to use format strings for logging
#include <boost/format.hpp>

// As long as we're already pulling in a whole lot of boost, why not this too?
#include <boost/chrono.hpp>

namespace burningmime::setmatch
{
    //======================================================================================================================
    // General macro helpers
    //======================================================================================================================

    #define _SETMATCH_CONCAT_2(X, Y) X##Y
    #define _SETMATCH_CONCAT(X, Y) _SETMATCH_CONCAT_2(X, Y)
    #define _SETMATCH_TOSTR_2(X) #X
    #define _SETMATCH_TOSTR(X) _SETMATCH_TOSTR_2(X)
    #define _SETMATCH_N_ARGS_2(e0,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16,count,...) count
    #define _SETMATCH_N_ARGS(...) _SETMATCH_N_ARGS_2(0,##__VA_ARGS__,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0)
    #define _SETMATCH_UNIQUE_NAME(X) _SETMATCH_CONCAT(__, _SETMATCH_CONCAT(X, _SETMATCH_CONCAT(_, __LINE__)))

    //======================================================================================================================
    // Wrappers around compiler-specific attributes
    //======================================================================================================================

    // gcc is currently the only supported compiler (these also all work on clang)
    // note that in some cases, i'm using the gcc extension instead of a C++ standard, and where I do, it's because
    // of IDE support. for example, clang warns about __attribute__((warn_unused_result)), but not [[nodiscard]]
    #define NEVER_INLINE __attribute__((noinline))
    #ifdef NDEBUG
        #define ALWAYS_INLINE __attribute__((always_inline))
    #else
        #define ALWAYS_INLINE
    #endif
    #define LIKELY(X) (__builtin_expect(!!(X), 1))
    #define UNLIKELY(X) (__builtin_expect(!!(X), 0))
    #define UNREACHABLE (__builtin_unreachable())
    #define NO_RETURN __attribute__((noreturn))
    #define MAYBE_UNUSED __attribute__((unused))
    #define STRUCT_PACKED __attribute__((packed))
    #define ALIGNED_BY(ALIGNMENT) __attribute__((aligned(ALIGNMENT)))
    #define ASSUME_ALIGNED(X, ALIGNMENT) static_cast<decltype(X)>(__builtin_assume_aligned((X), ALIGNMENT))
    #define FALL_THROUGH [[fallthrough]]
    #define NO_DISCARD __attribute__((warn_unused_result))



    //======================================================================================================================
    // C++ implicitly creates copy/move constructors in some circumstances but not others. This makes it explicit what's
    // going on (and IMO a bit more readable)
    //======================================================================================================================

    #define DISABLE_COPY_CONSTRUCTOR(TYPE) \
            TYPE(const TYPE& other) = delete; \
            TYPE& operator=(const TYPE& other) = delete;

    #define DEFAULT_COPY_CONSTRUCTOR(TYPE) \
            TYPE(const TYPE& other) = default; \
            TYPE& operator=(const TYPE& other) = default;

    #define DEFAULT_MOVE_CONSTRUCTOR(TYPE) \
            TYPE(TYPE&& other) noexcept = default; \
            TYPE& operator=(TYPE&& other) noexcept = default;

    //==================================================================================================================
    // Unicode helpers
    //==================================================================================================================

    std::string utf16to8(const char16_t* inChars, size_t inCount);
    std::u16string utf8to16(const char* inChars, size_t inCount);
    std::string cleanUtf8(const char* buf, size_t length);
    inline std::string utf16to8(const std::u16string& str) { return utf16to8(str.data(), str.length()); }
    inline std::u16string utf8to16(const std::string& str) { return utf8to16(str.data(), str.length()); }
    inline std::string cleanUtf8(const std::string& str) { return cleanUtf8(str.data(), str.length()); }

    //======================================================================================================================
    // Formatting (currently using boost::format)
    //======================================================================================================================

    template<typename LambdaString>
    constexpr inline int countFormatParams(LambdaString lambdaString)
    {
        const char* str = lambdaString();
        int count = 0;
        for(int i = 0; str[i] != '\0'; ++i)
        {
            char c = str[i];
            // TODO should also validate string is in correct format and return a -1 or something if it isn't
            if(c == '%')
            {
                ++i;
                if(str[i] == '\0')
                    return -1;
                else if(str[i] != '%')
                    ++count;
            }
        }
        return count;
    }

    template<typename LambdaString>
    constexpr inline const char* baseFilename(LambdaString lambdaString)
    {
        const char* path = lambdaString();
        int index = 0, slashIndex = -1;
        while(path[index])
        {
            if(path[index] == '/' || path[index] == '\\')
                slashIndex = index;
            ++index;
        }
        int basenameIndex = slashIndex + 1;
        return path + basenameIndex;
    }

    template<typename... TArgs>
    NEVER_INLINE inline std::string setmatchFormatImpl(const char* fmt, TArgs&&... args)
    {
        return cleanUtf8((boost::format{fmt} % ... % std::forward<TArgs>(args)).str());
    }

    #define _SETMATCH_FORMAT_COUNT(FMT) ::burningmime::setmatch::countFormatParams([](){return FMT;})
    #define _SETMATCH_FORMAT_STRING(FMT, COUNT) ({ static_assert(COUNT == _SETMATCH_FORMAT_COUNT(FMT), "Wrong number of arguments in format string: " FMT); FMT; })
    #define _SETMATCH_FILENAME ::burningmime::setmatch::baseFilename([](){return __FILE__;})
    #define _SETMATCH_LINENO_FMT "[%s@%s:%d]"
    #define _SETMATCH_LINENO_PARAMS (const char*) __FUNCTION__, (const char*) _SETMATCH_FILENAME, (int32_t) __LINE__
    #define setmatchFormat(FMT, ...) ::burningmime::setmatch::setmatchFormatImpl(_SETMATCH_FORMAT_STRING(FMT, _SETMATCH_N_ARGS(__VA_ARGS__)), ##__VA_ARGS__)
    #define setmatchFormatWithLine(FMT, ...) setmatchFormat(_SETMATCH_LINENO_FMT " " FMT, _SETMATCH_LINENO_PARAMS, ##__VA_ARGS__)

    //==================================================================================================================
    // Assertions and logging
    //==================================================================================================================

    // defined elsewhere
    enum class LogLevel { INFO = 1, WARNING = 2, ERROR = 3 };
    NO_RETURN NEVER_INLINE void xassertFailExit();
    NEVER_INLINE void printStackTrace();
    void logImpl(LogLevel level, const std::string& str);

    // note the do { } while(false) thing is to require there be a semicolon after it and stop this being used as an expression, only as a statement
    #define _SETMATCH_LOG(LOG_LEVEL, FMT, ...) do { ::burningmime::setmatch::logImpl(::burningmime::setmatch::LogLevel::LOG_LEVEL, setmatchFormatWithLine(FMT, ##__VA_ARGS__)); } while(false)
    #define logInfo(FMT, ...) _SETMATCH_LOG(INFO, FMT, ##__VA_ARGS__)
    #define logWarning(FMT, ...) _SETMATCH_LOG(WARNING, FMT, ##__VA_ARGS__)
    #define logError(FMT, ...) _SETMATCH_LOG(ERROR, FMT, ##__VA_ARGS__)
    #define xassert(EXPR, FMT, ...) do { if(UNLIKELY(!(EXPR))) { logError("ASSERT FAILED: %s\n" FMT, (const char*) #EXPR, ##__VA_ARGS__); ::burningmime::setmatch::xassertFailExit(); UNREACHABLE; } } while(false)
    #define xassert_false(FMT, ...) do { logError("UNREACHABLE: " FMT, ##__VA_ARGS__); ::burningmime::setmatch::xassertFailExit(); UNREACHABLE; } while(false)
    #define dassert(EXPR, FMT, ...) xassert(EXPR, FMT, ##__VA_ARGS__)

    // NDEBUG disables dassert but not xassert
    #ifdef NDEBUG
        #undef dassert
        #define dassert(EXPR, FMT, ...) do{}while(false)
    #endif

    //==================================================================================================================
    // Exceptions
    //==================================================================================================================

    enum class ErrorCode
    {
        // 1000...1999 = recoverable errors from external components
        EXTERNAL_ERROR = 1000,

        // 2000...2999 = recoverable errors from internal components
        RESOURCE_DELETED = 2001,
        BAD_JNI_CALL = 2002,

        // 3000...3999 = user input errors
        JSON_TOO_LONG = 3001,
        JSON_SYNTAX_ERROR = 3002,
        JSON_FORMAT_ERROR = 3003,
        RULE_TOO_COMPLEX = 3004,
        ALL_TERMS_NEGATED = 3005,
        EMPTY_AFTER_SIMPLIFY = 3006,
        TOO_MANY_PREDICATES = 3007,
        INVALID_ENCODED_RULE = 3008,
        INVALID_META_TAG = 3009,
        FEATURE_NOT_SUPPORTED = 3010,
        MISSING_OR_INVALID_PARAMETER = 3011,
        FEATURE_DISABLED_BY_CONFIG = 3012,
    };

    class SetmatchException : public std::runtime_error
    {
    public:
        const ErrorCode ec;
        inline SetmatchException(ErrorCode ec, std::string&& message) : std::runtime_error(message), ec(ec) { }
    };

    #define setmatchRaise(ECODE, FMT, ...) throw ::burningmime::setmatch::SetmatchException( \
        ::burningmime::setmatch::ErrorCode::ECODE, setmatchFormat("%d: " FMT, \
        (uint32_t) ::burningmime::setmatch::ErrorCode::ECODE, ##__VA_ARGS__))

    //==================================================================================================================
    // floating point helpers
    //==================================================================================================================

    constexpr const double NaN = std::numeric_limits<double>::quiet_NaN();
    constexpr const double posInf = std::numeric_limits<double>::infinity();
    constexpr const double negInf = -posInf;
    constexpr const double minNormal = std::numeric_limits<double>::min(); // strangely enough, this function is specced to return minimum normallized positive number

    // denormals kill performance and are a PITA with boost geometry
    // just spec that we can't handle them
    ALWAYS_INLINE inline double flushDenormals(double d)
    {
        return
            std::isnan(d) ? NaN :                        // all nans -> quiet nan (remove signalling nans, also all nans hash to the same value)
            std::isinf(d) ? (d > 0 ? posInf : negInf) :  // all infinities -> standard inf/-inf values
            d == 0 ? 0 :                                 // negative zero -> zero
            !std::isnormal(d) ? 0 : d;                   // denormals to 0
    }

    // std::nextafter except demormals are flushed
    ALWAYS_INLINE inline double nextAfterFinite(double d) { return d == -minNormal ? 0 : d == 0 ? minNormal : std::nextafter(d, posInf); }
    ALWAYS_INLINE inline double nextBeforeFinite(double d) { return d == 0 ? -minNormal : d == minNormal ? 0 : std::nextafter(d, negInf); }
    ALWAYS_INLINE inline double nextAfter(double d) { return std::isinf(d) || std::isnan(d) ? d : nextAfterFinite(d); }
    ALWAYS_INLINE inline double nextBefore(double d) { return std::isinf(d) || std::isnan(d) ? d : nextBeforeFinite(d); }


    //==================================================================================================================
    // allow C++ "foreach" style loops to work with a pointer and length
    //==================================================================================================================

    template<class T>
    struct SimplePointerIterable
    {
        T* ptr;
        int length;

        inline T* begin()
        {
            return ptr;
        }

        inline T* end()
        {
            return ptr + length;
        }
    };

    template<class T>
    inline SimplePointerIterable<T> iterate(T* ptr, int length)
    {
        return {ptr, length};
    }

    //==================================================================================================================
    // wrapper around std::map to allow for C# style TryGetValue
    //==================================================================================================================

    template<class TMap, class TKey, class TValue>
    inline bool tryGetValue(const TMap& map, const TKey& key, TValue* value)
    {
        auto it = map.find(key);
        if(it != map.end())
        {
            *value = it->second;
            return true;
        }
        else
        {
            return false;
        }
    }

    //==================================================================================================================
    // memory allocation
    //==================================================================================================================

    // aligned alloc/free (internally these use boost::align)
    // alignedAlloc() currently throws std::bad_alloc() if it fails
    void* alignedAlloc(size_t alignment, size_t size);
    void alignedFree(void* ptr) noexcept;

    // malloc/free replacements that throw std::bad_alloc
    ALWAYS_INLINE inline void* smAlloc(size_t size) { return new char[size]; }
    ALWAYS_INLINE inline void smFree(void* ptr) noexcept { delete[] ((char*) ptr); }

    //==================================================================================================================
    // misc helper functions
    //==================================================================================================================

    // helps with alignment
    ALWAYS_INLINE inline constexpr uint64_t nextMultipleOf(uint64_t size, uint64_t multiple)
    {
        return ((size + (multiple - 1)) / multiple) * multiple;
    }

    // hash an arbitrary set of bytes. hash code may be arbitrarily seeded on startup or have different implementations
    // on different platforms, meaning you should never store this outside the library, or return it in a way that a user
    // might store it
    ALWAYS_INLINE inline size_t hashBytes(const void* data, size_t len) noexcept
    {
        // this is a bit of a hack since it relies on the internals of the stdlib... also where i stole the constant from
        return std::_Hash_bytes(data, len, 0xc70f6907UL);
    }

    //==================================================================================================================
    // static assert for size of a type
    //==================================================================================================================

    // the outer assert always succeeds; it's just there to instantiate the template
    // the assert inside the template can fail, and if it does, the error message will contain the name of type type,
    // actual size, and expected size. this is better than static_assert(sizeof(T) == 32) or something like that because
    // that error message will not tell you the actual size
    #define ASSERT_TYPE_SIZE(TYPE, EXPECTED_SIZE) static_assert(::burningmime::setmatch::AssertSizeIs<TYPE, sizeof(TYPE), EXPECTED_SIZE>::same)
    template<typename TYPE, size_t ACTUAL_SIZE, size_t EXPECTED_SIZE> struct AssertSizeIs
            { static_assert(ACTUAL_SIZE == EXPECTED_SIZE); static constexpr const bool same = true; };

    // these assumptions are made throughout the code
    ASSERT_TYPE_SIZE(size_t, 8);
    ASSERT_TYPE_SIZE(void*, 8);
    ASSERT_TYPE_SIZE(int, 4);

    //==================================================================================================================
    // https://www.youtube.com/watch?v=7FYTGRzq9gM
    //==================================================================================================================

    // this makes std::unique_ptr usable for incomplete types
    #define PIMPL_DECLARE(TYPE) struct TYPE; struct TYPE##Deleter { void operator()(TYPE* p); };
    #define PIMPL_TYPENAME(TYPE) std::unique_ptr<TYPE, TYPE##Deleter>
    #define PIMPL_TYPEDEF(TYPE, ALIAS) PIMPL_DECLARE(TYPE) typedef PIMPL_TYPENAME(TYPE) ALIAS;
    #define PIMPL_VAR(TYPE, NAME) PIMPL_DECLARE(TYPE) PIMPL_TYPENAME(TYPE) NAME;
    #define PIMPL_IMPL(TYPE) void TYPE##Deleter::operator()(TYPE* ptr) { delete ptr; }

    //==================================================================================================================
    // Generic buffer type
    //==================================================================================================================

    struct UBuffer
    {
        void* _data;
        size_t _length;
    public:
        ALWAYS_INLINE inline UBuffer(void* data, size_t length) : _data(data), _length(length) { }
        ALWAYS_INLINE inline void* data() const { return _data; }
        ALWAYS_INLINE inline size_t length() const { return _length; }
        DISABLE_COPY_CONSTRUCTOR(UBuffer)

        ALWAYS_INLINE inline void claim(void** data, size_t* length)
        {
            *data = _data;
            *length = _length;
            _data = nullptr;
            _length = 0;
        }

        ALWAYS_INLINE inline UBuffer& operator=(UBuffer&& other)
        {
            if(this != &other)
            {
                if(_data)
                    smFree(_data);
                _data = other._data;
                _length = other._length;
                other._data = nullptr;
                other._length = 0;
            }
            return *this;
        }

        ALWAYS_INLINE inline UBuffer(UBuffer&& other) : _data(other._data), _length(other._length)
        {
            other._data = nullptr;
            other._length = 0;
        }

        ALWAYS_INLINE inline ~UBuffer()
        {
            if(_data)
                smFree(_data);
            _data = nullptr;
            _length = 0;
        }
    };

    //==================================================================================================================
    // pointer to a C string without any memory management
    // defined in setmatch.h as setmatch_String and reinterpret_casted to this. i don't want to include the external API
    // expect where absolutely necessary, so creating wrapper types for the parts that make it into internal code
    // the data layout must be preserved for forward compatibility.
    //==================================================================================================================

    struct SimpleString
    {
        const char* str = nullptr;
        size_t length = 0;
    };

    typedef SimpleString(*CSimpleStringIterator)(void* state);
    struct SimpleStringIterator
    {
        CSimpleStringIterator callback;
        void* state;
        inline SimpleString operator()() { return callback(state); }
    };

    template<typename F>
    struct FSimpleStringIterator
    {
        F state;
        inline FSimpleStringIterator(F&& state) : state(state) { }
        static inline SimpleString callbackImpl(void* state) { return (*((F*) state))(); }
        inline SimpleStringIterator operator*() { return SimpleStringIterator{&callbackImpl, &state}; }
    };

    //==================================================================================================================
    // pointer to a string that also includes a hash (calculated when it is constructed). to keep it 16 bytes, the
    // length has been reduced to only 4 bytes which is only a problem if you're trying to use 2GB strings
    //=================================================================================================================

    struct HashedString
    {
        const char* str;
        uint32_t length;
        uint32_t hash;

        inline HashedString() : str(nullptr), length(0), hash(0) { }
        inline explicit HashedString(SimpleString ss) : str((char*) ss.str), length((uint32_t) ss.length), hash((uint32_t) (ss.length == 0 ? 0 : hashBytes(ss.str, ss.length))) { }
        inline explicit HashedString(const char* _str, uint32_t _length) : str(_str), length(_length), hash((uint32_t) (_length == 0 ? 0 : hashBytes(_str, (size_t) _length))) { }
        inline explicit HashedString(const char* _str, uint32_t _length, uint32_t _hash) : str(_str), length(_length), hash(_hash) { }

        struct Hash { size_t operator()(const HashedString& s) const noexcept { return (size_t) s.hash; } };
        inline explicit operator bool() const noexcept { return length != 0; }
        inline bool operator!=(const HashedString &other) const noexcept { return !(*this == other); }
        inline bool operator==(const HashedString &other) const noexcept
        {
            if(str == other.str)
                return true;
            if(hash != other.hash || length != other.length)
                return false;
            return !memcmp(str, other.str, (size_t) length);
        }

        DEFAULT_MOVE_CONSTRUCTOR(HashedString);
        DEFAULT_COPY_CONSTRUCTOR(HashedString);
    };

    //==================================================================================================================
    // String specifically designed for use as a key in a hashtable. When it's in the hashtable, it owns its own
    // memory, but when it's used for lookup, it's only a reference
    //==================================================================================================================

    class KeyString
    {
        const char* str;
        int32_t mlength;
        uint32_t hash;

        ALWAYS_INLINE inline size_t length() const { return (size_t) (mlength < 0 ? -mlength : mlength); }
        ALWAYS_INLINE inline bool isOwner() const { return mlength < 0; }

        ALWAYS_INLINE inline KeyString(HashedString s, bool takeOwnership)
        {
            if(!s.length)
            {
                str = nullptr;
                mlength = 0;
                hash = 0;
            }
            else if(takeOwnership)
            {
                char* p = (char*) smAlloc((size_t) s.length + 1);
                memcpy(p, s.str, (size_t) s.length);
                p[s.length] = 0;
                str = p;
                mlength = -s.length;
                hash = s.hash;
            }
            else
            {
                str = s.str;
                mlength = s.length;
                hash = s.hash;
            }
        }

    public:
        KeyString() : str(nullptr), mlength(0), hash(0) { }
        static KeyString copy(HashedString s) { return KeyString(s, true); }
        static KeyString ref(HashedString s) { return KeyString(s, false); }

        HashedString operator*() { return HashedString(str, (uint32_t) (mlength < 0 ? -mlength : mlength), hash); }
        struct Hash { size_t operator()(const KeyString& s) const { return (size_t) s.hash; } };
        inline explicit operator bool() const noexcept { return mlength != 0; }
        inline bool operator!=(const KeyString &other) const noexcept { return !(*this == other); }
        inline bool operator==(const KeyString &other) const noexcept
        {
            if(str == other.str)
                return true;
            if(hash != other.hash || length() != other.length())
                return false;
            return !memcmp(str, other.str, length());
        }

        KeyString(KeyString&& other) noexcept
            : str(other.str), mlength(other.mlength), hash(other.hash)
        {
            other.str = nullptr;
            other.mlength = 0;
            other.hash = 0;
        }

        KeyString& operator=(KeyString&& other) noexcept
        {
            if(this != &other)
            {
                if(str && isOwner())
                    smFree((void*) str);
                str = other.str;
                mlength = other.mlength;
                hash = other.hash;
                other.str = nullptr;
                other.mlength = 0;
                other.hash = 0;
            }
            return *this;
        }

        ~KeyString()
        {
            if(str && isOwner())
            {
                smFree((void*) str);
                str = nullptr;
            }
        }

        DISABLE_COPY_CONSTRUCTOR(KeyString)
    };

    ASSERT_TYPE_SIZE(SimpleString, 16);
    ASSERT_TYPE_SIZE(HashedString, 16);
    ASSERT_TYPE_SIZE(KeyString, 16);

    ALWAYS_INLINE inline std::string_view stringForLog(SimpleString s, size_t maxLength = UINT64_MAX) { return s.str ? std::string_view{s.str, std::min(s.length, maxLength)} : "<NULL>"; }
    ALWAYS_INLINE inline std::string_view stringForLog(HashedString s, size_t maxLength = UINT64_MAX) { return s.str ? std::string_view{s.str, std::min((size_t) s.length, maxLength)} : "<NULL>"; }
    ALWAYS_INLINE inline std::string_view stringForLog(KeyString s, size_t maxLength = UINT64_MAX) { return stringForLog(*s, maxLength); }

    //==================================================================================================================
    // A very simple wrapper type that acts as a "typesafe typedef"
    // Eventually plan to use this most places that are currently using "ints" with -1 as a sentinel value since this
    // is safer and more clear what's happening.
    //==================================================================================================================

    template<typename TSelf>
    class UInt32Wrapper
    {
        uint32_t value;
    public:
        inline explicit UInt32Wrapper(uint32_t _value) : value(_value) { }
        inline uint32_t operator*() { return value;  }

        inline bool operator==(const UInt32Wrapper& other) const { return value == other.value; }
        inline bool operator!=(const UInt32Wrapper& other) const { return value != other.value; }
        inline bool operator<(const UInt32Wrapper& other) const { return value < other.value; }
        inline bool operator>(const UInt32Wrapper& other) const { return value > other.value; }
        inline bool operator<=(const UInt32Wrapper& other) const { return value <= other.value; }
        inline bool operator>=(const UInt32Wrapper& other) const { return value >= other.value; }
        inline friend std::ostream& operator<<(std::ostream& ss, const UInt32Wrapper& obj)
            { ss << obj.value; return ss; }

        static inline TSelf none() { return TSelf(UINT32_MAX); }
        inline bool isValid() const { return value != UINT32_MAX; }
        operator bool() = delete; // explicitly require hasValue() call

        struct Hash { inline size_t operator()(const UInt32Wrapper& obj) { return obj.value; } };
    } STRUCT_PACKED;

    #define MAKE_UINT32_WRAPPER(NAME) class NAME : public burningmime::setmatch::UInt32Wrapper<NAME> \
        { public: using UInt32Wrapper::UInt32Wrapper; } STRUCT_PACKED; \
        ASSERT_TYPE_SIZE(NAME, 4);

    //==================================================================================================================
    // Time functions
    //==================================================================================================================

    typedef boost::chrono::high_resolution_clock ClockType;
    typedef ClockType::duration SystemTime;
    typedef boost::chrono::duration<double> TimeSeconds;
    typedef boost::chrono::duration<double, boost::milli> TimeMilliseconds;

    inline SystemTime now() { return ClockType::now().time_since_epoch(); }
    inline constexpr double toSeconds(SystemTime t) { return boost::chrono::duration_cast<TimeSeconds>(t).count(); }
    inline constexpr SystemTime fromSeconds(double seconds) { return boost::chrono::duration_cast<SystemTime>(TimeSeconds(seconds)); }
    inline constexpr double toMilliseconds(SystemTime t) { return boost::chrono::duration_cast<TimeMilliseconds>(t).count(); }

    //==================================================================================================================
    // Predicate types
    //==================================================================================================================

    enum class PredicateType : uint32_t
    {
        PHRASE = 1,
        RANGE = 2,
    };

    //==================================================================================================================
    // Global limits
    //==================================================================================================================

    constexpr size_t MAX_PHRASES_PER_SUBRULE = 256;
    constexpr size_t MAX_SUBRULES_PER_RULE = 1024;
    constexpr size_t MAX_TOTAL_PHRASES_PER_RULE = (UINT16_MAX - 1);
    constexpr size_t MAX_TERM_LEN = 1024;
    constexpr size_t MAX_TERMS_PER_PHRASE = 64;
    #define SETMATCH_VERSION_STRING "setmatch 0.8.2-SNAPSHOT"
}
