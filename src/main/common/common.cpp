/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.hpp"
#include <boost/align/aligned_alloc.hpp>

#if !NDEBUG
#include <iostream>
#include <boost/stacktrace.hpp>
#endif

namespace burningmime::setmatch
{
    //==================================================================================================================
    // Aligned allocation
    //==================================================================================================================

    void* alignedAlloc(size_t alignment, size_t size)
    {
        if(UNLIKELY(size == 0))
            throw std::bad_alloc();
        void* ptr = boost::alignment::aligned_alloc(alignment, size);
        if(UNLIKELY(!ptr))
            throw std::bad_alloc();
        return ptr;
    }

    void alignedFree(void* ptr) noexcept
    {
        if(LIKELY(ptr))
        {
            boost::alignment::aligned_free(ptr);
        }
    }

    //==================================================================================================================
    // Debug helpers
    //==================================================================================================================

    NO_RETURN NEVER_INLINE void xassertFailExit()
    {
        fflush(stdout);
        fflush(stderr);
        printStackTrace();
        exit(1);
    }

    void printStackTrace()
    {
        #if !NDEBUG
            std::cerr << boost::stacktrace::stacktrace();
            fflush(stderr);
        #endif
    }
}
