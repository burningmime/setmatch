/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "varset.hpp"
#include "boolean.hpp"
#include <vector>
#include <string>

namespace burningmime::setmatch
{
    struct VarsetRemap { int indices[256]; int count; };
    static void writeUShort(std::vector<char>& buf, uint16_t value)
    {
        buf.push_back((char) (value & 0xFF));
        buf.push_back((char) ((value >> 8) & 0xFF));
    }

    static void rewriteUShort(std::vector<char>& buf, size_t at, uint16_t value)
    {
        buf[at    ] = (char) ((value      ) & 0xFF);
        buf[at + 1] = (char) ((value >>  8) & 0xFF);
    }

    static void writeStringPool(std::vector<char>& buf, StringPool& strings, VarsetRemap& remap)
    {
        xassert(strings.size() > 0 && strings.size() <= (int) MAX_PHRASES_PER_SUBRULE, "strings.size() = %d", (int) strings.size());
        writeUShort(buf, (uint16_t) remap.count);
        for(int oldIndex = 0; oldIndex < strings.size(); ++oldIndex)
        {
            int newIndex = remap.indices[oldIndex];
            if(newIndex != -1)
            {
                PredicateInfo& pi = strings.getById(oldIndex);
                buf.push_back((char) '\xB5');
                if(pi.type() == PredicateType::PHRASE)
                {
                    buf.push_back((char) 1);
                    const std::string& str = pi.getString();
                    buf.insert(buf.end(), str.begin(), str.end());
                }
                else
                {
                    buf.push_back((char) 2);
                    static_assert(sizeof(Interval) == 16);
                    IntervalPredicateInfo ii = pi.getInterval();
                    char* reInterval = reinterpret_cast<char*>(&ii.interval);
                    buf.insert(buf.end(), reInterval, reInterval + 16);
                    int oldRef = ii.stringIndex;
                    int newRef = remap.indices[oldRef];
                    writeUShort(buf, (uint16_t) newRef);
                }
            }
        }
    }

    static void writePhrase(std::vector<char>& buf, int oldIndex, bool negated, StringPool& strings, VarsetRemap& remap)
    {
        int newIndex = remap.indices[oldIndex];
        buf.push_back((char) '\xB8');
        buf.push_back((char) (negated ? 1 : 0)); // negation marker
        buf.push_back((char) (strings.getById(oldIndex).type() == PredicateType::PHRASE ? 1 : 2)); // predicate type
        writeUShort(buf, (uint16_t) newIndex);
    }

    static int writeSubrule(std::vector<char>& buf, Varset& varset, StringPool& strings, VarsetRemap& remap)
    {
        int popcount = varset.popcount();
        buf.push_back((char) '\xB7');
        writeUShort(buf, (uint16_t) popcount);
        for(int index : varset.positive)
            writePhrase(buf, index, false, strings, remap);
        for(int index : varset.negative)
            writePhrase(buf, index, true, strings, remap);
        return popcount;
    }


    static void gcMark(FixedSizedBitset& marked, StringPool& strings, int index)
    {
        if(marked.set(index))
        {
            PredicateInfo& pi = strings.getById(index);
            if(pi.type() == PredicateType::RANGE)
                marked.set(pi.getInterval().stringIndex);
        }
    }

    static VarsetRemap garbageCollect(MintermSet& minterms, StringPool& strings)
    {
        FixedSizedBitset marked;
        for(const Varset& varset : minterms)
        {
            for(int index : varset.positive)
                gcMark(marked, strings, index);
            for(int index : varset.negative)
                gcMark(marked, strings, index);
        }

        VarsetRemap remap;
        int j = 0;
        for(int i = 0; i < 256; ++i)
        {
            if(marked.test(i))
                remap.indices[i] = j++;
            else
                remap.indices[i] = -1;
        }
        remap.count = j;

        return remap;
    }

    UBuffer encodeRule(MintermSet& varsets, StringPool& strings)
    {
        xassert(!varsets.empty() && varsets.size() <= MAX_SUBRULES_PER_RULE, "varsets.size() = %d", (int) varsets.size());
        VarsetRemap remap = garbageCollect(varsets, strings);

        std::vector<char> buf;

        // magic constant/version identifier
        buf.push_back('S');
        buf.push_back('M');
        buf.push_back('r');
        buf.push_back('2');

        // string pool first
        writeStringPool(buf, strings, remap);

        buf.push_back((char) '\xB6');  // marker for beginning of rule
        size_t putTotalHere = buf.size();
        writeUShort(buf, 0);

        writeUShort(buf, (uint16_t) varsets.size());
        int totalPredicates = 0;
        for(auto& cvarset : varsets)
        {
            // i have too much actual work to do on this to waste time fucking around with const-corectness for
            // iterators; just know that nothing will be modified
            auto varset = const_cast<Varset&>(cvarset);
            totalPredicates += writeSubrule(buf, varset, strings, remap);
        }

        // put predicate count at reserved space
        if(totalPredicates > (int) MAX_TOTAL_PHRASES_PER_RULE || totalPredicates < 0)
            setmatchRaise(TOO_MANY_PREDICATES, "total predicates in rule was %d, but a maximum of %d are supported", totalPredicates, (int) MAX_TOTAL_PHRASES_PER_RULE);
        rewriteUShort(buf, putTotalHere, (uint16_t) totalPredicates);

        // copy result to new buffer
        char* result = (char*) smAlloc(buf.size());
        memcpy(result, buf.data(), buf.size());
        return {result, buf.size()};
    }
}
