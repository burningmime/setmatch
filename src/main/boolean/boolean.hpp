/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <iosfwd>
#include <set>
#include <vector>
#include <variant>
#include <optional>
#include <memory>
#include "../common/common.hpp"
#include "../common/hash.hpp"
#include "varset.hpp"
#include "../common/interval.hpp"

namespace burningmime::setmatch
{
    enum class NodeKind
    {
        PHRASE = 1,
        AND = 2,
        OR = 3,
    };

    typedef std::optional<class BooleanNode> MaybeNode;
    typedef std::set<Varset> MintermSet;

    class BooleanNode
    {
        struct Private;

        NodeKind kind;
        bool negated;
        int stringId;
        std::vector<BooleanNode> children;

        inline explicit BooleanNode(NodeKind _kind, std::vector<BooleanNode>&& _children) : kind(_kind), negated(false), stringId(-1), children(std::move(_children)) { }
        inline explicit BooleanNode(NodeKind _kind, bool _negated, int _stringId) : kind(_kind), negated(_negated), stringId(_stringId), children() { }
        friend struct Expression; // let Expression look at BooleanNode's privates

        static inline MaybeNode makeExpr(NodeKind kind, std::vector<BooleanNode>&& children)
        {
            if(children.empty())
                return std::nullopt;
            else if(children.size() == 1)
                return std::move(children[0]);
            else
                return MaybeNode(BooleanNode(kind, std::move(children)));
        }

    public:
        inline NodeKind getKind() { return kind; }
        inline bool isNegated() { return negated; }
        inline void negate() { negated = !negated; }
        inline int getStringId() { xassert(kind == NodeKind::PHRASE, "(was %d)", (int) kind); return stringId; }
        inline std::vector<BooleanNode>& getChildren() { return children; }

        static inline MaybeNode And(std::vector<BooleanNode>&& children) { return makeExpr(NodeKind::AND, std::move(children)); }
        static inline MaybeNode Or(std::vector<BooleanNode>&& children) { return makeExpr(NodeKind::OR, std::move(children)); }
        static inline BooleanNode Leaf(int stringId) { return BooleanNode(NodeKind::PHRASE, false, stringId); }
        static inline BooleanNode Leaf(bool negated, int stringId) { return BooleanNode(NodeKind::PHRASE, negated, stringId); }

        DISABLE_COPY_CONSTRUCTOR(BooleanNode)
        DEFAULT_MOVE_CONSTRUCTOR(BooleanNode)
    };

    struct IntervalPredicateInfo
    {
        Interval interval;
        int stringIndex;
        inline bool operator==(const IntervalPredicateInfo& other) const { return interval == other.interval && stringIndex == other.stringIndex; }
        inline bool operator!=(const IntervalPredicateInfo& other) const { return !(*this == other); }
        inline size_t hash() const { return (stringIndex * 1099511628211ULL) ^ Interval::Hash()(interval); }
    };

    class PredicateInfo
    {
        std::variant<std::string, IntervalPredicateInfo> value;
        size_t hash;
    public:
        inline explicit PredicateInfo(std::string phrase) : value(phrase), hash(hashBytes(phrase.data(), phrase.length())) { }
        inline explicit PredicateInfo(IntervalPredicateInfo ii) : value(ii), hash(ii.hash()) { }
        inline const size_t getHash() { return hash; }
        inline PredicateType type() const { return value.index() ? PredicateType::RANGE : PredicateType::PHRASE; }
        inline const std::string& getString() { xassert(type() == PredicateType::PHRASE, "was %d", (int) type()); return std::get<std::string>(value); }
        inline const IntervalPredicateInfo& getInterval() { xassert(type() == PredicateType::RANGE, "was %d", (int) type()); return std::get<IntervalPredicateInfo>(value); }
        inline bool operator==(const PredicateInfo& other) const { return hash == other.hash && value == other.value; }
        inline bool operator!=(const PredicateInfo& other) const { return !(*this == other); }
        DISABLE_COPY_CONSTRUCTOR(PredicateInfo)
        DEFAULT_MOVE_CONSTRUCTOR(PredicateInfo)
    };

    class StringPool
    {
        typedef std::vector<PredicateInfo> KeyVector;
        struct IndexedRef
        {
            void* ptr;
            size_t index;
            inline explicit IndexedRef(PredicateInfo* key) : ptr(key), index(UINT64_MAX) { }
            inline explicit IndexedRef(KeyVector* list, size_t index) : ptr(list), index(index) { }
            inline PredicateInfo& get() const { return index == UINT64_MAX ? *((PredicateInfo*) ptr) : (*((KeyVector*) ptr))[index]; }
            inline bool operator==(const IndexedRef& other) const { return get() == other.get(); }
            inline bool operator!=(const IndexedRef& other) const { return !(*this == other); }
            struct Hash { inline size_t operator()(const IndexedRef& obj) const { return obj.get().getHash(); } };
        };

        std::unique_ptr<KeyVector> idToKey = std::make_unique<KeyVector>(); // this needs to be a pointer so indexed ref can keep addresses
        HashSet<IndexedRef, typename IndexedRef::Hash> keyToId;

    public:
        inline StringPool() = default;
        DEFAULT_MOVE_CONSTRUCTOR(StringPool)
        DISABLE_COPY_CONSTRUCTOR(StringPool)

        inline int size() { return (int) idToKey->size(); }
        inline auto begin() { return idToKey->begin(); }
        inline auto end() { return idToKey->end(); }

        inline PredicateInfo& getById(int index)
        {
            xassert(index >= 0 && index < (int) idToKey->size(), "requested index = %d, max index = %d", index, size());
            return (*idToKey)[index];
        }

        inline int getOrAdd(PredicateInfo&& key)
        {
            IndexedRef temp(&key);
            auto it = keyToId.find(temp);
            if(it == keyToId.end())
            {
                size_t index = idToKey->size();
                if(index >= MAX_PHRASES_PER_SUBRULE)
                    setmatchRaise(TOO_MANY_PREDICATES, "Rule too complex. The rule must not contain more than %d predicates", MAX_PHRASES_PER_SUBRULE);
                idToKey->push_back(std::move(key));
                keyToId.insert(IndexedRef(idToKey.get(), index));
                return (int) index;
            }
            else
            {
                size_t index = it->index;
                dassert(index != UINT64_MAX, "Should always have a valid index here");
                return (int) index;
            }
        }
    };

    struct Expression
    {
        BooleanNode root;
        StringPool strings;

        void varsetToString(Varset& varset, std::ostringstream& ss);
        std::string dnfToString(MintermSet& dnf);
        MintermSet toDnf();
        inline explicit Expression(BooleanNode&& _root, StringPool&& _strings) : root(std::move(_root)), strings(std::move(_strings)) { }
        DISABLE_COPY_CONSTRUCTOR(Expression)
        DEFAULT_MOVE_CONSTRUCTOR(Expression)
    };
}
