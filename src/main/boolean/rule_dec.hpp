/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include "../common/interval.hpp"
#include <vector>
#include <variant>

namespace burningmime::setmatch
{
    struct PredicateHeader
    {
        PredicateType predicateType;
        bool isNegated;
    };

    enum class ParseTokenType
    {
        BEGIN_RULE               = 1,
        BEGIN_SUBRULE            = 2,
        BEGIN_PREDICATE          = 3,
        STRING                   = 4,
        INTERVAL                 = 5,
        END                      = -1,
        INVALID                  = -2,
    };

    class ParseToken
    {
    private:
        ParseTokenType mType;
        std::variant<size_t, PredicateHeader, SimpleString, Interval> data;
        explicit ParseToken(ParseTokenType type) : mType(type) { }
        template<typename T> ParseToken(ParseTokenType type, T data) : mType(type), data(data) { }

    public:
        inline static ParseToken beginRule(size_t totalPredicateCount) { return ParseToken{ParseTokenType::BEGIN_RULE, totalPredicateCount}; }
        inline static ParseToken beginSubrule() { return ParseToken{ParseTokenType::BEGIN_SUBRULE}; }
        inline static ParseToken beginPredicate(PredicateHeader header) { return ParseToken{ParseTokenType::BEGIN_PREDICATE, header}; }
        inline static ParseToken string(SimpleString string) { return ParseToken{ParseTokenType::STRING, string}; }
        inline static ParseToken interval(Interval interval) { return ParseToken{ParseTokenType::INTERVAL, interval}; }
        inline static ParseToken end() { return ParseToken{ParseTokenType::END}; }
        inline static ParseToken invalid() { return ParseToken(ParseTokenType::INVALID); }

        inline ParseTokenType type() { return mType; };
        inline size_t getTotalPredicateCount() { xassert(mType == ParseTokenType::BEGIN_RULE, "mType = %d", (int) mType); return std::get<size_t>(data);  }
        inline PredicateHeader getPredicateHeader() {  xassert(mType == ParseTokenType::BEGIN_PREDICATE, "mType = %d", (int) mType); return std::get<PredicateHeader>(data); }
        inline SimpleString getString() { xassert(mType == ParseTokenType::STRING, "mType = %d", (int) mType); return std::get<SimpleString>(data); }
        inline Interval getInterval() { xassert(mType == ParseTokenType::INTERVAL, "mType = %d", (int) mType); return std::get<Interval>(data); }

        // for debugging/testing
        std::string debugString();
        bool deepEquals(ParseToken& other);
    };

    std::vector<ParseToken> decodeBinaryRule(void* ruleData, size_t ruleLength);
}
