/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "rule_dec.hpp"
#include <string>
#include <boost/container/small_vector.hpp>
#include <sstream>

namespace burningmime::setmatch
{
    constexpr int VERSION_MARKER = ('S') | ('M' << 8) | ('r' << 16) | ('2' << 24);
    std::vector<ParseToken> decodeBinaryRule(void* ruleData, size_t ruleLength)
    {
        #define rassert(EXPR, FMT, ...) do { if(UNLIKELY(!(EXPR))) { setmatchRaise(INVALID_ENCODED_RULE, \
                "Invalid encoded rule\n    Assert failed: " _SETMATCH_LINENO_FMT " %s\n    " FMT, _SETMATCH_LINENO_PARAMS, #EXPR, ##__VA_ARGS__); } } while(false)
        #define wrassert(EXPR, ...) do { if(UNLIKELY(!(EXPR))) { logWarning(__VA_ARGS__); } } while(false)
        typedef boost::container::small_vector<SimpleString, 1> TokenSmallVector;
        struct TempPredicate
        {
            PredicateType type;
            TokenSmallVector tokens;
            Interval interval;

            explicit TempPredicate(TokenSmallVector&& tokens) : type(PredicateType::PHRASE), tokens(tokens) { }
            explicit TempPredicate(TokenSmallVector tokens, Interval interval) : type(PredicateType::RANGE), tokens(std::move(tokens)), interval(interval) { }
        };

        class RuleReader
        {
            uint8_t* start;
            uint8_t* p;
            uint8_t* end;

            void checkBounds(int n) { rassert(p + n <= end, "Unexpected end of data"); }

        public:
            explicit RuleReader(void* ruleData, size_t ruleLength) : start((uint8_t*) ruleData), p(start), end(p + ruleLength) { }
            uint8_t readByte() { checkBounds(1); auto x = *p; p += 1; return x; }
            uint16_t readUShort() { checkBounds(2); auto x = *((uint16_t*) p); p += 2; return x; }
            int readInt() { checkBounds(4); auto x = *((int*) p); p += 4; return x; }
            double readDouble() { checkBounds(8); auto x = *((double*) p); p += 8; return x; }
            char* readString(int n) { if(n <= 0) return nullptr; checkBounds(n); auto x = ((char*) p); p += n; return x; }
            void checkMarkerByte(uint8_t expected) { uint8_t mark = readByte(); rassert(mark == expected, "mark = 0x%02X, expected = 0x%02X", (int) mark, (int) expected); }
            size_t bytesRead() { return (size_t) (p - start); }
        };

        rassert(ruleData != nullptr && ruleLength > 4, "Null or too short data");
        RuleReader reader{ruleData, ruleLength};
        std::vector<ParseToken> sink;
        int vmark = reader.readInt();
        rassert(vmark == VERSION_MARKER, "Expected version marker 'SMr2'");

        // first up is the string table
        std::vector<TempPredicate> stringPool;
        int nStrings = reader.readUShort();
        rassert(nStrings > 0 && nStrings <= (int) MAX_PHRASES_PER_SUBRULE, "nStrings = %d, max = %d", nStrings, MAX_PHRASES_PER_SUBRULE);

        stringPool.reserve((size_t) nStrings);
        for(int i = 0; i < nStrings; ++i)
        {
            reader.checkMarkerByte(0xB5);
            int predType = reader.readByte();
            if(predType == 1)
            {
                int nTokens = reader.readUShort();
                rassert(nTokens > 0 && nTokens < (int) MAX_TERMS_PER_PHRASE, "nTokens = %d, max = %d", nTokens, MAX_TERMS_PER_PHRASE);
                TokenSmallVector tokens;
                tokens.reserve((size_t) nTokens);
                for(int j = 0; j < nTokens; ++j)
                {
                    size_t tokenLen = reader.readUShort();
                    rassert(tokenLen > 0 && tokenLen <= MAX_TERM_LEN, "tokenLen = %d, max = %d", tokenLen, MAX_TERM_LEN);
                    tokens.push_back(SimpleString{reader.readString(tokenLen), tokenLen});
                }
                stringPool.emplace_back(std::move(tokens));
            }
            else if(predType == 2)
            {
                double min = reader.readDouble();
                double max = reader.readDouble();
                int stringIndex = reader.readUShort();
                rassert(!std::isnan(min) && !std::isnan(max) && (!std::isinf(min) || !std::isinf(max)), "min = %f, max = %f", min, max);
                rassert(min <= max, "min = %f, max = %f", min, max);
                rassert(stringIndex < i, "stringIndex = %d, max = %d", stringIndex, i);
                rassert(stringPool[stringIndex].type == PredicateType::PHRASE, "stringIndex = %d, type = %d", stringIndex, (int) stringPool[stringIndex].type);
                stringPool.emplace_back(stringPool[stringIndex].tokens, Interval{min, max});
            }
            else
            {
                rassert(false, "Invalid predicate type code %d", predType);
            }
        }

        // now is the rule
        reader.checkMarkerByte(0xB6);
        int totalPredicates = reader.readUShort();

        sink.push_back(ParseToken::beginRule((size_t) totalPredicates));

        int nSubrules = reader.readUShort();
        rassert(nSubrules > 0 && nSubrules < (int) MAX_SUBRULES_PER_RULE, "nSubrules = %d, max = %d", nSubrules, MAX_SUBRULES_PER_RULE);
        for(int i = 0; i < nSubrules; i++)
        {
            sink.push_back(ParseToken::beginSubrule());
            reader.checkMarkerByte(0xB7);
            int nPhrases = reader.readUShort();
            rassert(nPhrases > 0 && nPhrases <= (int) MAX_PHRASES_PER_SUBRULE, "nPhrases = %d, max = %d", nPhrases, MAX_PHRASES_PER_SUBRULE);
            for(int j = 0; j < nPhrases; j++)
            {
                reader.checkMarkerByte(0xB8);

                int negatedByte = reader.readByte();
                rassert(negatedByte == 0 || negatedByte == 1, "negatedByte = %d", negatedByte);
                bool isNegated = (bool) negatedByte;

                int currentPredicateType = reader.readByte();
                int stringIndex = reader.readUShort();
                rassert(stringIndex >= 0 && stringIndex < nStrings, "stringIndex = %d, nStrings = %d", stringIndex, nStrings);

                TempPredicate& predRef = stringPool[stringIndex];
                // TODO it's possible to transmit some of this info to add rule so it doesn't need to look up tokens in the map
                if(currentPredicateType == 1)
                {
                    rassert(predRef.type == PredicateType::PHRASE, "predRef.type = %d", (int) predRef.type);
                    dassert(!predRef.tokens.empty(), "Should have at least one token here");
                    dassert(!((bool) predRef.interval), "Shouldn't have an interval here");
                    sink.push_back(ParseToken::beginPredicate({PredicateType::PHRASE, isNegated}));
                    for(auto& token : predRef.tokens)
                        sink.push_back(ParseToken::string(token));
                }
                else if(currentPredicateType == 2)
                {
                    rassert(predRef.type == PredicateType::RANGE, "predRef.type = %d", (int) predRef.type);
                    dassert((bool) predRef.interval, "Interval should be valid here");
                    wrassert(predRef.tokens.size() == 1, "Range references phrase with multiple tokens; using only first (first two: %s, %s)",
                            stringForLog(predRef.tokens[0]), stringForLog(predRef.tokens[1]));
                    sink.push_back(ParseToken::beginPredicate({PredicateType::RANGE, isNegated}));
                    sink.push_back(ParseToken::string(predRef.tokens[0]));
                    sink.push_back(ParseToken::interval(predRef.interval));
                }
                else
                {
                    rassert(false, "Invalid predicate type %d", (int) currentPredicateType);
                }
            }
        }

        rassert(reader.bytesRead() == ruleLength, "Did not consume entire rule (bytesRead = %d, length = %d)", reader.bytesRead(), ruleLength);
        sink.push_back(ParseToken::end());
        return sink;
        #undef rassert
        #undef wrassert
    }

    std::string ParseToken::debugString()
    {
        std::ostringstream ss;
        switch(mType)
        {
            case ParseTokenType::BEGIN_RULE:
                ss << "BEGIN_RULE[" << getTotalPredicateCount() << "]";
                break;
            case ParseTokenType::BEGIN_SUBRULE:
                ss << "BEGIN_SUBRULE";
                break;
            case ParseTokenType::BEGIN_PREDICATE:
                ss << "BEGIN_PREDICATE[";
                switch(getPredicateHeader().predicateType)
                {
                    case PredicateType::PHRASE: ss << "PHRASE"; break;
                    case PredicateType::RANGE: ss << "RANGE"; break;
                    default: xassert_false("unknown predicate type %d", (int) getPredicateHeader().predicateType);
                }
                if(getPredicateHeader().isNegated)
                    ss << ", negated";
                ss << "]";
                break;
            case ParseTokenType::STRING:
                ss << "STRING[" << cleanUtf8(getString().str, getString().length) << "]";
                break;
            case ParseTokenType::INTERVAL:
                ss << "INTERVAL[" << getInterval().getLeft() << "," << getInterval().getRight() << "]";
                break;
            case ParseTokenType::END:
                ss << "END";
                break;
            case ParseTokenType::INVALID:
                ss << "INVALID";
                break;
            default:
                xassert_false("unknown ParseTokenType %d", (int) mType);
        }
        return ss.str();
    }

    bool ParseToken::deepEquals(ParseToken& other)
    {
        if(mType != other.mType)
            return false;
        switch(mType)
        {
            case ParseTokenType::BEGIN_RULE:
                return getTotalPredicateCount() == other.getTotalPredicateCount();
            case ParseTokenType::BEGIN_SUBRULE:
                return true;
            case ParseTokenType::BEGIN_PREDICATE:
            {
                PredicateHeader myph = getPredicateHeader();
                PredicateHeader oph = other.getPredicateHeader();
                return myph.predicateType == oph.predicateType && myph.isNegated == oph.isNegated;
            }
            case ParseTokenType::STRING:
            {
                SimpleString myString = getString();
                SimpleString otherString = other.getString();
                return myString.length == otherString.length &&
                    !memcmp(myString.str, otherString.str, myString.length);
            }
            case ParseTokenType::INTERVAL:
                return getInterval() == other.getInterval();
            case ParseTokenType::END:
                return true;
            case ParseTokenType::INVALID:
                return true;
            default:
                xassert_false("unhandled case");
        }
    }
}
