/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include "../common/bitset.hpp"

namespace burningmime::setmatch
{
    // a 256-bit bit array, which uses the blockset functions to be fairly fast
    // in addition to performance advantages, it much simplifies the logic wrt hashing/equality since we don't need
    // to worry about changing size. this lets us relatively painlessly add merged intervals
    class FixedSizedBitset
    {
        int64_t words[4] = {0};
    public:
        class Iterator
        {
            const int64_t* words;
            int current;
        public:
            inline Iterator(const FixedSizedBitset& bitset, int _current) : words(bitset.words), current(_current) { }
            inline Iterator& operator++() { current = bitscanNext64bs(words, current); return *this; }
            inline explicit operator bool() const { return current != -1; }
            inline int operator*() const { return current; }
            inline bool operator==(const Iterator& other) const { return current == other.current; }
            inline bool operator!=(const Iterator& other) const { return !(*this == other); }
        };

        inline bool set(int index) { dassert(index >= 0 && index < 256, "index = %d", index); int64_t old = words[index / 64]; int64_t next = (words[index / 64] |= sbit((int) (index % 64))); return old != next; }
        inline bool unset(int index) { dassert(index >= 0 && index < 256, "index = %d", index); int64_t old = words[index / 64]; int64_t next = (words[index / 64] &= nbit((int) (index % 64))); return old != next; }
        inline void clear() { words[0] = 0; words[1] = 0; words[2] = 0; words[3] = 0; }
        inline void merge(const FixedSizedBitset& other) { for(size_t i = 0; i < 4; ++i) words[i] |= other.words[i]; }
        inline bool test(int index) const { dassert(index >= 0 && index < 256, "index = %d", index); return (words[index / 64] & sbit((int) (index % 64))) != 0; }
        inline int first() const { return bitscanFirst64bs(words); }
        inline int next(int after) const { return bitscanNext64bs(words, after); }
        inline void* data() const { return (void*) words; }
        inline size_t hash() const { return hashBytes(words, 32); }
        inline size_t popcount() const { return __builtin_popcountll(words[0]) + __builtin_popcountll(words[1]) + __builtin_popcountll(words[2]) + __builtin_popcountll(words[3]); }
        inline bool any() const { return words[0] != 0 || words[1] != 0 || words[2] != 0 || words[3] != 0; }
        inline bool intersects(const FixedSizedBitset& other) const { for(size_t i = 0; i < 4; ++i) { if((words[i] & other.words[i]) != 0) return true; } return false; }
        inline bool isSubsetOf(const FixedSizedBitset& other) const { for(size_t i = 0; i < 4; ++i) { if((words[i] & other.words[i]) != words[i]) return false; } return true; }
        inline Iterator begin() const { return Iterator(*this, first()); }
        inline Iterator end() const { return Iterator(*this, -1); }
        inline bool operator==(const FixedSizedBitset& other) const { return memcmp(words, other.words, 32) == 0; }
        inline bool operator!=(const FixedSizedBitset& other) const { return !(*this == other); }
        inline bool operator<(const FixedSizedBitset& other) const { return memcmp(words, other.words, 32) < 0; }
    };

    struct Varset
    {
        FixedSizedBitset positive, negative;

        explicit Varset() = default;
        explicit Varset(int index, bool negated)
        {
            (negated ? negative : positive).set((size_t) index);
        }

        // (need "const" all over the place so it works with STL set)
        inline bool operator==(const Varset& other) const { return positive == other.positive && negative == other.negative; }
        inline bool operator!=(const Varset& other) const { return !(*this == other); }
        inline bool operator<(const Varset& other) const { return positive == other.positive ? negative < other.negative : positive < other.positive; }

        inline void merge(const Varset& other)
        {
            positive.merge(other.positive);
            negative.merge(other.negative);
        }

        inline bool isAlwaysFalse() const
        {
            return positive.intersects(negative);
        }

        inline bool isRedundantWith(const Varset& other) const
        {
            // if something is a subset then we're redundant with it. at first blush, that might seem contradictory,
            // but consider that (a&b&c) matches less than (a&b), therefore (a&b&c) is the redundant one. similarly,
            // (~a&~b&~c) matches less than (~a&~b). put differently, (a&b)|(a&b&c)=(a&b) and (~a&~b)|(~a&~b&~c) = (~a&~b)
            return (positive == other.positive && other.negative.isSubsetOf(negative)) ||
                   (negative == other.negative && other.positive.isSubsetOf(positive));
        }

        inline int popcount() const
        {
            return (int) (negative.popcount() + positive.popcount());
        }

        struct Hash
        {
            inline size_t operator()(const Varset& varset) const
            {
                return varset.positive.hash() * 1099511628211ULL + varset.negative.hash();
            }
        };
    };
}
