/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "boolean.hpp"
#include "../common/json.h"
#include <iostream>

namespace burningmime::setmatch
{
    static MaybeNode parsePhrase(JsonNode array, StringPool& ctx)
    {
        if(array.GetType() == rapidjson::kArrayType)
        {
            size_t count = array.Size();
            size_t actualCount = 0;
            if(count == 0)
                return std::nullopt;
            std::string ss;
            ss.push_back((char) 0); // reserve some space for the number of tokens
            ss.push_back((char) 0);
            for(size_t i = 0; i < count; i++)
            {
                JsonNode child = array[i];
                if(child.GetType() != rapidjson::kStringType)
                    jsonFormatError("elements in \"phrase\" array must be strings");
                size_t strlen = child.GetStringLength(), subStart = 0;
                if(strlen == 0)
                    continue;
                if(strlen > MAX_TERM_LEN)
                    jsonFormatError("Terms cannot have more than %d characters, but found one with %d (first 50: \"%s\")",
                            MAX_TERM_LEN, strlen, (std::string_view{child.GetString(), 50}));

                actualCount++;
                ss.push_back((char) (strlen & 0xFF));
                ss.push_back((char) ((strlen >> 8) & 0xFF));
                ss.insert(ss.size(), child.GetString() + subStart, strlen);
            }

            if(!actualCount)
                return std::nullopt;
            else if(actualCount > MAX_TERMS_PER_PHRASE)
                jsonFormatError("a single \"phrase\" array may not contain more than %d terms, but got %d", (int) MAX_TERMS_PER_PHRASE, (int) count);
            else
            {
                ss[0] = (char) (actualCount & 0xFF);
                ss[1] = (char) ((actualCount >> 8) & 0xFF);
                ss.shrink_to_fit();
                return BooleanNode::Leaf(ctx.getOrAdd(PredicateInfo(std::move(ss))));
            }
        }
        else
        {
            jsonFormatError("value of \"phrase\" node must be an array of strings");
        }
    }

    static std::string encodeSingleTermAsPhrase(std::string str)
    {
        dassert(str.length() > 0, "shouldn't have empty string here");
        std::string result;
        result.reserve(str.length() + 4);
        result.push_back((char) 1);
        result.push_back((char) 0);
        result.push_back((char) (str.length() & 0xFFu));
        result.push_back((char) ((str.length() >> 8u) & 0xFFu));
        result.insert(result.end(), str.begin(), str.end());
        return result;
    }

    static MaybeNode parseRange(JsonNode node, StringPool& ctx)
    {
        if(node.GetType() != rapidjson::kObjectType)
            jsonFormatError("value of \"range\" nodes must be an object");
        double min = negInf;
        double max = posInf;
        std::string nkey;
        IntervalKind kind = IntervalKind::INVALID;
        for(auto it = node.MemberBegin(); it != node.MemberEnd(); ++it)
        {
            auto key = it->name.GetString();
            auto& value = it->value;
            if(isKey(key, "min"))
            {
                if(value.GetType() != rapidjson::kNumberType)
                    jsonFormatError("'range.min' field must be a double");
                double dval = value.GetDouble();
                if(std::isnan(dval) || std::isinf(dval))
                    jsonFormatError("'range.min' value must not be NaN or infinity");
                min = dval;
            }
            else if(isKey(key, "max"))
            {
                if(value.GetType() != rapidjson::kNumberType)
                    jsonFormatError("'range.max' field must be a double");
                double dval = value.GetDouble();
                if(std::isnan(dval) || std::isinf(dval))
                    jsonFormatError("'range.max' value must not be NaN or infinity");
                max = dval;
            }
            else if(isKey(key, "term"))
            {
                if(value.GetType() != rapidjson::kStringType)
                    jsonFormatError("'range.term' must be a string");
                if(value.GetStringLength() == 0)
                    jsonFormatError("'range.term' cannot be empty");
                if(value.GetStringLength() > MAX_TERM_LEN)
                    jsonFormatError("'range.term' may not be longer than %d characters", (int) MAX_TERM_LEN);
                nkey = std::string(value.GetString(), value.GetStringLength());
            }
            else if(isKey(key, "kind"))
            {
                if(value.GetType() != rapidjson::kStringType)
                    jsonFormatError("'range.kind' must be a string");
                const char* skind = value.GetString();
                if(isKey(skind, "EXCLUSIVE"))
                    kind = IntervalKind::EXCLUSIVE;
                else if(isKey(skind, "MAX_INCLUSIVE"))
                    kind = IntervalKind::MAX_INCLUSIVE;
                else if(isKey(skind, "MIN_INCLUSIVE"))
                    kind = IntervalKind::MIN_INCLUSIVE;
                else if(isKey(skind, "INCLUSIVE"))
                    kind = IntervalKind::INCLUSIVE;
                else
                    jsonFormatError("'range.kind' must be one of 'EXCLUSIVE', 'INCLUSIVE', "
                           "'MAX_INCLUSIVE' or 'MIN_INCLUSIVE', but was %s", skind);
            }
            else
            {
                jsonFormatError("Invalid property '%s' in range node", key);
            }
        }

        if(std::isinf(min) && std::isinf(max))
            jsonFormatError("'range' does must have a 'max', 'min', or both");
        if(nkey.empty())
            jsonFormatError("'range' nodes must have a 'term' element");
        if(kind == IntervalKind::INVALID)
            jsonFormatError("'range' nodes must have a 'kind' element");
        if(max < min)
            jsonFormatError("Range %s maximum element %f is less than minimum element %f", nkey, max, min);
        if(max == min && kind != IntervalKind::INCLUSIVE)
            jsonFormatError("Range %s minimum and maximum element are equal in an exclusive or semi-exclusive range. "
                   "No values fall into this range.", nkey);

        Interval interval = Interval::makeNormalized(min, max, kind);
        int stringIndex = ctx.getOrAdd(PredicateInfo{encodeSingleTermAsPhrase(nkey)});
        IntervalPredicateInfo ii{interval, stringIndex};
        return BooleanNode::Leaf(ctx.getOrAdd(PredicateInfo{ii}));
    }

    static MaybeNode parseNode(JsonNode node, StringPool& ctx);
    static MaybeNode parseExpr(NodeKind op, JsonNode array, StringPool& ctx)
    {
        if(array.GetType() != rapidjson::kArrayType)
            jsonFormatError("value of \"and\" or \"or\" nodes must be an array of json objects");
        size_t count = array.Size();
        if(count == 0)
            return std::nullopt;
        else if(count == 1)
            return parseNode(array[0], ctx);
        else
        {
            std::vector<BooleanNode> children;
            children.reserve(count);
            for(size_t i = 0; i < count; i++)
            {
                MaybeNode child = parseNode(array[i], ctx);
                if(child)
                    children.push_back(std::move(child.value()));
            }

            return op == NodeKind::OR ? BooleanNode::Or(std::move(children)) : BooleanNode::And(std::move(children));
        }
    }

    static MaybeNode parseNode(JsonNode node, StringPool& ctx)
    {
        if(node.GetType() == rapidjson::kObjectType)
        {
            size_t count = node.MemberCount();
            if(count != 1)
                jsonFormatError("object nodes inside rule must have exactly one child");
            auto it = node.MemberBegin();
            auto key = it->name.GetString();
            auto& value = it->value;
            if(isKey(key, "and"))
                return parseExpr(NodeKind::AND, value, ctx);
            else if(isKey(key, "or"))
                return parseExpr(NodeKind::OR, value, ctx);
            else if(isKey(key, "not"))
            {
                MaybeNode child = parseNode(value, ctx);
                if(child)
                {
                    child.value().negate();
                    return child;
                }
                else
                {
                    return std::nullopt;
                }
            }
            else if(isKey(key, "phrase"))
                return parsePhrase(value, ctx);
            else if(isKey(key, "range"))
                return parseRange(value, ctx);
            else
                jsonFormatError("encountered unknown node key %s while parsing json", key);
        }
        else
        {
            jsonFormatError("unexpected json type as node -- expected an object");
        }
    }

    static MaybeNode parseRoot(JsonNode node, StringPool& ctx)
    {
        if(node.GetType() != rapidjson::kObjectType)
            jsonFormatError("root node must be a json object");
        auto it = node.FindMember("rule");
        if(it == node.MemberEnd())
            jsonFormatError("did not find \"rule\" element in root object");
        return parseNode(it->value, ctx);
    }

    Expression ruleJsonToBooleanExpr(const char* ruleJson_utf8_nt)
    {
        rapidjson::Document document = parseJson(ruleJson_utf8_nt);
        StringPool strings;
        MaybeNode root = parseRoot(document, strings);
        if(!root)
            setmatchRaise(EMPTY_AFTER_SIMPLIFY, "tree evaluated to empty (this can happen if an expression would be always false, "
                    "e.g. \"A and (not A)\")");
        return Expression(std::move(root.value()), std::move(strings));
    }
}

