/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "boolean.hpp"
#include <sstream>
#include "varset.hpp"
#include <stdexcept>
#include <set>

namespace burningmime::setmatch
{
    constexpr int MAX_VARSETS_PRE_SIMPLIFY = 4096;
    constexpr int MAX_VARSETS_POST_SIMPLIFY = MAX_SUBRULES_PER_RULE;
    constexpr size_t MAX_JSON_CHARS_IN_ERROR = 512;
    constexpr size_t MAX_JSON_INPUT_SIZE = 1024 * 256;
    constexpr const char* tooComplexMessage =
            "Rule is too complex. This can happen if you have many ORs nested inside of ANDs. To fix this, split "
            "the rule into multiple rules, or rewrite it to reduce complexity.";


    struct BooleanNode::Private
    {
        static void toNnf(BooleanNode& node, bool negate);
        static void absorb(BooleanNode& node);
        static MintermSet distribute(BooleanNode& node, StringPool& context);
    };

    //==================================================================================================================
    // NNF - Push negations down to leaf nodes
    //==================================================================================================================

    void BooleanNode::Private::toNnf(BooleanNode& node, bool negate)
    {
        switch(node.kind)
        {
            case NodeKind::PHRASE:
            {
                node.negated = node.negated ^ negate;
                break;
            }

            case NodeKind::AND:
            case NodeKind::OR:
            {
                if(node.negated)
                {
                    node.kind = node.kind == NodeKind::AND ? NodeKind::OR : NodeKind::AND;
                    negate = !negate;
                    node.negated = false;
                }
                for(BooleanNode& child : node.children)
                    toNnf(child, negate);
                break;
            }

            default:
                xassert_false("Unknown node kind %d", (int) node.kind);
        }
    }

    //==================================================================================================================
    // Absorption -- pull ORs into parent ORs and ANDs into parent ANDs
    //==================================================================================================================

    // absorb children of the same kind (ORs into parent ORs, ANDs into parent ANDs, etc)
    void BooleanNode::Private::absorb(BooleanNode& node)
    {
        if(node.kind != NodeKind::AND && node.kind != NodeKind::OR)
            return;

        NodeKind otherKind = node.kind == NodeKind::AND ? NodeKind::OR : NodeKind::AND;
        size_t i = 0;
        while(i < node.children.size())
        {
            BooleanNode& child = node.children[i];
            if(child.kind == node.kind)
            {
                std::vector<BooleanNode> grandchildren = std::move(child.children);
                node.children.erase(node.children.begin() + i);
                for(BooleanNode& grandchild : grandchildren)
                    node.children.push_back(std::move(grandchild));
            }
            else if(child.kind == otherKind)
            {
                absorb(child);
                i++;
            }
            else
            {
                i++;
            }
        }
    }

    //==================================================================================================================
    // Interval merging -- note we do this during expansion so that we continually merge at every level and eliminate
    // unsatisfiables ASAP.
    //==================================================================================================================

    enum class MergeVarsetIntervalsResult { NONE, UNSATISFIABLE };
    MergeVarsetIntervalsResult mergeVarsetIntervals(FixedSizedBitset& ba, StringPool& context)
    {
        auto it1 = ba.begin();
        while(it1)
        {
            int idx1 = *it1;
            auto it2 = ++it1;
            if(it1) // don't do this on the last iteration
            {
                bool anyRemoved = false;
                PredicateInfo& pi1 = context.getById(idx1);
                if(pi1.type() == PredicateType::RANGE)
                {
                    IntervalPredicateInfo ii1 = pi1.getInterval();
                    Interval current = ii1.interval;
                    Interval old = ii1.interval;
                    int stringId = ii1.stringIndex;
                    for(; it2; ++it2)
                    {
                        int idx2 = *it2;
                        PredicateInfo& pi2 = context.getById(idx2);
                        if(pi2.type() == PredicateType::RANGE)
                        {
                            IntervalPredicateInfo ii2 = pi2.getInterval();
                            if(ii2.stringIndex == stringId)
                            {
                                current = Interval::mergeAnd(current, ii2.interval);
                                if(!current)
                                    return MergeVarsetIntervalsResult::UNSATISFIABLE;
                                ba.unset(idx2); // can always remove the others then replace the first if needed
                                anyRemoved = true;
                            }
                        }
                    }

                    if(current != old)
                    {
                        IntervalPredicateInfo newPi{current, stringId};
                        int newIdx = context.getOrAdd(PredicateInfo{newPi});
                        ba.unset(idx1);
                        ba.set(newIdx);
                        anyRemoved = true;
                    }
                }

                // we could have removed the next one we want to visit anyway
                if(anyRemoved && !ba.test(*it1))
                    ++it1;
            }
        }

        return MergeVarsetIntervalsResult::NONE;
    }

    MergeVarsetIntervalsResult mergeVarsetIntervals(Varset& varset, StringPool& context)
    {
        if(mergeVarsetIntervals(varset.positive, context) == MergeVarsetIntervalsResult::UNSATISFIABLE)
            return MergeVarsetIntervalsResult::UNSATISFIABLE;
        if(mergeVarsetIntervals(varset.negative, context) == MergeVarsetIntervalsResult::UNSATISFIABLE)
            varset.negative.clear();
        return MergeVarsetIntervalsResult::NONE;
    }

    //==================================================================================================================
    // Conversion to DNF (exponential algorithm since we generate the full cartesian product)
    //==================================================================================================================

    MintermSet BooleanNode::Private::distribute(BooleanNode& node, StringPool& context)
    {
        if(node.kind == NodeKind::PHRASE)
        {
            MintermSet sets;
            sets.insert(Varset(node.stringId, node.negated));
            return sets;
        }
        else if(node.kind == NodeKind::OR)
        {
            MintermSet sets;
            for(auto& child : node.children)
            {
                if(child.kind == NodeKind::AND)
                    sets.merge(distribute(child, context));
                else if(child.kind == NodeKind::PHRASE)
                    sets.insert(Varset(child.stringId, child.negated));
                else
                    xassert_false("Unexpected node type %d", (int) child.kind);
            }
            return sets;
        }
        else if(node.kind == NodeKind::AND)
        {
            std::vector<MintermSet> inputs;
            for(auto& child : node.children)
            {
                if(child.kind == NodeKind::OR)
                    inputs.push_back(distribute(child, context));
                else if(child.kind == NodeKind::PHRASE)
                {
                    MintermSet cset;
                    cset.insert(Varset(child.stringId, child.negated));
                    inputs.push_back(cset);
                }
                else
                    xassert_false("Unexpected node type %d", (int) child.kind);
            }

            MintermSet workingSet;
            while(!inputs.empty())
            {
                auto input = std::move(inputs.back());
                inputs.pop_back();
                if(workingSet.empty())
                    workingSet = std::move(input);
                else
                {
                    MintermSet nextSet;
                    for(auto& old : workingSet)
                    {
                        for(auto& otherProductSet : input)
                        {
                            Varset productSet = old;
                            productSet.merge(otherProductSet);
                            auto unsat = mergeVarsetIntervals(productSet, context);
                            if(unsat != MergeVarsetIntervalsResult::UNSATISFIABLE && !productSet.isAlwaysFalse())
                            {
                                if(nextSet.size() > MAX_VARSETS_PRE_SIMPLIFY)
                                    setmatchRaise(RULE_TOO_COMPLEX, "%s", tooComplexMessage);
                                nextSet.insert(productSet);
                            }
                        }
                    }
                    workingSet = std::move(nextSet);
                }
            }

            return workingSet;
        }
        else
            xassert_false("Unknown node type %d", (int) node.kind);
    }

    //==================================================================================================================
    // Delete any product terms covered by other product terms
    //==================================================================================================================

    static void minimize(MintermSet& sets, Expression& expr)
    {
        if(sets.size() > MAX_VARSETS_PRE_SIMPLIFY) // we check this some places while distributing, but not everywhere
            setmatchRaise(RULE_TOO_COMPLEX, "%s", tooComplexMessage);

        auto it1 = sets.begin(), ite = sets.end();
        while(it1 != ite)
        {
            auto itc = it1++; // iterator will be invalid if we remove an element, so must increment before remove
            auto v = const_cast<Varset&>(*itc);
            bool removeThis = false;
            for(auto it2 = sets.begin(); it2 != ite; ++it2)
            {
                if(it2 == itc)
                    continue;
                const Varset& v2 = *it2;
                if(v.isRedundantWith(v2))
                {
                    // TODO there's one additional lowish-hanging fruit: (A&B&C)|(A&B&~C) = (A&B)
                    // if we want to get more complex than that, consider espresso
                    removeThis = true;
                    break;
                }
            }

            if(removeThis)
                sets.erase(itc);
        }

        if(sets.size() > MAX_VARSETS_POST_SIMPLIFY)
            setmatchRaise(RULE_TOO_COMPLEX, "%s", tooComplexMessage);
    }

    //==================================================================================================================
    // Debugging
    //==================================================================================================================

    static void decodePhraseString(const std::string& in, std::ostringstream& ss)
    {
        const char* data = in.data();
        size_t termCount = *((uint16_t*) data);
        data += 2;
        if(termCount > 1)
            ss << '"';
        for(size_t i = 0; i < termCount; ++i)
        {
            if(i != 0)
                ss << " ";
            size_t termLength = *((uint16_t*) data);
            data += 2;
            ss << cleanUtf8(data, termLength);
            data += termLength;
        }
        if(termCount > 1)
            ss << '"';
    }

    static void varToString(PredicateInfo& pi, StringPool& strings, std::ostringstream& ss)
    {
        if(pi.type() == PredicateType::PHRASE)
        {
            decodePhraseString(pi.getString(), ss);
        }
        else
        {
            IntervalPredicateInfo ii = pi.getInterval();
            PredicateInfo& pi2 = strings.getById((int) ii.stringIndex);
            xassert(pi2.type() == PredicateType::PHRASE, "intervals should point to phrases");
            decodePhraseString(pi2.getString(), ss);
            ss << "[" << ii.interval.getLeft() << "," << ii.interval.getRight() << "]";
        }
    }

    void Expression::varsetToString(Varset& varset, std::ostringstream& ss)
    {
        bool first = true;
        for(int index : varset.positive)
        {
            if(!first) ss << "&";
            first = false;
            varToString(strings.getById(index), strings, ss);
        }
        for(int index : varset.negative)
        {
            if(!first) ss << "&";
            first = false;
            ss << "~";
            varToString(strings.getById(index), strings, ss);
        }
    }

    std::string Expression::dnfToString(MintermSet& dnf)
    {
        std::ostringstream ss;
        bool first = true;
        for(auto& cvarset : dnf)
        {
            auto varset = const_cast<Varset&>(cvarset);
            if(!first) ss << "|";
            first = false;
            ss << "(";
            varsetToString(varset, ss);
            ss << ")";
        }
        return ss.str();
    }

    //==================================================================================================================
    // API for this whole mess -- callers don't need to know anything about the boolean tree at all, just that JSON
    // goes in and finished rule comes out
    //==================================================================================================================

    MintermSet Expression::toDnf()
    {
        BooleanNode::Private::toNnf(root, false);
        BooleanNode::Private::absorb(root);
        MintermSet sets = BooleanNode::Private::distribute(root, strings);
        minimize(sets, *this);
        return sets;
    }

    // the parse and encoding logic are elsewhere, so this brings them together
    UBuffer encodeRule(MintermSet& varsets, StringPool& strings);
    Expression ruleJsonToBooleanExpr(const char* ruleJson_utf8_nt);
    UBuffer compile(const char* ruleJson_utf8_nt)
    {
        size_t jsonLength = strlen(ruleJson_utf8_nt);
        try
        {
            Expression expr = ruleJsonToBooleanExpr(ruleJson_utf8_nt);
            MintermSet sets = expr.toDnf();
            return encodeRule(sets, expr.strings);
        }
        catch(SetmatchException& ex)
        {
            throw SetmatchException(ex.ec, setmatchFormat("%s\nWhen parsing:\n    %s", ex.what(),
                    (std::string_view{ruleJson_utf8_nt, std::min(jsonLength, MAX_JSON_CHARS_IN_ERROR)})));
        }
    }
}
