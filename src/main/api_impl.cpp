/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common/common.hpp"
#include "common/sync.hpp"
#include "index/sync_db.hpp"
#include <cstdio>
#include <boost/core/demangle.hpp>
#include <sstream>

#if defined(__MINGW64__)
 #define SETMATCH_IMPORT __attribute__((visibility("default"))) __declspec(dllexport)
#else
 #define SETMATCH_IMPORT __attribute__((visibility("default")))
#endif
#include "../public/setmatch.h"

#define BEGIN_TRY_CATCH try {
#define END_TRY_CATCH } \
    catch(SetmatchException& ex) { return onSetmatchException(ex); } \
    catch(std::exception& ex) { return onOtherException(ex); } \
    return nullptr;
#define NOT_NULL(X) if(UNLIKELY(!(X))) { return "3011: " #X " cannot be null"; } // 3011 is error code MISSING_OR_INVALID_PARAMETER; hardcoding it here to allow these strings to be constant
#define NOT_ZERO(X) if(UNLIKELY(!(X))) { return "3011: " #X " cannot be zero"; }

namespace burningmime::setmatch
{
    // don't import all of boolean.hpp
    UBuffer compile(const char* ruleJson_utf8_nt);

    // store a thread-local exception buffer; clear it before each call
    static ThreadLocal<std::string> tlExceptionBuf;

    NEVER_INLINE static const char* onOtherException(const std::exception& ex)
    {
        std::ostringstream ss;
        ss << "1000: Unexpected exception from external component:\n";
        ss << "    " << boost::core::demangle(typeid(ex).name()) << '\n';
        ss << "    " << ex.what();
        auto& exceptionBuf = *tlExceptionBuf;
        exceptionBuf = ss.str();
        return exceptionBuf.data();
    }

    NEVER_INLINE static const char* onSetmatchException(const SetmatchException& ex)
    {
        auto& exceptionBuf = *tlExceptionBuf;
        exceptionBuf = ex.what();
        return exceptionBuf.data();
    }

    static void defaultLogCallback(::setmatch_LogLevel level, const char* message)
    {
        const char* levelStr = level == ::setmatch_LogLevel_INFO ? "[INFO] " : level == ::setmatch_LogLevel_WARNING ? "[WARNING] " : "[ERROR] ";
        auto stream = level == ::setmatch_LogLevel_ERROR ? stderr : stdout;
        fprintf(stream, "%s%s\n", levelStr, message);
        fflush(stream);
    }

    static ::setmatch_LogCallback logCallback = &defaultLogCallback;
    void logImpl(LogLevel level, const std::string& str)
    {
        logCallback((::setmatch_LogLevel) level, str.c_str());
    }
}

using namespace burningmime::setmatch;

extern "C"
{
    SETMATCH_IMPORT void setmatch_setLogCallback(setmatch_LogCallback logCallback)
    {
        burningmime::setmatch::logCallback = logCallback ? logCallback : &defaultLogCallback;
    }

    SETMATCH_IMPORT const char* setmatch_compileRuleJson(const char* ruleJson_utf8_nt, setmatch_CompiledRule* out_result)
    {
        NOT_NULL(ruleJson_utf8_nt)
        NOT_NULL(out_result)
        BEGIN_TRY_CATCH
            out_result->data = nullptr;
            out_result->length = 0; // in case of exception
            UBuffer buf = compile(ruleJson_utf8_nt);
            buf.claim(&(out_result->data), &(out_result->length));
        END_TRY_CATCH
    }

    SETMATCH_IMPORT void setmatch_deleteCompiledRule(setmatch_CompiledRule rule)
    {
        if(rule.data)
            smFree(rule.data);
    }

    SETMATCH_IMPORT const char* setmatch_newRuleDB(const char* configJson_utf8_nt, setmatch_RuleDB** out_result)
    {
        NOT_NULL(out_result)
        BEGIN_TRY_CATCH
            *out_result = nullptr; // in case of exception
            *out_result = (setmatch_RuleDB*) (void*) new SRuleDB(DBConfig::forJson(configJson_utf8_nt));
        END_TRY_CATCH
    }

    SETMATCH_IMPORT const char* setmatch_addRule(setmatch_RuleDB* db, int64_t id, setmatch_CompiledRule compiledRule, int* out_replaced)
    {
        NOT_NULL(db)
        NOT_NULL(compiledRule.data)
        NOT_ZERO(compiledRule.length)
        BEGIN_TRY_CATCH
            if(out_replaced) *out_replaced = 0; // in case of exception
            bool replaced = ((SRuleDB*) (void*) db)->add(id, compiledRule.data, compiledRule.length);
            if(out_replaced) *out_replaced = replaced ? 1 : 0;
        END_TRY_CATCH
    }

    SETMATCH_IMPORT const char* setmatch_addRuleJson(setmatch_RuleDB* db, int64_t id, const char* ruleJson_utf8_nt, int* out_replaced)
    {
        NOT_NULL(db)
        NOT_NULL(ruleJson_utf8_nt)
        BEGIN_TRY_CATCH
            if(out_replaced) *out_replaced = 0; // in case of exception
            UBuffer buf = compile(ruleJson_utf8_nt);
            bool replaced = ((SRuleDB*) (void*) db)->add(id, buf.data(), buf.length());
            if(out_replaced) *out_replaced = replaced ? 1 : 0;
        END_TRY_CATCH
    }

    SETMATCH_IMPORT const char* setmatch_removeRule(setmatch_RuleDB* db, int64_t id, int* out_removed)
    {
        NOT_NULL(db)
        BEGIN_TRY_CATCH
            if(out_removed) *out_removed = 0; // in case of exception
            bool removed = ((SRuleDB*) (void*) db)->remove(id);
            if(out_removed) *out_removed = removed ? 1 : 0;
        END_TRY_CATCH
    }

    SETMATCH_IMPORT const char* setmatch_match(setmatch_RuleDB* db, setmatch_TokenIterator tokenIterator, void* tokenIteratorState, setmatch_MatchCollection* out_result)
    {
        NOT_NULL(db)
        NOT_NULL(tokenIterator)
        NOT_NULL(out_result)
        BEGIN_TRY_CATCH
            out_result->matchedIds = nullptr;
            out_result->count = 0; // in case of exception
            SimpleStringIterator iterator{(CSimpleStringIterator) tokenIterator, tokenIteratorState};
            std::vector<int64_t>* result = ((SRuleDB*) (void*) db)->match(iterator);
            out_result->matchedIds = result->data();
            out_result->count = result->size();
        END_TRY_CATCH
    }

    SETMATCH_IMPORT const char* setmatch_matchArrayN(setmatch_RuleDB* db, const char** tokens, size_t tokens_count, setmatch_MatchCollection* out_result)
    {
        NOT_NULL(tokens)
        size_t i = 0;
        FSimpleStringIterator it = [&i, tokens, tokens_count]() -> SimpleString
        {
            while(true)
            {
                if(i >= tokens_count)
                    return {nullptr, 0};
                const char* str = tokens[i++];
                size_t length;
                if(str && (length = strlen(str)) > 0)
                    return {str, length};
            }
        };
        auto iterFunc = (setmatch_TokenIterator) &decltype(it)::callbackImpl;
        void* iterState = &it.state;
        return setmatch_match(db, iterFunc, iterState, out_result);
    }

    SETMATCH_IMPORT const char* setmatch_debugGetRuleAsString(setmatch_RuleDB* db, int64_t id, char* buf, size_t buf_lengthBytes, int* out_found)
    {
        NOT_NULL(db)
        NOT_NULL(buf)
        NOT_ZERO(buf_lengthBytes)
        BEGIN_TRY_CATCH
            if(out_found) *out_found = 0; // in case of exception
            auto pair = ((SRuleDB*) (void*) db)->debugGetRuleString(id);
            if(pair.second.length() + 1 < buf_lengthBytes)
            {
                // +1 for null terminator
                memcpy(buf, pair.second.c_str(), pair.second.length() + 1);
            }
            else
            {
                memcpy(buf, pair.second.c_str(), buf_lengthBytes - 1);
                buf[buf_lengthBytes - 1] = '\0';
            }
            if(out_found)
                *out_found = pair.first ? 1 : 0;
        END_TRY_CATCH
    }

    SETMATCH_IMPORT const char* setmatch_containsRule(setmatch_RuleDB* db, int64_t id, int* out_found)
    {
        NOT_NULL(db)
        NOT_NULL(out_found)
        BEGIN_TRY_CATCH
            *out_found = 0; // in case of exception
            *out_found = ((SRuleDB*) (void*) db)->contains(id) ? 1 : 0;
        END_TRY_CATCH
    }

    SETMATCH_IMPORT const char* setmatch_pollMetrics(setmatch_RuleDB* db, setmatch_Metrics* out_metrics)
    {
        NOT_NULL(db)
        NOT_NULL(out_metrics)
        BEGIN_TRY_CATCH
            out_metrics->metrics = nullptr;
            out_metrics->count = 0; // in case of exception
            auto& src = ((SRuleDB*) (void*) db)->getMetrics();
            out_metrics->metrics = (setmatch_Metric*) src.data();
            out_metrics->count = src.size();
        END_TRY_CATCH
    }

    SETMATCH_IMPORT void setmatch_deleteRuleDB(setmatch_RuleDB* db)
    {
        if(db)
        {
            try
            {
                delete (((SRuleDB*) (void*) db));
            }
            catch(std::exception& ex)
            {
                // can't throw from destructor
                logError("Exception in destructor: %s", ex.what());
            }
        }
    }
}
