/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

//======================================================================================================================
//                                         SURGEON GENERAL'S WARNING
//
// This is very old code from when I was first learning C++. It is written in a very C-like style. It doesn't help that
// it deals with all sorts of strange alignment and bitset hacks, some of which are required for AVX, and some of which
// are good old premature micro-optimizations. Don't read this code.
//======================================================================================================================

#include "pred_list.hpp"
#include <algorithm>

namespace burningmime::setmatch
{
    //==================================================================================================================
    // Small lists are stored as actual lists, big lists are funky pseudo-bitarrays with strange compression and
    // comparison semantics.
    //==================================================================================================================

    constexpr int PL_SMALL_SIZE = 15;
    constexpr int PL_SMALL_ALIGN = 16;
    ALWAYS_INLINE inline static bool isSmallList(const void* inPtr, int*& smallPtr, int& smallSize)
    {
        smallPtr = ASSUME_ALIGNED((int*) (((size_t) inPtr) & ~((size_t) PL_SMALL_SIZE)), PL_SMALL_ALIGN);
        smallSize = (int) ((ptrdiff_t) inPtr - (ptrdiff_t) smallPtr);
        return smallSize != 0;
    }

    //==================================================================================================================
    // Building from index list
    //==================================================================================================================

    void predListBuild(int blockSize, Blockset& blockset, void*& blockData, int nPhrases, int* phraseIndices, bool isSorted)
    {
        xassert(nPhrases > 0, "nPhrases = %d", nPhrases);
        if(blockData)
            predListDestruct(blockData);
        if(!isSorted)
            std::sort(phraseIndices, phraseIndices + nPhrases);

        // first pass -- create blockset & count how many blocks we need
        blockset[0] = 0;
        blockset[1] = 0;
        blockset[2] = 0;
        blockset[3] = 0;
        int nBlocks = 0;
        for(int i : iterate(phraseIndices, nPhrases))
        {
            int block = i / blockSize;
            xassert(block < 256, "block = %d", block);
            int nWord = block / 64;
            int64_t old = blockset[nWord];
            int64_t _new = old | sbit(block % 64);
            if(_new != old)
            {
                blockset[nWord] = _new;
                nBlocks++;
            }
        }

        xassert(nBlocks > 0, "Didn't get any blocks");
        if(blockSize == 1)
            return;

        if(nPhrases <= PL_SMALL_SIZE)
        {
            int* b = (int*) alignedAlloc(PL_SMALL_ALIGN, sizeof(int) * nPhrases);
            for(int i = 0; i < nPhrases; ++i)
                b[i] = phraseIndices[i];
            blockData = (void*) (((size_t) b) | ((size_t) nPhrases));
        }
        else
        {
            // allocate block storage
            void* b;
            size_t btSize = (size_t) (nBlocks * blockSize / 8);
            btSize = (btSize + 31) & ~((size_t) 31); // always allocate a multiple of 32 for alignment reasons
            b = alignedAlloc(32, btSize);
            memset(b, 0, btSize);
            blockData = b;

            // now loop through the phrases again and set those bits in the blocks
            int blockGlobal = -1, blockLocal = -1;
            switch(blockSize)
            {
                #define CASE_BLOCK_SMALL(X) \
                    case X: \
                        for(int i : iterate(phraseIndices, nPhrases)) \
                        { \
                            int block = i / X; \
                            if(block > blockGlobal) \
                            { \
                                blockGlobal = block; \
                                blockLocal++; \
                            }\
                            ((BitBlock##X*) b)[blockLocal] |= (BitBlock##X) (((BitBlock##X) 1) << (i % X)); \
                        } \
                        break;
                CASE_BLOCK_SMALL(8)
                CASE_BLOCK_SMALL(16)
                CASE_BLOCK_SMALL(32)
                CASE_BLOCK_SMALL(64)
                #undef CASE_BLOCK_SMALL

                default:
                    xassert(blockSize % 64 == 0, "blockSize = %d", blockSize);
                    for(int i : iterate(phraseIndices, nPhrases))
                    {
                        int block = i / blockSize;
                        if(block > blockGlobal)
                        {
                            blockGlobal = block;
                            blockLocal++;
                        }
                        int subBlock = (blockLocal * blockSize/64) + (i % blockSize)/64;
                        ((int64_t*) b)[subBlock] |= sbit(i % 64);
                    }
                    break;
            }
        }
    }

    //==================================================================================================================
    // Iteration
    //==================================================================================================================

    constexpr int PL_IT_MODE_FINISHED = -1;
    constexpr int PL_IT_MODE_SMALL = 1;
    constexpr int PL_IT_MODE_BIG = 2;

    void PredicateIterator::first(int64_t* blockset, void* blockData)
    {
        int smallSize, *small;
        if(isSmallList(blockData, small, smallSize))
        {
            mode = PL_IT_MODE_SMALL;
            sm.values = (int*) small;
            sm.index = 0;
            sm.size = smallSize;
            value = *small;
        }
        else
        {
            bg.blockset = blockset;
            bg.blockData = blockData;
            bg.globalBlock = bitscanFirst64bs(blockset);
            if(bg.globalBlock == -1)
            {
                mode = PL_IT_MODE_FINISHED;
                bg.localBlock = -1;
                bg.innerIndex = -1;
                value = -1;
            }
            else if(blockSize == 1)
            {
                mode = PL_IT_MODE_BIG;
                bg.localBlock = -1;
                bg.innerIndex = -1;
                value = bg.globalBlock;
            }
            else
            {
                mode = PL_IT_MODE_BIG;
                dassert(blockData != nullptr, "No block data");
                dassert(blockSize == 8 || blockSize == 16 || blockSize == 32 || blockSize % 64 == 0, "Invalid block size %d", blockSize);
                switch(blockSize)
                {
                    #define CASE_BLOCK_SMALL(X) case X: bg.innerIndex = bitscanFirst##X(((BitBlock##X*) blockData)[0]); break;
                    CASE_BLOCK_SMALL(8)
                    CASE_BLOCK_SMALL(16)
                    CASE_BLOCK_SMALL(32)
                    CASE_BLOCK_SMALL(64)
                    #undef CASE_BLOCK_SMALL

                    default:
                        bg.innerIndex = bitscanFirst64n((int64_t*) blockData, blockSize / 64);
                        break;
                }
                dassert(bg.innerIndex != -1, "block %d was empty", bg.globalBlock);
                bg.localBlock = 0;
                value = bg.globalBlock * blockSize + bg.innerIndex;
            }
        }
    }

    void PredicateIterator::next()
    {
        if(mode == PL_IT_MODE_SMALL)
        {
            int i = ++sm.index;
            if(i >= sm.size)
            {
                mode = PL_IT_MODE_FINISHED;
                value = -1;
            }
            else
            {
                value = sm.values[i];
            }
        }
        else if(mode == PL_IT_MODE_BIG)
        {
            if(blockSize == 1)
            {
                if(bg.globalBlock != -1)
                    bg.globalBlock = bitscanNext64bs(bg.blockset, bg.globalBlock);
                value = bg.globalBlock;
            }
            else
            {
                dassert(bg.globalBlock >= -1 && bg.localBlock >= -1 && bg.innerIndex >= 0,
                        "invalid iterator state (globalBlock = %d, localBlock = %d, innerIndex = %d)", bg.globalBlock, bg.localBlock, bg.innerIndex);
                if(bg.innerIndex >= blockSize - 1) // we can skip checking our current block
                {
                    goto LNextBlock;
                }
                switch(blockSize)
                {
                    #define CASE_BLOCK_SMALL(X) case X: bg.innerIndex = bitscanNext##X(((BitBlock##X*) bg.blockData)[bg.localBlock], bg.innerIndex % X); break;
                    CASE_BLOCK_SMALL(8)
                    CASE_BLOCK_SMALL(16)
                    CASE_BLOCK_SMALL(32)
                    CASE_BLOCK_SMALL(64)
                    #undef CASE_BLOCK_SMALL
                    default:
                        bg.innerIndex = bitscanNext64n(((int64_t*) bg.blockData) + (bg.localBlock * blockSize / 64), blockSize / 64, bg.innerIndex);
                        break;
                }
                if(bg.innerIndex != -1)
                    goto LEnd;
            LNextBlock:
                bg.globalBlock = bitscanNext64bs(bg.blockset, bg.globalBlock);
                if(bg.globalBlock == -1)
                {
                    mode = PL_IT_MODE_FINISHED;
                    value = -1;
                    return;
                }
                bg.localBlock++;
                switch(blockSize)
                {
                    #define CASE_BLOCK_SMALL(X) case X: bg.innerIndex = bitscanFirst##X(((BitBlock##X*) bg.blockData)[bg.localBlock]); break;
                    CASE_BLOCK_SMALL(8)
                    CASE_BLOCK_SMALL(16)
                    CASE_BLOCK_SMALL(32)
                    CASE_BLOCK_SMALL(64)
                    #undef CASE_BLOCK_SMALL
                    default:
                    {
                        int64_t* firstWord = ((int64_t*) bg.blockData) + (bg.localBlock * blockSize / 64);
                        bg.innerIndex = bitscanFirst64n(firstWord, blockSize / 64);
                        break;
                    }
                }
                dassert(bg.innerIndex != -1, "we shouldn't ever have an empty block");
            LEnd:
                value = bg.globalBlock * blockSize + bg.innerIndex;
            }
        }
    }

    //==================================================================================================================
    // Comparison & hashing
    //==================================================================================================================

    int blockCount(const Blockset& blockset)
    {
        return __builtin_popcountll(blockset[0]) +
               __builtin_popcountll(blockset[1]) +
               __builtin_popcountll(blockset[2]) +
               __builtin_popcountll(blockset[3]);
    }

    bool predListEquals(int blockSize, const Blockset& blockset1, const void* blockData1, const Blockset& blockset2, const void* blockData2)
    {
        if(blockset1[0] != blockset2[0] || blockset1[1] != blockset2[1] || blockset1[2] != blockset2[2] || blockset1[3] != blockset2[3])
            return false;
        if(blockData1)
        {
            dassert(blockData2 != nullptr, "Either both should be null or neither should be");
            int smallSize1, *small1, smallSize2, *small2;
            bool isSmall1 = isSmallList(blockData1, small1, smallSize1);
            bool isSmall2 = isSmallList(blockData2, small2, smallSize2);
            if(isSmall1)
                return smallSize1 == smallSize2 && !memcmp(small1, small2, sizeof(int) * smallSize1);
            else if(isSmall2)
                return false;
            else
            {
                int count = blockCount(blockset1);
                dassert(count > 0, "Should never have an empty predicate list here");
                size_t nBytes = (size_t) count * blockSize/8;
                return !memcmp(blockData1, blockData2, nBytes);
            }
        }
        else
        {
            dassert(blockSize == 1, "should only have null data when block size is 1");
            return true;
        }
    }

    size_t predListHash(int blockSize, const Blockset& blockset, const void* blockData)
    {
        if(blockData != nullptr)
        {
            int *small, smallSize;
            if(isSmallList(blockData, small, smallSize))
            {
                return hashBytes(small, sizeof(int) * smallSize);
            }
            else
            {
                int count = blockCount(blockset);
                dassert(count > 0, "Should never have an empty predicate list here");
                size_t nBytes = (size_t) count * blockSize/8;
                return (hashBytes(blockset, 32) * 1099511628211ULL) ^ hashBytes(blockData, nBytes);
            }
        }
        else
        {
            dassert(blockSize == 1, "should only have null data when block size is 1");
            return hashBytes(blockset, 32);
        }
    }

    //==================================================================================================================
    // Build blockset from big bitset
    //==================================================================================================================

    void blocksetFromBitset(int blockSize, BitArray& bitset, BitBlock256& result)
    {
        int64_t resultWords[4] = {0};
        for(int bit : bitset)
        {
            int block = bit / blockSize;
            dassert(block < 256, "block = %d", block);
            resultWords[block / 64] |= sbit(block % 64);
        }
        result = loadUnaligned256(resultWords);
    }

    //==================================================================================================================
    // Full/slow match implementation
    //==================================================================================================================

    bool fullMatch(int blockSize, const Blockset& blockset, const void* blockData, const BitArray& bitset)
    {
        dassert(blockSize != 0 && blockData != nullptr, "Invalid phrase list for full match");

        int *small, smallSize;
        if(isSmallList(blockData, small, smallSize))
        {
            for(int i = 0; i < smallSize; ++i)
                if(!bitset.test(small[i]))
                    return false;
            return true;
        }
        else
        {
            // ok, that passed, now we have to check each block
            // note this has a bunch of duplicated code because i want the switch statement outside the loop
            // while i'd hope the compiler would be smart enough, i don't want to take the risk here since this is
            // basically *the* hot loop
            int globalBlock = bitscanFirst64bs(blockset);
            int localBlock = 0;
            const void* __restrict__ theirData = ASSUME_ALIGNED(bitset.data(), 32);
            const void* __restrict__ myData = ASSUME_ALIGNED(blockData, 32);
            switch(blockSize)
            {
                #define CASE_BLOCK_SMALL(X) \
                case X: \
                    while(globalBlock != -1) \
                    { \
                        BitBlock##X theirs = ((BitBlock##X*) theirData)[globalBlock]; \
                        BitBlock##X mine = ((BitBlock##X*) myData)[localBlock]; \
                        if(!matchBlockSmall<BitBlock##X>(mine, theirs)) \
                            return false; \
                        globalBlock = bitscanNext64bs(blockset, globalBlock); \
                        localBlock++; \
                    } \
                    break;
                CASE_BLOCK_SMALL(8)
                CASE_BLOCK_SMALL(16)
                CASE_BLOCK_SMALL(32)
                CASE_BLOCK_SMALL(64)
                #undef CASE_BLOCK_SMALL

                // larger: we can use vectorized loops. do the first few ddqwords (qqwords?) using 256-bit vectors, and then
                // the final word will be done differently depending on how much is left over
                default:
                    switch(blockSize % 256)
                    {
                        case 0:
                            while(globalBlock != -1)
                            {
                                // can use aligned loads and don't need to worry about masking
                                int globalBase = (blockSize / 8) * globalBlock;
                                int localBase = (blockSize / 8) * localBlock;
                                for(int i = 0; i < blockSize / 256; i++)
                                {
                                    char* pMine = (char*) myData + localBase + i*32;
                                    char* pTheirs = (char*) theirData + globalBase + i*32;
                                    if(!matchBlock256<true, true>(pMine, pTheirs))
                                        return false;
                                }
                                globalBlock = bitscanNext64bs(blockset, globalBlock);
                                localBlock++;
                            }
                            break;

                        // otherwise, we need to use unaligned and treat the last block separately
                        #define FIRST_BLOCKS_UNALIGNED_LOOP \
                            int i = 0; \
                            int globalBase = (blockSize / 8) * globalBlock; \
                            int localBase = (blockSize / 8) * localBlock; \
                            for(; i < blockSize / 256; i++) \
                            { \
                                char* pMine = (char*) myData + localBase + i*32; \
                                char* pTheirs = (char*) theirData + globalBase + i*32; \
                                if(!matchBlock256<false, false>(pMine, pTheirs)) \
                                    return false; \
                            }

                        case 64:
                            while(globalBlock != -1)
                            {
                                FIRST_BLOCKS_UNALIGNED_LOOP
                                int64_t qwMine = *((int64_t*) ((char*) myData + localBase + i*32));
                                int64_t qwTheirs = *((int64_t*) ((char*) theirData + globalBase + i*32));
                                if(!matchBlockSmall<int64_t>(qwMine, qwTheirs))
                                    return false;
                                globalBlock = bitscanNext64bs(blockset, globalBlock);
                                localBlock++;
                            }
                            break;

                        case 128:
                            while(globalBlock != -1)
                            {
                                FIRST_BLOCKS_UNALIGNED_LOOP
                                char* pMine = (char*) myData + localBase + i*32;
                                char* pTheirs = (char*) theirData + globalBase + i*32;
                                if(!matchBlock128<true, true>(pMine, pTheirs)) // incidentally, these two will be aligned on a 16-byte boundary, just not on 32
                                    return false;
                                globalBlock = bitscanNext64bs(blockset, globalBlock);
                                localBlock++;
                            }
                            break;

                        case 192:
                            while(globalBlock != -1)
                            {
                                FIRST_BLOCKS_UNALIGNED_LOOP
                                char* pMine = (char*) myData + localBase + i*32;
                                char* pTheirs = (char*) theirData + globalBase + i*32;
                                // need to do this in 2 steps to avoid accessing invalid memory
                                if(!matchBlock128<false, false>(pMine, pTheirs))
                                    return false;
                                int64_t qwMine = *((int64_t*) (pMine + 16));
                                int64_t qwTheirs = *((int64_t*) (pTheirs + 16));
                                if(!matchBlockSmall<int64_t>(qwMine, qwTheirs))
                                    return false;
                                globalBlock = bitscanNext64bs(blockset, globalBlock);
                                localBlock++;
                            }
                            break;

                        default:
                            dassert(false, "blockSize was not a multiple of 64"); return false;
                        #undef FIRST_BLOCKS_UNALIGNED_LOOP
                    }
                    break;
            }
            return true;
        }

    }

    //==================================================================================================================
    // Destructor
    //==================================================================================================================

    void predListFree(Blockset& blockset, void*& blockData)
    {
        blockset[0] = 0;
        blockset[1] = 0;
        blockset[2] = 0;
        blockset[3] = 0;
        if(blockData)
        {
            predListDestruct(blockData);
            blockData = nullptr;
        }
    }

    void predListDestruct(void* blockData)
    {
        alignedFree((void*) (((size_t) blockData) & ~((size_t) PL_SMALL_SIZE)));
    }
}
