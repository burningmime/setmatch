/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "subrule.hpp"

namespace burningmime::setmatch
{
    enum SubruleFlags
    {
        HAS_POSITIVE = 1,
        HAS_NEGATIVE = 2,
    };

    //==================================================================================================================
    // Forward declaration of private functions
    //==================================================================================================================

    struct SubruleList::Private
    {
        ALWAYS_INLINE inline static bool hasPositive(const Subrule& sr) { return (sr.flags & HAS_POSITIVE) != 0; }
        ALWAYS_INLINE inline static bool hasNegative(const Subrule& sr) { return (sr.flags & HAS_NEGATIVE) != 0; }
    };

    //==================================================================================================================
    // Subrule is initially built on the stack, then hashed and compared to see if we have an existing subrule. If
    // we do, then we retrun that. That's why all this crab out IndexedRefs and StagingSubrules is needed in the first
    // place. So all this is to support the function getOrAdd().
    //==================================================================================================================

    struct StagingSubrule
    {
        int blockSize;
        int flags;
        Blockset blocksetPos;
        Blockset blocksetNeg;
        void* blockDataPos = nullptr;
        void* blockDataNeg = nullptr;

        StagingSubrule(int _blockSize, std::vector<int>& pos, std::vector<int>& neg)
                : blockSize(_blockSize)
        {
            xassert(!pos.empty() || !neg.empty(), "Need something");
            flags = 0;
            if(!pos.empty())
            {
                predListBuild(blockSize, blocksetPos, blockDataPos, (int) pos.size(), pos.data(), false);
                dassert(_blockSize == 1 || blockDataPos, "should have built something");
                flags |= HAS_POSITIVE;
            }
            else
            {
                blockDataPos = nullptr;
            }

            if(!neg.empty())
            {
                predListBuild(blockSize, blocksetNeg, blockDataNeg, (int) neg.size(), neg.data(), false);
                dassert(_blockSize == 1 || blockDataNeg, "should have built something");
                flags |= HAS_NEGATIVE;
            }
            else
            {
                blockDataNeg = nullptr;
            }
        }

        ~StagingSubrule()
        {
            // if this data was never used (ie we found a duplicate rule), then free it
            if(blockDataPos)
                predListDestruct(blockDataPos);
            if(blockDataNeg)
                predListDestruct(blockDataNeg);
        }
    };

    struct SubruleList::IndexedRefPrivate
    {
        ALWAYS_INLINE static inline int getBlockSize(const IndexedRef& r) { return r.index < 0 ? ((StagingSubrule*) r.ptr)->blockSize : ((SubruleList*) r.ptr)->blockSize; }
        ALWAYS_INLINE static inline Blockset& getBlocksetPos(const IndexedRef& r) { return r.index < 0 ? ((StagingSubrule*) r.ptr)->blocksetPos : ((SubruleList*) r.ptr)->blocksetsPos[r.index]; }
        ALWAYS_INLINE static inline Blockset& getBlocksetNeg(const IndexedRef& r) { return r.index < 0 ? ((StagingSubrule*) r.ptr)->blocksetNeg : ((SubruleList*) r.ptr)->blocksetsNeg[r.index]; }
        ALWAYS_INLINE static inline void* getBlockDataPos(const IndexedRef& r) { return r.index < 0 ? ((StagingSubrule*) r.ptr)->blockDataPos : ((SubruleList*) r.ptr)->subrulesByIndex[r.index].blockDataPos; }
        ALWAYS_INLINE static inline void* getBlockDataNeg(const IndexedRef& r) { return r.index < 0 ? ((StagingSubrule*) r.ptr)->blockDataNeg : ((SubruleList*) r.ptr)->subrulesByIndex[r.index].blockDataNeg; }
        ALWAYS_INLINE static inline int getFlags (const IndexedRef& r) { return r.index < 0 ? ((StagingSubrule*) r.ptr)->flags : ((SubruleList*) r.ptr)->subrulesByIndex[r.index].flags; }
        ALWAYS_INLINE static inline bool hasPositive(const IndexedRef& r) { return (getFlags(r) & HAS_POSITIVE) != 0; }
        ALWAYS_INLINE static inline bool hasNegative(const IndexedRef& r) { return (getFlags(r) & HAS_NEGATIVE) != 0; }
    };

    bool SubruleList::IndexedRef::operator==(const SubruleList::IndexedRef& other) const
    {
        int blockSize = IndexedRefPrivate::getBlockSize(*this);
        return blockSize == IndexedRefPrivate::getBlockSize(other) &&
               IndexedRefPrivate::getFlags(*this) == IndexedRefPrivate::getFlags(other) &&
               (!IndexedRefPrivate::hasPositive(*this) || predListEquals(blockSize, IndexedRefPrivate::getBlocksetPos(*this), IndexedRefPrivate::getBlockDataPos(*this), IndexedRefPrivate::getBlocksetPos(other), IndexedRefPrivate::getBlockDataPos(other))) &&
               (!IndexedRefPrivate::hasNegative(*this) || predListEquals(blockSize, IndexedRefPrivate::getBlocksetNeg(*this), IndexedRefPrivate::getBlockDataNeg(*this), IndexedRefPrivate::getBlocksetNeg(other), IndexedRefPrivate::getBlockDataNeg(other)));
    }

    size_t SubruleList::IndexedRefHash::operator()(const SubruleList::IndexedRef& r) const
    {
        int blockSize = IndexedRefPrivate::getBlockSize(r);
        size_t hashPos = IndexedRefPrivate::hasPositive(r) ? predListHash(blockSize, IndexedRefPrivate::getBlocksetPos(r), IndexedRefPrivate::getBlockDataPos(r)) : 0;
        size_t hashNeg = IndexedRefPrivate::hasNegative(r) ? predListHash(blockSize, IndexedRefPrivate::getBlocksetNeg(r), IndexedRefPrivate::getBlockDataNeg(r)) : 0;
        return (hashPos * 1099511628211ULL) ^ hashNeg;
    }

    Subrule& SubruleList::getOrAdd(std::vector<int>& pos, std::vector<int>& neg)
    {
        StagingSubrule staged = StagingSubrule(blockSize, pos, neg);
        auto it = subrulesByHash.find(IndexedRef{&staged, -1});
        if(it == subrulesByHash.end())
        {
            auto birth = freeList.acquire();
            int index = birth.index;
            Subrule* subrule;
            if(birth.isNew)
            {
                subrulesByIndex.push_back(Subrule(index));
                subrule = &subrulesByIndex[index];
                blocksetsPos.push_back(staged.blocksetPos);
                blocksetsNeg.push_back(staged.blocksetNeg);
            }
            else
            {
                subrule = &subrulesByIndex[index];
                xassert(!subrule->isAlive(), "Supposably tombstoned subrule at index %d was still alive", index);
                blocksetsPos[index] = staged.blocksetPos;
                blocksetsNeg[index] = staged.blocksetNeg;
            }
            subrule->flags = staged.flags;
            subrule->blockDataPos = staged.blockDataPos;
            staged.blockDataPos = nullptr; // set this null so it's not freed
            subrule->blockDataNeg = staged.blockDataNeg;
            staged.blockDataNeg = nullptr;
            subrulesByHash.insert(IndexedRef{this, index});
            vacuous.resize(subrulesByIndex.size());
            if(pos.empty())
            {
                vacuous.set(index);
                ++vacuousCount;
            }
            averageBlockCountCached = -1;
            return *subrule;
        }
        else
        {
            Subrule& result = subrulesByIndex[it->index];
            xassert(it->index >= 0 && it->index < (int) subrulesByIndex.size(), "Hash gave invalid index %d (size = %d)", it->index, (int) subrulesByIndex.size());
            xassert(result.isAlive(), "hash pointed to a dead thing at index %d", it->index);
            return result;
        }
    }

    //==================================================================================================================
    // Resizing blocks requires we rebuild every list then rehash
    //==================================================================================================================

    void SubruleList::changeBlockSize(int newBlockSize)
    {
        int oldBlockSize = blockSize;
        blockSize = newBlockSize;
        subrulesByHash.clear();
        std::vector<int> iPos, iNeg;
        size_t count = subrulesByIndex.size();
        for(size_t i = 0; i < count; ++i)
        {
            Subrule& subrule = subrulesByIndex[i];
            if(subrule.isAlive())
            {
                Blockset& bsPos = blocksetsPos[i];
                if(Private::hasPositive(subrule))
                {
                    for(int iPred : predListIter(oldBlockSize, bsPos, subrule.blockDataPos))
                        iPos.push_back(iPred);
                    predListBuild(newBlockSize, bsPos, subrule.blockDataPos, (int) iPos.size(), iPos.data(), true);
                    iPos.clear();
                }

                Blockset& bsNeg = blocksetsNeg[i];
                if(Private::hasNegative(subrule))
                {
                    for(int iPred : predListIter(oldBlockSize, bsNeg, subrule.blockDataNeg))
                        iNeg.push_back(iPred);
                    predListBuild(newBlockSize, bsNeg, subrule.blockDataNeg, (int) iNeg.size(), iNeg.data(), true);
                    iNeg.clear();
                }

                subrulesByHash.insert(IndexedRef{this, (int) i});
            }
        }
        averageBlockCountCached = -1;
    }

    //==================================================================================================================
    // Reindexing involves copying the old list to this one with new indices
    //==================================================================================================================

    void SubruleList::copyForReindex(SubruleList& other, std::vector<int>& predOldToNew)
    {
        xassert(size() == 0, "copyForReindex() must be called on an empty list");
        std::vector<int> iPos, iNeg;
        size_t count = other.subrulesByIndex.size();
        blockSize = other.blockSize;
        vacuous = other.vacuous;
        vacuousCount = other.getVacuousCount();
        subrulesByIndex.reserve(count);
        blocksetsPos.resize(count);
        blocksetsNeg.resize(count);
        for(size_t i = 0; i < count; ++i)
        {
            subrulesByIndex.push_back(Subrule((int) i));
            Subrule& thisSubrule = subrulesByIndex[i];
            Subrule& otherSubrule = other.subrulesByIndex[i];
            thisSubrule.flags = otherSubrule.flags;
            if(thisSubrule.isAlive())
            {
                thisSubrule.ruleIds() = otherSubrule.ruleIds();

                if(Private::hasPositive(otherSubrule))
                {
                    for(int iPred : predListIter(blockSize, other.blocksetsPos[i], otherSubrule.blockDataPos))
                    {
                        int newIndex = predOldToNew[iPred];
                        dassert(newIndex >= 0, "Invalid new index for old index %d", iPred);
                        iPos.push_back(newIndex);
                    }
                    predListBuild(blockSize, blocksetsPos[i], thisSubrule.blockDataPos, (int) iPos.size(), iPos.data(), false);
                    iPos.clear();
                }

                if(Private::hasNegative(otherSubrule))
                {
                    for(int iPred : predListIter(blockSize, other.blocksetsNeg[i], otherSubrule.blockDataNeg))
                    {
                        int newIndex = predOldToNew[iPred];
                        dassert(newIndex >= 0, "Invalid new index for old index %d", iPred);
                        iNeg.push_back(newIndex);
                    }
                    predListBuild(blockSize, blocksetsNeg[i], thisSubrule.blockDataNeg, (int) iNeg.size(), iNeg.data(), false);
                    iNeg.clear();
                }

                subrulesByHash.insert(IndexedRef{this, (int) i});
            }
            else
            {
                dassert(other.freeList.test(i), "Subrule should be free at %i", (int) i);
            }
        }

        // copy tombstone list
        freeList = other.freeList;
        averageBlockCountCached = -1;
    }

    //==================================================================================================================
    // Leave gaps in the structure instead of moving things around.
    //==================================================================================================================

    void SubruleList::tombstone(int index)
    {
        Subrule& subrule = subrulesByIndex[index];
        xassert(subrule.isAlive(), "subrule %d is already tombstoned", index);
        xassert(subrule.ruleIds().empty(), "Trying to free subrule (index %d) but it still has at least one rule "
                                         "(id %" PRId64 ")", index, subrule.ruleIds()[0]);
        auto it = subrulesByHash.find({this, index});
        xassert(it != subrulesByHash.end() && it->index == index, "Something went wrong in our hashtable...");
        subrulesByHash.erase(it);
        predListFree(blocksetsPos[index], subrule.blockDataPos);
        predListFree(blocksetsNeg[index], subrule.blockDataNeg);
        subrule.flags = 0;
        subrule.ruleIds().shrink_to_fit();
        if(vacuous.test(index))
        {
            --vacuousCount;
            vacuous.unset(index);
        }
        freeList.release(index);
        averageBlockCountCached = -1;
    }

    //==================================================================================================================
    // Used for stats right now.
    //==================================================================================================================

    double SubruleList::averageBlockCount()
    {
        // IMPORTANT NOTE: this is not thread-safe. I've decided to leave it that way and just make the variable atomic.
        // This does mean that in the rare case 2 threads are polling metrics at once, they'll both calculate it,
        // however, they will both calculate the same value.
        if(averageBlockCountCached < 0)
        {
            size_t live = liveCount();
            if(live == 0)
            {
                averageBlockCountCached = 0;
            }
            else
            {
                double total = 0;
                size_t indexCount = subrulesByIndex.size();
                bool size1 = blockSize == 1; // should cause compiler to hoist the condition out of the loop. hopefully.
                for(size_t i = 0; i < indexCount; ++i)
                {
                    Subrule& subrule = subrulesByIndex[i];
                    if(size1 || subrule.blockDataPos) total += blockCount(blocksetsPos[i]);
                    if(size1 || subrule.blockDataNeg) total += blockCount(blocksetsNeg[i]);
                }
                averageBlockCountCached = total / (double) live;
            }
        }
        return averageBlockCountCached;
    }

    //==================================================================================================================
    // Iterator wrapper; goes through both sides.
    //==================================================================================================================

    SubruleList::SubruleIterator::SubruleIterator(int blockSize, int flags, Blockset& blocksetPos, void* blockDataPos, Blockset& blocksetNeg, void* blockDataNeg)
    {
        if(flags & HAS_POSITIVE)
        {
            this->flags = flags;
            state = State::POS;
            inner = PredicateIterator(blockSize, blocksetPos, blockDataPos);
            negBlockset = &blocksetNeg;
            negData = blockDataNeg;
        }
        else if(flags & HAS_NEGATIVE)
        {
            this->flags = flags;
            state = State::NEG;
            inner = PredicateIterator(blockSize, blocksetNeg, blockDataNeg);
            negBlockset = nullptr;
            negData = nullptr;
        }
        else
        {
            this->flags = flags;
            state = State::END;
            inner = PredicateIterator();
            negBlockset = nullptr;
            negData = nullptr;
        }
    }

    void SubruleList::SubruleIterator::next()
    {
        if(state == State::POS)
        {
            ++inner;
            if(!inner)
            {
                if(flags & HAS_NEGATIVE)
                {
                    inner = PredicateIterator(inner.getBlockSize(), *negBlockset, negData);
                    state = State::NEG;
                }
                else
                {
                    inner = PredicateIterator();
                    state = State::END;
                }
            }
        }
        else if(state == State::NEG)
        {
            ++inner;
            if(!inner)
            {
                inner = PredicateIterator();
                state = State::END;
            }
        }
    }

    //==================================================================================================================
    // Core matching logic. Everything else is around just to support this code right here.
    // TODO rewrite parts of this in assembly
    //==================================================================================================================

    void SubruleList::match(BitArray& __restrict__ candidates, BitArray& __restrict__ predBitset, BitArray& __restrict__ results,
            int* __restrict__ out_nFiltered, int* __restrict__ out_nHits)
    {
        dassert(candidates.bitCount() == subrulesByIndex.size(), "%d != %d", candidates.bitCount(), subrulesByIndex.size());
        const int blockSize = this->blockSize;
        int nFiltered = 0, nHits = 0;
        ALIGNED_BY(32) BitBlock256 predBlockset;
        blocksetFromBitset(blockSize, predBitset, predBlockset);

        const Blockset* blocksetsPos = ASSUME_ALIGNED(this->blocksetsPos.data(), 32);
        const Blockset* blocksetsNeg = ASSUME_ALIGNED(this->blocksetsNeg.data(), 32);
        const Subrule* subrules = ASSUME_ALIGNED(this->subrulesByIndex.data(), 32);

        // match candidates
        for(int i : candidates)
        {
            ALIGNED_BY(32) const Blockset& blocksetPos = blocksetsPos[i];
            if(!blocksetMatch(predBlockset, blocksetPos))
                continue;
            ++nFiltered;
            const Subrule& subrule = subrules[i];
            dassert(subrule.isAlive(), "Trying to match deleted subrule %d", i);
            dassert(Private::hasPositive(subrule), "All subrules in this part of the loop should have a positive branch");
            void* blockDataPos = subrule.blockDataPos;
            if(blockDataPos != nullptr && !fullMatch(blockSize, blocksetPos, blockDataPos, predBitset))
                continue;
            if(Private::hasNegative(subrule))
            {
                ALIGNED_BY(32) const Blockset& blocksetNeg = blocksetsNeg[i];
                if(blocksetMatch(predBlockset, blocksetNeg))
                {
                    const void* blockDataNeg = subrule.blockDataNeg;
                    if(blockDataNeg == nullptr || fullMatch(blockSize, blocksetNeg, blockDataNeg, predBitset))
                        continue;
                }
            }
            ++nHits;
            for(RuleIndex ruleId : subrule.ruleIds())
                results.set(*ruleId);
        }

        // vacuous (negative-only) rules need to be checked on every pass
        if(vacuousCount > 0)
        {
            for(int i : vacuous)
            {
                // this code is basically copy-pasted from above, but I didn't want to risk it not being inlined properly
                ++nFiltered;
                const Subrule& subrule = subrules[i];
                dassert(subrule.isAlive(), "Subrule %d was deleted, but still in vacuous set", i);
                dassert(Private::hasNegative(subrule), "all vacuous should have a negative branch");
                ALIGNED_BY(32) const Blockset& blocksetNeg = blocksetsNeg[i];
                if(blocksetMatch(predBlockset, blocksetNeg))
                {
                    const void* blockDataNeg = subrule.blockDataNeg;
                    if(blockDataNeg == nullptr || fullMatch(blockSize, blocksetNeg, blockDataNeg, predBitset))
                        continue;
                }
                ++nHits;
                for(RuleIndex ruleId : subrule.ruleIds())
                    results.set(*ruleId);
            }
        }

        *out_nFiltered = nFiltered;
        *out_nHits = nHits;
    }

    //==================================================================================================================
    // Needs a full destructor to free memory associated w/ block data.
    //==================================================================================================================

    SubruleList::~SubruleList()
    {
        for(auto& subrule : subrulesByIndex)
        {
            if(subrule.blockDataPos)
                predListDestruct(subrule.blockDataPos);
            if(subrule.blockDataNeg)
                predListDestruct(subrule.blockDataNeg);
        }
    }
}
