/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include "rule.hpp"
#include "pred_list.hpp"
#include "../common/hash.hpp"
#include <boost/align/aligned_allocator.hpp>
#include <boost/align/aligned_delete.hpp>
#include <boost/container/small_vector.hpp>
#include <vector>
#include <atomic>

namespace burningmime::setmatch
{
    class Subrule
    {
    private:
        friend class SubruleList;
        int index;
        int flags = 0; // 0 for tombstoned, implementation-defined nonzero value when alive
        void* blockDataPos = nullptr;
        void* blockDataNeg = nullptr;
        typedef boost::container::small_vector<RuleIndex, 4> RuleIndexList;
        RuleIndexList ruleIds_;
        Subrule(int _index) : index(_index) { }
    public:
        RuleIndexList& ruleIds() { return ruleIds_; }
        const RuleIndexList& ruleIds() const { return ruleIds_; }
        int getIndex() const { return index; }
        bool isAlive() const { return flags != 0; }

        // can't use DEFAULT_MOVE_CONSTRUCTOR macro because small_vector's move is not noexcept
        DISABLE_COPY_CONSTRUCTOR(Subrule)
        Subrule(Subrule&& other) = default;
        Subrule& operator=(Subrule&& other) = default;
    }; ASSERT_TYPE_SIZE(Subrule, 64);

    class SubruleList
    {
        struct Private;
        struct IndexedRef { void* ptr; int index; bool operator==(const IndexedRef& other) const; };
        struct IndexedRefHash { size_t operator()(const IndexedRef& ref) const; };
        struct IndexedRefPrivate;

        typedef std::vector<Blockset, boost::alignment::aligned_allocator<Blockset, 32>> BlocksetVector; // these *must* be aligned to use aligned AVX loads
        typedef std::vector<Subrule, boost::alignment::aligned_allocator<Subrule, 64>> SubruleVector; // these don't need to be aligned, but they're 64 bytes each; minor perf boost this way

        int blockSize = 1;
        BlocksetVector blocksetsPos;
        BlocksetVector blocksetsNeg;
        SubruleVector subrulesByIndex;
        HashSet<IndexedRef, IndexedRefHash> subrulesByHash;
        TombstoneList freeList;
        BitArray vacuous;
        size_t vacuousCount;
        std::atomic<double> averageBlockCountCached = -1;

    public:
        struct IndexAndNegation { int index; bool isNegated; };
        class SubruleIterable; // fwd declaration needed for iterator to friend it as an inner class
        class SubruleIterator
        {
            enum class State { NEG, POS, END };
            int flags;
            PredicateIterator inner;
            Blockset* negBlockset;
            void* negData;
            State state;
            void next();
            
            friend SubruleIterable;
            SubruleIterator(int blockSize, int flags, Blockset& blocksetPos, void* blockDataPos, Blockset& blocksetNeg, void* blockDataNeg);

        public:
            inline SubruleIterator() : state(State::END) { }
            inline SubruleIterator& operator++() { next(); return *this; }
            inline explicit operator bool() const { return state != State::END; }
            inline IndexAndNegation operator*() const { return {*inner, state == State::NEG}; }
            inline bool operator!=(const SubruleIterator& other) const { return !(*this == other); }
            inline bool operator==(const SubruleIterator& other) const { return state == other.state && (state == State::END || inner == other.inner); }
        };

        class SubruleIterable
        {
            friend SubruleList;
            SubruleList* list;
            int index;
            SubruleIterable(SubruleList* _list, int _index) : list(_list), index(_index) { }
        public:
            inline SubruleIterator begin()
            {
                Subrule& subrule = list->subrulesByIndex[index];
                return SubruleIterator(list->blockSize, subrule.flags, list->blocksetsPos[index], subrule.blockDataPos, list->blocksetsNeg[index], subrule.blockDataNeg);
            }

            inline SubruleIterator end()
            {
                return SubruleIterator();
            }
        };

        inline Subrule& operator[](int index) { return subrulesByIndex[index]; }
        inline const Subrule& operator[](int index) const { return subrulesByIndex[index]; }
        inline size_t size() const { return subrulesByIndex.size(); }
        inline size_t liveCount() const { return subrulesByHash.size(); }
        inline size_t getVacuousCount() const { return vacuousCount; }
        Subrule& getOrAdd(std::vector<int>& pos, std::vector<int>& neg);
        int getBlockSize() const { return blockSize; }
        size_t predicateCapacity() const { return (size_t) blockSize * 256; }
        void changeBlockSize(int newBlockSize);
        void copyForReindex(SubruleList& other, std::vector<int>& predOldToNew);
        double averageBlockCount();
        SubruleIterable iterate(int index) { return SubruleIterable(this, index); }
        void match(BitArray& candidates, BitArray& predBitset, BitArray& results, int* nFiltered, int* nHits);
        void tombstone(int index);
        ~SubruleList();

        SubruleList() = default;
        DISABLE_COPY_CONSTRUCTOR(SubruleList)
        // not allowing move either, since IndexedRef refers to a specific location
        // copyForReindex() can be used for explicit copying, which also reorders predicates
    };
}
