/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "db_index.hpp"
#include <sstream>
#include "../boolean/rule_dec.hpp"
#include "../common/metric.hpp"
#include "../predicate/tag.hpp"

namespace burningmime::setmatch
{
    struct CRuleDB::Private
    {
        static void getOrCreateSubrule(CRuleDB& db, RuleIndex ruleId, Rule& ruleObj, std::vector<int>& positive, std::vector<int>& negative, bool skipSearch);
        static void resize(CRuleDB& db, int newBlockSize);
        static void processInput(CRuleDB& db, SimpleStringIterator input, std::vector<TaggedToken>& results);
    };

    //==================================================================================================================
    // Metrics
    //==================================================================================================================

    struct CRuleDB::Metrics
    {
        ExponentialDecayMetric& candidateCount;
        ExponentialDecayMetric& hitCount;
        ExponentialDecayMetric& fastFilterCount;
        ExponentialDecayMetric& slowFilterCount;
        ExponentialDecayMetric& candidatesToSizeRatio;
        ExponentialDecayMetric& hitToSizeRatio;
        ExponentialDecayMetric& hitToCandidatesRatio;
        ExponentialDecayMetric& fastFilterRatio;
        ExponentialDecayMetric& gatherMs;
        ExponentialDecayMetric& filterMs;
        SingleValueMetric& resizeMs;

        #define MATCH_METRIC(N) N(reg.add(new ExponentialDecayMetric("match." #N ".weightedMean")))
        #define MATCH_METRIC_T(N) N(reg.add(new ExponentialDecayMetric("match.timers." #N ".weightedMean")))
        #define LAMBDA_METRIC(N, V) reg.add(newLambdaMetric("db." #N ".current", [&db]() -> double { return (double) (V); }))
        explicit Metrics(MetricRegistry& reg, CRuleDB& db) :
            MATCH_METRIC(candidateCount),
            MATCH_METRIC(hitCount),
            MATCH_METRIC(fastFilterCount),
            MATCH_METRIC(slowFilterCount),
            MATCH_METRIC(candidatesToSizeRatio),
            MATCH_METRIC(hitToSizeRatio),
            MATCH_METRIC(hitToCandidatesRatio),
            MATCH_METRIC(fastFilterRatio),
            MATCH_METRIC_T(gatherMs),
            MATCH_METRIC_T(filterMs),
            resizeMs(reg.add(new SingleValueMetric("resize.timeMs.mostRecent")))
        {
            LAMBDA_METRIC(blockSize, db.cdata->subrules.getBlockSize());
            LAMBDA_METRIC(ruleCount, db.liveRuleCount);
            LAMBDA_METRIC(subruleCount, db.cdata->subrules.liveCount());
            LAMBDA_METRIC(predicateCount, db.livePhraseCount);
            LAMBDA_METRIC(averageBlockCount, db.cdata->subrules.averageBlockCount());
        }
    }; PIMPL_IMPL(CRuleDB::Metrics)
    #undef MATCH_METRIC
    #undef MATCH_METRIC_T
    #undef LAMBDA_METRIC

    //==================================================================================================================
    // Constructor
    //==================================================================================================================

    CRuleDB::CRuleDB(DBConfig& config, MetricRegistry& metricRegistry) :
        config(config),
        cdata(std::make_unique<CData>()),
        metrics(config.collectMetrics ? new Metrics(metricRegistry, *this) : nullptr)
    {
    }

    //==================================================================================================================
    // Adding a new rule
    //==================================================================================================================

    static size_t nextBlockSize(size_t prevBlockSize)
    {
        switch(prevBlockSize)
        {
            case 1: return 8;
            case 8: return 16;
            case 16: return 32;
            case 32: return 64;
            case 64: return 128;
            case 128: return 256;
            default:
                xassert(prevBlockSize >= 256 && prevBlockSize % 256 == 0, "prevBlockSize = %d", (int) prevBlockSize);
                return prevBlockSize + 256;
        }
    }

    void CRuleDB::Private::getOrCreateSubrule(CRuleDB& db, RuleIndex ruleId, Rule& ruleObj, std::vector<int>& positive,
            std::vector<int>& negative, bool skipSearch)
    {
        CData& cdata = *db.cdata;
        Subrule& subrule = cdata.subrules.getOrAdd(positive, negative);

        //we do a check to see if the rule ID is *already* in the subrule (ie there was some sort
        // of duplication among ORs). if so, then just skip it all
        for(RuleIndex otherRule : subrule.ruleIds())
            if(otherRule == ruleId)
                return;

        if(subrule.ruleIds().empty()) // if it's a new subrule...
        {
            // reference it in all its predicates
            for(int phraseIndex : positive)
                cdata.phrasesByIndex[phraseIndex]->subrulesPos().push_back(subrule.getIndex());
            for(int phraseIndex : negative)
                cdata.phrasesByIndex[phraseIndex]->subrulesNeg().push_back(subrule.getIndex());
        }
        else
        {
            #if !NDEBUG
            // assert that all its predicates reference it (only in debug mode)
            for(int phraseIndex : positive)
            {
                auto& ps = cdata.phrasesByIndex[phraseIndex]->subrulesPos();
                auto it = std::find(ps.begin(), ps.end(), subrule.getIndex());
                dassert(it != ps.end(), "did not find subrule index %d in phrase %d", subrule.getIndex(), phraseIndex);
            }
            for(int phraseIndex : negative)
            {
                auto& ps = cdata.phrasesByIndex[phraseIndex]->subrulesNeg();
                auto it = std::find(ps.begin(), ps.end(), subrule.getIndex());
                dassert(it != ps.end(), "did not find subrule index %d in phrase %d", subrule.getIndex(), phraseIndex);
            }
            #endif
        }

        subrule.ruleIds().push_back(ruleId);
        ruleObj.subrules.push_back(subrule.getIndex());
    }

    #define expect(TTY) xassert(t.type() == ParseTokenType::TTY, "t.type = %d, but expected " #TTY " (%d)", (int) t.type(), (int) ParseTokenType::TTY)
    #define nextToken() do { xassert(tokenIt != tokenEnd, "Unexpected end of tokens array"); t = *(tokenIt++); } while(false)
    bool CRuleDB::add(int64_t externalId, void* ruleData, size_t ruleLength)
    {
        // parse the rule
        // do this before making any modifications in case the rule is invalid
        std::vector<ParseToken> tokens = decodeBinaryRule(ruleData, ruleLength);
        auto tokenIt = tokens.begin();
        auto tokenEnd = tokens.end();
        ParseToken t = ParseToken::invalid();
        nextToken();
        expect(BEGIN_RULE);

        // if the rule exists, do an update (ie remove old one before adding new one)
        bool replaced = false;
        if(rulesByExternalId.find(externalId) != rulesByExternalId.end())
        {
            remove(externalId);
            replaced = true;
        }

        CData& cdata = *this->cdata;
        auto birth = rulesFree.acquire();
        Rule& ruleObj = birth.isNew ? rules.emplace_back() : rules[birth.index];
        xassert(ruleObj.subrules.empty(), "Tombstoned rules always should be empty");
        ruleObj.externalId = externalId;
        RuleIndex ruleIndex{(uint32_t) birth.index};
        rulesByExternalId.insert(std::pair{externalId, ruleIndex});
        liveRuleCount++;

        // if we're full, need to grow index
        size_t minCapacity = cdata.phrasesByIndex.size() + t.getTotalPredicateCount();
        if(minCapacity >= cdata.subrules.predicateCapacity())
        {
            size_t newBlockSize = nextBlockSize((size_t) cdata.subrules.getBlockSize());
            while(minCapacity >= newBlockSize * 256)
                newBlockSize = nextBlockSize(newBlockSize);
            logInfo("Index full -- increasing block size from %d to %d", cdata.subrules.getBlockSize(), (int) newBlockSize);
            Private::resize(*this, (int) newBlockSize);
        }

        // foreach subrule
        std::vector<int> positive, negative;
        bool skipSearch = false;
        nextToken();
        while(t.type() == ParseTokenType::BEGIN_SUBRULE)
        {
            nextToken();
            // foreach predicate
            while(t.type() == ParseTokenType::BEGIN_PREDICATE)
            {
                PredicateHeader header = t.getPredicateHeader();
                bool isPositive = !header.isNegated;
                Predicate* phrase;

                if(header.predicateType == PredicateType::PHRASE)
                {
                    nextToken();
                    expect(STRING);

                    // create a lambda that will iterate through the phrase's tokens
                    FSimpleStringIterator it = [&]() -> SimpleString
                    {
                        if(t.type() == ParseTokenType::STRING)
                        {
                            SimpleString str = t.getString();
                            nextToken();
                            return str;
                        }
                        else
                        {
                            return { nullptr, 0 };
                        }
                    };

                    // add phrase to trie
                   phrase = pindex.getOrAddPhrase(*it);
                }
                else if(header.predicateType == PredicateType::RANGE)
                {
                    nextToken();
                    expect(STRING);
                    SimpleString str = t.getString();
                    nextToken();
                    expect(INTERVAL);
                    Interval interval = t.getInterval();
                    xassert((bool) interval, "Interval should be valid here");
                    nextToken();
                    phrase = pindex.getOrAddInterval(str, interval);
                }
                else
                {
                    xassert_false("Unknown predicate type %d", (int) header.predicateType);
                }

                // if the phrase was new, insert it in the DB
                if(phrase->isNew())
                {
                    skipSearch = true; // if at least one phrase is new, the subrule will be new
                    livePhraseCount++;
                    auto birth = phrasesFree.acquire();
                    phrase->setIndex(birth.index);
                    if(birth.isNew)
                    {
                        cdata.phrasesByIndex.push_back(phrase);
                    }
                    else
                    {
                        xassert(cdata.phrasesByIndex[birth.index] == nullptr, "The free phrase at %d wasn't actually free", birth.index);
                        cdata.phrasesByIndex[birth.index] = phrase;
                    }
                }

                // always add its id to the current subrule
                (isPositive ? positive : negative).push_back(phrase->getIndex());
            }

            // find or create a new subrule
            CRuleDB::Private::getOrCreateSubrule(*this, ruleIndex, ruleObj, positive, negative, skipSearch);

            // clear these for next time
            positive.clear();
            negative.clear();
        }
        expect(END);

        return replaced;
    }
    #undef expect
    #undef nextToken

    //==================================================================================================================
    // Removing a rule
    //==================================================================================================================

    bool CRuleDB::remove(int64_t externalId)
    {
        CData& cdata = *this->cdata;

        auto it = rulesByExternalId.find(externalId);
        if(it == rulesByExternalId.end())
            return false;
        RuleIndex ruleId = it->second;
        Rule& rule = rules[*ruleId];
        xassert(!rule.subrules.empty(), "Rule was already removed");

        for(int subruleIndex : rule.subrules)
        {
            Subrule& subrule = cdata.subrules[subruleIndex];
            auto it2 = std::find(subrule.ruleIds().begin(), subrule.ruleIds().end(), ruleId);
            xassert(it2 != subrule.ruleIds().end(), "Could not find rule ID %d in subrule index %d", *ruleId, subruleIndex);
            subrule.ruleIds().erase(it2);
            if(subrule.ruleIds().empty())
            {
                // delete its phrases
                for(auto pair : cdata.subrules.iterate(subruleIndex))
                {
                    int phraseIndex = pair.index;
                    Predicate* phrase = cdata.phrasesByIndex[phraseIndex];
                    auto& subruleList = pair.isNegated ? phrase->subrulesNeg() : phrase->subrulesPos();
                    auto it3 = std::find(subruleList.begin(), subruleList.end(), subruleIndex);
                    xassert(it3 != subruleList.end(), "Did not find subrule index %d in predicate %d", subruleIndex, phraseIndex);
                    subruleList.erase(it3);
                    if(phrase->subrulesPos().empty() && phrase->subrulesNeg().empty())
                    {
                        livePhraseCount--;
                        cdata.phrasesByIndex[phraseIndex] = nullptr;
                        pindex.removeAndFree(phrase);
                        phrasesFree.release(phraseIndex);
                    }
                }

                // free it in the subrule list
                cdata.subrules.tombstone(subruleIndex);
            }
        }
        rule.subrules.clear();
        rule.subrules.shrink_to_fit();
        rulesFree.release((int) *ruleId);
        rulesByExternalId.erase(it);
        liveRuleCount--;
        return true;
    }

    //==================================================================================================================
    // Resize without reindexing
    //==================================================================================================================

    void CRuleDB::Private::resize(CRuleDB& db, int newBlockSize)
    {
        CData& cdata = *db.cdata;
        xassert(newBlockSize == 8 || newBlockSize == 16 || newBlockSize == 32 || newBlockSize % 64 == 0, "Invalid block size %d", newBlockSize);
        if(newBlockSize != db.cdata->subrules.getBlockSize())
        {
            METRIC_TIMER(db.metrics, resizeMs);
            xassert((size_t) newBlockSize * 256 >= cdata.phrasesByIndex.size(), "newBlockSize = %d, newBlockSize*256 = %d, phraseCount = %d",
                    newBlockSize, newBlockSize * 256, (int) cdata.phrasesByIndex.size());
            cdata.subrules.changeBlockSize(newBlockSize);
            logInfo("Resize took %.4f ms", resizeMs.elapsed());
        }
    }

    //==================================================================================================================
    // Reindexing
    //==================================================================================================================

    struct CRuleDB::ReindexData
    {
        std::vector<int> phOldToNew;
        std::unique_ptr<CData> newCData;

        void prepare(CRuleDB& db, CancellationToken& cancelled);
        void reindexPhrases(CRuleDB& db);
        void reindexSubrules(CRuleDB& db);
        void apply(CRuleDB& db);

    }; PIMPL_IMPL(CRuleDB::ReindexData)

    void CRuleDB::ReindexData::reindexPhrases(CRuleDB& db)
    {
        std::vector<Predicate*>& oldPhrasesByIndex = db.cdata->phrasesByIndex;
        std::vector<Predicate*>& newPhrasesByIndex = newCData->phrasesByIndex;
        newPhrasesByIndex.assign((size_t) db.livePhraseCount, nullptr);
        for(int oldPhraseIndex = 0; oldPhraseIndex < (int) oldPhrasesByIndex.size(); ++oldPhraseIndex)
        {
            int newPhraseIndex = phOldToNew[oldPhraseIndex];
            if(newPhraseIndex != -1)
            {
                xassert(newPhraseIndex < (int) newPhrasesByIndex.size() && newPhraseIndex >= 0, "newPhraseIndex = %d", newPhraseIndex);
                Predicate* oldPhrase = oldPhrasesByIndex[oldPhraseIndex];
                xassert(oldPhrase != nullptr, "old index = %d", oldPhraseIndex);
                newPhrasesByIndex[newPhraseIndex] = oldPhrase;
            }
        }

        // verify no gaps (this is quick enough that we do it even in release mode)
        for(int i = 0; i < (int) newPhrasesByIndex.size(); ++i)
            xassert(newPhrasesByIndex[i] != nullptr, "should have filled the whole array here [index %d] (maybe the phOldToNew map is outdated?)", i);
    }

    void CRuleDB::ReindexData::reindexSubrules(CRuleDB& db)
    {
        CData& oldData = *db.cdata, &newData = *newCData;
        SubruleList& oldSubrules = oldData.subrules;
        SubruleList& newSubrules = newData.subrules;
        newSubrules.copyForReindex(oldSubrules, phOldToNew);
    }

    CRuleDB::PReindexData CRuleDB::prepareReindex(CancellationToken cancelled) { PReindexData data(new ReindexData()); data->prepare(*this, cancelled); return data; }
    void CRuleDB::ReindexData::prepare(CRuleDB& db, CancellationToken& cancelled)
    {
        // step 1: run the actual graph optimization algorithm. this will figure out new phrase indicies that
        // are hopefully a bit better than our current. it will also filter out tombstoned phrases, such that the
        // result list contains only live phrases
        phOldToNew = db.optimize(cancelled);

        // Step 2: Create brand new copies of the phrase and subrule lists. When we're ready, we just swap these bad
        // boys in for the old ones, which is very quick and means we don't need to hold the write lock for very long
        newCData = std::make_unique<CData>();
        reindexPhrases(db);
        reindexSubrules(db);

        double abcOld = db.cdata->subrules.averageBlockCount();
        double abcNew = newCData->subrules.averageBlockCount();
        logInfo("Average block count from %.4f to %.4f", abcOld, abcNew);
    }

    void CRuleDB::applyReindex(ReindexData& data) { data.apply(*this); }
    void CRuleDB::ReindexData::apply(CRuleDB& db)
    {
        xassert(phOldToNew.size() == db.cdata->phrasesByIndex.size(), "%d != %d (upgrade lock problem? should not have accepted writers in between)",
                (int) phOldToNew.size(), (int) db.cdata->phrasesByIndex.size());
        xassert(newCData->phrasesByIndex.size() == (size_t) db.livePhraseCount, "%d != %d (upgrade lock problem? should not have accepted writers in between)",
                (int) newCData->phrasesByIndex.size(), (int) db.livePhraseCount);

        // actually set the phrase indices (can't do this while the phrases were in use)
        CData& newData = *newCData;
        for(size_t i = 0; i < newData.phrasesByIndex.size(); ++i)
        {
            Predicate& phrase = *newData.phrasesByIndex[i];
            phrase.setIndex((int) i);
        }

        // part of the reindexing process will have removed all gaps in the phrase list
        db.phrasesFree.clearAndResize(newData.phrasesByIndex.size());

        // swap instead of move because we do not need to hold the write lock while the data gets freed
        std::swap(newCData, db.cdata);
    }

    //==================================================================================================================
    // Actual matching
    //==================================================================================================================

    struct MatchState
    {
        BitArray matchedPredicates;
        BitArray candidateSubrules;
        BitArray matchedRules;
        std::vector<int64_t> resultList;
        std::vector<TaggedToken> inputs;
    };

    static ThreadLocal<MatchState> tlMatchState;
    std::vector<int64_t>* CRuleDB::match(SimpleStringIterator input)
    {
        CData& cdata = *this->cdata;
        MatchState& s = *tlMatchState;
        s.resultList.clear(); // clear results from last time
        if(LIKELY(livePhraseCount > 0))
        {
            s.inputs.clear();
            Private::processInput(*this, input, s.inputs);
            if(LIKELY(!s.inputs.empty()))
            {
                // more clearing
                s.matchedRules.clearAndResize(rules.size());
                s.matchedPredicates.clearAndResize(nextMultipleOf(cdata.phrasesByIndex.size(), cdata.subrules.getBlockSize()));
                s.candidateSubrules.clearAndResize(cdata.subrules.size());
                int nCandidates;

                // iterate through the phrases and create two bitsets. One bitset contains all phrase IDs, the other contains all candidate IDs
                {
                    SystemTime gatherStart = now(); // MetricTimerMillis was reporting bogus values, so have to time this manually
                    pindex.match(s.inputs, s.matchedPredicates);
                    nCandidates = (int) cdata.subrules.getVacuousCount();
                    for(int phraseIndex : s.matchedPredicates)
                    {
                        dassert(cdata.phrasesByIndex[phraseIndex], "phraseIndex = %d", phraseIndex);
                        for(int subruleIndex : cdata.phrasesByIndex[phraseIndex]->subrulesPos())
                        {
                            if(s.candidateSubrules.set((size_t) subruleIndex))
                            {
                                dassert(cdata.subrules[subruleIndex].isAlive(), "phrase %d referenced dead subrule %d", phraseIndex, subruleIndex);
                                nCandidates++;
                            }
                        }
                    }
                    if(metrics)
                    {
                        metrics->gatherMs.push(toMilliseconds(now() - gatherStart));
                        metrics->candidateCount.push(nCandidates);
                    }
                }

                {
                    // main filtering logic is done inside the subrules object here
                    SystemTime filterStart = now();
                    int nFiltered, nHits;
                    cdata.subrules.match(s.candidateSubrules, s.matchedPredicates, s.matchedRules, &nFiltered, &nHits);

                    if(metrics)
                    {
                        metrics->filterMs.push(toMilliseconds(now() - filterStart));
                        int nFast = nCandidates - nFiltered;
                        int nSlow = nCandidates - nFast - nHits;
                        int nSubrules = (int) cdata.subrules.liveCount(); // cdata.subrulesByIndex includes tombstoned ones
                        metrics->hitCount.push(nHits);
                        metrics->fastFilterCount.push(nFast);
                        metrics->slowFilterCount.push(nSlow);
                        metrics->candidatesToSizeRatio.push((double) nCandidates / nSubrules);
                        metrics->hitToSizeRatio.push((double) nHits / nSubrules);
                        if(nCandidates > 0)
                            metrics->hitToCandidatesRatio.push((double) nHits / nCandidates);
                        if(nFast > 0 || nSlow > 0)
                            metrics->fastFilterRatio.push((double) nFast / (nFast + nSlow));
                    }
                }

                for(int index : s.matchedRules)
                    s.resultList.push_back(rules[index].externalId);
            }
        }

        return &s.resultList;
    }

    void CRuleDB::Private::processInput(CRuleDB& db, SimpleStringIterator input, std::vector<TaggedToken>& results)
    {
        dassert(results.empty(), "Result list should be empty here");
        while(true)
        {
            SimpleString src = input();
            if(src.length == 0)
                return;
            TaggedString tagged = parseTags(src);
            results.push_back(db.pindex.strToTok(tagged));
        }
    }

    //==================================================================================================================
    // Debug helpers
    //==================================================================================================================

    bool CRuleDB::contains(int64_t ruleId)
    {
        auto it = rulesByExternalId.find(ruleId);
        return it != rulesByExternalId.end();
    }

    std::pair<bool, std::string> CRuleDB::debugGetRuleString(int64_t externalId)
    {
        CData& cdata = *this->cdata;
        std::ostringstream ss;
        auto it = rulesByExternalId.find(externalId);
        if(it == rulesByExternalId.end())
        {
            ss << "[Could not find rule with ID " << externalId << "]";
            return std::make_pair(false, ss.str());
        }
        else
        {
            bool firstSubrule = true;
            RuleIndex ruleIndex = it->second;
            Rule& rule = rules[*ruleIndex];
            for(int iSubrule : rule.subrules)
            {
                if(!firstSubrule) ss << " OR ";
                firstSubrule = false;
                xassert(iSubrule < (int) cdata.subrules.size(), "iSubrule = %d, size = %d, ruleId = %d", iSubrule, cdata.subrules.size(), externalId);
                Subrule& subrule = cdata.subrules[iSubrule];
                xassert(subrule.isAlive(), "Tombstoned subrules should never be referenced by a rule");
                ss << "(";
                bool firstPhrase = true;
                for(auto subrulePhrase : cdata.subrules.iterate(iSubrule))
                {
                    if(!firstPhrase) ss << " ";
                    if(subrulePhrase.isNegated) ss << "~";
                    firstPhrase = false;
                    xassert(subrulePhrase.index < (int) cdata.phrasesByIndex.size(), "iPhrase = %d, size = %d", (int) iSubrule, (int) cdata.phrasesByIndex.size());
                    ss << '\"' << pindex.toString(cdata.phrasesByIndex[subrulePhrase.index]) << '\"';
                }
                ss << ")";
            }
            return std::make_pair(true, cleanUtf8(ss.str()));
        }
    }
}
