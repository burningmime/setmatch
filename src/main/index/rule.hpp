/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include <vector>

namespace burningmime::setmatch
{
    // right now a rule is just a list of subrule indices
    struct Rule
    {
        std::vector<int> subrules;
        int64_t externalId;

        Rule() = default;
        ~Rule() = default;
        Rule(Rule&& other) noexcept = default;
        Rule& operator=(Rule&& other) noexcept = default;
        DISABLE_COPY_CONSTRUCTOR(Rule)
    };

    MAKE_UINT32_WRAPPER(RuleIndex)
}
