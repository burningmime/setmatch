/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "db_index.hpp"
#include "../common/metric.hpp"
#include <algorithm>
#include <random>

namespace burningmime::setmatch
{
    // TODO this is just temporary stuff or testing

    struct CRuleDB::GraphOptimizer
    {
        CRuleDB& db;
        CancellationToken& cancelled;
        std::vector<int> result;

        GraphOptimizer(CRuleDB& _db, CancellationToken& _cancelled) : db(_db), cancelled(_cancelled) { }
        void run();
    };

    void CRuleDB::GraphOptimizer::run()
    {
        CData& cdata = *db.cdata;
        std::vector<int> source;
        for(int i = 0; i < (int) cdata.phrasesByIndex.size(); ++i)
        {
            if(cdata.phrasesByIndex[i] != nullptr)
                source.push_back(i);
        }

        result.assign(cdata.phrasesByIndex.size(), -1);
        std::shuffle(source.begin(), source.end(), std::default_random_engine((uint64_t) now().count()));
        int dstIndex = 0;
        for(int srcIndex : source)
            result[srcIndex] = dstIndex++;
    }

    std::vector<int> CRuleDB::optimize(CancellationToken& cancelled)
    {
        GraphOptimizer instance(*this, cancelled);
        instance.run();
        return std::move(instance.result);
    }
}
