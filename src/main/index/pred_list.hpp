/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "match.hpp"
#include "../common/common.hpp"
#include "../common/bitset.hpp"

namespace burningmime::setmatch
{
    struct Blockset
    {
        int64_t blocks[4] = {0};

        ALWAYS_INLINE inline operator int64_t*() { return blocks; }
        ALWAYS_INLINE inline int64_t& operator[](int i) { return blocks[i]; }

        // a perfect demonstration of what's wrong with const. as if C++ wasn't verbose enough already...
        ALWAYS_INLINE inline operator const int64_t*() const { return blocks; }
        ALWAYS_INLINE inline int64_t operator[](int i) const { return blocks[i]; }
    };

    class PredicateIterator
    {
        int blockSize;
        int mode;
        union
        {
            struct
            {
                int* values;
                int size;
                int index;
            } sm;
            struct
            {
                int64_t* blockset;
                void* blockData;
                int globalBlock;
                int localBlock;
                int innerIndex;
            } bg;
        };
        int value;

        void first(int64_t* blockset, void* blockData);
        void next();


    public:
        inline PredicateIterator(int _blockSize, Blockset& _blockset, void* _blockData) :
            blockSize(_blockSize) { first(_blockset, _blockData); }
        inline PredicateIterator() : mode(-1), value(-1) { }
        inline PredicateIterator& operator++() { next(); return *this; }
        inline explicit operator bool() const { return value != -1; }
        inline int operator*() { return value; }
        inline int operator*() const { return value; }
        inline bool operator==(const PredicateIterator& other) const { return value == other.value; }
        inline bool operator!=(const PredicateIterator& other) const { return !(*this == other); }
        int getBlockSize() { return blockSize; }
    };

    class PredicateIterable
    {
        int blockSize;
        Blockset& blockset;
        void* data;
        inline PredicateIterable(int _blockSize, Blockset& _blockset, void* _data) : blockSize(_blockSize), blockset(_blockset), data(_data) { }
        friend PredicateIterable predListIter(int blockSize, Blockset& blockset, void* data);
    public:
        inline PredicateIterator begin() { return PredicateIterator(blockSize, blockset, data); }
        inline PredicateIterator end() { return PredicateIterator(); }
        DISABLE_COPY_CONSTRUCTOR(PredicateIterable)
    };

    // these are free functions instead of wrapped in a type to signify that the data may be stored in separate places
    // while it would be syntactically a bit nicer to make a wrapper type instead of doing it all C-style, I want to make
    // it perfectly clear to anyone reading it (ie future me) that predicate lists are not contiguous memory objects
    // (for cache efficiency reasons)
    void predListBuild(int blockSize, Blockset& blockset, void*& blockData, int nPhrases, int* phraseIndices, bool isSorted);
    void predListFree(Blockset& blockset, void*& blockData);
    void predListDestruct(void* blockData);
    bool predListEquals(int blockSize, const Blockset& blockset1, const void* blockData1, const Blockset& blockset2, const void* blockData2);
    size_t predListHash(int blockSize, const Blockset& blockset, const void* blockData);
    inline PredicateIterable predListIter(int blockSize, Blockset& blockset, void* data) { return PredicateIterable(blockSize, blockset, data); }
    int blockCount(const Blockset& blockset);

    // match operations
    // i'd rather not have the blockset match functions in the header but it is CRUCIAL that they be inlined. these functions
    // are where most of the match time is spent. LTO or the unity build *should* take care of this, but this is the best
    // way to make sure that these functions are inlined, no questions asked.
    void blocksetFromBitset(int blockSize, BitArray& bitset, BitBlock256& result);
    ALWAYS_INLINE inline bool blocksetMatch(const BitBlock256& __restrict__ phraseBlockset, const Blockset& __restrict__ testBlockset)
        { return matchBlock256<true>(testBlockset, phraseBlockset); }
    bool fullMatch(int blockSize, const Blockset& blockset, const void* __restrict__ blockData, const BitArray& phraseBitset);
}
