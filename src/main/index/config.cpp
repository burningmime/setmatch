/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "config.h"
#include "../common/hash.hpp"
#include "../common/json.h"
#include <iostream>

namespace burningmime::setmatch
{
    typedef const rapidjson::Value& JsonNode;

    void DBConfig::init(const char* configJson_utf8_nt)
    {
        // null config -> just use defaults
        if(!configJson_utf8_nt)
            return;

        // otherwise we need to parse it
        rapidjson::Document root = parseJson(configJson_utf8_nt);
        xassert(root.GetType() == rapidjson::kObjectType, "parseJson() should have checked this");
        HashSet<HashedString, HashedString::Hash> duplicates;
        for(auto it = root.MemberBegin(); it != root.MemberEnd(); ++it)
        {
            // check for duplicate
            const char* key = it->name.GetString();
            HashedString hkey{key, (uint32_t) it->name.GetStringLength()};
            if(duplicates.contains(hkey))
                setmatchRaise(JSON_FORMAT_ERROR, "Duplicate config key %s", key);
            duplicates.insert(hkey);

            auto& value = it->value;
            if(isKey(key, "collectMetrics"))
            {
                if(value.GetType() == rapidjson::kTrueType)
                    collectMetrics = true;
                else if(value.GetType() == rapidjson::kFalseType)
                    collectMetrics = false;
                else
                    jsonFormatError("Value of '%s' config must be a boolean", key);
            }
            else
            {
                logWarning("Unknown config key %s", key);
            }
        }
    }
}
