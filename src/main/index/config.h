/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"

namespace burningmime::setmatch
{
    struct DBConfig
    {
        // Configurations themselves (currently just public to make this easier to use in tests)
    public:
        bool collectMetrics = true;

        // Initialization and stuff
    private:
        struct Private;
        void init(const char* configJson_utf8_nt);
    public:
        DBConfig() { }

        ALWAYS_INLINE static inline DBConfig forJson(const char* configJson_utf8_nt)
        {
            DBConfig c{};
            c.init(configJson_utf8_nt);
            return c;
        }

        DEFAULT_COPY_CONSTRUCTOR(DBConfig)
        DEFAULT_MOVE_CONSTRUCTOR(DBConfig)
    };
}
