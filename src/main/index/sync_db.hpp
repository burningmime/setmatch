/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "db_index.hpp"

namespace burningmime::setmatch
{
    /**
     * Wrapper around CRuleDB that synchronizes access.
     */
    class SRuleDB
    {
        DBConfig config;
        MetricRegistry metricRegistry;
        CrowMutex mutex;
        CRuleDB index;
        std::atomic<int64_t> changesSinceLastReindex = 0;
        PIMPL_VAR(Metrics, metrics)

    public:
        explicit SRuleDB(DBConfig config);
        bool add(int64_t ruleId, void* ruleData, size_t ruleLength);
        bool remove(int64_t ruleId);
        std::vector<int64_t>* match(SimpleStringIterator input);
        std::pair<bool, std::string> debugGetRuleString(int64_t ruleId);
        bool contains(int64_t ruleId);
        std::vector<MetricValue>& getMetrics();
        void reindex();
        ~SRuleDB() = default;

        DISABLE_COPY_CONSTRUCTOR(SRuleDB)
    };
}
