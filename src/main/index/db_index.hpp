/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../predicate/pred_index.hpp"
#include "rule.hpp"
#include "subrule.hpp"
#include "../common/hash.hpp"
#include <vector>
#include "../common/metric.hpp"
#include "../common/sync.hpp"
#include "config.h"

namespace burningmime::setmatch
{
    /**
     * Pronounced "cruel DB".
     */
    class CRuleDB
    {
        struct Private;
        struct GraphOptimizer;
        std::vector<int> optimize(CancellationToken& cancelled);

        struct CData
        {
            std::vector<Predicate*> phrasesByIndex;
            SubruleList subrules;

            CData() = default;
            ~CData() = default;
            DISABLE_COPY_CONSTRUCTOR(CData)
        };

        DBConfig& config;
        PredicateIndex pindex;
        HashMap<int64_t, RuleIndex> rulesByExternalId;
        std::vector<Rule> rules;
        std::unique_ptr<CData> cdata;  // to support low-latency reindexing, we want just a pointer instead of value semantics here
        TombstoneList phrasesFree;
        TombstoneList rulesFree;
        PIMPL_VAR(Metrics, metrics)
        int livePhraseCount = 0;
        int liveRuleCount = 0;

    public:
        PIMPL_TYPEDEF(ReindexData, PReindexData)
        
        CRuleDB(DBConfig& config, MetricRegistry& metricRegistry);
        bool add(int64_t ruleId, void* ruleData, size_t ruleLength);
        bool remove(int64_t ruleId);
        bool contains(int64_t ruleId);
        std::vector<int64_t>* match(SimpleStringIterator input);
        PReindexData prepareReindex(CancellationToken cancel);
        void applyReindex(ReindexData& data);
        std::pair<bool, std::string> debugGetRuleString(int64_t ruleId);

        DISABLE_COPY_CONSTRUCTOR(CRuleDB)
    };
}
