/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include <immintrin.h>

namespace burningmime::setmatch
{
    typedef int8_t BitBlock8;
    typedef int16_t BitBlock16;
    typedef int32_t BitBlock32;
    typedef int64_t BitBlock64;
    typedef __m128i BitBlock128;
    typedef __m256i BitBlock256;
    
    ALWAYS_INLINE inline BitBlock128 loadUnaligned128(const void* __restrict__ data) { return _mm_loadu_si128((BitBlock128*) data); }
    ALWAYS_INLINE inline BitBlock128 loadAligned128(const void* __restrict__ data) { return _mm_load_si128((BitBlock128*) data); }
    ALWAYS_INLINE inline BitBlock256 loadUnaligned256(const void* __restrict__ data) { return _mm256_loadu_si256((BitBlock256*) data); }
    ALWAYS_INLINE inline BitBlock256 loadAligned256(const void* __restrict__ data) { return _mm256_load_si256((BitBlock256*) data); }

    ALWAYS_INLINE inline bool matchBlock128(const BitBlock128& __restrict__ mine, const BitBlock128& __restrict__ theirs)
    {
        // TODO there's a big penalty when changing to use XMM from YMM: https://stackoverflow.com/questions/7839925/using-avx-cpu-instructions-poor-performance-without-archavx
        // should vzeroupper and use YMM only
        return (bool) _mm_testc_si128(theirs, mine);
    }

    template<bool mineAligned, bool theirsAligned>
    ALWAYS_INLINE inline bool matchBlock128(const void* __restrict__ myData, const void* __restrict__ theirData)
    {
        BitBlock128 mine = mineAligned ? loadAligned128(myData) : loadUnaligned128(myData);
        BitBlock128 theirs = theirsAligned ? loadAligned128(theirData) : loadUnaligned128(theirData);
        return matchBlock128(mine, theirs);
    }

    ALWAYS_INLINE inline bool matchBlock256(const BitBlock256& __restrict__ mine, const  BitBlock256& __restrict__ theirs)
    {
        return (bool) _mm256_testc_si256(theirs, mine);
    }

    template<bool mineAligned, bool theirsAligned>
    ALWAYS_INLINE inline bool matchBlock256(const void* __restrict__ myData, const void* __restrict__ theirData)
    {
        BitBlock256 mine = mineAligned ? loadAligned256(myData) : loadUnaligned256(myData);
        BitBlock256 theirs = theirsAligned ? loadAligned256(theirData) : loadUnaligned256(theirData);
        return matchBlock256(mine, theirs);
    }

    template<bool mineAligned>
    ALWAYS_INLINE inline bool matchBlock256(const void* __restrict__ myData, const BitBlock256& __restrict__ theirs)
    {
        BitBlock256 mine = mineAligned ? loadAligned256(myData) : loadUnaligned256(myData);
        return matchBlock256(mine, theirs);
    }

    template<typename T>
    ALWAYS_INLINE inline bool matchBlockSmall(const T mine, const T theirs)
    {
        return (mine & theirs) == mine;
    }
}
