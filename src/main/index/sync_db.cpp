/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "sync_db.hpp"
#include "../common/metric.hpp"

namespace burningmime::setmatch
{
    struct SRuleDB::Metrics
    {
        ExponentialDecayMetric& matchMs;
        SingleValueMetric& reindexTotalMs;
        SingleValueMetric& reindexPrepareMs;
        SingleValueMetric& reindexApplyMs;

        explicit Metrics(MetricRegistry& reg) :
            matchMs(reg.add(new ExponentialDecayMetric("match.timers.totalMs.weightedMean"))),
            reindexTotalMs(reg.add(new SingleValueMetric("reindex.totalMs.mostRecent"))),
            reindexPrepareMs(reg.add(new SingleValueMetric("reindex.prepareMs.mostRecent"))),
            reindexApplyMs(reg.add(new SingleValueMetric("reindex.applyMs.mostRecent")))
        {
        }
    }; PIMPL_IMPL(SRuleDB::Metrics)

    SRuleDB::SRuleDB(DBConfig config) :
        config(config),
        index(config, metricRegistry),
        metrics(config.collectMetrics ? new Metrics(metricRegistry) : nullptr)
    {
    }

    bool SRuleDB::add(int64_t ruleId, void* ruleData, size_t ruleLength)
    {
        auto lock = mutex.writeLock();
        bool ret = index.add(ruleId, ruleData, ruleLength);
        changesSinceLastReindex++;
        return ret;
    }

    bool SRuleDB::remove(int64_t ruleId)
    {
        auto lock = mutex.writeLock();
        bool ret = index.remove(ruleId);
        if(ret) changesSinceLastReindex++;
        return ret;
    }

    std::vector<int64_t>* SRuleDB::match(SimpleStringIterator input)
    {
        SystemTime start = now();
        auto lock = mutex.readLock();
        METRIC_TIMER_T(metrics, matchMs, start);
        return index.match(input);
    }

    static ThreadLocal<std::vector<MetricValue>> tlMetricBuf;
    std::vector<MetricValue>& SRuleDB::getMetrics()
    {
        if(!metrics)
            setmatchRaise(FEATURE_DISABLED_BY_CONFIG , "This database was created with collectMetrics = false");
        auto lock = mutex.readLock();
        auto& results = *tlMetricBuf;
        results.resize(metricRegistry.size());
        for(size_t i = 0; i < metricRegistry.size(); ++i)
        {
            auto& tmp = metricRegistry[i];
            results[i].name = tmp.name();
            results[i].value = tmp.get();
        }
        return results;
    }

    std::pair<bool, std::string> SRuleDB::debugGetRuleString(int64_t ruleId)
    {
        auto lock = mutex.readLock();
        return index.debugGetRuleString(ruleId);
    }

    bool SRuleDB::contains(int64_t ruleId)
    {
        auto lock = mutex.readLock();
        return index.contains(ruleId);
    }

    void SRuleDB::reindex()
    {
        METRIC_TIMER(metrics, reindexTotalMs);

        {
            // this is in a scope so it's freed before we report time
            CRuleDB::PReindexData reindex;

            {
                // this is in a scope because we want to release lock before freeing data
                auto lock = mutex.upgradeLock();
                if(changesSinceLastReindex == 0)
                {
                    logInfo("Skipping reindex because no changes have been made since last reindex");
                    return;
                }

                {
                    // this is another scope so the timer only measures this part
                    METRIC_TIMER(metrics, reindexPrepareMs);
                    reindex = index.prepareReindex(mutex.token());
                    logInfo("Preparing reindex took %.4f ms", reindexPrepareMs.elapsed());
                }

                // upgrade our read lock to a write lock
                if(mutex.isCancelled()) return;
                lock.upgrade();
                if(mutex.isCancelled()) return; // check again after acquiring lock

                {
                    // this is another scope so the timer only measures this part
                    METRIC_TIMER(metrics, reindexApplyMs);
                    index.applyReindex(*reindex);
                    changesSinceLastReindex = 0;
                    logInfo("Applying reindex took %.4f ms", reindexApplyMs.elapsed());
                }
            }
        }

        logInfo("Total reindex time (including time for locks and frees): %.4f ms", reindexTotalMs.elapsed());
    }
}
