/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include "../common/bitset.hpp"
#include "../common/hash.hpp"
#include <vector>
#include <string>

namespace burningmime::setmatch
{
    MAKE_UINT32_WRAPPER(TokenId)

    /**
     * This class stores the actual string tokens and maps them to integer keys used by the phrase trie and interval
     * set.
     */
    class TokenMap
    {
        struct KeyAndRefCount { KeyString key; uint32_t refOwners; };
        HashMap<HashedString, TokenId, HashedString::Hash> tokenToId;
        std::vector<KeyAndRefCount> idToToken;
        TombstoneList freeList;
    public:
        enum class OwnerClass : uint32_t
        {
            PHRASE_TRIE = 1,
            INTERVAL_INDEX = 2,
            // note this is used as a bitset, so values should be 1, 2, 4, 8...
        };

        TokenMap() = default;
        DISABLE_COPY_CONSTRUCTOR(TokenMap)

        TokenId getOrAdd(HashedString str, OwnerClass cl);
        void release(TokenId id, OwnerClass cl);
        TokenId operator[](HashedString str);
        HashedString operator[](TokenId id);
        inline size_t size() { return tokenToId.size(); }
    };

    class Predicate
    {
    private:
        std::vector<int> mSubrulesPos;
        std::vector<int> mSubrulesNeg;
    protected:
        int index = -1;
    public:
        virtual PredicateType getType() = 0;
        inline int getIndex() { return index; }
        inline bool isNew() { return index == -1; }
        virtual void setIndex(int newIndex) { index = newIndex; } // virtual because phrases use it differently
        inline std::vector<int>& subrulesPos() { return mSubrulesPos; }
        inline std::vector<int>& subrulesNeg() { return mSubrulesNeg; }
        virtual std::string toString(TokenMap& tokenMap) = 0;
    };

}
