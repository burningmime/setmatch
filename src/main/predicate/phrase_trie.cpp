/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

//======================================================================================================================
//                                         SURGEON GENERAL'S WARNING
//
// This is very old code from when I was first learning C++. It is written in a very C-like style, with
// explicit memory management and avoidance of actual types. Some of it has been updated a bit, but overall it is
// still very ugly. It doesn't help much that the underlying algorithm is a bit complex, and is further made worse
// because I didn't want to use pointers-to-pointers, instead opting to track and replace pointers manually. Finally,
// there's the compression of structures (ie using 6 bytes instead of 8 bytes for pointers in some places, or impacting
// enum values into the low bits of pointers). Taken together, this means this code is an unreadable, under-commented
// nightmare.
//======================================================================================================================

#include "pred_index.hpp"
#include <sstream>

namespace burningmime::setmatch
{
    //==================================================================================================================
    // Only the bottom 48 bits of 64-bit pointers are used (but you need to sign extend the top 16 anyway)
    //==================================================================================================================

    template<typename T> static T* fetchMiniPointer(const char* where)
    {
        int64_t value = *((int64_t*) where);
        return (T*) ((value << 16) >> 16);
    }

    template<typename T> static void storeMiniPointer(char* where, T* ptr)
    {
        *((uint32_t*) where) = (uint32_t) ((int64_t) ptr);
        *((uint16_t*) (where + 4)) = (uint16_t) (((int64_t) ptr) >> 32);
    }

    //==================================================================================================================
    // Term class, forward declarations & basic stuff
    //==================================================================================================================

    struct PhraseTrie::Phrase final : public Predicate
    {
        PhraseTrie::Term* lastTerm;
        explicit inline Phrase(PhraseTrie::Term* _lastTerm) : lastTerm(_lastTerm) { }
        void setIndex(int newIndex) override;
        std::string toString(TokenMap& tokenMap) override;
        PredicateType getType() override;
        DISABLE_COPY_CONSTRUCTOR(Phrase)
    };

    // WARNING: we need to be extremely careful with this, especially where we keep pointers to it and the memory management
    // thereof. that's because there's actually 4 versions of the term with different sizes (probably could be subclasses)
    //
    // - terminal nodes (with no children) are stored in a small struct containing only the string (which is inserted at the
    //       end of the term struct for better memory locality) and the required pointers (parent, fail state, outputs). it is
    //       expected that the majority of nodes will fall into this category.
    // - nodes with a single child (common in long phrases) are stored in a slightly larger struct with the child pointer and
    //       child token impacted at the end
    // - nodes with 2-6 children are stored with a list embedded at the end of the structure. both the pointers
    //       (compressed to 6 bytes) and the token IDs are stored. a lookup involves a linear scan of the list, which
    //       exists on the same cache line (plus immediate next) as the rest of the token structure.
    // - nodes with many children are promoted to a hashtable, which can dynamically grow and shrink in external memory.
    //       the root always uses a hash table.
    //
    // so anywhere that keeps a Term* pointer is possibly suspect
    struct PhraseTrie::Term
    {
        Term* parent;
        Term* fail;
        size_t phrasePtrAndMode;
        TokenId key;
        int phraseIndex;

        typedef HashMap<TokenId, Term*, TokenId::Hash> ChildMap;
        ASSERT_TYPE_SIZE(ChildMap, 32);

        enum
        {
            MODE_STRING_ONLY = 0,
            MODE_SINGLEGTON = 1,
            MODE_EMBEDDED_LIST = 2,
            MODE_HASH = 3,

            SIZEOF_STRING_ONLY = 32,
            SIZEOF_SINGLETON = 48, // actually 44; last 4 bytes are padding. with mini pointer, could be 42, but who's ever heard of allocating 42 bytes?
            SIZEOF_EMBEDDED_LIST = 96,
            SIZEOF_HASH = 64,

            EMBEDDED_LIST_SIZE = 6,
            OFS_POINTER_START = EMBEDDED_LIST_SIZE * 4,
            OFS_COUNT = EMBEDDED_LIST_SIZE * 10, // PERHAPS rearrange where this is in the struct to keep it on same cache line instead of next?
            HASH_CUTOFF_SIZE = 4,
            OFS_SINGLETON_CHILD = SIZEOF_STRING_ONLY,
            OFS_SINGLETON_TOKEN = SIZEOF_STRING_ONLY + 8,
        };

        // use these instead of contructor/destructor/new/delete
        static Term* allocRoot();
        static Term* allocEmpty(TokenId key);
        static void release(Term* term);
        static void destructRecursive(Term* term);


        // doing get/set for everything here because this might eventually be compressed in some strange way
        ALWAYS_INLINE inline bool isRoot() { return parent == nullptr; }
        ALWAYS_INLINE inline Term* getParent() { return parent; }
        ALWAYS_INLINE inline void setParent(Term* value) { parent = value; }
        ALWAYS_INLINE inline Term* getFail() { return fail; }
        ALWAYS_INLINE inline void setFail(Term* value) { fail = value; }
        ALWAYS_INLINE inline int getPhraseIndex() { return phraseIndex; }
        ALWAYS_INLINE inline Phrase* getPhrasePtr() { return reinterpret_cast<Phrase*>(phrasePtrAndMode & ~3ull); }
        ALWAYS_INLINE inline int getMode() { return (int) (phrasePtrAndMode & 3ull); }
        ALWAYS_INLINE inline TokenId getToken() { return key; }
        void setPhrase(Phrase* value);
        void toStringRecursive(std::ostringstream& ss, TokenMap& tokenMap);

        // data can be in several different formats
        int getChildCount();
        bool tryGetChild(TokenId token, Term** out);
        Term* addChild(Term* child);
        Term* removeChild(Term* child);
        void replaceChild(Term* old, Term* next);

        static Term* promoteToSingleton(Term* old);
        static Term* promoteToList(Term* old);
        static Term* promoteToHash(Term* old);
        static Term* demoteToList(Term* old);

        // NOTE ABOUT DEMOTION: we only ever demote from a hash to a list. this is because the hash can take up an
        // unbounded ammount of external memory. in other cases, we assume that if a term had X children at one point,
        // it might have that number again, so it's not worth the cost of reallocation to save a few bytes.

        // singleton mode helpers
        ALWAYS_INLINE inline Term*& singletonChild() { return *((Term**) (((char*) this) + OFS_SINGLETON_CHILD)); }
        ALWAYS_INLINE inline TokenId& singletonToken() { return *((TokenId*) (((char*) this) + OFS_SINGLETON_TOKEN)); }

        // list mode helpers
        void addToList(Term* child);
        void removeFromList(Term* child);
        ALWAYS_INLINE inline TokenId* listToken(int i) { return (TokenId*) childrenList((size_t) i * 4); }
        ALWAYS_INLINE inline char* childrenList() { return ((char*) this) + SIZEOF_STRING_ONLY; }
        ALWAYS_INLINE inline char* childrenList(size_t ofs) { return ((char*) this) + SIZEOF_STRING_ONLY + ofs; }

        // hash mode helpers
        ALWAYS_INLINE inline ChildMap& childrenHash() { return *pChildrenHash(); }
        ALWAYS_INLINE inline ChildMap* pChildrenHash() { return (ChildMap*) (((char*) this) + SIZEOF_STRING_ONLY); }

        // not supporting any of these; doing memory management internally
        static void* operator new(size_t) = delete;
        static void operator delete(void* p) = delete;
        Term() = delete;
        ~Term() = delete;
        Term(const Term& other) = delete;
        Term& operator=(const Term& other) = delete;
    } STRUCT_PACKED;

    struct PhraseTrie::Private
    {
        ASSERT_TYPE_SIZE(Term, 32);
        static Term* addOne(PhraseTrie& t, HashedString token, Term* node);
        static void computeFailState(PhraseTrie& t, Term* term);
        static Term* iterNext(TokenId token, Term* pCurrent);
        static void replacePointer(PhraseTrie& t, Term* old, Term* next);
    };

    PhraseTrie::PhraseTrie(TokenMap& _tokenMap) : root(Term::allocRoot()), allTermsByToken(), tokenMap(_tokenMap) { }
    PhraseTrie::~PhraseTrie() { Term::destructRecursive(root); }
    void PhraseTrie::Phrase::setIndex(int newIndex) { index = newIndex; lastTerm->phraseIndex = newIndex; }
    PredicateType PhraseTrie::Phrase::getType() { return PredicateType::PHRASE; }
    void PhraseTrie::Term::setPhrase(Phrase* value)
    {
        xassert((reinterpret_cast<size_t>(value) & 3ull) == 0, "Pointers must at least be a multiple of 4");
        phrasePtrAndMode = (reinterpret_cast<size_t>(value) & ~3ull) | (phrasePtrAndMode & 3ull);
        phraseIndex = value == nullptr ? -1 : value->getIndex();
    }

    //==================================================================================================================
    // Adding a new term or removing an old one
    //==================================================================================================================

    Predicate* PhraseTrie::getOrAdd(SimpleStringIterator input)
    {
        Term* node = root;
        HashedString token;
        while((token = HashedString(input())))
            node = Private::addOne(*this, token, node);
        if(node->getPhrasePtr() == nullptr)
            node->setPhrase(new Phrase(node));
        return node->getPhrasePtr();
    }

    PhraseTrie::Term* PhraseTrie::Private::addOne(PhraseTrie& t, HashedString tokenStr, Term* node)
    {
        Term* next;
        TokenId tokenId = t.tokenMap.getOrAdd(tokenStr, TokenMap::OwnerClass::PHRASE_TRIE);
        if(!node->tryGetChild(tokenId, &next))
        {
            auto it = t.allTermsByToken.find(tokenId);
            bool isNewToken;
            if(it == t.allTermsByToken.end())
            {
                // this is the first node using the token
                // make a copy of the string in app-controlled memory
                TermVector vec;
                next = Term::allocEmpty(tokenId);
                vec.push_back(next);
                t.allTermsByToken.insert_or_assign(tokenId, std::move(vec));
                isNewToken = true;
            }
            else
            {
                next = Term::allocEmpty(it->first);
                isNewToken = false;
            }
            Term* newNode = node->addChild(next);
            if(node != newNode)
                replacePointer(t, node, newNode);
            dassert(next->getParent() == newNode, "Failed to set parent properly");
            if(!isNewToken)
            {
                // update fail states for other tokens
                // do this after inserting the token itself
                for(auto it2 : it->second)
                    computeFailState(t, it2);
                // then add this token (if it's a new token, that was done above)
                it->second.push_back(next);
            }
            computeFailState(t, next);
        }
        return next;
    }

    void PhraseTrie::Private::computeFailState(PhraseTrie& t, Term* term)
    {
        // find the closest child of an ancestor that uses the same token. we start by going 2 levels up (since
        // obviously the parent has a child with this token)
        xassert(term != t.root, "trying to compute fail state for root");
        if(term->getParent() != t.root)
        {
            Term* fail = term->getParent()->getFail();
            TokenId token = term->getToken();
            while(fail != nullptr)
            {
                Term* found;
                if(fail->tryGetChild(token, &found))
                {
                    term->setFail(found);
                    return;
                }
                fail = fail->getFail();
            }
        }
        // if we get here, just fail to the root
        term->setFail(t.root);
    }

    //==================================================================================================================
    // Removing a node
    //==================================================================================================================

    void PhraseTrie::removeAndFree(Predicate* predicate)
    {
        xassert(predicate != nullptr, "Should have valid predicate here");
        xassert(predicate->getType() == PredicateType::PHRASE, "predicate does not point to a phrase");
        auto phrase = (Phrase*) predicate;

        // need to work backwards so the fail states of other terms are correctly updated
        Term* term = phrase->lastTerm;
        xassert(term->getPhrasePtr() == phrase, "the term the phrase points to doesn't have the phrase as its output -- "
                "something went wrong while building or updating the trie");
        term->setPhrase(nullptr);
        while(term != nullptr && term != root)
        {
            // if a term has any children, we leave it in the tree as a non-productive node, otherwise we can free it
            if(term->getChildCount() == 0 && term->getPhrasePtr() == nullptr)
            {
                TokenId token = term->getToken();

                // remove the term from its parent
                Term* oldParent = term->getParent();
                Term* next = oldParent->removeChild(term);
                if(oldParent != next)
                    Private::replacePointer(*this, oldParent, next);

                auto it = allTermsByToken.find(token);
                xassert(it != allTermsByToken.end(), "Could not find token %d in dictionary", *token);
                TermVector& termVec = it->second;
                xassert(!termVec.empty(), "Should never have empty vectors in here");
                if(termVec.size() == 1)
                {
                    xassert(termVec[0] == term, "Wrong term pointer in hash");
                    tokenMap.release(token, TokenMap::OwnerClass::PHRASE_TRIE);
                    allTermsByToken.erase(it);
                }
                else
                {
                    auto removeAt = termVec.end();
                    for(auto it2 = termVec.begin(); it2 != termVec.end(); ++it2)
                    {
                        auto other = *it2;
                        if(other == term)
                        {
                            xassert(removeAt == termVec.end(), "Found term in vector twice");
                            removeAt = it2;
                        }
                        else if(other->getFail() == term)
                        {
                            // fix up every term that failed to this one
                            Private::computeFailState(*this, other);
                        }
                    }
                    xassert(removeAt != termVec.end(), "Did not find place to remove from term vec");
                    termVec.erase(removeAt);
                }

                // free the term itself
                Term::release(term);

                // loop into parent
                term = next;
            }
            else
            {
                break;
                // we won't be able to free any of the node's parents since they will still be in use
            }
        }

        // at long last, we can delete the phrase
        delete phrase;
    }

    //==================================================================================================================
    // Replacing a pointer
    //==================================================================================================================

    void PhraseTrie::Private::replacePointer(PhraseTrie& t, Term* old, Term* next)
    {
        if(old == next) return;
        xassert(!old->isRoot() && !next->isRoot(), "These should never be the root");

        // we need to replace every pointer that we had for the old term with the new one
        // first, go through our hash. we need to replace the term itself, but also update the fail state
        // of any terms that failed to that term
        auto it = t.allTermsByToken.find(old->getToken());
        xassert(it != t.allTermsByToken.end(), "Could not find %d in the hash", *(old->getToken()));
        TermVector& termVec = it->second;
        bool found = false;
        for(auto it2 = termVec.begin(); it2 != termVec.end(); ++it2)
        {
            Term* other = *it2;
            if(other == old)
            {
                xassert(!found, "Found %d twice in the hash", *(old->getToken()));
                found = true;
                *it2 = next;
            }
            else if(other->getFail() == old)
            {
                other->setFail(next);
            }
        }
        xassert(found, "Did not find old pointer when replacing %d", *(old->getToken()));

        // next, replace the old term in the parent
        next->getParent()->replaceChild(old, next);

        // if we're the end of a phrase, we need to replace it there, too
        Phrase* phrasePtr = old->getPhrasePtr();
        if(phrasePtr)
            phrasePtr->lastTerm = next;

        // at long last, we can free the old term
        Term::release(old);
    }

    //==================================================================================================================
    // Iterating matches
    //==================================================================================================================

    void PhraseTrie::match(const std::vector<TaggedToken>& input, BitArray& output)
    {
        Term* root = this->root;
        Term* current = root;
        for(TaggedToken tt : input)
        {
            TokenId tok = tt.token;
            if(!tok.isValid())
                continue;
            current = PhraseTrie::Private::iterNext(tok, current);
            Term* retrace = current;
            while(retrace != root)
            {
                dassert(retrace != nullptr, "should always reach root before nullptr");
                int pindex = retrace->getPhraseIndex();
                if(pindex != -1)
                    output.set(pindex);
                retrace = retrace->getFail();
            }
        }
    }

    PhraseTrie::Term* PhraseTrie::Private::iterNext(TokenId token, Term* current)
    {
        // this logic is kind of awkward, especially with the retrace stuff going on above, but whatever
        while(true)
        {
            Term* next;
            if(current->tryGetChild(token, &next))
                return next;
            Term* fail = current->getFail();
            if(fail == nullptr)
                return current; // should be root
            current = fail;
        }
    }

    //==================================================================================================================
    // Debugging help for phrase
    //==================================================================================================================

    void PhraseTrie::Term::toStringRecursive(std::ostringstream& ss, TokenMap& tokenMap)
    {
        xassert(!isRoot(), "should never get to the root here");
        if(!getParent()->isRoot())
        {
            getParent()->toStringRecursive(ss, tokenMap);
            ss << ' ';
        }
        HashedString token = tokenMap[getToken()];
        ss << std::string(token.str, (size_t) token.length);
    }

    std::string PhraseTrie::Phrase::toString(TokenMap& tokenMap)
    {
        std::ostringstream ss;
        lastTerm->toStringRecursive(ss, tokenMap);
        return ss.str();
    }

    //==================================================================================================================
    // Memory management
    //==================================================================================================================

    PhraseTrie::Term* PhraseTrie::Term::allocRoot()
    {
        Term* term = (Term*) smAlloc(SIZEOF_HASH);
        memset((void*) term, 0, SIZEOF_HASH);
        new (term->pChildrenHash()) ChildMap();
        term->phraseIndex = -1;
        term->phrasePtrAndMode = (size_t) MODE_HASH;
        return term;
    }

    PhraseTrie::Term* PhraseTrie::Term::allocEmpty(TokenId key)
    {
        Term* term = (Term*) smAlloc(SIZEOF_STRING_ONLY);
        memset((void*) term, 0, SIZEOF_STRING_ONLY);
        term->key = key;
        term->phraseIndex = -1;
        return term;
    }

    PhraseTrie::Term* PhraseTrie::Term::promoteToSingleton(Term* old)
    {
        xassert(old->getMode() == MODE_STRING_ONLY, "old.mode = %d", old->getMode());
        Term* term = (Term*) smAlloc(SIZEOF_SINGLETON);
        memset((void*) term, 0, SIZEOF_SINGLETON);
        term->key = old->key;
        term->parent = old->parent;
        term->fail = old->fail;
        term->phrasePtrAndMode = (old->phrasePtrAndMode & ~3ull) | MODE_SINGLEGTON;
        term->phraseIndex = old->phraseIndex;
        return term;
    }

    PhraseTrie::Term* PhraseTrie::Term::promoteToList(Term* old)
    {
        xassert(old->getMode() == MODE_SINGLEGTON, "old.mode = %d", old->getMode());
        xassert(old->singletonChild() != nullptr, "If this was null, we don't need to promote");
        Term* term = (Term*) smAlloc(SIZEOF_EMBEDDED_LIST);
        memset((void*) term, 0, SIZEOF_EMBEDDED_LIST);
        term->key = old->key;
        term->parent = old->parent;
        term->fail = old->fail;
        term->phrasePtrAndMode = (old->phrasePtrAndMode & ~3ull) | MODE_EMBEDDED_LIST;
        term->phraseIndex = old->phraseIndex;
        term->addToList(old->singletonChild());
        return term;
    }

    PhraseTrie::Term* PhraseTrie::Term::promoteToHash(Term* old)
    {
        xassert(old->getMode() == MODE_EMBEDDED_LIST, "old.mode = %d", old->getMode());
        Term* term = (Term*) smAlloc(SIZEOF_HASH);
        memset((void*) term, 0, SIZEOF_HASH);
        term->key = old->key;
        term->parent = old->parent;
        term->fail = old->fail;
        term->phrasePtrAndMode = (old->phrasePtrAndMode & ~3ull) | MODE_HASH;
        term->phraseIndex = old->phraseIndex;

        // copy in children list
        new (term->pChildrenHash()) ChildMap();
        int nChildren = *old->childrenList(OFS_COUNT);
        for(int i = 0; i < nChildren; i++)
        {
            Term* child = fetchMiniPointer<Term>(old->childrenList(OFS_POINTER_START + i*6));
            term->childrenHash()[child->getToken()] = child;
            child->setParent(term);
        }
        return term;
    }

    PhraseTrie::Term* PhraseTrie::Term::demoteToList(Term* old)
    {
        xassert(old->getMode() == MODE_HASH, "old.mode = %d", old->getMode());
        xassert(!old->isRoot(), "Should never demote root");
        Term* term = (Term*) smAlloc(SIZEOF_EMBEDDED_LIST);
        memset((void*) term, 0, SIZEOF_EMBEDDED_LIST);
        term->key = old->key;
        term->parent = old->parent;
        term->fail = old->fail;
        term->phrasePtrAndMode = (old->phrasePtrAndMode & ~3ull) | MODE_EMBEDDED_LIST;
        term->phraseIndex = old->phraseIndex;

        int i = 0;
        for(auto it : old->childrenHash())
        {
            Term* child = it.second;
            *term->listToken(i) = child->getToken();
            storeMiniPointer<Term>(term->childrenList(OFS_POINTER_START + i*6), child);
            child->setParent(term);
            i++;
        }
        *term->childrenList(OFS_COUNT) = (char) i;
        return term;
    }

    void PhraseTrie::Term::release(Term* term)
    {
        int mode = term->getMode();
        if(mode == MODE_HASH)
            term->childrenHash().ChildMap::~HashMap(); // call destructor but don't free memory
        smFree(term);
    }

    void PhraseTrie::Term::destructRecursive(Term* term)
    {
        int mode = term->getMode();
        if(mode == MODE_SINGLEGTON)
        {
            if(term->singletonChild())
                destructRecursive(term->singletonChild());
        }
        else if(mode == MODE_EMBEDDED_LIST)
        {
            int nChildren = *term->childrenList(OFS_COUNT);
            for(int i = 0; i < nChildren; i++)
                destructRecursive(fetchMiniPointer<Term>(term->childrenList(OFS_POINTER_START + i*6)));
        }
        else if(mode == MODE_HASH)
        {
            for(auto it : term->childrenHash())
                destructRecursive(it.second);
        }
        if(term->getPhrasePtr())
            delete term->getPhrasePtr();
        release(term);
    }

    //==================================================================================================================
    // Terms have 4 forms for storing children; see above
    //==================================================================================================================

    int PhraseTrie::Term::getChildCount()
    {
        int mode = getMode();
        return
            mode == MODE_STRING_ONLY ? 0 :
            mode == MODE_SINGLEGTON ? (singletonChild() ? 1 : 0) :
            mode == MODE_EMBEDDED_LIST ? (int) *childrenList(OFS_COUNT) :
            (int) childrenHash().size();
    }

    bool PhraseTrie::Term::tryGetChild(TokenId tok, Term** out)
    {
        dassert(tok.isValid(), "Trying to get child for null token why?");
        int mode = getMode();
        if(mode == MODE_STRING_ONLY)
        {
            return false;
        }
        else if(mode == MODE_SINGLEGTON)
        {
            if(tok != singletonToken())
                return false;
            *out = singletonChild();
            return LIKELY(singletonChild() != nullptr);
        }
        else if(mode == MODE_EMBEDDED_LIST)
        {
            int nChildren = *childrenList(OFS_COUNT);
            #define TRY_GET_CHILD_UNROLL(N) \
            { \
                if(nChildren <= N) \
                    return false; \
                TokenId ctok = *listToken(N); \
                if(tok == ctok) \
                { \
                    *out = fetchMiniPointer<Term>(childrenList(OFS_POINTER_START + N * 6)); \
                    return true; \
                } \
            }
            TRY_GET_CHILD_UNROLL(0)
            TRY_GET_CHILD_UNROLL(1)
            TRY_GET_CHILD_UNROLL(2)
            TRY_GET_CHILD_UNROLL(3)
            TRY_GET_CHILD_UNROLL(4)
            TRY_GET_CHILD_UNROLL(5)
            #undef TRY_GET_CHILD_UNROLL
            return false;
        }
        else
        {
            return tryGetValue(childrenHash(), tok, out);
        }
    }

    void PhraseTrie::Term::replaceChild(Term* old, Term* next)
    {
        xassert(old->getToken().isValid(), "Must have a valid string here");
        xassert(old->getToken() == next->getToken(), "Must replace a node with one for the same string");
        int mode = getMode();
        if(mode == MODE_SINGLEGTON)
        {
            xassert(singletonChild() == old, "Singleton had a different child");
            singletonChild() = next;
            next->setParent(this);
        }
        else if(mode == MODE_EMBEDDED_LIST)
        {
            int nChildren = *childrenList(OFS_COUNT);
            for(int i = 0; i < nChildren; i++)
            {
                Term* child = fetchMiniPointer<Term>(childrenList(OFS_POINTER_START + i*6));
                if(child == old)
                {
                    xassert(*listToken(i) == next->getToken(), "Wrong hash code!");
                    storeMiniPointer<Term>(childrenList(OFS_POINTER_START + i*6), next);
                    next->setParent(this);
                    return;
                }
            }
            xassert_false("Could not find child in list");
        }
        else if(mode == MODE_HASH)
        {
            auto it = childrenHash().find(old->getToken());
            xassert(it != childrenHash().end(), "Could not find child in hash");
            xassert(it->second == old, "Not erasing the right element (maybe 2 of the same term somehow ended up in the hash?)");
            childrenHash().erase(it);
            childrenHash()[next->getToken()] = next;
            next->setParent(this);
        }
        else
        {
            xassert_false("replaceChild() should always be called on a node with children");
        }
    }

    void PhraseTrie::Term::addToList(Term* child)
    {
        int nChildren = *childrenList(OFS_COUNT);
        *listToken(nChildren) = child->getToken();
        storeMiniPointer<Term>(childrenList(OFS_POINTER_START + nChildren * 6), child);
        *childrenList(OFS_COUNT) = (char) (nChildren + 1);
        child->setParent(this);
    }

    PhraseTrie::Term* PhraseTrie::Term::addChild(Term* child)
    {
        int mode = getMode();
        if(mode == MODE_STRING_ONLY)
        {
            Term* next = promoteToSingleton(this);
            next->singletonChild() = child;
            next->singletonToken() = child->getToken();
            child->setParent(next);
            return next;
        }
        else if(mode == MODE_SINGLEGTON)
        {
            if(singletonChild() == nullptr)
            {
                singletonChild() = child;
                singletonToken() = child->getToken();
                child->setParent(this);
                return this;
            }
            else
            {
                Term* next = promoteToList(this);
                next->addToList(child);
                return next;
            }
        }
        if(mode == MODE_EMBEDDED_LIST)
        {
            int nChildren = *childrenList(OFS_COUNT);
            if(nChildren == EMBEDDED_LIST_SIZE)
            {
                // need to promote to a hash
                Term* next = promoteToHash(this);
                next->childrenHash()[child->getToken()] = child;
                child->setParent(next);
                return next;
            }
            else
            {
                addToList(child);
                return this;
            }
        }
        else
        {
            childrenHash()[child->getToken()] = child;
            child->setParent(this);
            return this;
        }
    }

    void PhraseTrie::Term::removeFromList(Term* child)
    {
        int nChildren = *childrenList(OFS_COUNT);
        xassert(nChildren > 0, "can't remove from an empty list");
        int i = 0, j;
        for(; i < nChildren; i++)
            if(child == fetchMiniPointer<Term>(childrenList(OFS_POINTER_START + i*6)))
                break;
        xassert(i < nChildren, "didn't find term to remove in child list");
        for(j = i; j < nChildren - 1; j++)
            *listToken(j) = *listToken(j + 1);
        for(; j < EMBEDDED_LIST_SIZE; j++) // while we don't have to do this, delete isn't called in any hot loops, so let's just do it for debug purposes
            *listToken(j) = TokenId::none();
        for(j = i; j < nChildren - 1; j++)
            storeMiniPointer<Term>(childrenList(OFS_POINTER_START + j*6), fetchMiniPointer<Term>(childrenList(OFS_POINTER_START + (j+1)*6)));
        for(; j < EMBEDDED_LIST_SIZE; j++)
            storeMiniPointer<Term>(childrenList(OFS_POINTER_START + j*6), nullptr);
        *childrenList(OFS_COUNT) = (char) (nChildren - 1);
    }

    PhraseTrie::Term* PhraseTrie::Term::removeChild(Term* child)
    {
        int mode = getMode();
        if(mode == MODE_SINGLEGTON)
        {
            xassert(singletonChild() == child, "Singleton has wrong child");
            singletonChild() = nullptr;
            singletonToken() = TokenId::none();
            return this;
        }
        else if(mode == MODE_EMBEDDED_LIST)
        {
            int nChildren = *childrenList(OFS_COUNT);
            xassert(nChildren > 0, "can't remove from an empty list");
            removeFromList(child); // never demote from a list to an empty string; assume it might get another child eventually
            return this;
        }
        else if(mode == MODE_HASH)
        {
            auto it = childrenHash().find(child->getToken());
            xassert(it != childrenHash().end(), "didn't find token in hash when trying to remove child");
            xassert(it->second == child, "the child pointer in the hash is not the same as the child we're removing");
            childrenHash().erase(it);
            if(!isRoot() && childrenHash().size() <= HASH_CUTOFF_SIZE)
                return demoteToList(this);
            else
                return this;
        }
        else
        {
            xassert_false("Cannot remove child from empty node");
            return this;
        }
    }
}
