/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "pred_index.hpp"
#include <sstream>
#include "../../thirdparty/btree/btree_map.h"
#include <boost/function_output_iterator.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>
namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

// Handling every case perfectly is tricky without knowing much about the user's data and access patterns. So this
// is kind of a "first draft" that will work OK in some cases and shouldn't ever be disastrous, but may use up WAY more
// memory than needed in a few cases.
//
// We're gonna need to allocate a Predicate object per range anyway, since it's passed back to the index. So if a token
// only refers to a single range, we just store that range pointer directly. I don't think this will be a very common
// case (because why use intervals at all?), but it's low impact to have this optimization.
//
// The second case is for tokens which only refer to 8 or fewer ranges. For example, consider the set:
// {age<18, age:18-25, age:25-35, age:35-50, age:>50}. These are stored, unsorted, in a preallocated array. It's
// allocated just once (might want to eventually change this to be dynamic or even use a vector).
//
// Finally, if there are dozens or more different ranges, we don't want to hit the worst case of scanning them all.
// So, these are split into 4 sub-collections. One btree is used for all "less than X" intervals, one btree for
// "greater than X", an rtree for "between X and Y", and a hashtable for "equal to X". These structures all have good
// worst-case complexity guaruntees. The hope is that very few tokens will need these full indexes, while most of them
// can use the smaller index (and, really, only a few tokens should ever need the interval index in an average database).

namespace burningmime::setmatch
{
    //==================================================================================================================
    // Range type -- used as both the predicate and a node set consisting only of itself
    //==================================================================================================================

    struct IntervalIndex::Range final : public Predicate
    {
        TokenId token;
        Interval interval;

        std::string toString(TokenMap& tokenMap) override
        {
            std::ostringstream ss;
            ss << std::string(tokenMap[token].str, tokenMap[token].length) << "[" <<
               interval.getLeft() << "," << interval.getRight() << "]";
            return ss.str();
        }

        PredicateType getType() override { return PredicateType::RANGE; }
        Range(TokenId token, Interval interval) : token(token), interval(interval) { }
        DISABLE_COPY_CONSTRUCTOR(Range)
    };

    //==================================================================================================================
    // List set -- if there are only a few intervals, we can just check them all
    // TODO do we actually want to store the intervals in the list too instead of chasing pointers?
    //==================================================================================================================

    struct IntervalIndex::ListNodeSet
    {
        static constexpr int LIST_SET_SIZE = 8;
        Range* list[LIST_SET_SIZE];

        struct GetOrAddResult { Range* range; bool overflow; };
        GetOrAddResult getOrAdd(TokenId token, Interval interval)
        {
            dassert(list[0] != nullptr && list[1] != nullptr, "should always have at least 2 elements");
            int insertPos = 0; // need to loop through first two even if non null since they might be same interval
            for(; insertPos < LIST_SET_SIZE; ++insertPos)
            {
                if(!list[insertPos])
                    break;
                else if(list[insertPos]->interval == interval)
                    return {list[insertPos], false};
            }

            Range* next = new Range(token, interval);
            if(insertPos == LIST_SET_SIZE)
            {
                return {next, true};
            }
            else
            {
                list[insertPos] = next;
                return {next, false};
            }
        }

        Range* remove(Range* range)
        {
            int removeAt = 0;
            for(; removeAt < LIST_SET_SIZE; ++removeAt)
                if(list[removeAt] == range)
                    break;

            if(removeAt == LIST_SET_SIZE)
            {
                xassert_false("Did not find range inside list");
            }

            if(!list[2]) // if size is 2, just return the other element
            {
                if(removeAt == 0)
                    return list[1];
                else if(removeAt == 1)
                    return list[0];
                else
                    xassert_false("removeAt should be 0 or 1 if size is 2");
            }
            else
            {
                // shift everything back one
                for(int j = removeAt; j < LIST_SET_SIZE - 1; ++j)
                    list[j] = list[j + 1];
                list[LIST_SET_SIZE - 1] = nullptr;
                return nullptr;
            }
        }

        void search(double value, BitArray& results)
        {
            for(int i = 0; i < LIST_SET_SIZE; ++i)
            {
                if(list[i])
                {
                    Range* range = list[i];
                    if(range->interval.contains(value))
                        results.set(range->getIndex());
                }
                else
                {
                    break;
                }
            }
        }

        void destructInner()
        {
            for(int i = 0; i < LIST_SET_SIZE; ++i)
            {
                if(list[i])
                    delete list[i];
                else
                    break;
            }
        }

        ListNodeSet(Range* x, Range* y)
        {
            list[0] = x;
            list[1] = y;
            for(size_t i = 2; i < LIST_SET_SIZE; ++i)
                list[i] = nullptr;
        }

        DISABLE_COPY_CONSTRUCTOR(ListNodeSet)
    };

    //==================================================================================================================
    // Split node set -- uses hashtable, btree, and/oe rtree depending on what's appropriate for the situation
    //==================================================================================================================

    struct IntervalIndex::SplitNodeSet
    {
        typedef bg::model::point<double, 1, bg::cs::cartesian> RTPoint;
        typedef bg::model::box<RTPoint> RTBox;
        typedef std::pair<RTBox, Range*> RTNode;
        typedef bgi::quadratic<16> RTParams;
        struct RTEquals { bool operator()(const RTNode& x, const RTNode& y) const { return x.second == y.second; } };
        typedef btree::btree_map<double, Range*> BTree;
        typedef HashMap<double, Range*> Hashtable;
        typedef bgi::rtree<RTNode, RTParams, bgi::indexable<RTNode>, RTEquals> RTree;

        BTree greaterThan;
        BTree lessThan;
        Hashtable equalTo;
        RTree betwixt;

        enum class SubCollection { GREATER_THAN, LESS_THAN, EQUAL_TO, BETWIXT };
        ALWAYS_INLINE inline static SubCollection classify(Interval i)
        {
            return
                i.getLeft() == i.getRight() ? SubCollection::EQUAL_TO :
                std::isinf(i.getLeft())     ? SubCollection::LESS_THAN :
                std::isinf(i.getRight())    ? SubCollection::GREATER_THAN :
                                              SubCollection::BETWIXT;
        }

        ALWAYS_INLINE inline static RTBox makeBox(Interval i)
        {
            // we need to use an open interval instead of a closed interval with boost
            double left = nextBeforeFinite(i.getLeft());
            double right = nextAfterFinite(i.getRight());
            return RTBox{RTPoint{left}, RTPoint{right}};
        }

        template<typename TCollection, typename TKey>
        inline static void add(TCollection& collection, TKey key, Range* value)
        {
            collection.insert({key, value});
        }

        inline void add(Range* range)
        {
            Interval interval = range->interval;
            switch(classify(interval))
            {
                case SubCollection::GREATER_THAN: add(greaterThan, interval.getLeft(), range); break;
                case SubCollection::LESS_THAN: add(lessThan, interval.getRight(), range); break;
                case SubCollection::EQUAL_TO: add(equalTo, interval.getLeft(), range); break;
                case SubCollection::BETWIXT: add(betwixt, makeBox(interval), range); break;
                default: UNREACHABLE;
            }
        }

        template<typename TCollection, typename TKey>
        inline static Range* getOrAdd(TCollection& collection, TKey key, TokenId token, Interval interval)
        {
            Range* range;
            if(tryGetValue(collection, key, &range))
                return range;
            range = new Range(token, interval);
            add(collection, key, range);
            return range;
        }

        inline static Range* getOrAdd(RTree& collection, TokenId token, Interval interval)
        {
            double midpoint = (interval.getLeft() + interval.getRight()) / 2;
            for(auto it = collection.qbegin(bgi::contains(RTPoint{midpoint})); it != collection.qend(); ++it)
                if(it->second->interval == interval) // need to chase pointer here because bg::equals() does an epsilon comparison
                    return it->second;
            Range* range = new Range(token, interval);
            add(collection, makeBox(interval), range);
            return range;
        }

        Range* getOrAdd(TokenId token, Interval interval)
        {
            switch(classify(interval))
            {
                case SubCollection::GREATER_THAN: return getOrAdd(greaterThan, interval.getLeft(), token, interval); break;
                case SubCollection::LESS_THAN: return getOrAdd(lessThan, interval.getRight(), token, interval); break;
                case SubCollection::EQUAL_TO: return getOrAdd(equalTo, interval.getLeft(), token, interval); break;
                case SubCollection::BETWIXT: return getOrAdd(betwixt, token, interval); break;
                default: UNREACHABLE;
            }
        }

        template<typename TCollection, typename TKey>
        inline static void remove(TCollection& collection, Range* range, TKey key)
        {
            auto it = collection.find(key);
            xassert(it != collection.end(), "did not find range");
            collection.erase(it);
        }

        inline static void remove(RTree& collection, Range* range, Interval key)
        {
            RTNode node{makeBox(key), range};
            bool removed = (bool) collection.remove(node);
            xassert(removed, "Did not find corresponding element in rtree");
        }

        template<typename TCollection>
        inline static Range* getLastElement(TCollection& collection)
        {
            dassert(collection.size() == 1, "should have exactly one element here");
            auto it = collection.begin();
            dassert(it != collection.end(), "should have gotten valid iterator");
            return it->second;
        }

        Range* remove(Range* range)
        {
            Interval interval = range->interval;
            switch(classify(interval))
            {
                case SubCollection::GREATER_THAN: remove(greaterThan, range, interval.getLeft()); break;
                case SubCollection::LESS_THAN: remove(lessThan, range, interval.getRight()); break;
                case SubCollection::EQUAL_TO: remove(equalTo, range, interval.getLeft()); break;
                case SubCollection::BETWIXT: remove(betwixt, range, interval); break;
                default: UNREACHABLE;
            }

            size_t count = greaterThan.size() + lessThan.size() + equalTo.size() + betwixt.size();
            if(UNLIKELY(count == 1))
            {
                if(!greaterThan.empty()) return getLastElement(greaterThan);
                else if(!lessThan.empty()) return getLastElement(lessThan);
                else if(!equalTo.empty()) return getLastElement(equalTo);
                else if(!betwixt.empty()) return getLastElement(betwixt);
                else xassert_false("where was the last element?");
            }
            else
            {
                xassert(count > 1, "should never be empty");
                return nullptr;
            }
        }

        void search(double value, BitArray& results)
        {
            if(!equalTo.empty())
            {
                Range* range;
                if(tryGetValue(equalTo, value, &range))
                    results.set(range->getIndex());
            }

            if(!lessThan.empty())
            {
                for(auto it = lessThan.lower_bound(value); it != lessThan.end(); ++it)
                    results.set(it->second->getIndex());
            }

            if(!greaterThan.empty())
            {
                // we do nextAfter since it's a "greater than or equal to" relationship; therefore if the last element is equal we want to allow that
                auto end = greaterThan.lower_bound(nextAfterFinite(value));
                for(auto it = greaterThan.begin(); it != end; ++it)
                    results.set(it->second->getIndex());
            }

            if(!betwixt.empty())
            {
                auto callbackFunctor = [&results](const RTNode& pair){ results.set(pair.second->getIndex()); };
                auto outputIterator = boost::make_function_output_iterator(callbackFunctor);
                betwixt.query(bgi::contains(RTPoint{value}), outputIterator);
            }
        }

        template<typename TCollection>
        inline static void destructInner(TCollection& collection)
        {
            for(auto& it : collection)
                delete it.second;
        }

        void destructInner()
        {
            destructInner(greaterThan);
            destructInner(lessThan);
            destructInner(equalTo);
            destructInner(betwixt);
        }

        SplitNodeSet(ListNodeSet* x, Range* y)
        {
            for(Range* z : x->list)
                add(z);
            add(y);
        }

        DISABLE_COPY_CONSTRUCTOR(SplitNodeSet)
    };

    //==================================================================================================================
    // Node Set wrapper type -- a tagged union of pointer using the lowest 2 bits as the tag
    //==================================================================================================================

    struct IntervalIndex::PNodeSet
    {
    private:
        size_t ptr;
    public:
        enum class Kind { RANGE = 0, LIST = 1, SPLIT = 2 };
        ALWAYS_INLINE inline explicit PNodeSet(Range* range) : ptr(reinterpret_cast<size_t>(range)) { }
        ALWAYS_INLINE inline explicit PNodeSet(ListNodeSet* nodeSet) : ptr(reinterpret_cast<size_t>(nodeSet) | 1ull) { }
        ALWAYS_INLINE inline explicit PNodeSet(SplitNodeSet* nodeSet) : ptr(reinterpret_cast<size_t>(nodeSet) | 2ull) { }
        ALWAYS_INLINE inline explicit PNodeSet(size_t ptr) : ptr(ptr) { }
        ALWAYS_INLINE inline operator size_t() const { return ptr; }
        ALWAYS_INLINE inline Kind kind() const { return static_cast<Kind>(ptr & 3ull); }
        ALWAYS_INLINE inline Range* asRange() const { return reinterpret_cast<Range*>(ptr); }
        ALWAYS_INLINE inline ListNodeSet* asList() const { return reinterpret_cast<ListNodeSet*>(ptr & ~3ull); }
        ALWAYS_INLINE inline SplitNodeSet* asSplit() const { return reinterpret_cast<SplitNodeSet*>(ptr & ~3ull); }
    };

    //==================================================================================================================
    // Main methods
    // Note that NodeSet used to be a nice, friendly virtual class without all the implementation details being split
    // all over the place. However, I like optimization. A lot. So I devirtualized it.
    //==================================================================================================================

    Predicate* IntervalIndex::getOrAdd(SimpleString key, Interval interval)
    {
        TokenId token = tokenMap.getOrAdd(HashedString(key), TokenMap::OwnerClass::INTERVAL_INDEX);
        auto it = nodeSets.find(token);
        if(it != nodeSets.end())
        {
            PNodeSet pNodeSet{it->second};
            switch(pNodeSet.kind())
            {
                case PNodeSet::Kind::RANGE:
                {
                    Range* range = pNodeSet.asRange();
                    if(interval == range->interval)
                    {
                        return range;
                    }
                    else
                    {
                        Range* next = new Range(token, interval);
                        it->second = PNodeSet{new ListNodeSet(range, next)};
                        return next;
                    }
                }
                case PNodeSet::Kind::LIST:
                {
                    ListNodeSet* list = pNodeSet.asList();
                    ListNodeSet::GetOrAddResult result = list->getOrAdd(token, interval);
                    if(result.overflow)
                    {
                        it->second = PNodeSet{new SplitNodeSet(list, result.range)};
                        delete list;
                    }
                    return result.range;
                }
                case PNodeSet::Kind::SPLIT:
                {
                    return pNodeSet.asSplit()->getOrAdd(token, interval);
                }
                default:
                    xassert_false("unknown node set kind");
            }
        }
        else
        {
            Range* range = new Range(token, interval);
            nodeSets.emplace(token, PNodeSet{range});
            return range;
        }
    }

    void IntervalIndex::removeAndFree(Predicate* predicate)
    {
        xassert(predicate && predicate->getType() == PredicateType::RANGE, "Invalid predicate or predicate type");
        Range* range = (Range*) predicate;
        TokenId token = range->token;
        auto it = nodeSets.find(token);
        if(UNLIKELY(it == nodeSets.end()))
        {
            HashedString hs = tokenMap[token];
            std::string ss = hs ? std::string(hs.str, hs.length) : "<UNKNOWN>";
            xassert_false("did not find node set for token %s", ss);
        }

        PNodeSet pNodeSet{it->second};
        switch(pNodeSet.kind())
        {
            case PNodeSet::Kind::RANGE:
            {
                if(UNLIKELY(pNodeSet.asRange() != range))
                {
                    HashedString hs = tokenMap[token];
                    std::string ss = hs ? std::string(hs.str, hs.length) : "<UNKNOWN>";
                    xassert_false("different range when trying to delete %s", ss);
                }
                tokenMap.release(range->token, TokenMap::OwnerClass::INTERVAL_INDEX);
                delete range;
                nodeSets.erase(it);
                break;
            }
            case PNodeSet::Kind::LIST:
            {
                ListNodeSet* list = pNodeSet.asList();
                Range* last = list->remove(range);
                if(last)
                {
                    xassert(last != range, "Should have a different item here");
                    it->second = PNodeSet{last};
                    delete list;
                }
                delete range;
                break;
            }
            case PNodeSet::Kind::SPLIT:
            {
                SplitNodeSet* split = pNodeSet.asSplit();
                Range* last = split->remove(range);
                if(last)
                {
                    xassert(last != range, "Should have a different item here");
                    it->second = PNodeSet{last};
                    delete split;
                }
                delete range;
                break;
            }
            default:
                xassert_false("unknown node set kind");
        }
    }

    void IntervalIndex::match(const std::vector<TaggedToken>& input, BitArray& output)
    {
        for(TaggedToken token : input)
        {
            if(token.hasValue())
            {
                auto it = nodeSets.find(token.token);
                if(it != nodeSets.end())
                {
                    double value = flushDenormals(token.value);
                    PNodeSet pNodeSet{it->second};
                    switch(pNodeSet.kind())
                    {
                        case PNodeSet::Kind::RANGE:
                        {
                            Range* range = pNodeSet.asRange();
                            if(range->interval.contains(value))
                                output.set(range->getIndex());
                            break;
                        }
                        case PNodeSet::Kind::LIST:
                        {
                            ListNodeSet* list = pNodeSet.asList();
                            list->search(value, output);
                            break;
                        }
                        case PNodeSet::Kind::SPLIT:
                        {
                            SplitNodeSet* split = pNodeSet.asSplit();
                            split->search(value, output);
                            break;
                        }
                        default:
                            xassert_false("unknown node set kind");
                    }
                }
            }
        }
    }

    IntervalIndex::~IntervalIndex()
    {
        // NOTE: we're assuming the whole world is being torn down, which is why we don't disconnect any of the tokens
        for(auto it : nodeSets)
        {
            PNodeSet pNodeSet{it.second};
            switch(pNodeSet.kind())
            {
                case PNodeSet::Kind::RANGE:
                {
                    Range* range = pNodeSet.asRange();
                    delete range;
                    break;
                }
                case PNodeSet::Kind::LIST:
                {
                    ListNodeSet* list = pNodeSet.asList();
                    list->destructInner();
                    delete list;
                    break;
                }
                case PNodeSet::Kind::SPLIT:
                {
                    SplitNodeSet* split = pNodeSet.asSplit();
                    split->destructInner();
                    delete split;
                    break;
                }
                default:
                    xassert_false("unknown node set kind");
            }
        }
    }
}

