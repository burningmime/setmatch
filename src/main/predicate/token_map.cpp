/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "predicate.hpp"

namespace burningmime::setmatch
{
    TokenId TokenMap::getOrAdd(HashedString str, OwnerClass cl)
    {
        TokenId item = TokenId::none();
        if(tryGetValue(tokenToId, str, &item))
        {
            xassert(item.isValid(), "Should have gotten a valid tokenId here");
            idToToken[*item].refOwners |= (uint32_t) cl;
            return item;
        }
        else
        {
            KeyAndRefCount stored = KeyAndRefCount{KeyString::copy(str), (uint32_t) cl};
            HashedString keyRef = *stored.key;
            auto birth = freeList.acquire();
            if(birth.isNew)
            {
                auto id = TokenId((uint32_t) idToToken.size());
                idToToken.push_back(std::move(stored));
                tokenToId.insert_or_assign(keyRef, id);
                return id;
            }
            else
            {
                auto id = TokenId((uint32_t) birth.index);
                xassert(!idToToken[*id].key, "Valid string was marked as tombstoned");
                idToToken[*id] = std::move(stored);
                tokenToId.insert_or_assign(keyRef, id);
                return id;
            }
        }
    }

    void TokenMap::release(TokenId id, OwnerClass cl)
    {
        auto bit = static_cast<uint32_t>(cl);
        xassert(*id < idToToken.size(), "idToToken.size() = %d, but id = %d", idToToken.size(), *id);
        KeyAndRefCount& stored = idToToken[*id];
        xassert((bool) stored.key, "Did not find a valid key at index %d", *id);
        xassert((stored.refOwners & bit) == bit, "That class didn't have a reference. stored.refOwners = %d, bit = %d", stored.refOwners, bit);
        stored.refOwners &= ~bit;
        if(stored.refOwners == 0)
        {
            auto it = tokenToId.find(*stored.key);
            xassert(it != tokenToId.end(), "Did not find token %s in the map", (*stored.key).str);
            tokenToId.erase(it);
            stored.key = KeyString();
            freeList.release((int) *id);
        }
    }

    TokenId TokenMap::operator[](HashedString str)
    {
        TokenId result = TokenId::none();
        return tryGetValue(tokenToId, str, &result) ? result : TokenId::none();
    }

    HashedString TokenMap::operator[](TokenId id)
    {
        if(*id >= idToToken.size())
            return HashedString();
        return *idToToken[*id].key;
    }
}
