/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "tag.hpp"

namespace burningmime::setmatch
{
    NO_RETURN static void throwInvalid(SimpleString s, const char* msg)
    {
        setmatchRaise(INVALID_META_TAG, "Invalid tag \"%s\" (%s)", stringForLog(s, 32), msg);
    }

    static double parseNumber(SimpleString original, const char* str)
    {
        char* pEnd = nullptr;
        double value = std::strtod(str, &pEnd);
        if(pEnd == nullptr || *pEnd != '}')
            throwInvalid(original, "Invalid number format");
        if(std::isnan(value) || std::isinf(value))
            throwInvalid(original, "Numbers may not be NaN or infinity");
        return value;
    }

    TaggedString parseTags(SimpleString original)
    {
        TaggedString current(original);
    LNextTag:
        size_t length = current.length();
        const char* str = current.ptr();
        if(length == 0)
        {
            // if the string actually is empty, we can return it
            // but if there are any tags before the string, something has to be there
            if(current.hasValue())
                throwInvalid(original, "Expected token string after tags");
            else
                return current;
        }
        else if(str[0] == '{')
        {
            if(length > 1 && str[1] == 'n')
            {
                if(current.hasValue())
                    throwInvalid(original, "Encountered multiple number tags");
                size_t i; for(i = 4; i < length; ++i)
                    if(str[i] == '}')
                        break;
                if(i >= length || str[2] != '=')
                    throwInvalid(original, "Number tags must be of form '{n=-123.45}, where -123.45 can be replaced by any decimal numeric constant.");
                // string to parse is s[3..i]
                current.value = parseNumber(original, str + 3);
                current.str = SimpleString{str + i + 1, length - i - 1};
                goto LNextTag;
            }
            else if(length > 1 && str[1] == '{')
            {
                current.str = SimpleString{str + 1, length - 1};
                return current;
            }
            else
            {
                throwInvalid(original, "Unknown escape sequence or is string too short. Note literals that begin with '{' "
                                       "should be prepended with an extra '{'");
            }
        }
        else
        {
            return current;
        }
    }
}
