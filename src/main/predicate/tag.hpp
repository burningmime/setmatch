/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include "predicate.hpp"
#include <limits>
#include <math.h>

namespace burningmime::setmatch
{
    struct TaggedString
    {
        // once there are more than numbers, we might want to use some kind of component pattern and/or a class hierarchy
        SimpleString str;
        double value;

        ALWAYS_INLINE inline TaggedString() : str(), value(NaN) { }
        ALWAYS_INLINE explicit inline TaggedString(SimpleString s) : str(s), value(NaN) { }
        ALWAYS_INLINE explicit inline TaggedString(SimpleString s, double v) : str(s), value(v) { dassert(!std::isinf(v) && !std::isnan(v), "NaN/infinity should already have been filtered"); }

        ALWAYS_INLINE inline explicit operator bool() { return str.length > 0; }
        ALWAYS_INLINE inline bool hasValue() const { return !std::isnan(value); }
        ALWAYS_INLINE inline size_t length() const { return str.length; }
        ALWAYS_INLINE inline const char* ptr() const { return str.str; }
        ALWAYS_INLINE inline const char operator[](size_t i) const { return ptr()[i]; }
    };

    struct TaggedToken
    {
        TokenId token;
        double value;

        ALWAYS_INLINE inline explicit TaggedToken(TokenId token) : token(token), value(NaN) { }
        ALWAYS_INLINE inline TaggedToken(TokenId token, double value) : token(token), value(value) { }
        ALWAYS_INLINE inline bool hasValue() const { return !std::isnan(value); }
        ALWAYS_INLINE inline bool isValid() const { return token.isValid(); }
    };

    TaggedString parseTags(SimpleString src);
}
