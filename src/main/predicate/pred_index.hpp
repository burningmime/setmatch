/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include "../common/common.hpp"
#include "../common/interval.hpp"
#include "predicate.hpp"
#include <boost/container/small_vector.hpp>
#include "tag.hpp"

namespace burningmime::setmatch
{
    class PhraseTrie
    {
        struct Private;
        struct Term;
        struct Phrase;
        typedef boost::container::small_vector<Term*, 1> TermVector;
        typedef HashMap<TokenId, TermVector, TokenId::Hash> TermMap;

        Term* root;
        TermMap allTermsByToken;
        TokenMap& tokenMap;

    public:
        explicit PhraseTrie(TokenMap& _tokenMap);
        Predicate* getOrAdd(SimpleStringIterator input);
        void removeAndFree(Predicate* phrase);
        void match(const std::vector<TaggedToken>& input, BitArray& output);
        ~PhraseTrie();
        DISABLE_COPY_CONSTRUCTOR(PhraseTrie)
    };

    class IntervalIndex
    {
        struct Range;
        struct PNodeSet;
        struct ListNodeSet;
        struct SplitNodeSet;

        TokenMap& tokenMap;
        HashMap<TokenId, size_t, TokenId::Hash> nodeSets;

    public:
        inline explicit IntervalIndex(TokenMap& tokenMap) : tokenMap(tokenMap) { }
        Predicate* getOrAdd(SimpleString key, Interval interval);
        void removeAndFree(Predicate* predicate);
        void match(const std::vector<TaggedToken>& input, BitArray& output);
        ~IntervalIndex();
        DISABLE_COPY_CONSTRUCTOR(IntervalIndex)
    };

    class PredicateIndex
    {
    private:
        TokenMap tokenMap;
        PhraseTrie phraseTrie;
        IntervalIndex intervalIndex;
    public:
        inline PredicateIndex() : tokenMap(), phraseTrie(tokenMap), intervalIndex(tokenMap) { }
        inline Predicate* getOrAddPhrase(SimpleStringIterator input) { return phraseTrie.getOrAdd(input); }
        inline Predicate* getOrAddInterval(SimpleString string, Interval interval) { return intervalIndex.getOrAdd(string, interval); }
        inline TaggedToken strToTok(TaggedString s) { return TaggedToken{tokenMap[HashedString(s.str)], s.value}; }
        inline std::string toString(Predicate* predicate) { return predicate->toString(tokenMap); }

        inline void match(const std::vector<TaggedToken>& input, BitArray& output)
        {
            phraseTrie.match(input, output);
            intervalIndex.match(input, output);
        }

        inline void removeAndFree(Predicate* predicate)
        {
            switch(predicate->getType())
            {
                case PredicateType::PHRASE:
                    phraseTrie.removeAndFree(predicate);
                    break;
                case PredicateType::RANGE:
                    intervalIndex.removeAndFree(predicate);
                    break;
                default:
                    xassert_false("Unknown predicate type");
            }
        }

        DISABLE_COPY_CONSTRUCTOR(PredicateIndex)
    };
}
