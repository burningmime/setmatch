/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

// for windows and osxcross builds, this is used a ghetto form of link-time optimization
#include "api_impl.cpp" // this has to be first to prevent redefinition of a preprocessor symbol

#include "common/common.cpp"
#include "common/metric.cpp"
#include "common/unicode.cpp"

#include "index/db_index.cpp"
#include "index/sync_db.cpp"
#include "index/graph_opt.cpp"
#include "index/pred_list.cpp"
#include "index/subrule.cpp"
#include "index/config.cpp"

#include "predicate/interval_index.cpp"
#include "predicate/token_map.cpp"
#include "predicate/tag.cpp"
#include "predicate/phrase_trie.cpp"

#include "boolean/parse_json.cpp"
#include "boolean/rule_enc.cpp"
#include "boolean/boolean.cpp"
#include "boolean/rule_dec.cpp"

#include "../jni/com_burningmime_setmatch_Native.cpp"
#include "../jni/utils_jni.cpp"
