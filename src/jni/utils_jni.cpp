/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "utils_jni.hpp"

namespace burningmime::setmatch::jni
{
    static JavaVM* jvm = nullptr;
    static jclass class_RuleDB;
    static jmethodID method_RuleDB_log;
    static jmethodID method_RuleDB_newException;
    jclass class_String;

    static std::u16string utf8to16Safe(const char* cstr)
    {
        try
        {
            return utf8to16(cstr, strlen(cstr));
        }
        catch(...)
        {
            return u"2002: MAJOR ERROR -- something went wrong, then went wrong again when trying to convert the error to UTF16";
        }
    }

    bool javaStringToUtf8(JNIEnv* env, jstring string, std::string& result)
    {
        JString src(env, string, false);
        try
        {
            result = burningmime::setmatch::utf16to8(src.data(), src.length());
            return true;
        }
        catch(std::exception& ex)
        {
            src.releaseCritical(); // must release criticals before calling JVM method
            throwJavaException(env, ex.what());
            return false;
        }
    }

    static JString toJString(JNIEnv* env, const char* cstr)
    {
        std::u16string utf16 = utf8to16Safe(cstr);
        jstring local = env->NewString((jchar*) utf16.data(), (jsize) utf16.length());
        return JString(env, local, true);
    }

    static void jniLogImpl(setmatch_LogLevel level, const char* message)
    {
        JNIEnv* env;
        jvm->GetEnv((void**) &env, JNI_VERSION_1_6);
        JString str = toJString(env, message);
        env->CallStaticVoidMethod(jni::class_RuleDB, jni::method_RuleDB_log, (int) level, *str);
    }

    void throwJavaException(JNIEnv* env, const char* err)
    {
        JString str = toJString(env, err);
        auto ex = (jthrowable) env->CallStaticObjectMethod(class_RuleDB, method_RuleDB_newException, *str);
        env->Throw(ex);
        env->DeleteLocalRef(ex);
    }

    static jclass findClass(JNIEnv* env, const char* name)
    {
        jclass local = env->FindClass(name);
        xassert(local, "Did not find class %s", name);
        jclass global = (jclass) env->NewGlobalRef(local);
        env->DeleteLocalRef(local);
        return global;
    }

    static jmethodID findMethod(JNIEnv* env, bool isStatic, jclass clazz, const char* name, const char* signature)
    {
        jmethodID method = isStatic ? env->GetStaticMethodID(clazz, name, signature) : env->GetMethodID(clazz, name, signature);
        xassert(method, "Did not find %s method %s with signature %s", isStatic ? "static" : "instance", name, signature);
        return method;
    }

    static void initJni(JavaVM* _jvm)
    {
        if(_jvm)
        {
            jvm = _jvm;
            JNIEnv* env;
            jvm->GetEnv((void**) &env, JNI_VERSION_1_6);
            class_RuleDB = findClass(env, "com/burningmime/setmatch/RuleDB");
            class_String = findClass(env, "java/lang/String");
            method_RuleDB_log = findMethod(env, true, class_RuleDB, "log", "(ILjava/lang/String;)V");
            method_RuleDB_newException = findMethod(env, true, class_RuleDB, "newException", "(Ljava/lang/String;)Ljava/lang/Throwable;");
            setmatch_setLogCallback(&jniLogImpl);
            logInfo("Initialized JNI library: " SETMATCH_VERSION_STRING);
        }
    }

    static void destroyJni()
    {
        if(jvm)
        {
            logInfo("Shutting down JNI library");
            setmatch_setLogCallback(nullptr);
            JNIEnv* env;
            jvm->GetEnv((void**) &env, JNI_VERSION_1_6);
            env->DeleteGlobalRef(class_RuleDB);
            env->DeleteGlobalRef(class_String);
        }
    }
}

extern "C"
{
    JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* jvm, void*)
    {
        burningmime::setmatch::jni::initJni(jvm);
        return JNI_VERSION_1_6;
    }

    JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* jvm, void*)
    {
        burningmime::setmatch::jni::destroyJni();
    }
}

// NOTE: if we ever want to start a thread, we will need something like this
//namespace burningmime::setmatch
//{
//    void targetSpecificAttachThread(const char* threadName)
//    {
//        JavaVMAttachArgs aa;
//        aa.name = (char*) threadName;
//        aa.version = JNI_VERSION_1_6;
//        JNIEnv *env;
//        jni::jvm->AttachCurrentThreadAsDaemon((void**) &env, &aa);
//    }
//
//    void targetSpecificDetachThread()
//    {
//        jni::jvm->DetachCurrentThread();
//    }
//}