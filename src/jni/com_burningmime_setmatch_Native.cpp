/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "utils_jni.hpp" // must include utils_jni.h first to fix an issue... see utils_jni.h to see what i'm talking about
#include "com_burningmime_setmatch_Native.h"
#include <memory>

using namespace burningmime::setmatch::jni;

JNIEXPORT jlong JNICALL Java_com_burningmime_setmatch_Native_newRuleDB(JNIEnv* env, jclass)
{
    setmatch_RuleDB* db = nullptr;
    const char* err = setmatch_newRuleDB(nullptr, &db);
    if(UNLIKELY(err)) throwJavaException(env, err);
    return (jlong) db;
}

JNIEXPORT jbyteArray JNICALL Java_com_burningmime_setmatch_Native_compileRuleJson(JNIEnv* env, jclass, jstring text)
{
    std::string utf8;
    bool success = javaStringToUtf8(env, text, utf8);
    if(UNLIKELY(!success)) return nullptr;

    // create wrapper struct that frees rule when it goes out of scope
    struct CompiledRuleWrapper
    {
        setmatch_CompiledRule cr{nullptr, 0};
        ~CompiledRuleWrapper() { setmatch_deleteCompiledRule(cr); }
    } crw;

    const char* err = setmatch_compileRuleJson(utf8.data(), &crw.cr);
    if(UNLIKELY(err))
    {
        throwJavaException(env, err);
        return nullptr;
    }
    else
    {
        jbyteArray jdst = env->NewByteArray((jsize) crw.cr.length);
        env->SetByteArrayRegion(jdst, 0, (jsize) crw.cr.length, (jbyte*) crw.cr.data);
        return jdst;
    }
}

JNIEXPORT jboolean JNICALL Java_com_burningmime_setmatch_Native_addRuleCompiled(JNIEnv* env, jclass, jlong db, jlong ruleId, jbyteArray jsrc)
{
    struct VoidDeleter { void operator()(void* ptr) noexcept { if(ptr) burningmime::setmatch::smFree(ptr); } };
    typedef std::unique_ptr<void, VoidDeleter> PBuffer;
    PBuffer tmp(nullptr);
    size_t length;
    {
        // keep this in a scope so that java releases critical data before we go into main functions
        // (can't cal some JNI functions after GetPrimitiveArrayCritical until ReleasePrimitiveArrayCritical)
        JByteArray src = JByteArray(env, jsrc, false);
        tmp = PBuffer(burningmime::setmatch::smAlloc(src.length()));
        length = src.length();
        memcpy(tmp.get(), src.data(), length);
    }

    int replaced = 0;
    const char* err = setmatch_addRule((setmatch_RuleDB*) db, ruleId, {tmp.get(), length}, &replaced);
    if(UNLIKELY(err)) throwJavaException(env, err);
    return (jboolean) (replaced != 0);
}

JNIEXPORT jboolean JNICALL Java_com_burningmime_setmatch_Native_addRuleJson(JNIEnv* env, jclass, jlong db, jlong ruleId, jstring text)
{
    std::string utf8;
    bool success = javaStringToUtf8(env, text, utf8);
    if(UNLIKELY(!success))
        return false;

    int replaced = 0;
    const char* err = setmatch_addRuleJson((setmatch_RuleDB*) db, ruleId, utf8.data(), &replaced);
    if(UNLIKELY(err)) throwJavaException(env, err);
    return (jboolean) (replaced != 0);
}

JNIEXPORT jboolean JNICALL Java_com_burningmime_setmatch_Native_containsRule(JNIEnv* env, jclass, jlong db, jlong ruleId)
{
    int found = 0;
    const char* err = setmatch_containsRule((setmatch_RuleDB*) db, ruleId, &found);
    if(UNLIKELY(err)) throwJavaException(env, err);
    return (jboolean) (found != 0);
}

JNIEXPORT jboolean JNICALL Java_com_burningmime_setmatch_Native_removeRule(JNIEnv* env, jclass, jlong db, jlong ruleId)
{
    int removed = 0;
    const char* err = setmatch_removeRule((setmatch_RuleDB*) db, ruleId, &removed);
    if(UNLIKELY(err)) throwJavaException(env, err);
    return (jboolean) (removed != 0);
}

JNIEXPORT jlongArray JNICALL Java_com_burningmime_setmatch_Native_match(JNIEnv* env, jclass, jlong db, jobjectArray jtokens, jint length)
{
    JStringArray tokens = JStringArray(env, jtokens, false, length);
    if(UNLIKELY(length == 0))
    {
        return env->NewLongArray(0);
    }
    else
    {
        JStringArrayIterator tokensIter(tokens);
        setmatch_TokenIterator cFunction = [](void* state) -> setmatch_String { return ((JStringArrayIterator*) state)->next(); };
        setmatch_MatchCollection results;
        const char* err = setmatch_match((setmatch_RuleDB*) db, cFunction, &tokensIter, &results);
        if(UNLIKELY(err))
        {
            throwJavaException(env, err);
            return nullptr;
        }
        else
        {
            jlongArray javaRules /* no it doesn't */ = env->NewLongArray((jsize) results.count);
            env->SetLongArrayRegion(javaRules, 0, (jsize) results.count, (jlong*) results.matchedIds);
            return javaRules;
        }
    }
}

JNIEXPORT jobjectArray JNICALL Java_com_burningmime_setmatch_Native_getMetricNames(JNIEnv* env, jclass, jlong db)
{
    setmatch_Metrics metrics;
    const char* err = setmatch_pollMetrics((setmatch_RuleDB*) db, &metrics);
    if(UNLIKELY(err))
    {
        throwJavaException(env, err);
        return nullptr;
    }
    else
    {
        jobjectArray results = env->NewObjectArray((jsize) metrics.count, class_String, nullptr);
        for(size_t i = 0; i < metrics.count; ++i)
        {
            setmatch_Metric& metric = metrics.metrics[i];
            std::u16string utf16 = burningmime::setmatch::utf8to16(metric.name, strlen(metric.name));
            JRef<jstring> name(env, env->NewString((jchar*) utf16.data(), (jsize) utf16.length()), true);
            env->SetObjectArrayElement(results, (jsize) i, *name);
        }
        return results;
    }
}

JNIEXPORT void JNICALL Java_com_burningmime_setmatch_Native_pollMetrics(JNIEnv* env, jclass, jlong db, jdoubleArray values)
{
    setmatch_Metrics metrics;
    const char* err = setmatch_pollMetrics((setmatch_RuleDB*) db, &metrics);
    if(UNLIKELY(err))
    {
        throwJavaException(env, err);
    }
    else
    {
        JDoubleArray values2 = JDoubleArray(env, values, true);
        if(UNLIKELY(values2.length() != metrics.count))
        {
            throwJavaException(env, "2002: wrong array length");
        }
        else
        {
            double* values3 = values2.data();
            for(size_t i = 0; i < metrics.count; ++i)
                values3[i] = metrics.metrics[i].value;
        }
    }
}

JNIEXPORT jstring JNICALL Java_com_burningmime_setmatch_Native_debugGetRuleAsString(JNIEnv* env, jclass, jlong db, jlong ruleId)
{
    char buf[1024];
    int found = 0;
    const char* err = setmatch_debugGetRuleAsString((setmatch_RuleDB*) db, ruleId, buf, sizeof(buf), &found);
    if(UNLIKELY(err))
    {
        throwJavaException(env, err);
        return nullptr;
    }
    else
    {
        std::u16string utf16 = burningmime::setmatch::utf8to16(buf, strlen(buf));
        return env->NewString((jchar*) utf16.data(), (jsize) utf16.length());
    }
}

JNIEXPORT void JNICALL Java_com_burningmime_setmatch_Native_deleteRuleDB(JNIEnv* env, jclass, jlong db)
{
    setmatch_deleteRuleDB((setmatch_RuleDB*) db);
}
