/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

// MinGW cross-compiling fix -- MinGW on Linux uses the Linux JNIIMPORT and JNIEXPORT instead of the Windows
// one (because Windows jni_md.h is not installed on Linux). The *best* solution would be to just distribute
// the win32 header, but the license agreement forbids that. Another option would be to ask users to manually
// download and install it (and put it in the dockerfile)... which is fine, but adds another build step when
// cross-compiling. so, for now, just redefine it
#include <jni.h>
#undef JNIEXPORT
#if defined(__MINGW64__)
#define JNIEXPORT __attribute__((visibility("default"))) __declspec(dllexport)
#else
#define JNIEXPORT __attribute__((visibility("default")))
#endif

#include "../main/common/common.hpp"
#include "../public/setmatch.h"
#include <exception>
#include <functional>

namespace burningmime::setmatch::jni
{
    // defined in utils_jni.cpp
    void throwJavaException(JNIEnv* env, const char* err);
    bool javaStringToUtf8(JNIEnv* env, jstring string, std::string& result);
    extern jclass class_String;

    //==================================================================================================================
    // RAII helpers -- I don't want to wrap all the JNI types, but it's important these get properly freed in the event
    // of an exception, so here's the subset that are actually used.
    //==================================================================================================================

    template<typename TJObject>
    class JRef
    {
    protected:
        JNIEnv* env;
        TJObject obj;
        int isRefOwner;

    private:
        inline void freeRef() noexcept { if(obj && isRefOwner) env->DeleteLocalRef(obj); }

    public:
        inline JRef(JNIEnv* _env, TJObject _obj, bool _isRefOwner) noexcept : env(_env), obj(_obj), isRefOwner(_isRefOwner) { }
        inline explicit operator bool() const noexcept { return obj != nullptr; }
        inline ~JRef() { freeRef(); }

        inline JRef(JRef&& other) noexcept
            : env(other.env), obj(other.obj), isRefOwner(other.isRefOwner)
        {
            other.env = nullptr;
            other.obj = nullptr;
            other.isRefOwner = 0;
        }

        inline JRef& operator=(JRef&& other) noexcept
        {
            if(&other != this)
            {
                freeRef();
                env = other.env;
                obj = other.obj;
                isRefOwner = other.isRefOwner;
                other.env = nullptr;
                other.obj = nullptr;
                other.isRefOwner = 0;
            }
            return *this;
        }

        inline TJObject operator*() const noexcept
        {
            return obj;
        }

        DISABLE_COPY_CONSTRUCTOR(JRef)
    };

    template<typename TJObject, auto GetLength>
    class JSizedRef : public JRef<TJObject>
    {
    private:
        typedef JRef<TJObject> base;
    protected:
        jint mLength;
    public:
        inline JSizedRef(JNIEnv* _env, TJObject _obj, bool _isRefOwner) : base(_env, _obj, _isRefOwner), mLength(_obj ? std::invoke(GetLength, _env, _obj) : 0) { }
        inline JSizedRef(JNIEnv* _env, TJObject _obj, bool _isRefOwner, jint _length) : base(_env, _obj, _isRefOwner), mLength(_length) { }
        inline size_t length() { return (size_t) mLength; }
        inline JSizedRef(JSizedRef&& other) noexcept : base(std::forward<base>(other)), mLength(other.mLength) { other.mLength = 0; }
        inline JSizedRef& operator=(JSizedRef&& other) noexcept
        {
            if(&other != this)
            {
                mLength = other.mLength;
                other.mLength = 0;
                base::operator=(std::move(other));
            }
            return *this;
        }
    };

    template<typename TJObject, auto GetLength, typename TDataInt, typename TDataExt, auto GetCritical, auto ReleaseCritical>
    class JSizedCritical : public JSizedRef<TJObject, GetLength>
    {
    private:
        typedef JSizedRef<TJObject, GetLength> base;
        TDataInt critical = nullptr;
    public:
        using base::JSizedRef;

        inline void releaseCritical()
        {
            if(critical)
            {
                std::invoke(ReleaseCritical, base::env, base::obj, critical);
                critical = nullptr;
            }
        }

        inline ~JSizedCritical()
        {
            releaseCritical();
        }

        inline TDataExt data()
        {
            if(!critical && base::obj)
                critical = std::invoke(GetCritical, base::env, base::obj, nullptr);
            return (TDataExt) critical;
        }

        inline JSizedCritical(JSizedCritical&& other) noexcept
            : base(std::forward<base>(other)), critical(other.critical)
        {
            other.critical = nullptr;
        }

        inline JSizedCritical& operator=(JSizedCritical&& other) noexcept
        {
            if(&other != this)
            {
                releaseCritical();
                critical = other.critical;
                other.critical = nullptr;
                base::operator=(std::move(other));
            }
            return *this;
        }

        DISABLE_COPY_CONSTRUCTOR(JSizedCritical)
    };

    // note there's probably some smarter, safer way to do this instead of relying on std::invoke, but this is straightforward and avoids
    // the awkward pointer-to-member-function syntax that i can never remember
    inline void doReleasePrimitiveArrayCritical(JNIEnv* env, jarray array, void* data) { env->ReleasePrimitiveArrayCritical(array, data, 0); }
    typedef JSizedCritical<jstring, &JNIEnv::GetStringLength, const jchar*, char16_t*, &JNIEnv::GetStringCritical, &JNIEnv::ReleaseStringCritical> JString;
    typedef JSizedCritical<jbyteArray, &JNIEnv::GetArrayLength, void*, void*, &JNIEnv::GetPrimitiveArrayCritical, &doReleasePrimitiveArrayCritical> JByteArray;
    typedef JSizedCritical<jdoubleArray, &JNIEnv::GetArrayLength, void*, double*, &JNIEnv::GetPrimitiveArrayCritical, &doReleasePrimitiveArrayCritical> JDoubleArray;

    class JStringArray : public JSizedRef<jobjectArray, &JNIEnv::GetArrayLength>
    {
    private:
        typedef JSizedRef<jobjectArray, &JNIEnv::GetArrayLength> base;
    public:
        using base::JSizedRef;
        inline JStringArray(JStringArray&& other) noexcept : base(std::forward<base>(other)) { }
        inline JStringArray& operator=(JStringArray&& other) noexcept { base::operator=(std::move(other)); return *this; }
        DISABLE_COPY_CONSTRUCTOR(JStringArray)

        inline JString operator[](size_t index)
        {
            dassert(index < (size_t) mLength, "index = %d, length = %d", (int) index, (int) mLength);
            return JString(env, (jstring) env->GetObjectArrayElement(obj, (jsize) index), true);
        }
    };

    //==================================================================================================================
    // String iterator (passed as a functor into core DB operations)
    //==================================================================================================================

    class JStringArrayIterator
    {
        std::string current;
        JStringArray& array;
        size_t index;

    public:
        explicit inline JStringArrayIterator(JStringArray& _array) : array(_array), index(0) { }
        inline setmatch_String next()
        {
            while(true)
            {
                if(index >= array.length())
                {
                    current = std::string{}; // release its memory right away
                    return {nullptr, 0};
                }
                JString string = array[index++];
                if(LIKELY(string && string.length()))
                {
                    // note this can throw, but since it's being called from within an internal try-catch; it will
                    // be converted to an exception type correctly
                    current = utf16to8(string.data(), string.length());
                    return {current.data(), current.length()};
                }
            }
        }
    };
}
