#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "setmatch.h"

void checkError(const char *err)
{
    // errors are actually just a string
    // if the string is non-null, then something went terribly wrong
    if(err)
    {
        fprintf(stderr, "ERROR: %s\n", err);
        exit(1);
    }
}

void compileAndAdd(setmatch_RuleDB *db, int64_t id, const char *json)
{
    setmatch_CompiledRule rule;
    const char *err;

    // compile the JSON to a rule
    // this is the most likely place for an error, especially if you're taking input data from users at any point
    err = setmatch_compileRuleJson(json, &rule);
    checkError(err);

    // add the rule
    err = setmatch_addRule(db, id, rule, NULL);
    checkError(err);

    // free the rule once you're done (this will never error)
    setmatch_deleteCompiledRule(rule);
}

// qsort() comparison function for int64_t
int compareInt64s(const void *a, const void *b)
{
    int64_t result = *(int64_t*) a - *(int64_t*) b;
    return result < 0 ? -1 : result == 0 ? 0 : 1;
}

void matchAndPrint_(setmatch_RuleDB *db, const char **tokens, int nTokens)
{
    const char *err;
    setmatch_MatchCollection results;

    // run the actual matching, passing the strings
    // there are other versions of this function that allow you to pass a callback functor or custom
    // structure; see the documentation if you need that
    err = setmatch_matchArrayN(db,tokens, (size_t) nTokens, &results);
    checkError(err);

    // the results will be in an implementation-defined order. usually, that's fine, but let's go ahead and sort
    // them fot display
    qsort(results.matchedIds, results.count, sizeof(int64_t), &compareInt64s);

    // print results
    printf("[");
    for(size_t i = 0; i < results.count; ++i)
    {
        if(i != 0) { printf(", "); }
        printf("%" PRId64, results.matchedIds[i]);
    }
    printf("]\n");
}

// hacky wrapper macro to make the example code look nice. only works for compile-time string arrays, so probably
// not very useful in general
#define matchAndPrint(db, ...) \
{ \
    static const char *strings[] = { __VA_ARGS__ }; \
    matchAndPrint_(db, strings, sizeof(strings) / sizeof(const char*)); \
}

void removeRule(setmatch_RuleDB *db, int64_t id)
{
    const char *err = setmatch_removeRule(db, id, NULL);
    checkError(err);
}

int main(int argc, const char **argv)
{
    const char *err;
    setmatch_RuleDB *db;

    // create a database object
    err = setmatch_newRuleDB(NULL, &db);
    checkError(err);

    // This is a direct translation of the JSON which the Java wrapper generates.
    // You'll need to generate it in your code. There will probably never be an easy API from pure C to generate the
    // JSON, but there are several great external C libraries to do taht like: http://www.digip.org/jansson/
    compileAndAdd(db, 1, "{\"rule\":{\"phrase\":[\"sports\"]}}");
    compileAndAdd(db, 2, "{\"rule\":{\"and\":[{\"phrase\":[\"games\"]},{\"phrase\":[\"18-25\"]}]}}");
    compileAndAdd(db, 3, "{\"rule\":{\"or\":[{\"phrase\":[\"games\"]},{\"phrase\":[\"sports\"]}]}}");
    compileAndAdd(db, 4, "{\"rule\":{\"and\":[{\"phrase\":[\"news\"]},{\"not\":{\"phrase\":[\"18-25\"]}}]}}");
    compileAndAdd(db, 5, "{\"rule\":{\"and\":[{\"or\":[{\"phrase\":[\"18-25\"]},{\"phrase\":[\"25-40\"]}]},{\"phrase\":[\"sports\"]}]}}");
    compileAndAdd(db, 6, "{\"rule\":{\"and\":[{\"not\":{\"or\":[{\"phrase\":[\"games\"]},{\"phrase\":[\"sports\"]}]}},{\"phrase\":[\"news\"]}]}}");

    // run the actual matching logic
    matchAndPrint(db, "18-25", "games", "sports");          // [1, 2, 3, 5]
    matchAndPrint(db, "18-25", "news", "technology");       // [6]
    matchAndPrint(db, "25-40", "games", "news", "sports");  // [1, 3, 4, 5]
    matchAndPrint(db, "40-50", "sports");                   // [1, 3]

    // remove a rule
    removeRule(db, 4);
    matchAndPrint(db, "25-40", "games", "news", "sports");  // [1, 3, 5]

    // free the database (this will never error)
    setmatch_deleteRuleDB(db);
}
