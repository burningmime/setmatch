#include "setmatch.hpp"
#include <iostream>
#include <string>

using namespace setmatch;
using namespace std::literals::string_literals;

// helper function to comma-separate items in a list (there might be a better way to do this)
template<typename T> struct CommaSeparated { const T& iterable; };
template<typename T> CommaSeparated<T> commaSeparated(const T& iterable) { return CommaSeparated<T>{iterable}; }
template<typename T> std::ostream& operator<<(std::ostream& os, const CommaSeparated<T>& cs)
{
    bool first = true;
    for(auto& item : cs.iterable)
    {
        if (!first)
            os << ", ";
        first = false;
        os << item;
    }
    return os;
}

void matchAndPrint(RuleDB& db, std::initializer_list<std::string> tokens)
{
    // The easiest way to match is with a pair of iterators to std::strings. There are also of overloads that let you
    // do tricky things with streams or use raw pointers, but this is good for now.
    MatchCollection matches = db.match(tokens.begin(), tokens.end());

    // There aren't any duplicate elements, but they're in an implementation-defined order. For most use cases, that's
    // fine, but since we're displaying them, let's sort the results.
    matches.sort();

    // Print them out
    std::cout << "match(" << commaSeparated(tokens) << ") = [" << commaSeparated(matches) << "]" << std::endl;
}

int main(int argc, char** argv)
{
    RuleDB db;

    // This is a direct translation of the JSON which the Java wrapper generates.
    // The C++ API doesn't have an easy JSON generator the same way that the Java API does, so you'll need to generate
    // these in your own code.
    db.addRuleJson(1, R"_({"rule":{"phrase":["sports"]}})_"s);
    db.addRuleJson(2, R"_({"rule":{"and":[{"phrase":["games"]},{"phrase":["18-25"]}]}})_"s);
    db.addRuleJson(3, R"_({"rule":{"or":[{"phrase":["games"]},{"phrase":["sports"]}]}})_"s);
    db.addRuleJson(4, R"_({"rule":{"and":[{"phrase":["news"]},{"not":{"phrase":["18-25"]}}]}})_"s);
    db.addRuleJson(5, R"_({"rule":{"and":[{"or":[{"phrase":["18-25"]},{"phrase":["25-40"]}]},{"phrase":["sports"]}]}})_"s);
    db.addRuleJson(6, R"_({"rule":{"and":[{"not":{"or":[{"phrase":["games"]},{"phrase":["sports"]}]}},{"phrase":["news"]}]}})_"s);

    // At least the matching is easy!
    matchAndPrint(db, { "18-25", "games", "sports" });          // [1, 2, 3, 5]
    matchAndPrint(db, { "18-25", "news", "technology" });       // [6]
    matchAndPrint(db, { "25-40", "games", "news", "sports" });  // [1, 3, 4, 5]
    matchAndPrint(db, { "40-50", "sports" });                   // [1, 3]

    // Removing a rule just needs the ID
    db.removeRule(4);
    matchAndPrint(db, { "25-40", "games", "news", "sports" });  // [1, 3, 5]
}
