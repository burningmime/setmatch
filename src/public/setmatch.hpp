/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SETMATCH_HPP
#define SETMATCH_HPP

#include <cstdint>
#include <cstddef>
#include <stdexcept>
#include <type_traits>
#include <string>
#include <algorithm>
#include <functional>

#ifndef SETMATCH_IMPORT
 #ifdef _MSC_VER
  #define SETMATCH_IMPORT __declspec(dllimport)
 #else
  #define SETMATCH_IMPORT
 #endif
#endif

/**
 * @file
 * Wrapper around the setmatch C API which adds RAII, exceptions, and templates.
 */

/**
 * Wrapper around the setmatch C API which adds RAII, exceptions, and templates.
 */
namespace setmatch
{
    // Purposely not documented.
    //
    // This is the C API. Earlier, I was doing #include "setmatch.h", but that led to a bunch of awkwardness and
    // required the user have both header files. There are few enough declarations there that it's fine to just
    // copy them here and make everything a bit easier for the user
    namespace detail
    {
        extern "C"
        {
            struct setmatch_CompiledRule { void* data; size_t length; };
            struct setmatch_String { const char* str; size_t length; };
            struct setmatch_MatchCollection { int64_t* matchedIds; size_t count; };
            struct setmatch_Metric { const char* name; double value; };
            struct setmatch_Metrics { setmatch_Metric* metrics; size_t count; };
            typedef struct setmatch_RuleDB setmatch_RuleDB;
            typedef setmatch_String (*setmatch_TokenIterator)(void* state);
            typedef int32_t setmatch_LogLevel;
            typedef void(*setmatch_LogCallback)(setmatch_LogLevel level, const char* message);

            SETMATCH_IMPORT void setmatch_setLogCallback(setmatch_LogCallback logCallback);
            SETMATCH_IMPORT const char* setmatch_compileRuleJson(const char* jsonUtf8, setmatch_CompiledRule* out_result);
            SETMATCH_IMPORT void setmatch_deleteCompiledRule(setmatch_CompiledRule rule);
            SETMATCH_IMPORT const char* setmatch_newRuleDB(const char* configJson_utf8_nt, setmatch_RuleDB** out_db);
            SETMATCH_IMPORT const char* setmatch_addRule(setmatch_RuleDB* db, int64_t id, setmatch_CompiledRule compiledRule, int* out_replaced);
            SETMATCH_IMPORT const char* setmatch_addRuleJson(setmatch_RuleDB* db, int64_t id, const char* jsonUtf8, int* out_replaced);
            SETMATCH_IMPORT const char* setmatch_removeRule(setmatch_RuleDB* db, int64_t id, int* out_removed);
            SETMATCH_IMPORT const char* setmatch_containsRule(setmatch_RuleDB* db, int64_t id, int* out_found);
            SETMATCH_IMPORT const char* setmatch_match(setmatch_RuleDB* db, setmatch_TokenIterator tokenIterator, void* tokenIteratorState, setmatch_MatchCollection* out_matches);
            SETMATCH_IMPORT const char* setmatch_pollMetrics(setmatch_RuleDB* db, setmatch_Metrics* out_metrics);
            SETMATCH_IMPORT const char* setmatch_debugGetRuleAsString(setmatch_RuleDB* db, int64_t id, char* buf, size_t buf_lengthBytes, int* out_found);
            SETMATCH_IMPORT void setmatch_deleteRuleDB(setmatch_RuleDB* db);
        }
    }

    /**
     * Level at which a log message is reported.
     */
    enum class LogLevel : int
    {
        /**
         * An informational message.
         */
        INFO = 1,

        /**
         * A warning message.
         */
        WARNING = 2,

        /**
         * An internal error.
         */
        ERROR = 3
    };

    /**
     * User-provided logging function for displaying informational, warning, or error messages. This function may be called
     * on any thread (and might even be called on multiple threads at once), so be sure to lock appropriately and avoid
     * static data.
     *
     * @param level one of the values of the #LogLevel enumeration, specifying the importance of the message.
     * @param message a null-terminated string containing the message to write. this buffer is only valid for the length
     *     of the call, so if you plan to use it afterwards, you must make a copy of it.
     */
    typedef void(*LogCallback)(LogLevel level, const char* message);

    /**
     * Sets the log function called by the library when it wants to report something. If you set this to NULL, the
     * default behaviour of reporting to stdout will be used. Therefore, if you want to suppress all log messages, you
     * must create a no-op function and pass it here.
     *
     * This is global and will affect all databases and threads.
     *
     * @param logCallback the log function to call.
     */
    inline void setLogCallback(LogCallback logCallback) noexcept
    {
        detail::setmatch_setLogCallback((detail::setmatch_LogCallback) logCallback);
    }

    /**
     * Exception class thrown when something fails. There are currently no exception classes more specific than this,
     * therefore you must inspect the message if you want additional details.
     */
    class SetmatchError : public std::runtime_error
    {
    public:
        /**
         * Creates a new SetmatchError (mostly for internal sue by the library).
         * @param message message describing the error.
         */
        inline explicit SetmatchError(const char* message) : std::runtime_error(message) { }
    };

    /**
     * A binary rule, compiled with compileRuleJson(), ready to be passed into RuleDB#addRule().
     * 
     * You may directly access the data()/length() methods to store the compiled rule in a file, database,
     * etc. The binary format is forward compatible, and compatible with the other APIs (like the Java API).
     * 
     * Supports move construction/assignment but not copy construction/assignment.
     */
    class CompiledRule
    {
        void* data_;
        size_t length_;
        friend CompiledRule compileRuleJson(const char* ruleJson_utf8_nt);
        CompiledRule(void* _data, size_t _length) noexcept : data_(_data), length_(_length) { }

    public:
        /**
         * Create an empty CompiledRule.
         */
        CompiledRule() noexcept : data_(nullptr), length_(0)
        {
        }

        /**
         * Moves the other rule into this one and invalidates the other rule.
         * @param other rule to move.
         */
        CompiledRule(CompiledRule&& other) noexcept
        {
            if(other.data_)
            {
                data_ = other.data_;
                length_ = other.length_;
                other.data_ = nullptr;
                other.length_ = 0;
            }
            else
            {
                data_ = nullptr;
                length_ = 0;
            }
        }

        /**
         * Moves the other rule and assigns it to this one (freeing any rule that was here before if needed).
         * Invalidates the moved rule.
         * @param other rule to copy.
         * @return {@code *this}.
         */
        CompiledRule& operator=(CompiledRule&& other) noexcept
        {
            if(this != &other)
            {
                if(data_)
                {
                    detail::setmatch_deleteCompiledRule({data_, length_});
                }

                if(other.data_)
                {
                    data_ = other.data_;
                    length_ = other.length_;
                    other.data_ = nullptr;
                    other.length_ = 0;
                }
                else
                {
                    data_ = nullptr;
                    length_ = 0;
                }
            }
            return *this;
        }

        /**
         * Gets a pointer to the actual data.
         */
        void* data() const noexcept
        {
            return data_;
        }

        /**
         * Gets the length of the data, in bytes.
         */
        size_t length() const noexcept
        {
            return length_;
        }

        /**
         * Frees resources associated with the rule.
         */
        ~CompiledRule() noexcept
        {
            if(data_)
            {
                detail::setmatch_deleteCompiledRule({data_, length_});
                data_ = nullptr;
                length_ = 0;
            }
        }
    };

    /**
     * Compiles a rule from a JSON representation, and returns an encoded representation that can be passed to
     * {@link RuleDB#addRule}. This process can be slow for complex rules, so it's recommended to cache the
     * result if you plan on reusing it.
     * @param ruleJson_utf8_nt pointer to input JSON, as a null-terminated C-style string, in UTF-8 format.
     * @return the compiled rule.
     * @throws SetmatchError if something goes wrong
     */
    inline CompiledRule compileRuleJson(const char* ruleJson_utf8_nt)
    {
        detail::setmatch_CompiledRule temp{};
        const char* err = detail::setmatch_compileRuleJson(ruleJson_utf8_nt, &temp);
        if(err) throw SetmatchError(err);
        return CompiledRule(temp.data, temp.length);
    }

    /**
     * Compiles a rule from a JSON representation, and returns an encoded representation that can be passed to
     * {@link RuleDB#addRule}. This process can be slow for complex rules, so it's recommended to cache the
     * result if you plan on reusing it.
     * @tparam TString String type. Must have data() and length() methods.
     * @param jsonUtf8 input JSON in UTF-8 format.
     * @return the compiled rule.
     * @throws SetmatchError if something goes wrong
     */
    template<typename TString>
    inline CompiledRule compileRuleJson(const TString& jsonUtf8)
    {
        return compileRuleJson(jsonUtf8.data());
    }

    // base type for collections that are just pointers to an internal buffer
    // not documenting the class itself here; instead letting the methods be pulled into the child classes. the fact
    // they're both based on this is just an implementation detail
    template<typename T>
    class BaseThreadLocalBuffer
    {
    protected:
        T* results;
        size_t count;
        BaseThreadLocalBuffer() noexcept : results(nullptr), count(0) { }
        BaseThreadLocalBuffer(T* _results, size_t _count) noexcept : results(_results), count(_count) { }

    public:
        /**
         * Iterator type.
         */
        typedef T* iterator;

        /**
         * Const iterator type.
         */
        typedef const T* const_iterator;

        /**
         * Iterator to the beginning of the matches.
         */
        iterator begin() const noexcept
        {
            return results;
        }

        /**
         * Const iterator to the beginning of the matches.
         */
        const_iterator cbegin() const noexcept
        {
            return results;
        }

        /**
         * Iterator to the end of the matches.
         */
        iterator end() const noexcept
        {
            return results + count;
        }

        /**
         * Const iterator to the end of the matches.
         */
        const_iterator cend() const noexcept
        {
            return results + count;
        }

        /**
         * Copy results into a user-defined vector (or other container with assign() method). Clears anything that
         * existed in the container before this call.
         * @tparam TVector container type. must have an assign() method that takes a start and end iterator.
         * @param vector The vector to copy the results into.
         */
        template<typename TVector>
        void copyTo(TVector& vector) const noexcept
        {
            vector.assign(begin(), end());
        }

        /**
         * Gets the item at the given index. Throws a runtime_error if index is out of bounds.
         * @param index the index to check.
         * @return the result at the given index.
         * @throws SetmatchError for an out-of-bounds index.
         */
        T at(const size_t index) const
        {
            if(index >= count)
            {
                throw SetmatchError(std::string("Attempt to access out-of-bounds index on setmatch ThreadLocalTempCollection array (index = ") +
                        std::to_string(index) + ", count = " + std::to_string(count) + ")");
            }
            return results[index];
        }

        /**
         * Gets the item at the given index. Does not perform any bounds checking, so trying to access an out-of-range
         * index will lead to undefined behaviour (bad data or segfauls).
         * @param index the index to check.
         * @return the result at the given index.
         */
        T operator[](const size_t index) const noexcept
        {
            return results[index];
        }

        /**
         * Gets the number of items in the collection.
         * @return number of items in the collection.
         */
        size_t size() const noexcept
        {
            return count;
        }

        /**
         * Returns true iff there are no items.
         * @return true iff there are no items.
         */
        bool empty() noexcept
        {
            return size() == 0;
        }

        /**
         * Gets a raw pointer to the data array. Has the same validity as the collection itself, so you should only
         * use this pointer until the next call to RuleDB#match() on this thread.
         * @return
         */
        T* data() const noexcept
        {
            return results;
        }
    };

    /**
     * Results of the most recent match operation performed on this thread. There won't be any duplicate entries, but
     * they will be in an implementation-defined order, unless you call sort().
     * 
     * The results are thread-local and only valid until the next call to RuleDB#match() on this thread. If you need
     * these results for an extended period of time, you must copy them into your own buffer (copyTo() might help).
     * Even a call to match on a different RuleDB will invalidate the buffer, since it's local to the thread instead
     * of the database.
     * 
     * Since this is just a pointer and length, you may copy/move it around, but the copies have the same validity
     * semantics as the original. If you actually want a copy of the data, use copyTo().
     */
    class MatchCollection : public BaseThreadLocalBuffer<int64_t>
    {
        // copy in base class constructors
        friend class RuleDB;
        using BaseThreadLocalBuffer<int64_t>::BaseThreadLocalBuffer;

    public:
        // default all of these -- not documenting them because they're trivial
        MatchCollection() noexcept = default;
        MatchCollection(const MatchCollection& other) noexcept = default;
        MatchCollection(MatchCollection&& other) noexcept = default;
        MatchCollection& operator=(const MatchCollection& other) noexcept = default;
        MatchCollection& operator=(MatchCollection&& other) noexcept = default;

        /**
         * Sorts results in-place.
         */
        void sort()
        {
            std::sort(begin(), end());
        }
    };

    /**
     * A numerical value used to gather statistical information about a database.
     */
    class Metric
    {
        detail::setmatch_Metric metric;

    public:
        /**
         * Gets an implemtation-defined name associated with the metric. This is a pointer to a constant string,
         * so don't worry about memory management or using it later.
         * @return the name
         */
        const char* name() const noexcept
        {
            return metric.name;
        }

        /**
         * Gets the numeric value of the metric.
         * @return the value
         */
        double value() const noexcept
        {
            return metric.value;
        }
    };

    /**
     * Results of the most recent call to RuleDB#pollMetrics() from this thread.
     *
     * The results are thread-local and only valid until the next call to RuleDB#pollMetrics() on this thread. If you
     * need these results for an extended period of time, you must copy them into your own buffer (copyTo() might help).
     * Even a call to pollMetrics on a different RuleDB will invalidate the buffer, since it's local to the thread instead
     * of the database.
     *
     * Since this is just a pointer and length, you may copy/move it around, but the copies have the same validity
     * semantics as the original. If you actually want a copy of the data, use copyTo().
     */
    class MetricCollection : public BaseThreadLocalBuffer<Metric>
    {
        // copy in base class constructors
        friend class RuleDB;
        using BaseThreadLocalBuffer<Metric>::BaseThreadLocalBuffer;

    public:
        // default all of these -- not documenting them because they're trivial
        MetricCollection() noexcept = default;
        MetricCollection(const MetricCollection& other) noexcept = default;
        MetricCollection(MetricCollection&& other) noexcept = default;
        MetricCollection& operator=(const MetricCollection& other) noexcept = default;
        MetricCollection& operator=(MetricCollection&& other) noexcept = default;
    };

    /**
     * A simple wrapper around a const char* pointer and length, which you can return from your {@link RuleDB#match()}
     * functor instead of a std::string. Doesn't do any memory management or fancy stuff like that, so it's appropriate
     * for pointing to static or stack-allocated buffers.
     *
     * This is provided as a helper that's compatible with the {@link RuleDB#match()} template, but it isn't actually
     * used by any API functions. Feel free to use your own type instead, as long as it provides data() and length()
     * member functions.
     */
    class SimpleStringPointer
    {
        const char* data_;
        size_t length_;

    public:
        /**
         * Creates an empty string. If you return this from a {@link RuleDB#match()} functor, the match will end.
         */
        SimpleStringPointer() noexcept : data_(nullptr), length_(0)
        {
        }

        /**
         * Creates a string from a pointer and length.
         * @param _data Pointer to UTF-8 encoded string data.
         * @param _length Length of the string in bytes.
         */
        SimpleStringPointer(const char* _data, const size_t _length) noexcept : data_(_data), length_(_length)
        {
        }

        /**
         * Gets the data pointer.
         * @return the data pointer.
         */
        const char* data() const noexcept
        {
            return data_;
        }

        /**
         * Gets the length.
         * @return the length.
         */
        size_t length() const noexcept
        {
            return length_;
        }
    };

    /**
     * In-memory index of boolean expressions.
     * 
     * Supports move construction/assignment, but not copy construction/assignment. Note that this is a fairly "heavy"
     * object, with many sub-objects, pre-allocated buffers, etc, so be careful about accidentally constructing empty
     * ones you don't need, or using too many.
     */
    class RuleDB
    {
        detail::setmatch_RuleDB* db = nullptr;

    public:
        /**
         * Creates a new RuleDB instance.
         * @throws SetmatchError if something goes wrong
         */
        RuleDB()
        {
            const char* err = detail::setmatch_newRuleDB(nullptr, &db);
            if(err) throw SetmatchError(err);
        }

        /**
         * Creates a new RuleDB instance.
         * @param configJson_utf8_nt Pointer to UTF8-encoded, null-terminated, C-style string with configuration JSON.
         * @throws SetmatchError if something goes wrong
         */
        explicit RuleDB(const char* configJson_utf8_nt)
        {
            const char* err = detail::setmatch_newRuleDB(configJson_utf8_nt, &db);
            if(err) throw SetmatchError(err);
        }

        /**
         * Creates a new RuleDB instance.
         * @param configJson String containing configuration JSON in UTF8 format.
         * @tparam TString string type. Must have data() method.
         * @throws SetmatchError if something goes wrong
         */
        template<typename TString>
        explicit RuleDB(const TString& configJson)
        {
            const char* err = detail::setmatch_newRuleDB(configJson.data(), &db);
            if(err) throw SetmatchError(err);
        }

        /**
         * Move constructor. The new object takes ownership of the moved object's resources, and leaves the moved
         * object in an unknown state.
         * @param other object to move.
         */
        RuleDB(RuleDB&& other) noexcept : db(other.db)
        {
            other.db = nullptr;
        }

        /**
         * Move assignment operator. Frees the database associated with this variable (if any), then takes ownership
         * of the other database's data (and leaves that one in an unknown state).
         * @param other object to move
         * @return {@code *this}
         */
        RuleDB& operator=(RuleDB&& other) noexcept
        {
            if(this != &other)
            {
                if(db) detail::setmatch_deleteRuleDB(db);
                db = other.db;
                other.db = nullptr;
            }
            return *this;
        }

        /**
         * Adds a rule. If a rule with the same ID exists, that rule will be replaced.
         * @param id ID of the rule, which is returned by match operations, and used to refer to the rule when deleting/updating it.
         * @param data compiled rule binary data
         * @param length length of the data in bytes
         * @throws SetmatchError if something goes wrong
         */
        bool addRule(const int64_t id, const void* data, const size_t length)
        {
            detail::setmatch_CompiledRule temp{const_cast<void*>(data), length};
            int replaced;
            const char* err = detail::setmatch_addRule(db, id, temp, &replaced);
            if(err) throw SetmatchError(err);
            return replaced != 0;
        }

        /**
         * Adds a rule. If a rule with the same ID exists, that rule will be replaced.
         * @param id ID of the rule, which is returned by match operations, and used to refer to the rule when deleting/updating it.
         * @param rule Compiled rule binary, which you can get by calling {@link #compileRuleJson()}.
         * @return true iff another rule was replaced, false if this is a brand new rule.
         * @throws SetmatchError if something goes wrong
         */
        bool addRule(const int64_t id, const CompiledRule& rule)
        {
            return addRule(id, rule.data(), rule.length());
        }

        /**
         * Compiles a rule from a JSON representation, then adds it to the database. If a rule with the same ID exists,
         * that rule will be replaced. This is equivalent to calling {@link #compileRuleJson()} and then {@link addRule}.
         * @param id ID of the rule, which is returned by match operations, and used to refer to the rule when deleting/updating it.
         * @param jsonUtf8 pointer to UTF-8 representation of the input JSON. May contain embedded nulls if you really want to.
         * @param jsonUtf8_lengthBytes length of the UTF-8 string in bytes.
         * @return true iff another rule was replaced, false if this is a brand new rule.
         * @throws SetmatchError if something goes wrong
         */
        bool addRuleJson(const int64_t id, const char* jsonUtf8, const size_t jsonUtf8_lengthBytes)
        {
            int replaced;
            const char* err = detail::setmatch_addRuleJson(db, id, jsonUtf8, &replaced);
            if(err) throw SetmatchError(err);
            return replaced != 0;
        }

        /**
         * Compiles a rule from a JSON representation, then adds it to the database. If a rule with the same ID exists,
         * that rule will be replaced. This is equivalent to calling {@link #compileRuleJson()} and then {@link addRule}.
         * @tparam TString String type. Must have data() and length() methods.
         * @param id ID of the rule, which is returned by match operations, and used to refer to the rule when deleting/updating it.
         * @param jsonUtf8 input JSON in UTF-8 format.
         * @return true iff another rule was replaced, false if this is a brand new rule.
         * @throws SetmatchError if something goes wrong
         */
        template<typename TString>
        bool addRuleJson(const int64_t id, const TString& jsonUtf8)
        {
            return addRuleJson(id, jsonUtf8.data(), jsonUtf8.length());
        }

        /**
         * Removes the rule with the given ID.
         * @param id ID of the rule to remove.
         * @return true if the rule was removed, false if there wasn't any rule with that ID.
         * @throws SetmatchError if something goes wrong
         */
        bool removeRule(const int64_t id)
        {
            int removed;
            const char* err = detail::setmatch_removeRule(db, id, &removed);
            if(err) throw SetmatchError(err);
            return removed != 0;
        }

        /**
         * Checks to see if a rule with that ID exists in the database.
         * @param id id to look for
         * @return true iff the database contains the rule with the given ID.
         * @throws SetmatchError if something goes wrong
         */
        bool containsRule(const int64_t id) const
        {
            int found;
            const char* err = detail::setmatch_containsRule(db, id, &found);
            if(err) throw SetmatchError(err);
            return found != 0;
        }

    private:
        template<typename TString>
        static detail::setmatch_String _toSetmatchString(const TString& current)
        {
            auto length = (size_t) current.length();
            if(length == 0)
                return detail::setmatch_String{nullptr, 0};
            else
                return detail::setmatch_String{current.data(), length};
        }

        template<typename TString>
        static detail::setmatch_String _toSetmatchString(const TString* current)
        {
            if(!current)
                return detail::setmatch_String{nullptr, 0};
            auto length = (size_t) current->length();
            if(length == 0)
                return detail::setmatch_String{nullptr, 0};
            else
                return detail::setmatch_String{current->data(), length};
        }

        template<typename IteratorFunctor>
        MatchCollection _match(IteratorFunctor& iterator) const
        {
            // support for either value, reference, or pointer types
            using TIn = decltype(iterator());
            using TNoRef = typename std::remove_reference<TIn>::type;
            using TLocalRef = typename std::conditional<std::is_pointer<TIn>::value, TIn, TNoRef&>::type;
            using TStored = typename std::conditional<std::is_reference<TIn>::value, std::reference_wrapper<TNoRef>, TIn>::type;

            // store this on the stack in this function, and pass a pointer to it into the C library
            // the C callback will pass back the pointer, and we can get at all this stuff from within the lambda
            struct CallbackContext
            {
                bool isFirst;               // is this the first iteration?
                IteratorFunctor& iterator;  // reference to the iterator in a place we can get to from inside the C function pointer callback
                TStored current;            // current value (either an actual value stored on the stack, or a reference wrapper)
            };

            detail::setmatch_TokenIterator cFunctionPtr = [](void* pContext) -> detail::setmatch_String
            {
                CallbackContext& context = *((CallbackContext*) pContext);

                // on the first iteration, we return the value already fetched (need to do this to support things that
                // aren't DefaultConstructable). On subsequent iterations, call the functor again.
                if(context.isFirst)
                    context.isFirst = false;
                else
                    context.current = context.iterator();

                // unwrap reference_wrapper
                // make a reference to value types
                // keep pointers as pointers
                TLocalRef current = context.current;

                // this will call one of the above overloads, depending on return type
                return _toSetmatchString(current);
            };

            // create the context and make the first call to the iterator
            CallbackContext context{true, iterator, iterator()};

            // and then call the C function
            detail::setmatch_MatchCollection temp{};
            const char* err = detail::setmatch_match(db, cFunctionPtr, &context, &temp);
            if(err) throw SetmatchError(err);
            return MatchCollection(temp.matchedIds, temp.count);
        }

    public:

        /**
         * Matches a document and returns the IDs of matching rules.
         * @tparam IteratorFunctor Function that is called repeatedly to get the next rule. It should return
         *     a type with data() and length() member functions representing a UTF-8 encoded token in the document. As
         *    soon as it returns an empty string (length == 0) or null pointer, iteration will stop. You may return a
         *    reference. pointer, or value type. std::string works fine. If you just need a wrapper around a char*
         *    (for example, to point to a static or local buffer), #setmatch::SimpleStringPointer might help.
         * @param document the document stream to match.
         * @return IDs of rules that matched. This collection is a thread-local temporary, and only valid until the
         *     next call to #match() on this thread.
         * @throws SetmatchError if something goes wrong
         */
        template<typename IteratorFunctor>
        MatchCollection match(const IteratorFunctor& document) const
        {
            // all the underlying function does to the document is call operator(), so it's const-ness can be determined
            // by the caller. however, we need a const overload that accepts const because the C++ committee decided
            // metaprogramming is actually a synonym for copy and paste.
            return _match(const_cast<IteratorFunctor&>(document));
        }

        /**
          * Matches a document and returns the IDs of matching rules.
          * @tparam IteratorFunctor Function that is called repeatedly to get the next rule. It should return
         *     a type with data() and length() member functions representing a UTF-8 encoded token in the document. As
         *    soon as it returns an empty string (length == 0) or null pointer, iteration will stop. You may return a
         *    reference. pointer, or value type. std::string works fine. If you just need a wrapper around a char*
         *    (for example, to point to a static or local buffer), #setmatch::SimpleStringPointer might help.
          * @param document the document stream to match.
          * @return IDs of rules that matched. This collection is a thread-local temporary, and only valid until the
          *     next call to #match() on this thread.
          * @throws SetmatchError if something goes wrong
          */
        template<typename IteratorFunctor>
        MatchCollection match(IteratorFunctor& document) const
        {
            return _match(document);
        }

        /**
         * Matches a document and returns the IDs of matching rules.
         * @tparam IteratorFunctor Function that is called repeatedly to get the next rule. It should return
         *     a type with data() and length() member functions representing a UTF-8 encoded token in the document. As
         *    soon as it returns an empty string (length == 0) or null pointer, iteration will stop. You may return a
         *    reference. pointer, or value type. std::string works fine. If you just need a wrapper around a char*
         *    (for example, to point to a static or local buffer), #setmatch::SimpleStringPointer might help.
         * @param document the document stream to match.
         * @return IDs of rules that matched. This collection is a thread-local temporary, and only valid until the
         *     next call to #match() on this thread.
         * @throws SetmatchError if something goes wrong
         */
        template<typename IteratorFunctor>
        MatchCollection match(IteratorFunctor&& document) const
        {
            return _match(document);
        }

        /**
         * Matches a document and returns the IDs of matching rules. Skips empty tokens in the input collection. This
         * may be used if you have an STL collection, or prefer STL-style iterators instead of repeated calls. Strings
         * should be UTF-8 encoded.
         * 
         * Example for STL collection: `db.match(vec.cbegin(), vec.cend())`
         * @tparam StringIterator Iterator type. The underlying value type must have data() and length() functions, and
         *     must decay to something trivially constructable with length() == 0 when constructed this way.
         *     std::string works fine. If you just need a wrapper around a char*, #setmatch::SimpleStringPointer
         *     might help.
         * @param begin `vector.begin()` or another start iterator
         * @param end `vector.end()` or another end iterator
         * @return IDs of rules that matched. This collection is a thread-local temporary, and only valid until the
         *     next call to match() on this thread.
         * @throws SetmatchError if something goes wrong
         */
        template<typename StringIterator>
        MatchCollection match(StringIterator begin, StringIterator end) const
        {
            using TString = decltype(*begin);
            using TStringValue = typename std::decay<TString>::type;
            TStringValue emptyString{}; // length 0 string used for ending iteration
            return match([&]() -> const TString&
            {
                while(begin != end && begin->length() == 0)
                    ++begin; // skip empty strings
                if(begin == end)
                    return emptyString;
                else
                    return *(begin++);
            });
        }

        /**
         * Retrieves some statistical information about the database, such as how long certain calls took. As of the current
         * version, the data you retrieve from this is implementation-defined and probably not especially useful. In the future,
         * I may document this.
         * @return Collection of metric data. This collection is a thread-local temporary, and only valid until the
         *     next call to pollMetrics() on this thread.
         * @throws SetmatchError if something goes wrong
         */
        MetricCollection pollMetrics() const
        {
            detail::setmatch_Metrics metrics;
            const char* err = detail::setmatch_pollMetrics(db, &metrics);
            if(err) throw SetmatchError(err);
            return MetricCollection((Metric*) metrics.metrics, metrics.count);
        }

        /**
         * Gets an implementation-defined string representation of a rule. You should not rely on the format of this
         * string, as it may change between versions and be arbitrarily truncated, so this function is mostly just for
         * debugging. Result string is UTF-8 encoded. If there are any embedded nulls or ASCII control characters in the
         * rules, they will be escaped.
         * 
         * If the rule does not exist in the database, returns an implementation-defined message stating as much.
         * @param id
         * @return string representation of the rule.
         * @throws SetmatchError if something goes wrong
         * @deprecated This function returns an implementation-defined string that may be truncated.
         */
        std::string debugGetRuleAsString(int64_t id) const
        {
            char buf[2048];
            const char* err = detail::setmatch_debugGetRuleAsString(db, id, buf, sizeof(buf), nullptr);
            if(err) throw SetmatchError(err);
            return std::string(buf);
        }

        /**
         * Deletes the RuleDB and frees associated memory.
         */
        ~RuleDB() noexcept
        {
            if(db)
            {
                detail::setmatch_deleteRuleDB(db);
                db = nullptr;
            }
        }
    };
}

#endif
