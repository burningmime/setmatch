/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SETMATCH_H
#define SETMATCH_H
#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SETMATCH_IMPORT
 #ifdef _MSC_VER
  #define SETMATCH_IMPORT __declspec(dllimport)
 #else
  #define SETMATCH_IMPORT
 #endif
#endif

#if defined(__GNUC__) && (__GNUC__ >= 4)
 // preferring the GCC specific attribute instead of [[nodiscard]] on GCC because CLion only warns on this and not on
 // the C++ attribute
 #define SETMATCH_NODISCARD __attribute__((warn_unused_result))
#elif defined(__cplusplus) && (__cplusplus >= 201700)
 #define SETMATCH_NODISCARD [[nodiscard]]
#elif defined(_MSC_VER) && (_MSC_VER >= 1700)
 // Visual Studio 2012+ has _Check_return_
 #define SETMATCH_NODISCARD _Check_return_
#else
 #define SETMATCH_NODISCARD
#endif

/**
* @file
* Low-level C API to setmatch library.
*/

/**
 * Result of compiling a rule, which can be passed to setmatch_addRule().
 * 
 * You may safely store this data in a separate file or database and pass it on another startup or on another node.
 * Starting with version 0.2, the binary format will be forward compatible. It's the same format used by the Java API,
 * so you can use it across languages and even OSes.
 * 
 * **Memory management:** If it was allocated by setmatch_compileRuleJson(), you must free it by calling
 * setmatch_deleteCompiledRule().
 */
typedef struct setmatch_CompiledRule
{
    /**
     * Pointer to the data itself. It's in an arbitrary (but forward-compatible) binary format. You may store it in
     * a file or database, and may transfer it to other instances if you so choose.
     */
    void* data;

    /**
     * Length of the data in bytes.
     */
    size_t length;
} setmatch_CompiledRule;

/**
 * A UTF-8 encoded string. It does not need to be null-terminated (since it has the length), and may contain embedded
 * nulls if you so choose.
 */
typedef struct setmatch_String
{
    /**
     * Characters of the string.
     */
    const char* str;

    /**
     * The length of the string in bytes.
     */
    size_t length;
} setmatch_String;

/**
 * The result of calling setmatch_match().
 * 
 * **Memory management:** This is allocated by the library and must NOT be freed by callers. It is thread-local,
 * and valid until the next call to setmatch_match() from this thread.
 */
typedef struct setmatch_MatchCollection
{
    /**
     * Array of matched IDs. It will be a pointer to a thread-local buffer. You should not attempt to free it.
     * This pointer is valid until the next call to setmatch_match() from this thread. The size in bytes is (count * 8).
     */
    int64_t* matchedIds;

    /**
     * Number of matched IDs in the matchedIds array. If you want the size in bytes, multiply by 8.
     */
    size_t count;
} setmatch_MatchCollection;

/**
 * A single metric value stored in a setmatch_Metrics object.
 */
typedef struct setmatch_Metric
{
    /**
     * Name of the metric.
     */
    const char* name;

    /**
     * Value of the metric.
     */
    double value;
} setmatch_Metric;

/**
 * The result of calling setmatch_pollMetrics(). The metrics are double values identified by name, in
 * alphabetical order by name. The order and count will be the same for every call to setmatch_pollMetrics()
 * and for every database).
 * 
 * **Memory management:** This is allocated by the library and must NOT be freed by callers. It is thread-local,
 * and valid until the next call to setmatch_pollMetrics() from this thread.
 */
typedef struct setmatch_Metrics
{
    /**
     * Pointer to the metric data itself.
     */
    setmatch_Metric* metrics;

    /**
     * Number of metrics in the collection. This will be the same for every call to setmatch_pollMetrics() (even on
     * different databases), but can change between library versions.
     */
    size_t count;
} setmatch_Metrics;

/**
 * Internal database type. Get it from setmatch_newRuleDB(). Pass it into most setmatch functions. Free it with
 * setmatch_deleteRuleDB().
 *
 * **Memory management:** Allocated in setmatch_newRuleDB() and freed by setmatch_deleteRuleDB().
 */
typedef struct setmatch_RuleDB setmatch_RuleDB;

/**
 * A function that iterates through tokens to match. It will be called repeatedly to get the next token until it
 * returns a string with length set to 0.
 * 
 * You may return a pointer to a buffer on the stack if you want. The returned pointer only needs to be valid until
 * the next call to your iterator, or when setmatch_addRule() returns (whichever comes first), so you can reuse the
 * buffer for the next token.
 *
 * @param state arbitrary state pointer
 * @return the next token in sequence, or an empty token (length 0) if there are no more tokens to match.
 */
typedef setmatch_String (*setmatch_TokenIterator)(void* state);

/**
 * Level at which a log message is reported.
 */
typedef int32_t setmatch_LogLevel;
enum
{
    /**
     * An informational message.
     */
    setmatch_LogLevel_INFO = 1,

    /**
     * A warning message.
     */
    setmatch_LogLevel_WARNING = 2,

    /**
     * An internal error.
     */
    setmatch_LogLevel_ERROR = 3
};

/**
 * User-provided logging function for displaying informational, warning, or error messages. This function may be called
 * on any thread (and might even be called on multiple threads at once), so be sure to lock appropriately and avoid
 * static data.
 *
 * @param level one of the values of the #semtach_LogLevel enumeration, specifying the importance of the message.
 * @param message a null-terminated string containing the message to write. this buffer is only valid for the length
 *     of the call, so if you plan to use it afterwards, you must make a copy of it.
 */
typedef void(*setmatch_LogCallback)(setmatch_LogLevel level, const char* message);

/**
 * Sets the log function called by the library when it wants to report something. If you set this to NULL, the
 * default behaviour of reporting to stdout will be used. Therefore, if you want to suppress all log messages, you
 * must create a no-op function and pass it here.
 *
 * This is global and will affect all databases and threads.
 *
 * @param logCallback the log function to call.
 */
SETMATCH_IMPORT void setmatch_setLogCallback(setmatch_LogCallback logCallback);

/**
 * Compiles a rule and returns the binary. You can pass this binary on to setmatch_addRule(), and also save the binary
 * in a file or database if you plan to use it again. This process will be slow for complex rules (for *extremely*
 * complex rules, particularly those with many ORs nested inside of ANDs, it may just return an error and fail to compile).
 * 
 * Don't forget to call setmatch_deleteCompiledRule() when you're done.
 *
 * @param ruleJson_utf8_nt Null-terminated UTF-8 string containing the JSON to compile.
 * @param out_result The result if the compilation was successful, or NULL if it failed.
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *     valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_compileRuleJson(const char* ruleJson_utf8_nt, setmatch_CompiledRule* out_result);

/**
 * Deletes a rule previously compiled via a call to setmatch_compileRuleJson()
 * @param rule Rule to delete. You may pass NULL here (if you do, nothing will happen).
 */
SETMATCH_IMPORT void setmatch_deleteCompiledRule(setmatch_CompiledRule rule);

/**
 * Creates an empty database.
 * @param configJson_utf8_nt reserved for future use. Just pass NULL for now.
 * @param out_db Your brand new database! There are many like it, but this one is yours.
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *     valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_newRuleDB(const char* configJson_utf8_nt, setmatch_RuleDB** out_db);

/**
 * Adds a rule that was previously compiled with setmatch_compileRuleJson(). If a rule with the same ID already existed,
 * it will be replaced. Note this may trigger a resize, which will cause all threads to pause for a few milliseconds.
 * @param db Rule database to add the rule to
 * @param id ID of the new rule.
 * @param compiledRule a compiled binary rule
 * @param out_replaced set to 1 if there was a rule with the same ID that was replaced, or 0 if the rule is new.
 *     You may pass NULL if you don't care.
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *     valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_addRule(setmatch_RuleDB* db, int64_t id, setmatch_CompiledRule compiledRule, int* out_replaced);

/**
 * Compiles and adds a rule. This is equivalent to calling {@link setmatch_compileRuleJson}, {@link setmatch_addRule},
 * and {@link setmatch_deleteCompiledRule} all in one operation. If a rule with the same ID already existed, it will
 * be replaced. Note this may trigger a resize, which will cause all threads to pause for a few milliseconds.
 * @param db Rule database to add the rule to
 * @param id ID of the new rule.
 * @param ruleJson_utf8_nt Null-terminated UTF-8 string containing the JSON to compile.
 * @param out_replaced set to 1 if there was a rule with the same ID that was replaced, or 0 if the rule is new.
 *     You may pass NULL if you don't care.
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *     valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_addRuleJson(setmatch_RuleDB* db, int64_t id, const char* ruleJson_utf8_nt, int* out_replaced);


/**
 * Removes a rule with the given ID.
 * @param db Rule database to remove from
 * @param id ID of the rule to remove
 * @param out_removed set to 1 if there was a rule with the that ID that was removed, or 0 if that ID wasn't found.
 *     You may pass NULL if you don't care.
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *     valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_removeRule(setmatch_RuleDB* db, int64_t id, int* out_removed);

/**
 * Checks if the given ID is assigned to a rule in the database
 * @param db database to scan
 * @param id id to look for
 * @param out_found 1 iff the rule was found, 0 otherwise
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *     valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_containsRule(setmatch_RuleDB* db, int64_t id, int* out_found);

/**
 * Runs a matching operation, using a callback.
 * @param db database to match against
 * @param tokenIterator function that iterates through the tokens to match
 * @param tokenIteratorState state that will be passed into each call of tokenIterator
 * @param out_matches The matched IDs. There won't be any duplicates, but the order of IDs is implementation-defined
 *      (ie totally out of order). It will be a pointer to a thread-local buffer, and valid only until
 *     your next call to setmatch_match() on this thread.
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *         valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_match(setmatch_RuleDB* db, setmatch_TokenIterator tokenIterator, void* tokenIteratorState, setmatch_MatchCollection* out_matches);

/**
 * Runs a matching operation, using an array of null-terminated strings.
 * @param db database to match against
 * @param tokens array of null-terminated strings to use as tokens.
 * @param tokens_count number of elements in the tokens array
 * @param out_matches The matched IDs. There won't be any duplicates, but the order of IDs is implementation-defined
 *      (ie totally out of order). It will be a pointer to a thread-local buffer, and valid only until
 *     your next call to setmatch_match() on this thread.
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *         valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_matchArrayN(setmatch_RuleDB* db, const char** tokens, size_t tokens_count, setmatch_MatchCollection* out_matches);


/**
 * Retrieves some statistical information about the database, such as how long certain calls took. As of the current
 * version, the data you retrieve from this is implementation-defined and probably not especially useful. In the future,
 * I may document this.
 * @param db database to query
 * @param out_metrics the metric data. It points to a thread-local buffer that is valid until the next call to
 *     setmatch_pollMetrics().
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *     valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_pollMetrics(setmatch_RuleDB* db, setmatch_Metrics* out_metrics);

/**
 * Gets an implementation-defined string representation of a rule and stores it in the provided buffer. Truncates if
 * the buffer is too small. You should not rely on the format of this string, as it may change between versions, so
 * this function is mostly just for debugging. Result string is UTF-8 encoded and will be null-terminated. If there are
 * any embedded nulls or ASCII control characters in the rules, they will be escaped.
 * 
 * If the rule with the given ID is not found, out_found will be set to 0, and the buffer will contain a message about
 * that ID not existing.
 * @param db the database to search
 * @param id id of the rule to find
 * @param buf a buffer in which to store the result
 * @param buf_lengthBytes length of inout_buf parameter in bytes
 * @param out_found will be set to 1 if the rule was found, or 0 if not. pass NULL if you don't care (string will still
 *     be valid even if rule is not found).
 * @return Null-terminated description of an error if any occurred, or NULL on success. Error buffer is thread-local and
 *      valid until the next call to any setmatch function on this thread. Do not attempt to free it.
 * @deprecated This function returns an implementation-defined string that may be truncated.
 */
SETMATCH_IMPORT SETMATCH_NODISCARD const char* setmatch_debugGetRuleAsString(setmatch_RuleDB* db, int64_t id, char* buf, size_t buf_lengthBytes, int* out_found);

/**
 * Frees the database and all associated memory. Will not return an error.
 * @param db the database to free.
 */
SETMATCH_IMPORT void setmatch_deleteRuleDB(setmatch_RuleDB* db);

#ifdef __cplusplus
}
#endif
#endif
