/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test_common.hpp"
#include <boost/algorithm/string.hpp>
#include "../../thirdparty/rapidjson/writer.h"

namespace burningmime::setmatch { UBuffer compile(const char* ruleJson_utf8_nt); }
namespace burningmime::setmatch::test
{
    typedef rapidjson::Writer<rapidjson::StringBuffer> JsonWriter;
    static std::vector<std::string> tokenizeToVector(std::string& text)
    {
        std::vector<std::string> strs;
        boost::split(strs, text, boost::is_any_of(" "));
        return strs;
    }

    static void writePhrase(JsonWriter& writer, PhraseWrapper& phrase)
    {
        if(!phrase.isInterval())
        {
            writer.StartObject();
            writer.String("phrase");
            writer.StartArray();
            std::vector<std::string> tokens = tokenizeToVector(phrase.str);
            for(auto& token : tokens)
                writer.String(token.c_str());
            writer.EndArray();
            writer.EndObject();
        }
        else
        {
            writer.StartObject();
            writer.String("range");
            writer.StartObject();
            writer.String("term");
            writer.String(phrase.str.c_str());
            if(!std::isnan(phrase.min) && !std::isinf(phrase.min))
            {
                writer.String("min");
                writer.Double(phrase.min);
            }
            if(!std::isnan(phrase.max) && !std::isinf(phrase.max))
            {
                writer.String("max");
                writer.Double(phrase.max);
            }
            writer.String("kind");
            switch(phrase.ikind)
            {
                case IntervalKind::EXCLUSIVE: writer.String("EXCLUSIVE"); break;
                case IntervalKind::MAX_INCLUSIVE: writer.String("MAX_INCLUSIVE"); break;
                case IntervalKind::MIN_INCLUSIVE: writer.String("MIN_INCLUSIVE"); break;
                case IntervalKind::INCLUSIVE: writer.String("INCLUSIVE"); break;
                default: xassert_false("invalid enum value");
            }
            writer.EndObject();
            writer.EndObject();
        }
    }

    UBuffer encodeTestCase(const std::vector<std::vector<PhraseWrapper>>& subrules)
    {
        rapidjson::StringBuffer sb;
        JsonWriter writer(sb);

        writer.StartObject();
        writer.String("rule");
        writer.StartObject();
        writer.String("or");
        writer.StartArray();
        for(auto& subrule : subrules)
        {
            writer.StartObject();
            writer.String("and");
            writer.StartArray();
            for(auto& phrase : subrule)
            {
                if(phrase.isNegated)
                {
                    writer.StartObject();
                    writer.String("not");
                    writePhrase(writer, const_cast<PhraseWrapper&>(phrase));
                    writer.EndObject();
                }
                else
                {
                    writePhrase(writer, const_cast<PhraseWrapper&>(phrase));
                }
            }
            writer.EndArray();
            writer.EndObject();
        }
        writer.EndArray();
        writer.EndObject();
        writer.EndObject();

        return compile(sb.GetString());
    }

    UBuffer encodeStringList(const std::vector<std::string>& subrule)
    {
        std::vector<std::vector<PhraseWrapper>> lists;
        lists.reserve(1);
        std::vector<PhraseWrapper> inner;
        inner.reserve(subrule.size());
        for(auto& phrase : subrule)
            inner.emplace_back(phrase);
        lists.push_back(std::move(inner));
        return encodeTestCase(lists);
    }

    UBuffer encodeAsPhrase(const std::vector<std::string>& subrule)
    {
        std::ostringstream ss;
        bool isFirst = true;
        for(auto& phrase : subrule)
        {
            if(isFirst)
                isFirst = false;
            else
                ss << " ";
            ss << phrase;
        }

        std::vector<std::vector<PhraseWrapper>> lists;
        lists.reserve(1);
        std::vector<PhraseWrapper> inner;
        inner.reserve(1);
        inner.emplace_back(ss.str());
        lists.push_back(std::move(inner));
        return encodeTestCase(lists);
    }
}
