/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <string>
#include <vector>
#include <memory>
#include <boost/test/unit_test.hpp>
#include "../../main/common/common.hpp"
#include "../../main/common/interval.hpp"

// forward declare the pieces we can
namespace setmatch { class RuleDB; }
namespace burningmime::setmatch { struct Expression; }

namespace burningmime::setmatch::test
{
    //==================================================================================================================
    // Declaring tests
    //==================================================================================================================

    struct SignalToStacktraceScope
    {
        SignalToStacktraceScope();
        ~SignalToStacktraceScope();
        DISABLE_COPY_CONSTRUCTOR(SignalToStacktraceScope)
    };

    #define SIGNAL_TO_STACKTRACE_SCOPE ::burningmime::setmatch::test::SignalToStacktraceScope _SETMATCH_UNIQUE_NAME(signalToStacktraceScope){}
    #define WRAP_TEST_FUNCTION_CALL(TEST_NAME, TEST_EXPRESSION) do { SIGNAL_TO_STACKTRACE_SCOPE; { TEST_EXPRESSION; } } while(false)
    #define MAKE_TEST_CASE(TEST_FUNCTION, ...) BOOST_AUTO_TEST_CASE(TEST_FUNCTION, ##__VA_ARGS__) { \
            WRAP_TEST_FUNCTION_CALL(TEST_FUNCTION, (burningmime::setmatch::test::TEST_FUNCTION())); }
    #define MAKE_TEST_CASE_PARAM(TEST_NAME, TEST_FUNCTION, ...) BOOST_AUTO_TEST_CASE(TEST_NAME) { \
            WRAP_TEST_FUNCTION_CALL(TEST_NAME, (burningmime::setmatch::test::TEST_FUNCTION(__VA_ARGS__))); }

    // tests we don't want to run under valgrind because they're too slow or don't work
    #define TEST_TAG_DISABLED *boost::unit_test::disabled()
    #define TEST_TAG_NO_VALGRIND *boost::unit_test::label("novalgrind")

    //==================================================================================================================
    // General purpose helpers
    //==================================================================================================================

    void printMetrics(::setmatch::RuleDB& db);
    std::vector<std::vector<std::string>>& getNGrams();

    // tokenizer functor
    class SimpleTokenizer
    {
        const char* str;
        int index;
    public:
        explicit inline SimpleTokenizer(const char* _str) : str(_str), index(0) { }
        SimpleString operator()();
    };

    //==================================================================================================================
    // Convert iterable to string
    //==================================================================================================================

    template<typename T>
    inline std::string iterableToString(const T& iterable)
    {
        std::ostringstream ss;
        ss << "{";
        bool first = true;
        for(const auto& item : iterable)
        {
            if (!first)
                ss << ",";
            first = false;
            ss << item;
        }
        ss << "}";
        return ss.str();
    }

    //==================================================================================================================
    // Rule encoding
    //==================================================================================================================

    struct PhraseWrapper
    {
        std::string str;
        double min = 0;
        double max = 0;
        IntervalKind ikind = IntervalKind::INVALID;
        bool isNegated = false;
        PhraseWrapper(std::string str) : str(std::move(str)) { }
        PhraseWrapper(const char* str) : str(str) { }
        PhraseWrapper(std::string str, bool isNegated) : str(std::move(str)), isNegated(isNegated) { }
        PhraseWrapper(std::string str, double min, double max, IntervalKind ikind, bool isNegated = false)
                : str(std::move(str)), min(min), max(max), ikind(ikind), isNegated(isNegated) { }
        bool isInterval() const { return ikind != IntervalKind::INVALID; }
    };

    UBuffer encodeTestCase(const std::vector<std::vector<PhraseWrapper>>& lists);
    UBuffer encodeStringList(const std::vector<std::string>& subrule);
    UBuffer encodeAsPhrase(const std::vector<std::string>& subrule);

    //==================================================================================================================
    // Shitty parser that should only be used for tests
    // The reason it's here is because `a&b|c` is a fair bit easier to read than `{"rule":{"or":[{"phrase":["a"],{"and":...`
    // It's also the format that the expression to string will spit out (aside from some extra parens), so it's good for comparison
    //==================================================================================================================

    Expression parseBoolExprSimple(const char* str);
}
