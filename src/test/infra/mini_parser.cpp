/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test_common.hpp"
#include "../../main/boolean/boolean.hpp"

namespace burningmime::setmatch::test
{
    static std::string makePhraseString(char c)
    {
        std::string result;
        result.reserve(5);
        result.push_back((char) 1);
        result.push_back((char) 0);
        result.push_back((char) 1);
        result.push_back((char) 0);
        result.push_back(c);
        return result;
    }

    static MaybeNode makeStringTerm(char c, StringPool& strings)
    {
        int stringId = strings.getOrAdd(PredicateInfo(makePhraseString(c)));
        return BooleanNode::Leaf(stringId);
    }

    static double readDouble(char before, const char** p)
    {
        xassert(**p == before, "expected character '%c'", before); (*p)++;
        char* pEnd = nullptr;
        double result = std::strtod(*p, &pEnd);
        *p = pEnd;
        return result;
    }

    static MaybeNode makeIntervalTerm(const char** p, char c, StringPool& strings)
    {
        double left = readDouble('[', p);
        double right = readDouble(',', p);
        xassert(**p == ']', "expected close bracket"); (*p)++;
        Interval interval = Interval::makeNormalized(left, right, IntervalKind::INCLUSIVE);
        int stringId = strings.getOrAdd(PredicateInfo(makePhraseString(c)));
        IntervalPredicateInfo ip{interval, stringId};
        int rangeId = strings.getOrAdd(PredicateInfo(ip));
        return BooleanNode::Leaf(rangeId);
    }

    static MaybeNode parseOrExpr(const char** p, StringPool& strings); // for recursive call
    static MaybeNode parseTerm(const char** p, StringPool& strings)
    {
        bool negate = false;
        if(**p == '~')
        {
            negate = true;
            (*p)++;
        }

        if(**p == '(')
        {
            (*p)++;
            MaybeNode node = parseOrExpr(p, strings);
            xassert(**p == ')', "missing close paren");
            (*p)++;
            if(negate)
            {
                xassert((bool) node, "node should exist here");
                node.value().negate();
            }
            return node;
        }
        else
        {
            char c = **p;
            (*p)++;
            xassert((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'), "Unexpected character %c", c);
            MaybeNode node;
            if(**p == '[')
                node = makeIntervalTerm(p, c, strings);
            else
                node = makeStringTerm(c, strings);
            if(negate)
            {
                xassert((bool) node, "node should exist here");
                node.value().negate();
            }
            return node;
        }
    }

    typedef MaybeNode (*FParseNext)(const char**, StringPool&);
    template<NodeKind nOp, char chOp, FParseNext parseNext>
    static MaybeNode parseAndOr(const char** p, StringPool& strings)
    {
        MaybeNode oneNode = parseNext(p, strings);
        xassert((bool) oneNode, "node should exist here");
        if(**p == chOp)
        {
            std::vector<BooleanNode> children;
            children.push_back(std::move(oneNode.value()));
            while(**p == chOp)
            {
                (*p)++;
                children.push_back(std::move(parseNext(p, strings).value()));
            }
            return nOp == NodeKind::AND ?
                   std::move(BooleanNode::And(std::move(children))) :
                   std::move(BooleanNode::Or(std::move(children)));
        }
        else
        {
            return oneNode;
        }
    }

    static MaybeNode parseAndExpr(const char** p, StringPool& strings) { return parseAndOr<NodeKind::AND, '&', &parseTerm>(p, strings); }
    static MaybeNode parseOrExpr(const char** p, StringPool& strings) { return parseAndOr<NodeKind::OR, '|', &parseAndExpr>(p, strings); }

    Expression parseBoolExprSimple(const char* str)
    {
        StringPool strings;
        MaybeNode root = parseOrExpr(&str, strings);
        xassert((bool) root, "node should exist here");
        return Expression(std::move(root.value()), std::move(strings));
    }
}
