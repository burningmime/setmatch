/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test_common.hpp"
#include "../../main/common/sync.hpp"
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/thread/mutex.hpp>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <boost/thread/lock_guard.hpp>
#include <boost/dll/runtime_symbol_info.hpp>
#include "../../public/setmatch.hpp"
#include <csignal>

namespace fs = boost::filesystem;

namespace burningmime::setmatch::test
{
    //==================================================================================================================
    // Simple tokenizer functor
    //==================================================================================================================

    SimpleString SimpleTokenizer::operator()()
    {
        if(index == -1)
            return {nullptr, 0};
        int start = index;
        while(true)
        {
            char ch = str[index++];
            if(ch == ' ' || ch == '\0')
            {
                size_t len = (size_t) (index - start - 1);
                if(ch == '\0')
                    index = -1;
                if(len > 0)
                    return {str + start, len};
            }
        }
    }

    //==================================================================================================================
    // N-Grams test data
    //==================================================================================================================

    static boost::mutex mutex_singletons;
    static std::vector<std::vector<std::string>> nGramData;

    static std::string getNgramPath()
    {
        auto myDir = boost::dll::program_location().parent_path();
        const char* filename = "sample.txt";
        const char* repoRelativeDir = "java/src/test/resources";

        std::initializer_list<fs::path> paths =
        {
            myDir / filename,
            myDir / repoRelativeDir / filename,
            myDir.parent_path() / repoRelativeDir / filename,
            myDir.parent_path().parent_path() / repoRelativeDir / filename,
            myDir.parent_path().parent_path().parent_path() / repoRelativeDir / filename
        };

        for(auto& path : paths)
        {
            if(fs::is_regular_file(path))
            {
                logInfo("Loading test data file %s", path);
                return path.string();
            }
        }

        std::ostringstream err;
        err << "Could not find file " << filename << std::endl;
        err << "Tried: " << std::endl;
        for(auto& path : paths)
            err << "    " << path << std::endl;

        logError("%s", err.str().c_str());
        exit(1);
    }

    static std::vector<std::vector<std::string>> readNGrams(std::vector<std::vector<std::string>>& lines)
    {
        logInfo("======== DOING I/O ========");
        std::string path = getNgramPath();
        std::ifstream in(path);
        for(std::string line; std::getline(in, line);)
        {
            // just a smidgen more complex than...
            //     Arrays.stream(line.split(" ")).map(s -> s.replaceAll("[^a-zA-Z]", "").toLowerCase()).filter(s -> s.length() > 2).toArray(String[]::new);
            // which is *still* way more than it could be (see: python)
            // also way slower than java... could make it as fast by copying the strings instead of removing chars in place, but then it just gets to be even *more* of a bitch
            std::vector<std::string> tokens;
            std::vector<std::string> results;
            boost::split(tokens, line, boost::is_any_of(" \t"));
            for(std::string& token : tokens)
            {
                auto it = token.begin();
                while(it != token.end())
                {
                    char c = *it;
                    if(c >= 'A' && c <= 'Z') { *it = (char) (c + 32); ++it; }
                    else if(c >= 'a' && c <= 'z') { ++it; }
                    else { token.erase(it); }
                }
                if(token.length() > 2)
                    results.push_back(token);
            }
            if(!results.empty())
                lines.push_back(results);
        }
        logInfo("======== I/O DONE ========");
        xassert(!lines.empty(), "Something went very wrong when trying to read file %s", path);
        return lines;
    }

    std::vector<std::vector<std::string>>& getNGrams()
    {
        LOCK_GUARD_SCOPE(mutex_singletons);
        if(nGramData.empty())
            readNGrams(nGramData);
        return nGramData;
    }

    void printMetrics(::setmatch::RuleDB& db)
    {
        for(auto metric : db.pollMetrics())
            logInfo("%s = %.5f", metric.name(), metric.value());
    }

    #if !NDEBUG && !BOOST_OS_WINDOWS
        typedef void (*FSignalHandler)(int signum);
        static void stacktraceHandler(int signum, const char* signame, FSignalHandler oldHandler)
        {
            fprintf(stderr, "\n\n===========================================\nReceived signal %s\n", signame);
            fflush(stderr); // flush before stack trace in case getting the stack trace itself produces another segfault
            burningmime::setmatch::printStackTrace();
            fprintf(stderr, "===========================================\n\n");
            fflush(stderr);
            signal(SIGABRT, SIG_DFL);
            exit(signum); // don't bother trying to recover since there are multiple threads and stuff might be broken all over the place
        }

        #define FOREACH_SIGNAL(F) \
            F(SIGSEGV) \
            F(SIGFPE) \
            F(SIGABRT) \
            F(SIGTERM)

        #define DEFINE_SIGNAL_WRAPPER(S) \
            static FSignalHandler oldHandler_##S = nullptr; \
            static void stacktraceHandler_##S(int signum)  { stacktraceHandler(signum, #S, oldHandler_##S); }
        FOREACH_SIGNAL(DEFINE_SIGNAL_WRAPPER)
        #undef DEFINE_SIGNAL_WRAPPER

        SignalToStacktraceScope::SignalToStacktraceScope()
        {
            #define HOOK_SIGNAL_WRAPPER(S) oldHandler_##S = signal(S, &stacktraceHandler_##S);
            FOREACH_SIGNAL(HOOK_SIGNAL_WRAPPER)
            #undef HOOK_SIGNAL_WRAPPER
        }

        SignalToStacktraceScope::~SignalToStacktraceScope()
        {
            #define UNHOOK_SIGNAL_WRAPPER(S) { signal(S, oldHandler_##S); oldHandler_##S = nullptr; }
            FOREACH_SIGNAL(UNHOOK_SIGNAL_WRAPPER)
            #undef UNHOOK_SIGNAL_WRAPPER
        }

        #undef FOREACH_SIGNAL
    #else
        SignalToStacktraceScope::SignalToStacktraceScope() { }
            SignalToStacktraceScope::~SignalToStacktraceScope() { }
    #endif
}
