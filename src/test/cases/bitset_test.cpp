/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>
#include "../../main/common/bitset.hpp"
#include "../infra/test_common.hpp"

namespace burningmime::setmatch::test
{
    static void assignFromString(BitArray& ba, const char* chars)
    {
        ba.resize(strlen(chars));
        for(size_t i = 0; i < ba.bitCount(); ++i)
        {
            if(chars[i] == '1')
                ba.set((int) i);
            else if(chars[i] == '0')
                ba.unset((int) i);
            else
                BOOST_TEST_FAIL("Expected a '0' or '1' character");
        }
    }

    static void testEqualsString(BitArray& ba, const char* expectedChars)
    {
        std::string value;
        value.resize(ba.bitCount());
        for(size_t i = 0; i < ba.bitCount(); ++i)
            value[i] = ba.test((int) i) ? '1' : '0';
        std::string expected(expectedChars);
        BOOST_TEST(value == expected);
    }

    static void growShrinkGrow()
    {
        BitArray ba;
        assignFromString(ba, "101100101101");
        testEqualsString(ba, "101100101101");
        ba.resize(5);
        testEqualsString(ba, "10110");
        ba.resize(10);
        testEqualsString(ba, "1011000000");
    }

    BOOST_AUTO_TEST_SUITE(BitsetTest)
        MAKE_TEST_CASE(growShrinkGrow)
    BOOST_AUTO_TEST_SUITE_END()
}
