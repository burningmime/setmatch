/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../../main/predicate/pred_index.hpp"
#include "../infra/test_common.hpp"
#include <functional>
#include <unordered_set>

namespace burningmime::setmatch::test
{
    //==============================================================================================================
    // Helpers
    //==============================================================================================================

    class PhraseTrieTestContext
    {
        TokenMap tokenMap;
        PhraseTrie trie;
        std::vector<Predicate*> phrasesByIndex;
        BitArray results;

        void failWithRemaining(std::unordered_multiset<int>& matchSet, const char* document)
        {
            std::ostringstream ss;
            for(auto it : matchSet)
                ss << "\n  " << phrasesByIndex[it]->toString(tokenMap);
            BOOST_FAIL("While matching\n  " << document << "\nmissing phrase(s)" << ss.str());
        }

        void failWithUnexpected(int unexpectedIndex, const char* document)
        {
            BOOST_FAIL("While matching:\n  " << document << "\nfound unexpected phrase\n  " << phrasesByIndex[unexpectedIndex]->toString(tokenMap));
        }

    public:
        PhraseTrieTestContext() : tokenMap(), trie(tokenMap), phrasesByIndex(), results() { }
        int count() { return (int) phrasesByIndex.size(); }

        void expect(const char* document, std::initializer_list<int> matchList)
        {
            // convert expected matches to an unordered multiset
            std::unordered_multiset<int> matchSet;
            for(int m : matchList)
                matchSet.insert(m);

            // convert document to token id list
            std::vector<TaggedToken> tokenList;
            SimpleTokenizer tokenizer(document);
            while(true)
            {
                SimpleString ss = tokenizer();
                if(ss.length == 0) break;
                TokenId tid = tokenMap[HashedString(ss)];
                TaggedToken tt{tid};
                tokenList.push_back(tt);
            }

            // iterate matches
            results.clearAndResize(phrasesByIndex.size());
            trie.match(tokenList, results);
            for(int phraseIndex : results)
            {
                auto it2 = matchSet.find(phraseIndex);
                if(it2 == matchSet.end())
                    failWithUnexpected(phraseIndex, document);
                else
                    matchSet.erase(it2);
            }

            // if there are any matches left, some are unexpected
            if(!matchSet.empty())
                failWithRemaining(matchSet, document);
        }
        
        void add(int n, const char* str)
        {
            xassert(n == (int) phrasesByIndex.size(), "expected: %d, got %d", (int) phrasesByIndex.size(), n);
            FSimpleStringIterator it = SimpleTokenizer(str);
            Predicate *p = trie.getOrAdd(*it);
            xassert(p->isNew(), "test doesn't work w/ duplicates");
            int i = phrasesByIndex.size();
            p->setIndex(i);
            phrasesByIndex.push_back(p);
        }
        
        void remove(int n)
        {
            trie.removeAndFree(phrasesByIndex[n]); 
            phrasesByIndex[n] = nullptr;
        }
    };

    //==============================================================================================================
    // Main tests
    //==============================================================================================================

    static void testSimple()
    {
        PhraseTrieTestContext ctx;

        ctx.add(0, "enki was here");
        ctx.add(1, "enki was a cat");
        ctx.add(2, "enki was a lion");
        ctx.add(3, "lion named enki");
        ctx.add(4, "enki was");
        ctx.add(5, "a lion");
        ctx.add(6, "a lion king was born");
        ctx.add(7, "a lion prince was born");
        ctx.add(8, "red lion");
        ctx.add(9, "enki was a lion queen");

        ctx.expect("enki was a lion named enki was here", { 0, 2, 3, 4, 5 });
        ctx.expect("enki", {  });
        ctx.expect("a lion was here named enki", { 5 });
        ctx.expect("once there was a lion named enki", { 3, 5 });
        ctx.expect("once enki was a cat but now she is a lion", { 1, 4, 5 });
        ctx.expect("once enki was a cat and then enki was a lion queen", { 1, 4, 5, 2, 9 });
    }

    static void testAddChangesFailState()
    {
        PhraseTrieTestContext ctx;

        ctx.add(0, "anu is also a lion");

        ctx.expect("rafiki said a lion king was born but it was just anu", { });

        ctx.add(1, "a lion king was born");

        ctx.expect("rafiki said a lion king was born but it was just anu", { 1 });
        ctx.expect("you know anu is also a lion king", { 0 });
        ctx.expect("anu is also a lion king was born", { 0, 1 });
    }

    static void testRemoveChangesFailState()
    {
        PhraseTrieTestContext ctx;

        ctx.add(0, "anu is also a lion");
        ctx.add(1, "also a lion was enki");
        ctx.add(2, "a lion king was born");

        ctx.expect("anu is also a lion king was born", { 0, 2 });
        ctx.expect("anu is also a lion was enki", { 0, 1 });

        ctx.remove(1);

        ctx.expect("anu is also a lion king was born", { 0, 2 });
        ctx.expect("anu is also a lion was enki", { 0 });
    }

    static void testPromoteToHashAndBack()
    {
        PhraseTrieTestContext ctx;

        ctx.add(0, "anu is a cat");
        ctx.add(1, "anu is a wizard");

        ctx.expect("anu is a wizard", { 1 });
        ctx.expect("anu is a demon", { });

        ctx.add(2, "anu is a lion");
        ctx.remove(1);
        ctx.add(3, "anu is a tiger");
        ctx.add(4, "anu is a cheetah");
        ctx.add(5, "anu is a demon");
        ctx.add(6, "anu is a geomancer");
        ctx.add(7, "anu is a sleep");

        ctx.expect("anu is a cat", { 0 });
        ctx.expect("anu is a wizard", { });
        ctx.expect("anu is a demon", { 5 });

        ctx.remove(4);
        ctx.remove(0);
        ctx.remove(3);
        ctx.remove(6);

        ctx.expect("anu is a cat", { });
        ctx.expect("anu is a wizard", { });
        ctx.expect("anu is a demon", { 5 });
    }

    static void testCascadingOverlap()
    {
        PhraseTrieTestContext ctx;

        ctx.add(0, "it's an \xF0\x9F\xA6\x89wl");
        ctx.add(1, "an \xF0\x9F\xA6\x89wl named");
        ctx.add(2, "\xF0\x9F\xA6\x89wl named enki");
        ctx.add(3, "named enki the");
        ctx.add(4, "enki the \xF0\x9F\xA6\x89wl");
        ctx.add(5, "an \xF0\x9F\xA6\x89wl named enki");

        ctx.expect("omg look it's an \xF0\x9F\xA6\x89wl named enki the \xF0\x9F\xA6\x89wl", { 0, 1, 2, 3, 4, 5 });
    }

    // this is just to paper over the unit test framework API (also in case the test framework changes)
    BOOST_AUTO_TEST_SUITE(PhraseTrieTest)
        MAKE_TEST_CASE(testSimple)
        MAKE_TEST_CASE(testAddChangesFailState)
        MAKE_TEST_CASE(testRemoveChangesFailState)
        MAKE_TEST_CASE(testPromoteToHashAndBack)
        MAKE_TEST_CASE(testCascadingOverlap)
    BOOST_AUTO_TEST_SUITE_END()
}
