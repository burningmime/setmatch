/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../../main/index/db_index.hpp"
#include "../infra/test_common.hpp"
#include <random>
#include <stdexcept>

namespace burningmime::setmatch::test
{
    class IndexTestContext
    {
        DBConfig config;
        MetricRegistry metrics;
        CRuleDB db;

        void assertMatchesImpl(const char* document, std::initializer_list<int64_t> expected)
        {
            FSimpleStringIterator it = SimpleTokenizer(document);
            std::vector<int64_t>& results = *db.match(*it);
            std::vector<int64_t> vExpected(expected.begin(), expected.end());
            std::sort(vExpected.begin(), vExpected.end());
            std::sort(results.begin(), results.end());
            if(results != vExpected)
            {
                std::ostringstream ss;
                ss << "\n    When matching:\n        " << document << "\n    Matched rules:";
                for(auto ruleId : results)
                    ss << "\n        " << ruleId << ": " << db.debugGetRuleString(ruleId).second;
                ss << "\n    But expected:";
                for(auto ruleId : vExpected)
                    ss << "\n        " << ruleId << ": " << db.debugGetRuleString(ruleId).second;
                BOOST_TEST_ERROR(ss.str());
            }
        }

    public:
        IndexTestContext() : db(config, metrics) { }
        void addRule(int64_t id, const std::vector<std::vector<PhraseWrapper>>& lists) { addRule(id, encodeTestCase(lists)); }
        void addRule(int64_t id, UBuffer&& buf) { db.add(id, buf.data(), buf.length()); }
        template<typename... TArgs> inline void assertMatches(const char* document, TArgs... args) { assertMatchesImpl(document, { args... }); }
        void removeRule(int64_t id) { db.remove(id); }
        void reindex() { auto reindexData = db.prepareReindex(CancellationToken::makeNull()); db.applyReindex(*reindexData); }
    };

    static std::string makeLongString(size_t length)
    {
        std::string result;
        std::mt19937_64 rng{12345};
        result.resize(length);
        for(size_t i = 0; i < length; ++i)
        {
            // it should support the whole range. we'll have a separate test for embedded zeroes and unicode, so
            // it won't break the long string test if those start failing
            // also, we're skipping spaces because the test framework automatically splits on spaces, which we don't
            // want to do in this case
            result[i] = (char) (rng() % 94 + 33);
        }
        return result;
    }

    static PhraseWrapper negP(const char* s)
    {
        return PhraseWrapper{s, true};
    }

    static void testSimple()
    {
        IndexTestContext ctx;
        ctx.addRule(1, {{"anu", "enki"}, {"it's an \xF0\x9F\xA6\x89wl"}});
        ctx.addRule(2, {{"enki", "cat"}, {"owl", "lol", "lolcat", "cat"}, {"anu"}, {"kitten", "enki"}});
        ctx.addRule(3, {{"cat", "anu"}});
        ctx.reindex();
        ctx.assertMatches("lol it's a cat named enki", 2);
        ctx.assertMatches("lol it's a cat named anu", 2, 3);
        ctx.assertMatches("lol it's an \xF0\x9F\xA6\x89wl named enki", 1);
        ctx.assertMatches("lol it's an \xF0\x9F\xA6\x89wl named anu", 1, 2);
    }

    static void testNegationSimple()
    {
        IndexTestContext ctx;
        ctx.addRule(1, {{"sleep"}});
        ctx.addRule(2, {{"sleep", negP("dream")}});
        ctx.assertMatches("to die to sleep", 1, 2);
        ctx.assertMatches("to sleep perchance to dream", 1);
    }

    static void testNegationMulti()
    {
        IndexTestContext ctx;
        ctx.addRule(1, {{"sleep", negP("dream"), negP("trip-balls")}});
        ctx.assertMatches("sleep", 1);
        ctx.assertMatches("sleep dream", 1);
        ctx.assertMatches("sleep trip-balls", 1);
        ctx.assertMatches("sleep dream trip-balls");
    }

    static void testMaxSubrules()
    {
        IndexTestContext ctx;
        std::vector<std::vector<PhraseWrapper>> lists;
        for(size_t i = 1; i <= MAX_PHRASES_PER_SUBRULE; ++i)
        {
            std::vector<PhraseWrapper>& list = lists.emplace_back();
            list.emplace_back(std::to_string(i));
        }

        ctx.addRule(1, encodeTestCase(std::move(lists)));

        // everything up to here should match the rule
        for(size_t i = 1; i <= MAX_PHRASES_PER_SUBRULE; ++i)
            ctx.assertMatches(std::to_string(i).c_str(), 1);

        // now it's one beyond the end of what we added
        ctx.assertMatches(std::to_string(MAX_PHRASES_PER_SUBRULE + 1).c_str());
    }

    static void testLongTokens()
    {
        IndexTestContext ctx;

        std::string longStr = makeLongString(255);
        std::string longerStr = makeLongString(256);
        std::string evenLongerStr = makeLongString(400);
        std::string longestStr = makeLongString(800);
        std::string iLiedThisOneIsLonger = makeLongString(1500);

        ctx.addRule(1, {{longStr}});
        ctx.addRule(2, {{longerStr}});
        ctx.addRule(3, {{longStr}});
        ctx.addRule(4, {{longerStr}});
        ctx.addRule(5, {{longestStr}});

        // can't add a rule with more than 1024 characters
        BOOST_REQUIRE_THROW((ctx.addRule(6, {{iLiedThisOneIsLonger}})), std::runtime_error);

        ctx.assertMatches(longStr.c_str(), 1, 3);
        ctx.assertMatches(longerStr.c_str(), 2, 4);
        ctx.assertMatches(evenLongerStr.c_str());
        ctx.assertMatches(longestStr.c_str(), 5);
        ctx.assertMatches(iLiedThisOneIsLonger.c_str());
    }

    static void reindexWithSmall()
    {
        IndexTestContext ctx;

        ctx.addRule(1, {{"sleep"}});
        ctx.addRule(2, {{"sleep", negP("dream")}});
        ctx.addRule(3, {{"midsummer", negP("nights"), "dream"}});
        ctx.addRule(3, {{"dream", "of", negP("spring")}});
        ctx.addRule(5, {{negP("dream"), "of", negP("spring")}});

        ctx.assertMatches("to die to sleep", 1, 2);
        ctx.assertMatches("to sleep perchance to dream", 1);
        ctx.assertMatches("to sleep perchance to dream of winter", 1, 3, 5);
        ctx.assertMatches("to sleep perchance to dream of spring", 1);
        ctx.reindex();
        ctx.assertMatches("to die to sleep", 1, 2);
        ctx.assertMatches("to sleep perchance to dream", 1);
        ctx.assertMatches("to sleep perchance to dream of winter", 1, 3, 5);
        ctx.assertMatches("to sleep perchance to dream of spring", 1);
        ctx.removeRule(3);
        ctx.reindex();
        ctx.assertMatches("to die to sleep", 1, 2);
        ctx.assertMatches("to sleep perchance to dream", 1);
        ctx.assertMatches("to sleep perchance to dream of winter", 1, 5);
        ctx.assertMatches("to sleep perchance to dream of spring", 1);
    }

    static void intervalSimple()
    {
        // this is the same one as in the java example
        IndexTestContext ctx;

        PhraseWrapper ageUnder18{"age", negInf, 18, IntervalKind::EXCLUSIVE};
        PhraseWrapper age18To40{"age", 18, 40, IntervalKind::MIN_INCLUSIVE};
        PhraseWrapper ageOver40{"age", 40, posInf, IntervalKind::INCLUSIVE};
        PhraseWrapper ageEquals25{"age", 25, 25, IntervalKind::INCLUSIVE};

        ctx.addRule(1, {{ageUnder18, "rock"}});
        ctx.addRule(2, {{ageUnder18, "country"}, {ageEquals25, "country"}});
        ctx.addRule(3, {{ageOver40, "country"}});
        ctx.addRule(4, {{age18To40, "metal"}});
        ctx.addRule(5, {{ageOver40, "metal"}});

        ctx.assertMatches("{n=16}age country rock", 1, 2);
        ctx.assertMatches("{n=18}age country metal", 4);
        ctx.assertMatches("{n=25}age country metal", 2, 4);
        ctx.assertMatches("{n=58}age metal", 5);
    }

    static void allNegated()
    {
        IndexTestContext ctx;
        ctx.addRule(1, {{negP("apple"), negP("orange")}});
        ctx.addRule(2, {{negP("apple")}});
        ctx.addRule(3, {{negP("apple"), negP("orange")}});
        ctx.removeRule(1);
        ctx.assertMatches("banana apple", 3);
        ctx.assertMatches("grapes apple orange");
        ctx.assertMatches("lemon mango kitten", 2, 3);
    }

    static void removeMixed()
    {
        IndexTestContext ctx;
        ctx.addRule(1, {{PhraseWrapper("hello", 5, 10, IntervalKind::INCLUSIVE), "world"}});
        ctx.addRule(2, {{PhraseWrapper("hello", 5, 20, IntervalKind::INCLUSIVE), "world"}});
        ctx.assertMatches("world {n=8}hello", 1, 2);
        ctx.assertMatches("world {n=15}hello", 2);
        ctx.removeRule(1);
        ctx.assertMatches("world {n=8}hello", 2);
        ctx.removeRule(2);
        ctx.assertMatches("world {n=8}hello");
    }

    BOOST_AUTO_TEST_SUITE(CruelDBTest)
        MAKE_TEST_CASE(testSimple)
        MAKE_TEST_CASE(testNegationSimple)
        MAKE_TEST_CASE(testNegationMulti)
        MAKE_TEST_CASE(testMaxSubrules)
        MAKE_TEST_CASE(testLongTokens)
        MAKE_TEST_CASE(reindexWithSmall)
        MAKE_TEST_CASE(intervalSimple)
        MAKE_TEST_CASE(allNegated)
        MAKE_TEST_CASE(removeMixed)
    BOOST_AUTO_TEST_SUITE_END()
}
