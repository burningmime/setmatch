/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#if !defined(NDEBUG) && !defined(__MINGW64__)
#include <boost/stacktrace.hpp>
#include <sstream>
#include "../infra/test_common.hpp"

namespace burningmime::setmatch::test
{
    static void testStacktrace()
    {
        // this test may seem kind of useless, but stack traces broke at one point. so it's more a test that the build
        // environment is correctly configured for debug builds. having stacktraces is fairly important to me, so it's
        // definitely worth a test case
        std::ostringstream ss;
        ss << boost::stacktrace::stacktrace();
        std::string st = ss.str();
        std::string target = "burningmime::setmatch::test::StacktraceTest::testStacktrace";
        size_t pos = st.find(target);
        BOOST_REQUIRE_MESSAGE(pos != std::string::npos, "Expected to find string \"" << target << "\" in stacktrace string:\n" << st);
    }

    BOOST_AUTO_TEST_SUITE(StacktraceTest)
        MAKE_TEST_CASE(testStacktrace, TEST_TAG_NO_VALGRIND)
    BOOST_AUTO_TEST_SUITE_END()
}
#endif
