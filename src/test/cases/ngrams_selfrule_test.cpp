/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <atomic>
#include <boost/thread.hpp>
#include "../infra/test_common.hpp"
#include "../../public/setmatch.hpp"
#include <random>

// Extremely simple test where we gather up lines from a big-ish test file of ngrams, then use those lines both
// as the rules and as the matching input. lines will always match themselves, but often other rules as well
// (for example, duplicated lines, or short lines containing only a few words)

using namespace setmatch;

namespace burningmime::setmatch::test
{
    template<class T>
    static std::string join(const std::vector<T>& vec, const char* separator)
    {
        std::ostringstream ss;
        bool first = true;
        for(const auto& obj : vec)
        {
            if(!first)
                ss << separator;
            first = false;
            ss << obj;
        }
        return ss.str();
    }

    MAYBE_UNUSED static void releaseNgramsTest(RuleDB& db, std::vector<std::vector<std::string>>& lines)
    {
        SystemTime startTime = now();
        logInfo("Matching %d inputs", (int) lines.size());
        int32_t nThreads = (int32_t) boost::thread::hardware_concurrency();
        std::vector<std::unique_ptr<boost::thread>> threads;
        int32_t perThread = (int32_t) lines.size() / nThreads;
        std::atomic<int64_t> totalMatches = 0;
        for(int32_t iThread = 0; iThread < nThreads; iThread++)
        {
            int32_t idxStart = iThread * perThread;
            int32_t idxEnd = iThread == nThreads - 1 ? (int32_t) lines.size() : idxStart + perThread;
            threads.push_back(std::make_unique<boost::thread>(
                [&db, &totalMatches, &lines, idxStart, idxEnd, iThread]() -> void
                {
                    logInfo("Thread %d doing lines from %d to %d", iThread + 1, idxStart, idxEnd);
                    for(int idx = idxStart; idx < idxEnd; idx++)
                    {
                        auto results = db.match(lines[idx].begin(), lines[idx].end());
                        totalMatches += results.size();
                    }
                }));
        }
        for(auto& thread : threads)
            thread->join();
        logInfo("Total matches: %d", totalMatches.load());
        SystemTime endTime = now();
        double totalMs = toMilliseconds(endTime - startTime);
        double throughputMs = totalMs / lines.size();
        double latencyMs = throughputMs * nThreads;
        logInfo("Performance summary:\n            %-25s%10d\n            %-25s%10d\n            %-25s%10.4f\n            %-25s%10.4f\n            %-25s%10.4f",
                "Thread count", nThreads, "Line count", lines.size(), "Total Time (ms)", totalMs, "Throughput per line (ms)", throughputMs, "Latency per line (ms)", latencyMs);
    }

    MAYBE_UNUSED static void debugNgramsTest(RuleDB& db, std::vector<std::vector<std::string>>& lines)
    {
        std::mt19937_64 rng{12345};
        int iStart = (int) (rng() % (lines.size() - 100));
        int iEnd = iStart + 100;
        logInfo("Matching 100 inputs from %d to %d", iStart, iEnd);
        for(int i = iStart; i < iEnd; i++)
        {
            auto results = db.match(lines[i].begin(), lines[i].end());
            bool found = false;
            for(int64_t result : results)
                if(result == i + 1) // +1 beause rules are 1-indexed
                    found = true;

            if(!found)
            {
                std::ostringstream ss;
                ss << "failed to find " << i << " in the results list for that line" << std::endl;
                ss << "    found results:" << std::endl;
                for(auto result : results)
                    ss << "        " << result << ": " << db.debugGetRuleAsString(result) << std::endl;
                ss << "    line text was:" << std::endl << "        " << join(lines[i], " ");
                BOOST_TEST_FAIL(ss.str());
            }
        }
    }

    static void selfRuleTest()
    {
        RuleDB db;
        int64_t iRuleId = 1;
        auto& lines = getNGrams();
        for(auto& line : lines)
        {
            UBuffer encoded = encodeStringList(line);
            db.addRule(iRuleId++, encoded.data(), encoded.length());
        }
        #if NDEBUG
            // in release mode, test the whole thing
            releaseNgramsTest(db, lines);
        #else
            // in debug, only do the first 100 (or else it would take forever and not be meaningful)
            debugNgramsTest(db, lines);
        #endif
        printMetrics(db);
    }

    BOOST_AUTO_TEST_SUITE(NGramsSelfRuleTest)
        MAKE_TEST_CASE(selfRuleTest, TEST_TAG_NO_VALGRIND)
    BOOST_AUTO_TEST_SUITE_END()
}
