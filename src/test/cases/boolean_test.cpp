/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>
#include <boost/container/small_vector.hpp>
#include "../../main/boolean/boolean.hpp"
#include "../infra/test_common.hpp"
#include <iostream>

namespace burningmime::setmatch::test
{
    static void assertDnfEqual(const char* in, const char* out)
    {
        Expression ex = parseBoolExprSimple(in);
        MintermSet dnf = ex.toDnf();
        std::string actual = ex.dnfToString(dnf);
        std::string expected = out;
        BOOST_TEST(actual == expected);
    }

    static void testSimplifyAndDistribute()
    {
        assertDnfEqual("a&(b|c)", "(a&b)|(a&c)");
        assertDnfEqual("(((a&~b)|(c&d))&((b&~a)|d)&((c&~b)|d)&~(e&d))|(e&b&(~a|~c|~d))",
                "(a&d&~b&~e)|(c&d&~e)|(b&c&d&~a&~e)|(b&e&~a)|(b&e&~c)|(b&e&~d)");
    }

    static void testIntervalMergeBasic()
    {
        // there's another test suite that tests the main merging logic
        assertDnfEqual("a[-inf,1]&a[0,inf]", "(a[0,1])");
        assertDnfEqual("a[-inf,1]&a[-inf,0]", "(a[-inf,0])");
    }

    BOOST_AUTO_TEST_SUITE(BooleanTest)
        MAKE_TEST_CASE(testSimplifyAndDistribute)
        MAKE_TEST_CASE(testIntervalMergeBasic)
    BOOST_AUTO_TEST_SUITE_END()
}
