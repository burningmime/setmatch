/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <atomic>
#include <random>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include "../infra/test_common.hpp"
#include "../../main/index/sync_db.hpp"
#include "../../public/setmatch.hpp"

using namespace setmatch;

namespace burningmime::setmatch::test
{
    struct TestContext
    {
        RuleDB db;
        SystemTime stopAt;
        std::atomic<int64_t> maxRuleId;
        std::atomic<int> reindexCount;
    };

    static ThreadLocal<std::mt19937_64> rng;
    static size_t rand(size_t max) { return (*rng)() % max; }
    static void srand() { auto str = boost::lexical_cast<std::string>(boost::this_thread::get_id());
        auto hash = hashBytes(str.c_str(), str.size()); rng->seed((size_t) now().count() ^ hash); }

    static UBuffer doEncodeTest(std::vector<std::string>& subrule)
    {
        if((bool) rand(2))
            return encodeAsPhrase(subrule);
        else
            return encodeStringList(subrule);
    }

    static void adderThread(TestContext* ctx)
    {
        srand();
        auto& lines = getNGrams();
        while(now() < ctx->stopAt)
        {
            auto index = (int) rand(lines.size());
            UBuffer encoded = doEncodeTest(lines[index]);
            ctx->db.addRule((ctx->maxRuleId)++, encoded.data(), encoded.length());
            boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
        }
    }

    static void removerThread(TestContext* ctx)
    {
        srand();
        while(now() < ctx->stopAt)
        {
            if(ctx->maxRuleId == 0)
                boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
            else
            {
                auto ruleId = (int64_t) rand((size_t) ctx->maxRuleId);
                ctx->db.removeRule(ruleId);
                boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
            }
        }
    }

    static void replacerThread(TestContext* ctx)
    {
        srand();
        auto& lines = getNGrams();
        while(now() < ctx->stopAt)
        {
            if(ctx->maxRuleId == 0)
                boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
            else
            {
                auto ruleId = (int64_t) rand((size_t) ctx->maxRuleId);
                auto index = (int) rand(lines.size());
                UBuffer encoded = encodeStringList(lines[index]);
                ctx->db.addRule(ruleId, encoded.data(), encoded.length());
                boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
            }
        }
    }

    static void matcherThread(TestContext* ctx)
    {
        srand();
        auto& lines = getNGrams();
        while(now() < ctx->stopAt)
        {
            auto index = (int) rand(lines.size());
            ctx->db.match(lines[index].begin(), lines[index].end());
        }
    }

    static void reindexThread(TestContext* ctx)
    {
        while(now() < ctx->stopAt)
        {
            boost::this_thread::sleep_for(boost::chrono::seconds(10));
            ((SRuleDB*) *((void**) &(ctx->db)))->reindex(); // for now, do it this way... once reindexing is brought back, we'll look into doing it some other way
            logInfo("Time remaining: %f", toSeconds(ctx->stopAt - now()));
            ++(ctx->reindexCount);
        }
    }

    #define startNThreads(F, N) _startNThreads(N, &ctx, threads, &F)
    static void _startNThreads(int n, TestContext* ctx,
            std::vector<std::unique_ptr<boost::thread>>& threads,
            void (*function)(TestContext*))
    {
        for(int i = 0; i < n; ++i)
            threads.push_back(std::make_unique<boost::thread>(function, ctx));
    }

    static void doChaosTest()
    {
        getNGrams(); // do I/O before starting test
        logInfo("============ RUNNING CHAOS TEST FOR 120 SECONDS ============");
        TestContext ctx;
        ctx.maxRuleId = 0;
        ctx.reindexCount = 0;
        ctx.stopAt = now() + fromSeconds(120);
        std::vector<std::unique_ptr<boost::thread>> threads;

        startNThreads(adderThread, 4);
        startNThreads(removerThread, 2);
        startNThreads(replacerThread, 2);
        startNThreads(matcherThread, 4);
        startNThreads(reindexThread, 1);

        for(auto& thread : threads)
            thread->join();
        printMetrics(ctx.db);
        BOOST_TEST(ctx.reindexCount > 10, "This can fail if writer threads are being starved by a constant barrage"
                " of readers (as when std::thread is used instead of boost::thread). Ideally, there will be 11 or 12.");
    }

    BOOST_AUTO_TEST_SUITE(ChaosTest)
        MAKE_TEST_CASE(doChaosTest, TEST_TAG_DISABLED)
    BOOST_AUTO_TEST_SUITE_END()

    #undef startNThreads
}
