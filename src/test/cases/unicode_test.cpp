/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../infra/test_common.hpp"

namespace burningmime::setmatch::test
{
    static void _checkString(const char* buf, size_t length, std::string expected)
    {
        // test cleaning itself
        std::string cleaned = cleanUtf8(buf, length);
        BOOST_TEST(cleaned == expected);

        // converting from utf16 and back should not change anything
        std::string converted = utf16to8(utf8to16(cleaned));
        BOOST_TEST(converted == expected);
    }

    // macros make this work w/ embedded nulls
    #define checkString(LITERAL, EXPECTED) _checkString(LITERAL, sizeof(LITERAL) - 1, EXPECTED)
    #define checkStringWithThrow(INPUT, EXPECTED) \
            checkString(INPUT, EXPECTED); \
            BOOST_CHECK_THROW(utf8to16(INPUT, sizeof(INPUT) - 1), std::runtime_error)

    static void asciiOnly()
    {
        checkString("", "");
        checkString("hello world", "hello world");
    }

    static void controlAndNull()
    {
        checkString("\0hello\0", "\\x00hello\\x00");
        checkString("hello\0world", "hello\\x00world");
        checkString("hello\tworld", "hello    world");
        checkString("hello\x7fworld", "hello\\x7fworld");
    }

    static void newlines()
    {
        checkString("hello\r", "hello\\x0d");
        checkString("hello\r\n", "hello\n");
        checkString("hello\n", "hello\n");
        checkString("hello\r\r\n\rworld", "hello\\x0d\n\\x0dworld");
    }

    static void utf8Alloc()
    {
        // thanks to: https://www.utf8-chartable.de/unicode-utf8-table.pl
        checkString(u8"Ø", u8"Ø"); // 2 byte
        checkString(u8"หวัดดีชาวโลก", u8"หวัดดีชาวโลก"); // 3 byte e0
        checkString(u8"⍨", u8"⍨"); // 3 byte e1..ec
        checkString(u8"ힸ", u8"ힸ"); // 3 byte ed
        checkString(u8"Ａ", u8"Ａ"); // 3 byte ef
        checkString(u8"🐿️", u8"🐿️"); // 4 bytes f0
    }

    static void utf8Unalloc()
    {
        // nothing starting with F1..F4 has been allocated, but these should be valid codepoints, so not escaped and not throwing exceptions
        checkString("\xf1\x8d\x8d\xa1", "\xf1\x8d\x8d\xa1");
        checkString("\xf4\x8d\x8d\x8d", "\xf4\x8d\x8d\x8d");
    }

    static void utf8Invalid()
    {
        // invalid 2-byte sequences
        checkStringWithThrow("\xc1\x95", "\\xc1\\x95");
        checkStringWithThrow("\x80X", "\\x80X");
        checkString("\xc2", "\\xc2"); // for some reason, incomplete sequences don't throw
        checkStringWithThrow("\xc2X", "\\xc2X");

        // invalid 3-byte sequences
        checkStringWithThrow("\xe0\x90\x90", "\\xe0\\x90\\x90");
        checkStringWithThrow("\xe0\xa0\xd0", "\\xe0\\xa0\\xd0");
        checkString("\xe1\x85", "\\xe1\\x85");
        checkString("\xed\xa0\x80", "\\xed\\xa0\\x80"); // according to uinicode standard this is invalid, but codecvt accepts it so whatever

        // invalid 4-byte sequences
        checkStringWithThrow("\xf0\x85\x90\x90", "\\xf0\\x85\\x90\\x90");
        checkString("\xf0\x95\x96", "\\xf0\\x95\\x96");
    }

    BOOST_AUTO_TEST_SUITE(UnicodeTest)
        MAKE_TEST_CASE(asciiOnly)
        MAKE_TEST_CASE(controlAndNull)
        MAKE_TEST_CASE(newlines)
        MAKE_TEST_CASE(utf8Alloc)
        MAKE_TEST_CASE(utf8Unalloc)
        MAKE_TEST_CASE(utf8Invalid)
    BOOST_AUTO_TEST_SUITE_END()

    #undef checkString
    #undef checkStringWithThrow
}
