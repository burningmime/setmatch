/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../infra/test_common.hpp"
#include "../../main/predicate/tag.hpp"

namespace burningmime::setmatch::test
{
    // need to use macros to support embedded nulls -- strlen() doesn't work
    #define checkValid(IN, EXPECTED, ...) _checkValid(SimpleString{IN, sizeof(IN) - 1}, std::string(EXPECTED, sizeof(EXPECTED) - 1), ##__VA_ARGS__)
    #define checkInvalid(X) BOOST_CHECK_THROW(parseTags(SimpleString{X, sizeof(X) - 1}), std::runtime_error)
    static void _checkValid(SimpleString in, std::string expectedString, double expectedValue = NaN)
    {
        TaggedString out = parseTags(in);
        std::string outString(out.ptr(), out.length());
        BOOST_TEST(outString == expectedString);
        if(std::isnan(expectedValue))
        {
            BOOST_TEST(!out.hasValue());
        }
        else
        {
            BOOST_TEST(expectedValue == out.value, boost::test_tools::tolerance(0.00001));
        }
    }

    static void validThings()
    {
        checkValid("", "");
        checkValid(u8"🐿️", u8"🐿️");
        checkValid("{{", "{");
        checkValid("{{hello", "{hello");
        checkValid("{n=5}{{hello", "{hello", 5);
        checkValid("{n=-5.5} ", " ", -5.5);
        checkValid("{n=1.06e-3}x", "x", 0.00106);
        checkValid("{n=.3}x", "x", 0.3);
        checkValid("{n=-.3}x", "x", -0.3);
        checkValid("{{n=5}x", "{n=5}x");
        checkValid("hello\0world", "hello\0world");
        checkValid("{n=5}\0", "\0", 5);
    }

    static void invalidThings()
    {
        checkInvalid("{hello}");
        checkInvalid("{");
        checkInvalid("{n=}x");
        checkInvalid("{n=1}");
        checkInvalid("{n=n}x");
        checkInvalid("{n=5n}x");
        checkInvalid("{n=5}{n=10}x");
        checkInvalid("{n=5}{x");
        checkInvalid("{n=5 }x");
        checkInvalid("{n=5e-}x");
        checkInvalid("{n=-}x");
        checkInvalid("{n=NaN}x");
        checkInvalid("{n=inf}x");
        checkInvalid("{n=5\0}x");
    }

    BOOST_AUTO_TEST_SUITE(TagParser)
        MAKE_TEST_CASE(validThings)
        MAKE_TEST_CASE(invalidThings)
    BOOST_AUTO_TEST_SUITE_END()
}