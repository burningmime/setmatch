/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../infra/test_common.hpp"
#include "../../public/setmatch.hpp"
#include <iostream>

using namespace std::literals::string_literals;

namespace burningmime::setmatch::test
{
    static void testMatch(::setmatch::RuleDB& db, std::initializer_list<std::string> tokens, std::initializer_list<int64_t> expected)
    {
        ::setmatch::MatchCollection matches = db.match(tokens.begin(), tokens.end());
        matches.sort();
        BOOST_TEST(iterableToString(matches) == iterableToString(expected));
    }

    static void doPublicTest(::setmatch::RuleDB& db)
    {
        db.addRuleJson(1, R"_({"rule":{"phrase":["sports"]}})_"s);
        db.addRuleJson(2, R"_({"rule":{"and":[{"phrase":["games"]},{"phrase":["18-25"]}]}})_"s);
        db.addRuleJson(3, R"_({"rule":{"or":[{"phrase":["games"]},{"phrase":["sports"]}]}})_"s);
        db.addRuleJson(4, R"_({"rule":{"and":[{"phrase":["news"]},{"not":{"phrase":["18-25"]}}]}})_"s);
        db.addRuleJson(5, R"_({"rule":{"and":[{"or":[{"phrase":["18-25"]},{"phrase":["25-40"]}]},{"phrase":["sports"]}]}})_"s);
        db.addRuleJson(6, R"_({"rule":{"and":[{"not":{"or":[{"phrase":["games"]},{"phrase":["sports"]}]}},{"phrase":["news"]}]}})_"s);
        testMatch(db, { "18-25", "games", "sports" }, {1, 2, 3, 5});
        testMatch(db, { "18-25", "news", "technology" }, {6});
        testMatch(db, { "25-40", "games", "news", "sports" }, {1, 3, 4, 5});
        testMatch(db, { "40-50", "sports" }, {1, 3});
        BOOST_TEST(db.containsRule(4));
        db.removeRule(4);
        testMatch(db, { "25-40", "games", "news", "sports" }, {1, 3, 5});
        BOOST_TEST(!db.containsRule(4));
        BOOST_TEST(db.debugGetRuleAsString(1) == R"_(("sports"))_"s);
    }

    static void cPlusPlusExample()
    {
        // chaos test and selfrule test both also use the public api but don't run under valgrind
        ::setmatch::RuleDB db;
        doPublicTest(db);
        printMetrics(db);
    }

    static void metricsDisabled()
    {
        ::setmatch::RuleDB db{R"_({"collectMetrics": false})_"s};
        doPublicTest(db);
        BOOST_CHECK_THROW(printMetrics(db), ::setmatch::SetmatchError);
    }

    BOOST_AUTO_TEST_SUITE(PublicApiTest)
        MAKE_TEST_CASE(cPlusPlusExample)
        MAKE_TEST_CASE(metricsDisabled)
    BOOST_AUTO_TEST_SUITE_END()
}
