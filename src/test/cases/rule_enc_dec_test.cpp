/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sstream>
#include "../infra/test_common.hpp"
#include "../../main/boolean/rule_dec.hpp"

namespace burningmime::setmatch::test
{
    #define SMINTERVAL(MIN, MAX, KIND) Interval::makeNormalized(MIN, MAX, IntervalKind::KIND)
    static ParseToken TokString(const char* str) { return ParseToken::string(SimpleString{str, strlen(str)}); }

    static void testTokens(const UBuffer& encoded, std::vector<ParseToken> expectedTokens)
    {
        std::vector<ParseToken> actualTokens = decodeBinaryRule(encoded.data(), encoded.length());
        bool success = false;
        if(actualTokens.size() == expectedTokens.size())
        {
            success = true;
            for(size_t i = 0; i < actualTokens.size(); ++i)
            {
                if(!actualTokens[i].deepEquals(expectedTokens[i]))
                {
                    success = false;
                    break;
                }
            }
        }

        if(!success)
        {
            std::ostringstream ss;
            ss << "Expected tokens:\n";
            for(ParseToken& t : expectedTokens)
                ss << "  " << t.debugString() << "\n";
            ss << "But got tokens:\n";
            for(ParseToken& t : actualTokens)
                ss << "  " << t.debugString() << "\n";
            ss << "(note if these look the same and you're using range predicates, it might be mismatched interval kinds)";
            BOOST_TEST_ERROR(ss.str());
        }
    }

    static void testPhraseOnly()
    {
        testTokens(encodeTestCase({{
            "hello",
            PhraseWrapper("world", true)
        }}), {
            ParseToken::beginRule(2),
            ParseToken::beginSubrule(),
            ParseToken::beginPredicate({PredicateType::PHRASE, false}),
            TokString("hello"),
            ParseToken::beginPredicate({PredicateType::PHRASE, true}),
            TokString("world"),
            ParseToken::end()
        });
    }

    static void testIntervalOnly()
    {
        testTokens(encodeTestCase({{
            PhraseWrapper("toko", NaN, 79, IntervalKind::INCLUSIVE),
            PhraseWrapper("ibuki", 76, 77, IntervalKind::EXCLUSIVE),
            PhraseWrapper("miu", 99, NaN, IntervalKind::MAX_INCLUSIVE)
        }}), {
            ParseToken::beginRule(3),
            ParseToken::beginSubrule(),
            ParseToken::beginPredicate({PredicateType::RANGE, false}),
            TokString("toko"),
            ParseToken::interval(SMINTERVAL(negInf, 79, INCLUSIVE)),
            ParseToken::beginPredicate({PredicateType::RANGE, false}),
            TokString("ibuki"),
            ParseToken::interval(SMINTERVAL(76, 77, EXCLUSIVE)),
            ParseToken::beginPredicate({PredicateType::RANGE, false}),
            TokString("miu"),
            ParseToken::interval(SMINTERVAL(99, posInf, MAX_INCLUSIVE)),
            ParseToken::end()
        });
    }

    static void testIntervalMergeSimple()
    {
        testTokens(encodeTestCase({{
           PhraseWrapper("monokuma", 0, NaN, IntervalKind::INCLUSIVE),
           PhraseWrapper("monokuma", NaN, 1, IntervalKind::INCLUSIVE)
        }}), {
           ParseToken::beginRule(1),
           ParseToken::beginSubrule(),
           ParseToken::beginPredicate({PredicateType::RANGE, false}),
           TokString("monokuma"),
           ParseToken::interval(SMINTERVAL(0, 1, INCLUSIVE)),
           ParseToken::end()
       });
    }

    BOOST_AUTO_TEST_SUITE(RuleDecoderTest)
        MAKE_TEST_CASE(testPhraseOnly)
        MAKE_TEST_CASE(testIntervalOnly)
        MAKE_TEST_CASE(testIntervalMergeSimple)
    BOOST_AUTO_TEST_SUITE_END()

    #undef SMINTERVAL
}
