/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../infra/test_common.hpp"
#include <sstream>
#include <random>
#include "../../main/predicate/pred_index.hpp"

namespace burningmime::setmatch::test
{
    #define SMINTERVAL(MIN, MAX, KIND) Interval::makeNormalized(MIN, MAX, IntervalKind::KIND)

    struct IntervalTestContext
    {
        TokenMap tokenMap;
        IntervalIndex index;
        std::vector<Predicate*> predicatesByIndex;
        BitArray results;

        void add(int n, const char* token, Interval interval)
        {
            BOOST_TEST(n == (int) predicatesByIndex.size());
            SimpleString str{token, strlen(token)};
            Predicate* predicate = index.getOrAdd(str, interval);
            BOOST_TEST(predicate->isNew());
            predicate->setIndex(n);
            predicatesByIndex.push_back(predicate);
        }

        void dup(int n, const char* token, Interval interval)
        {
            BOOST_TEST(n < (int) predicatesByIndex.size());
            SimpleString str{token, strlen(token)};
            Predicate* predicate = index.getOrAdd(str, interval);
            BOOST_TEST(predicate->getIndex() == n);
            BOOST_TEST(predicate == predicatesByIndex[n]);
        }

        std::string search(const char* token, double value)
        {
            results.clearAndResize(predicatesByIndex.size());
            std::vector<TaggedToken> input;
            input.push_back(TaggedToken{tokenMap[HashedString{token, (uint32_t) strlen(token)}], value});
            index.match(input, results);
            return iterableToString(results);
        }

        void remove(int n)
        {
            index.removeAndFree(predicatesByIndex[n]);
            predicatesByIndex[n] = nullptr;
        }

        // for when you're testing intervals and don't care about the token
        void add(int n, Interval interval) { add(n, "test", interval); }
        void dup(int n, Interval interval) { dup(n, "test", interval); }
        std::string search(double value) { return search("test", value); }

        IntervalTestContext() : index(tokenMap) { }
        DISABLE_COPY_CONSTRUCTOR(IntervalTestContext)
    };

    static void denormalHandling()
    {
        double minNormal = std::numeric_limits<double>::min();
        double nextNormal = std::nextafter(minNormal, posInf);
        double minDenormal = std::nextafter(0, posInf);

        // first some basic tests of floating point; these should pass on any x64 CPU
        BOOST_TEST(std::isnormal(minNormal));
        BOOST_TEST(std::isnormal(nextNormal));
        BOOST_TEST(!std::isnormal(minDenormal));
        BOOST_TEST(nextNormal > minNormal);
        BOOST_TEST(minNormal > minDenormal);
        BOOST_TEST(minDenormal > 0);

        Interval i1 = SMINTERVAL(0, minDenormal, MAX_INCLUSIVE);
        BOOST_TEST(i1.getLeft() == 0);
        BOOST_TEST(i1.getRight() == 0);

        Interval i2 = SMINTERVAL(-5, -minNormal, MIN_INCLUSIVE);
        BOOST_TEST(i2.getLeft() == -5);
        BOOST_TEST(i2.getRight() == -nextNormal);

        Interval i3 = SMINTERVAL(-minNormal, minNormal, EXCLUSIVE);
        BOOST_TEST(i3.getLeft() == 0);
        BOOST_TEST(i3.getRight() == 0);

        Interval i4 = SMINTERVAL(minDenormal, minNormal, INCLUSIVE);
        BOOST_TEST(i4.getLeft() == 0);
        BOOST_TEST(i4.getRight() == minNormal);

        Interval i5 = SMINTERVAL(minNormal, 5, MAX_INCLUSIVE);
        BOOST_TEST(i5.getLeft() == nextNormal);
        BOOST_TEST(i5.getRight() == 5);
    }

    static void promoteToListAndBack()
    {
        IntervalTestContext ctx;
        Interval i0 = SMINTERVAL(negInf, 11037, INCLUSIVE);
        Interval i1 = SMINTERVAL(11037, posInf, INCLUSIVE);
        Interval i2 = SMINTERVAL(11036.9, 11037.1, EXCLUSIVE);
        Interval i3 = SMINTERVAL(11037, 11037, INCLUSIVE);
        Interval i4 = SMINTERVAL(11036, 11037, INCLUSIVE);
        Interval i5 = SMINTERVAL(11037, 11038, INCLUSIVE);
        ctx.add(0, i0);
        ctx.dup(0, i0);
        ctx.add(1, i1);
        ctx.dup(1, i1);
        ctx.dup(0, i0);
        ctx.add(2, i2);
        ctx.add(3, i3);
        ctx.add(4, i4);
        ctx.add(5, i5);
        BOOST_TEST(ctx.search(11036) == "{0,4}");
        BOOST_TEST(ctx.search(11037) == "{0,1,2,3,4,5}");
        BOOST_TEST(ctx.search(11038) == "{1,5}");
        BOOST_TEST(ctx.tokenMap.size() == 1);
        ctx.remove(0);
        ctx.dup(1, i1);
        BOOST_TEST(ctx.search(11036) == "{4}");
        BOOST_TEST(ctx.search(11037) == "{1,2,3,4,5}");
        BOOST_TEST(ctx.search(11038) == "{1,5}");
        ctx.remove(2);
        BOOST_TEST(ctx.search(11036) == "{4}");
        BOOST_TEST(ctx.search(11037) == "{1,3,4,5}");
        BOOST_TEST(ctx.search(11038) == "{1,5}");
        ctx.remove(1);
        ctx.remove(4);
        ctx.remove(5);
        BOOST_TEST(ctx.search(11036) == "{}");
        BOOST_TEST(ctx.search(11037) == "{3}");
        BOOST_TEST(ctx.search(11038) == "{}");
        ctx.remove(3);
        BOOST_TEST(ctx.tokenMap.size() == 0);
    }

    static void rtreeAdhoc()
    {
        IntervalTestContext ctx;

        // make sure we have an rtree
        for(int i = 0; i < 10; ++i)
            ctx.add(i, SMINTERVAL(i, i, INCLUSIVE));

        Interval i0 = SMINTERVAL(negInf, 11037, INCLUSIVE);
        Interval i1 = SMINTERVAL(11037, posInf, INCLUSIVE);
        Interval i2 = SMINTERVAL(11036.9, 11037.1, EXCLUSIVE);
        Interval i3 = SMINTERVAL(11037, 11037, INCLUSIVE);
        Interval i4 = SMINTERVAL(11036, 11037, INCLUSIVE);
        Interval i5 = SMINTERVAL(11037, 11038, INCLUSIVE);
        Interval i6 = SMINTERVAL(11037, 11038, MAX_INCLUSIVE);
        ctx.add(10, i0);
        ctx.dup(10, i0);
        ctx.add(11, i1);
        ctx.dup(11, i1);
        ctx.dup(10, i0);
        ctx.add(12, i2);
        ctx.add(13, i3);
        ctx.add(14, i4);
        ctx.add(15, i5);
        ctx.add(16, i6);
        BOOST_TEST(ctx.search(11036) == "{10,14}");
        BOOST_TEST(ctx.search(11037) == "{10,11,12,13,14,15}");
        BOOST_TEST(ctx.search(11038) == "{11,15,16}");
        ctx.remove(10);
        ctx.dup(11, i1);
        BOOST_TEST(ctx.search(11036) == "{14}");
        BOOST_TEST(ctx.search(11037) == "{11,12,13,14,15}");
        BOOST_TEST(ctx.search(11038) == "{11,15,16}");
        ctx.remove(12);
        BOOST_TEST(ctx.search(11036) == "{14}");
        BOOST_TEST(ctx.search(11037) == "{11,13,14,15}");
        BOOST_TEST(ctx.search(11038) == "{11,15,16}");
        for(int i = 0; i < 10; ++i)
            ctx.remove(i);
        ctx.remove(11);
        ctx.remove(13);
        ctx.remove(14);
        ctx.remove(16);
        BOOST_TEST(ctx.search(11036) == "{}");
        BOOST_TEST(ctx.search(11037) == "{15}");
        BOOST_TEST(ctx.search(11038) == "{15}");
        ctx.remove(15);
        BOOST_TEST(ctx.tokenMap.size() == 0);
    }

    static void rtreeBig()
    {
        // this requires both the btree and rtree to split down at least two levels so we're not just testing at the
        // root nodes. that's a bit of an implementation detail, but consider this a white box test since I'd rather
        // not expose all the node set stuff in an api, even to tests
        IntervalTestContext ctx;
        std::mt19937_64 rng{11037};
        std::uniform_real_distribution<double> dist(0, 100);
        std::vector<Interval> naieve;
        std::vector<double> checkNumbers;

        #define addBoth(MIN, MAX, KIND) do \
        { \
            Interval interval = SMINTERVAL(MIN, MAX, KIND); \
            ctx.add(j++, interval); \
            naieve.push_back(interval); \
        } while(false)
        double prev = NaN;
        for(int i = 0, j = 0; i < 300; ++i)
        {
            double n = dist(rng);
            checkNumbers.push_back(n);
            checkNumbers.push_back(nextAfterFinite(n)); // floating point is such a tricky thing
            checkNumbers.push_back(nextBeforeFinite(n));
            addBoth(n, n, INCLUSIVE);
            addBoth(negInf, n, INCLUSIVE);
            addBoth(n, posInf, INCLUSIVE);
            if(!std::isnan(prev))
            {
                double min = std::min(prev, n);
                double max = std::max(prev, n);
                switch(rng() % 4)
                {
                    case 0: addBoth(min, max, MAX_INCLUSIVE); break;
                    case 1: addBoth(min, max, MIN_INCLUSIVE); break;
                    case 2: addBoth(min, max, EXCLUSIVE); break;
                    case 3: addBoth(min, max, INCLUSIVE); break;
                }
            }
            prev = n;
        }
        #undef addBoth

        // intersperse some new numbers with the extreme values
        // note that we want to test with the original end values to make sure the handling of open/closed
        // intervals is done correctly
        for(int i = 0; i < 100; ++i)
            checkNumbers.push_back(dist(rng));
        std::shuffle(checkNumbers.begin(), checkNumbers.end(), rng);

        BitArray expected;
        for(auto n : checkNumbers)
        {
            expected.clearAndResize(naieve.size());
            for(int i = 0; i < (int) naieve.size(); ++i)
                if(naieve[i].contains(n))
                    expected.set(i);
            BOOST_TEST(ctx.search(n) == iterableToString(expected));
            // if there are any errors here, we can generate a string showing the exact differences, but as long
            // as this is passing I don't see that as priority
        }
    }

    BOOST_AUTO_TEST_SUITE(IntervalIndexTest)
        MAKE_TEST_CASE(denormalHandling)
        MAKE_TEST_CASE(promoteToListAndBack)
        MAKE_TEST_CASE(rtreeAdhoc)
        MAKE_TEST_CASE(rtreeBig)
    BOOST_AUTO_TEST_SUITE_END()

    #undef SMINTERVAL
}
