/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../infra/test_common.hpp"
#include "../../main/common/interval.hpp"
#include <sstream>

namespace burningmime::setmatch::test
{
    static std::string toStr(Interval interval)
    {
        if(!interval)
        {
            return "[INVALID]";
        }
        else
        {
            std::ostringstream ss;
            ss << "[" << interval.getLeft() << "," << interval.getRight() << "]";
            return ss.str();
        }
    }

    static void mergeFunctionDirect()
    {
        // want a macro instead of a function so that failures point to the correct line
        #define TEST_INTERVAL_MERGE(AX, AY, BX, BY, CX, CY) do { \
            Interval ia = std::isnan(AX) ? Interval::none() : Interval::makeNormalized(AX, AY, IntervalKind::INCLUSIVE); \
            Interval ib = std::isnan(BX) ? Interval::none() : Interval::makeNormalized(BX, BY, IntervalKind::INCLUSIVE); \
            Interval expected = std::isnan(CX) ? Interval::none() : Interval::makeNormalized(CX, CY, IntervalKind::INCLUSIVE); \
            Interval merged = Interval::mergeAnd(ia, ib); \
            BOOST_TEST(toStr(merged) == toStr(expected)); \
        } while(false)

        TEST_INTERVAL_MERGE(0, posInf, negInf, 1, 0, 1);
        TEST_INTERVAL_MERGE(0, 3, negInf, 1, 0, 1);
        TEST_INTERVAL_MERGE(-5, posInf, 0, 1, 0, 1);
        TEST_INTERVAL_MERGE(3, 5, 4, 6, 4, 5);
        TEST_INTERVAL_MERGE(3, 5, 6, 10, NaN, NaN);
        TEST_INTERVAL_MERGE(3, 5, negInf, 3, 3, 3);
        TEST_INTERVAL_MERGE(3, 5, negInf, 2, NaN, NaN);
        TEST_INTERVAL_MERGE(3, 5, 6, posInf, NaN, NaN);
        TEST_INTERVAL_MERGE(negInf, 3, 5, posInf, NaN, NaN);
        #undef TEST_INTERVAL_MERGE
    }

    BOOST_AUTO_TEST_SUITE(IntervalMerging)
        MAKE_TEST_CASE(mergeFunctionDirect)
    BOOST_AUTO_TEST_SUITE_END()
}