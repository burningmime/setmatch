/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../../main/index/pred_list.hpp"
#include <vector>
#include <algorithm>
#include "../infra/test_common.hpp"
#include <random>

namespace burningmime::setmatch::test
{
    struct PredicateList
    {
        Blockset blockset;
        void* blockData = nullptr;
        int blockSize = 0;
        ~PredicateList() { predListFree(blockset, blockData); }
    } ALIGNED_BY(64);

    //==============================================================================================================
    // Helpers
    //==============================================================================================================

    std::mt19937_64 seededRng(int blockSize)
    {
        return std::mt19937_64{(uint64_t) blockSize * 1099511628211ULL};
    }

    std::vector<int> generateNumbers(int blockSize, std::mt19937_64& rng)
    {
        std::vector<int> numbers;
        int n = 0;
        while(n < blockSize * 256)
        {
            numbers.push_back(n);
            if(rng() % 10 == 0) // occasionally jump ahead several blocks
                n += blockSize * (int) (rng() % 4 + 1);
            n += (int) (rng() % 15 + 1);
        }
        return numbers;
    }

    std::vector<int> generateNumbers(int blockSize)
    {
        auto rng = seededRng(blockSize);
        return generateNumbers(blockSize, rng);
    }

    PredicateList predListFromInts(int blockSize, std::vector<int>& numbers)
    {
        PredicateList list;
        list.blockSize = blockSize;
        predListBuild(blockSize, list.blockset, list.blockData, (int) numbers.size(), numbers.data(), false);
        return list;
    }

    BitArray bitsetFromInts(int blockSize, std::vector<int>& numbers)
    {
        BitArray bitset;
        bitset.clearAndResize((size_t) blockSize * 256);
        for(int n : numbers)
            bitset.set(n);
        return bitset;
    }

    bool match(PredicateList& list, BitArray& bitset)
    {
        BitBlock256 blockset;
        blocksetFromBitset(list.blockSize, bitset, blockset);
        return blocksetMatch(blockset, list.blockset) &&
            (list.blockSize == 1 || fullMatch(list.blockSize, list.blockset, list.blockData, bitset));
    }

    bool contains(std::vector<int>& numbers, int n)
    {
        auto begin = numbers.begin(), end = numbers.end();
        return std::find(begin, end, n) != end;
    }

    void makeSureItsNotASubset(std::vector<int>& sub, std::vector<int>& sup)
    {
        // while the odds are REALLY against us here, there's a small chance it would be a subset, and we don't
        // want random failures
        for(int n : sub)
            if(!contains(sup, n))
                return;
        auto it = std::find(sup.begin(), sup.end(), sub[0]);
        sup.erase(it);
    }

    //==============================================================================================================
    // Main tests
    //==============================================================================================================

    // simple test to make sure the iterator works properly
    void iterators(int blockSize)
    {
        auto numbers = generateNumbers(blockSize);
        PredicateList predList = predListFromInts(blockSize, numbers);
        auto iterable = predListIter(predList.blockSize, predList.blockset, predList.blockData);
        auto it1 = numbers.begin(), it1e = numbers.end();
        auto it2 = iterable.begin(), it2e = iterable.end();
        for(; it1 != it1e; ++it1, ++it2)
        {
            BOOST_TEST((it2 != it2e));
            BOOST_TEST(*it1 == *it2);
        }
    }

    void everythingEqual(int blockSize)
    {
        auto numbers = generateNumbers(blockSize);
        BitArray bitset = bitsetFromInts(blockSize, numbers);
        PredicateList predList = predListFromInts(blockSize, numbers);
        BOOST_TEST(match(predList, bitset));
    }

    void randomNotEqual(int blockSize)
    {
        auto numbersBitset = generateNumbers(blockSize);
        auto numbersPhraseList = generateNumbers(blockSize);
        makeSureItsNotASubset(numbersPhraseList, numbersBitset);
        BitArray bitset = bitsetFromInts(blockSize, numbersBitset);
        PredicateList predList = predListFromInts(blockSize, numbersPhraseList);
        BOOST_TEST(!match(predList, bitset));
    }

    void equalExceptLastBlock(int blockSize)
    {
        auto numbers = generateNumbers(blockSize);
        PredicateList predList = predListFromInts(blockSize, numbers);
        numbers.pop_back(); // there's always at least one entry, so it suffices to remove the last one
        BitArray bitset = bitsetFromInts(blockSize, numbers);
        BOOST_TEST(!match(predList, bitset));
    }

    void notNegatedEveryOther(int blockSize)
    {
        // this test only makes sense if we have actual blocks
        if(blockSize != 1)
        {
            auto rng = seededRng(blockSize);
            std::vector<int> numbers1;
            std::vector<int> numbers2;
            for(int i = 0; i < 256; i++)
            {
                if(i == 19 || (rng() % 20 == 0))
                {
                    for(int j = 0; j < blockSize; j += 2)
                    {
                        numbers1.push_back(i*blockSize + j);
                        numbers2.push_back(i*blockSize + j + 1);
                    }
                }
            }

            PredicateList predList = predListFromInts(blockSize, numbers1);
            BitArray bitset = bitsetFromInts(blockSize, numbers2);

            BitBlock256 blockset;
            blocksetFromBitset(blockSize, bitset, blockset);
            BOOST_TEST(blocksetMatch(blockset, predList.blockset));                             // should pass this test since they have the same blocks
            BOOST_TEST(!fullMatch(blockSize, predList.blockset, predList.blockData, bitset));   // ...but should fail here since none of the same bits are present
        }
    }

    void equalsAndHashCode(int blockSize)
    {
        auto rng = seededRng(blockSize);

        // first two are same
        auto numbers = generateNumbers(blockSize, rng);
        PredicateList predList1 = predListFromInts(blockSize, numbers);
        PredicateList predList2 = predListFromInts(blockSize, numbers);

        // erase one item at random
        numbers.erase(numbers.begin() + (rng() % numbers.size()));
        PredicateList predList3 = predListFromInts(blockSize, numbers);

        // technically, it's not completely safe to compare hash codes which might lead to spurious behaviour
        // but we're using a pre-seeded RNG which we know works with the current hash algorithm
        BOOST_TEST(predListEquals(blockSize, predList1.blockset, predList1.blockData, predList2.blockset, predList2.blockData));
        BOOST_TEST(predListHash(blockSize, predList1.blockset, predList1.blockData) == predListHash(blockSize, predList2.blockset, predList2.blockData));
        BOOST_TEST(!predListEquals(blockSize, predList1.blockset, predList1.blockData, predList3.blockset, predList3.blockData));
        BOOST_TEST(predListHash(blockSize, predList1.blockset, predList1.blockData) != predListHash(blockSize, predList3.blockset, predList3.blockData));
    }

    // we want to test these with a variety of block sizes since the code path is often different for different block sizes
    #define TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, BLOCK_SIZE) MAKE_TEST_CASE_PARAM(TEST_FUNCTION##BLOCK_SIZE, TEST_FUNCTION, BLOCK_SIZE)
    #define TEST_WITH_DIFFERENT_BLOCK_SIZES(TEST_FUNCTION) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 1) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 8) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 16) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 32) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 64) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 128) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 192) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 256) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 320) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 384) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 512) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 832) \
            TEST_WITH_BLOCK_SIZE(TEST_FUNCTION, 1728)

    BOOST_AUTO_TEST_SUITE(PhraseListTest)
        TEST_WITH_DIFFERENT_BLOCK_SIZES(iterators)
        TEST_WITH_DIFFERENT_BLOCK_SIZES(everythingEqual)
        TEST_WITH_DIFFERENT_BLOCK_SIZES(randomNotEqual)
        TEST_WITH_DIFFERENT_BLOCK_SIZES(equalExceptLastBlock)
        TEST_WITH_DIFFERENT_BLOCK_SIZES(notNegatedEveryOther)
        TEST_WITH_DIFFERENT_BLOCK_SIZES(equalsAndHashCode)
    BOOST_AUTO_TEST_SUITE_END()

    #undef TEST_WITH_DIFFERENT_BLOCK_SIZES
    #undef TEST_WITH_BLOCK_SIZE
}
