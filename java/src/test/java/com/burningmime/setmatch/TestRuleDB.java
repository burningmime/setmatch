/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import com.burningmime.setmatch.examples.JavaExample;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class TestRuleDB
{
    @Test
    public void testExamples()
    {
        // just make sure these all keep working
        JavaExample.advertisingExample();
    }

    @Test
    public void testExceptionWithMessage()
    {
        // This test exists mostly to ensure that we're properly passing the exception from the source, through the filter,
        // to the C API catch, to the C++ API throw, back to the JNI catch, and then into Java, all without modification.
        String[] tooLongPhrase = new String[80];
        for(int i = 0; i < tooLongPhrase.length; ++i)
            tooLongPhrase[i] = "a";
        assertThrowsContains(() -> RuleDB.compileRule(RuleNode.phrase(tooLongPhrase)),
                "a single \"phrase\" array may not contain more than 64 terms, but got 80");
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testStrangerStrings()
    {
        try(RuleDB db = new RuleDB())
        {
            db.addRule(0, RuleNode.or("\uD83C\uDFB8", "hello\0world", "หวัดดีชาวโลก"));
            assertMatches(db, "\uD83C\uDFB8", 0);
            assertMatches(db, "\uD83C\uDFB8\uD83E\uDD39");
            assertMatches(db, "hello");
            assertMatches(db, "hello\0world", 0);
            assertMatches(db, "หวัดดีชาวโลก", 0);
            assertEquals(db.debugGetRuleAsString(0), "(\"\uD83C\uDFB8\") OR (\"hello\\x00world\") OR (\"หวัดดีชาวโลก\")");
        }
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testGetRuleAsString()
    {
        // this is technically implementation-defined (especially wrt ordering predicates), so this is just testing that
        // the round trip between Java and C++ works correctly. the current implementation will always return this string
        // when it's added to an empty database, but if you add the same rule to a database with any of the predicates
        // already in it, stuff might be returned in a different order.
        String EXPECTED_STRING_1 = "(\"coffee\" \"eggs\" \"sausage\" \"toast\" ~\"gluten\") OR (\"eggs\" \"sausage\" " +
                "\"toast\" \"tea\" ~\"gluten\") OR (\"coffee\" \"eggs\" \"toast\" \"bacon\" ~\"gluten\") OR (\"eggs\" " +
                "\"toast\" \"tea\" \"bacon\" ~\"gluten\") OR (\"coffee\" \"toast\" \"cereal\" \"fresh fruit\" ~\"gluten\")" +
                " OR (\"toast\" \"tea\" \"cereal\" \"fresh fruit\" ~\"gluten\")";
        String EXPECTED_STRING_2 = "[Could not find rule with ID 0]";
        try(RuleDB db = new RuleDB())
        {
            db.addRule(-1,
                RuleNode.and(
                    RuleNode.or("coffee", "tea"),
                    RuleNode.or(
                        RuleNode.and("eggs", RuleNode.or("sausage", "bacon")),
                        RuleNode.and("cereal", RuleNode.phrase("fresh", "fruit"))),
                    RuleNode.and("toast", RuleNode.not("gluten"))));
            assertEquals(EXPECTED_STRING_1, db.debugGetRuleAsString(-1));
            assertEquals(EXPECTED_STRING_2, db.debugGetRuleAsString(0));
        }
    }

    @Test
    public void testIntervals()
    {
        try(RuleDB db = new RuleDB())
        {
            db.addRule(1, RuleNode.term("n").equalTo(3));
            db.addRule(2, RuleNode.term("n").greaterThan(3));
            db.addRule(3, RuleNode.term("n").greaterThanOrEqualTo(3));
            db.addRule(4, RuleNode.term("n").lessThan(3));
            db.addRule(5, RuleNode.term("n").lessThanOrEqualTo(3));
            db.addRule(6, RuleNode.term("n").between(2, 4, Interval.MIN_INCLUSIVE));
            db.addRule(7, RuleNode.term("n").between(2, 4, Interval.MAX_INCLUSIVE));
            db.addRule(8, RuleNode.term("n").between(2, 4, Interval.INCLUSIVE));
            db.addRule(9, RuleNode.term("n").between(2, 4, Interval.EXCLUSIVE));
            db.addRule(0, RuleNode.term("n").notEqualTo(3));

            assertMatches(db, Token.of("n", 2), 4, 5, 6, 8, 0);
            assertMatches(db, Token.of("n", 3), 1, 3, 5, 6, 7, 8, 9);
            assertMatches(db, Token.of("n", 4), 2, 3, 7, 8, 0);
        }
    }

    @Test
    public void testMetricsTwice()
    {
        // Just make sure we get the same metrics in the same order
        try(RuleDB db = new RuleDB())
        {
            db.addRule(0, RuleNode.phrase("hello", "world"));
            String m1 = Arrays.toString(db.pollMetrics());
            String m2 = Arrays.toString(db.pollMetrics());
            assertEquals(m1, m2);
        }
    }

    //==========================================================================
    // Helper functions
    //==========================================================================

    private static void assertMatches(RuleDB db, String text, long... expected)
    {
        List<Token> tokens = Arrays.stream(text.split(" ")).map(Token::of).collect(Collectors.toList());
        assertMatches(expected, db.match(tokens));
    }

    private static void assertMatches(RuleDB db, Token token, long... expected)
    {
        assertMatches(expected, db.match(token));
    }

    private static void assertMatches(long[] expected, long[] actual)
    {
        Arrays.sort(expected);
        Arrays.sort(actual);
        assertEquals(pretty(expected), pretty(actual));
    }

    private static String pretty(long[] longs)
    {
        return LongStream.of(longs).mapToObj(Long::toString).collect(Collectors.joining(", ", "[", "]"));
    }

    private static void assertThrowsContains(Executable ex, String expectedMessage)
    {
        boolean threw = false;
        try
        {
            ex.execute();
        }
        catch(Throwable t)
        {
            threw = true;
            // is it just me or is the order of parameters to assertEquals() ass-backwards?
            assertTrue(t instanceof SetmatchException, "Was: " + t.getClass());
            assertTrue(t.getMessage().contains(expectedMessage),
                    "Message:\n    " + t.getMessage() + "\nDid not contain expected string:\n    " + expectedMessage);
        }
        assertTrue(threw, "Exception wasn't even thrown");
    }
}
