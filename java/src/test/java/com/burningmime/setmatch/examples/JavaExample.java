package com.burningmime.setmatch.examples;

import com.burningmime.setmatch.Interval;
import com.burningmime.setmatch.RuleDB;
import com.burningmime.setmatch.RuleNode;
import com.burningmime.setmatch.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JavaExample {

  public static void advertisingExample() {

    // First, create a RuleDB (and don't forget to close it!)
    try(RuleDB db = new RuleDB()) {

      // Every rule has an ID, which you assign, You'll probably want to map
      // these IDs to something useful in your application. Adding a rule is
      // as simple as:
      db.addRule(1, RuleNode.term("interest:sports"));

      // You can use RuleNode.and() to require that both conditions are met.
      db.addRule(2, RuleNode.and("interest:sports", "interest:technology"));

      // "Or" rules will match if either condition is met.
      db.addRule(3, RuleNode.or("interest:technology", "interest:news"));

      // RuleNode.not() lets you exclude a sub-expression.
      db.addRule(4, RuleNode.and(
          "interest:careers",
          RuleNode.not("interest:news")));

      // You can combine these to make more complex rules. For example, the
      // newest life-destroying video game about tennis might want to target
      // these customers:
      db.addRule(5, RuleNode.and(
          RuleNode.or("interest:video-games", "interest:sports"),
          RuleNode.not(RuleNode.or("interest:family", "interest:career"))));
      
      // Sometimes you need to handle ranges of numbers. For example:
      db.addRule(6, RuleNode.term("age").lessThan(25));

      // Or even:
      db.addRule(7, RuleNode.and(
          RuleNode.term("geo:lng").between(-111.056888, -104.05216, Interval.EXCLUSIVE),
          RuleNode.term("geo:lat").between(40.994746, 45.005904, Interval.EXCLUSIVE)));

      // Which can be combined with term-based rules:
      db.addRule(8, RuleNode.and(
          "interest:music",
          RuleNode.term("age").greaterThan(30)));

      // In a real application, you probably won't be building the rules at
      // compile time, so these methods all accept collections (or even fancy
      // streams!)
      List<RuleNode> nodes = new ArrayList<>();
      nodes.add(RuleNode.term("interest:pets"));
      nodes.add(RuleNode.not(RuleNode.term("interest:sports")));
      nodes.add(RuleNode.term("age").between(35, 50, Interval.MIN_INCLUSIVE));
      db.addRule(9, RuleNode.and(nodes));

      // Now that we've added some rules, let's see what matches:
      long[] matchedIds = db.match(
          "interest:video-games",
          "interest:technology",
          Token.of("age", 27));

      // spoiler:
      assert(Arrays.toString(matchedIds).equals("[3, 5]"));

      // Let's try a few more for good measure (the helper method below
      // has all the details)

      // [3, 5]
      matchAndPrint(db,
          "interest:video-games",
          "interest:technology",
          Token.of("age", 27));

      //
      matchAndPrint(db,
          "interest:video-games",
          "interest:careers",
          "interest:pets",
          Token.of("age", 22));
    }
  }

  private static void matchAndPrint(RuleDB db, Object... tokens) {

    // This is all you need to do to match! it will return the IDs of the
    // matched rules.
    long[] results = db.match(tokens);

    // There won't be any duplicates, but they'll be in an undefined order.
    // Sorting them here just for display purposes.
    Arrays.sort(results);

    // The rest of this is just formatting.
    System.out.println("match(" + Arrays.toString(tokens) + ") = " +
      Arrays.toString(results));
  }
}
