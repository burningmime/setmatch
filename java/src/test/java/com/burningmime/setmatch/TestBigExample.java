package com.burningmime.setmatch;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * This functions more like a performance test than any functionality test.
 */
public class TestBigExample
{
    private static final long EXPECTED_MATCH_COUNT = 5436991L;

    @Test
    public void testBigData()
    {
        LibraryLoader.load(); // don't let performance metrics be affected by library load time -- normally this is done on the first call to RuleDB
        System.out.println("====== Reading input =======");
        String[][] lines = readInput();
        System.out.println("======= Compiling ==========");
        byte[][] compiledRules = compileRules(lines);
        try(RuleDB db = new RuleDB())
        {
            System.out.println("======= Inserting ==========");
            addRules(db, compiledRules);
            System.out.println("======= Matching ===========");
            matchLines(db, lines);
            System.out.println("======== Metrics ===========");
            for(Metric metric : db.pollMetrics())
                System.out.printf("%s = %.5f%n", metric.getName(), metric.getValue());
            System.out.println("===========================");
        }
    }

    private static BufferedReader getInputReader()
    {
        InputStream stream = TestBigExample.class.getClassLoader().getResourceAsStream("sample.txt");
        assertNotNull(stream);
        return new BufferedReader(new InputStreamReader(stream));
    }

    private static String[][] readInput()
    {
        long startTime = System.nanoTime();
        try(BufferedReader reader = getInputReader())
        {
            List<String[]> lines = new ArrayList<>();
            String line;
            while((line = reader.readLine()) != null)
            {
                String[] lineTokens = Arrays.stream(line.split(" "))
                        .map(s -> s.replaceAll("[^a-zA-Z]", "").toLowerCase())
                        .filter(s -> s.length() > 2)
                        .toArray(String[]::new);
                if(lineTokens.length > 0)
                    lines.add(lineTokens);
            }
            String[][] result = lines.toArray(new String[0][]);
            long ioTime = System.nanoTime() - startTime;
            System.out.printf("Reading and parsing %d lines took %.4f ms (%.04f ms per line)\n",
                    result.length, ioTime / 1000000.0, ioTime / (1000000.0 * result.length));
            return result;
        }
        catch(Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }

    private static byte[][] compileRules(String[][] lines)
    {
        long startTime = System.nanoTime();
        byte[][] compiledRules = Arrays.stream(lines)
                .map(line -> RuleNode.and(Arrays.stream(line).map(RuleNode::phrase)))
                .map(RuleDB::compileRule).toArray(byte[][]::new);
        long buildTime = System.nanoTime() - startTime;
        System.out.printf("Compiling %d rules took %.4f ms (%.4f ms per rule)\n",
                lines.length, buildTime / 1000000.0, buildTime / (1000000.0 * lines.length));
        return compiledRules;
    }

    private static void addRules(RuleDB db, byte[][] compiledRules)
    {
        long startTime = System.nanoTime();
        for(int i = 1; i <= compiledRules.length; i++)
            db.addRuleCompiled(i, compiledRules[i - 1]);
        long addTime = System.nanoTime() - startTime;
        System.out.printf("Inserting %d rules took %.4f ms (%.4f ms per rule)\n",
                compiledRules.length, addTime / 1000000.0, addTime / (1000000.0 * compiledRules.length));
    }

    private static void matchLines(RuleDB db, String[][] lines)
    {
        long startTime = System.nanoTime();
        long matchCount = Stream.of(lines).parallel().mapToLong(
                line -> db.match(Arrays.asList(line)).length).sum();
        long endTime = System.nanoTime();
        long totalNanos = endTime - startTime;
        assertEquals(EXPECTED_MATCH_COUNT, matchCount);
        int nThreads = ForkJoinPool.getCommonPoolParallelism() + 1; // +1 because calling thread also executes
        System.out.printf("Matching %d lines (%d matches) took %.4f ms with %d threads (avg. %.4f ms throughput / %.4f latency)\n",
                lines.length, matchCount, totalNanos / 1000000.0, nThreads, totalNanos / (1000000.0 * lines.length),
                (totalNanos * nThreads) / (1000000.0 * lines.length));
    }
}
