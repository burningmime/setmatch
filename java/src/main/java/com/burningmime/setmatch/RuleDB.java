/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Stream;

/**
 * Index of Boolean expressions for matching a stream of incoming documents.
 * <p>
 * <b>Example:</b>
 * <pre><code class="java">{@code
 *     try(RuleDB db = new RuleDB())
 *     {
 *         // First, add boolean expression trees as rules:
 *         db.addRule(1, RuleNode.term("sports"));
 *         db.addRule(2, RuleNode.and("games", "18-25"));
 *         db.addRule(3, RuleNode.or("games", "sports"));
 *         db.addRule(4, RuleNode.and("news", RuleNode.not("18-25")));
 *         db.addRule(5, RuleNode.and(RuleNode.or("18-25", "25-40"), "sports"));
 *         db.addRule(6, RuleNode.and(RuleNode.not(RuleNode.or("games", "sports")), "news"));
 *
 *         // Then, use match() to test documents. This will return an array of type long[] with the matched IDs.
 *         db.match("18-25", "games", "sports");          // [1, 2, 3, 5]
 *         db.match("18-25", "news", "technology");       // [6]
 *         db.match("25-40", "games", "news", "sports");  // [1, 3, 4, 5]
 *         db.match("40-50", "sports");                   // [1, 3]
 *     }}</code></pre>
 *
 * This class is thread-safe; all synchronization is done internally by the library. You may (and often should!) call
 * its methods from multiple threads without using synchronized.
 */
public final class RuleDB implements AutoCloseable
{
    /**
     * Compiles a rule from a JSON representation, and returns an encoded representation that can be passed to
     * {@link #addRuleCompiled(long, byte[])}. You may cache the result of this (for example as a BLOB in a database)
     * to make future startup faster.
     * @return the compiled rule.
     * @param jsonRule the JSON representing the rule.
     */
    public static byte[] compileRuleJson(String jsonRule)
    {
        if(jsonRule == null)
            throw new IllegalArgumentException("jsonRule is null");
        return Native.compileRuleJson(jsonRule);
    }

    /**
     * Compiles a rule from an expression tree, and returns an encoded representation that can be passed to
     * {@link #addRuleCompiled(long, byte[])}. You may cache the result of this (for example as a BLOB in a database)
     * to make future startup faster.
     * @param ruleNode the {@link RuleNode} at the root of the expression tree
     * @return the compiled rule.
     */
    public static byte[] compileRule(RuleNode ruleNode)
    {
        if(ruleNode == null)
            throw new IllegalArgumentException("ruleNode is null");
        return compileRuleJson(ruleNode.toJsonAsRoot());
    }

    /**
     * Creates a new instance. You should call {@link #close()} when you're done with it.
     */
    public RuleDB()
    {
        pRuleDB = Native.newRuleDB();
    }

    /**
     * Adds a rule. If a rule with the same ID exists, that rule will be replaced. Using this method instead of
     * {@link #addRule(long, RuleNode)} or {@link #addRuleJson(long, String)} will let you speed up startup a bit by
     * caching the results between different runs, but generally it won't have much of an effect.
     * @param id ID of the rule, which is returned by match operations, and used to refer to the rule when deleting/updating it.
     * @param compiledRule Compiled rule binary, which you can get by calling {@link #compileRuleJson(String)} or {@link #compileRule(RuleNode)}.
     * @return true if there was an existing rule that was replaced, false if this is a brand new rule.
     */
    public boolean addRuleCompiled(long id, byte[] compiledRule)
    {
        if(compiledRule == null)
            throw new IllegalArgumentException("compiledRule is null");
        checkClosed();
        return Native.addRuleCompiled(pRuleDB, id, compiledRule);
    }

    /**
     * Adds a rule. If a rule with the same ID exists, that rule will be replaced. The JSON should be the result of
     * calling {@link RuleNode#toJsonAsRoot()}, or something compatible with it (see C/C++ docs if you need the exact
     * format).
     * @param id ID of the rule, which is returned by match operations, and used to refer to the rule when deleting/updating it.
     * @param jsonRule rule in JSON format, described in {@link #compileRuleJson(String)}
     * @return true if there was an existing rule that was replaced, false if this is a brand new rule.
     */
    public boolean addRuleJson(long id, String jsonRule)
    {
        if(jsonRule == null)
            throw new IllegalArgumentException("jsonRule is null");
        checkClosed();
        return Native.addRuleJson(pRuleDB, id, jsonRule);
    }

    /**
     * Adds a rule. If a rule with the same ID exists, that rule will be replaced.
     * @param id ID of the rule, which is returned by match operations, and used to refer to the rule when deleting/updating it.
     * @param ruleNode the {@link RuleNode} at the root of the expression tree
     * @return true if there was an existing rule that was replaced, false if this is a brand new rule.
     */
    public boolean addRule(long id, RuleNode ruleNode)
    {
        if(ruleNode == null)
            throw new IllegalArgumentException("ruleNode is null");
        checkClosed();
        return Native.addRuleJson(pRuleDB, id, ruleNode.toJsonAsRoot());
    }

    /**
     * Removes the rule with the given ID.
     * @param id ID of the rule to remove.
     * @return true if the rule was removed, false if there wasn't any rule with that ID.
     */
    public boolean removeRule(long id)
    {
        checkClosed();
        return Native.removeRule(pRuleDB, id);
    }

    /**
     * Checks if a rule with the given ID exists in the database.
     * @param id ID of the rule to find
     * @return true if the database contains a rule with that ID, false otherwise.
     */
    public boolean containsRule(long id)
    {
        checkClosed();
        return Native.containsRule(pRuleDB, id);
    }

    /**
     * Matches a document and returns the IDs of matching rules.
     * @param tokens the document to match. Objects must be either Strings or {@link Token}s. Strings will be treated
     *               as if they were {@link Token#of(String)}s.
     * @return IDs of rules that matched. There won't be any duplicates, but the order is implementation-dependent.
     */
    public long[] match(Collection<?> tokens)
    {
        checkClosed();
        if(tokens == null)
            throw new IllegalArgumentException("tokens array cannot be null");
        int length = tokens.size();
        if(length == 0)
            return new long[0];
        // tried a thread-static buffer but it wasn't much faster and has API implications because of memory
        // the current Java GC is fast enough that this isn't really a major bottleneck
        String[] strings = new String[length];
        int i = 0;
        for(Object token : tokens)
            strings[i++] = toMetaString(token);
        return Native.match(pRuleDB, strings, length);
    }

    /**
     * Matches a document and returns the IDs of matching rules.
     * @param tokens the document to match. Objects must be either Strings or {@link Token}s. Strings will be treated
     *               as if they were {@link Token#of(String)}s.
     * @return IDs of rules that matched. There won't be any duplicates, but the order is implementation-dependent.
     */
    public long[] match(Object... tokens)
    {
        checkClosed();
        if(tokens == null)
            throw new IllegalArgumentException("tokens collection cannot be null");
        int length = tokens.length;
        if(length == 0)
            return new long[0];
        String[] strings = new String[length];
        for(int i = 0; i < length; ++i)
            strings[i] = toMetaString(tokens[i]);
        return Native.match(pRuleDB, strings, length);
    }

    /**
     * Matches a document and returns the IDs of matching rules.
     * @param tokens the document to match. Objects must be either Strings or {@link Token}s. Strings will be treated
     *               as if they were {@link Token#of(String)}s.
     * @return IDs of rules that matched. There won't be any duplicates, but the order is implementation-dependent.
     */
    public long[] match(Stream<?> tokens)
    {
        // Java's streams can be incredibly slow. The "better" way to handle this might be to use the spliterator
        // directly if it has the SIZED characteristic. However, that sounds very complex and a potential bug
        // minefield. So for now, just use the streams API.
        checkClosed();
        if(tokens == null)
            throw new IllegalArgumentException("tokens stream cannot be null");
        String[] strings = tokens.map(RuleDB::toMetaString).toArray(String[]::new);
        if(strings.length == 0)
            return new long[0];
        return Native.match(pRuleDB, strings, strings.length);
    }

    /**
     * Gets a string representation of the rule. The format is implementation-defined and may change between releases.
     * Long rules may be arbitrarily truncated, and may not contain all the data. This should only be used for debugging.
     * <p>
     * If there is no rule with the given ID, will return a string saying as much. Never returns null.
     * @param ruleId the ID to find
     * @return a string representation of the rule
     * @deprecated This function returns an implementation-defined string that may be truncated.
     */
    @Deprecated
    public String debugGetRuleAsString(long ruleId)
    {
        checkClosed();
        return Native.debugGetRuleAsString(pRuleDB, ruleId);
    }

    /**
     * Retrieves some statistical information about the database, such as how long certain calls took. As of the current
     * version, the data you retrieve from this is implementation-defined and probably not especially useful. In the future,
     * I may document this.
     * @return Collection of metric data.
     */
    public Metric[] pollMetrics()
    {
        checkClosed();
        synchronized(lock)
        {
            // since metrics are always in the same order, we can copy the strings the first time,
            // then just get the numbers from then on
            if(metricNames == null)
            {
                metricNames = Native.getMetricNames(pRuleDB);
                metricValues = new double[metricNames.length];
            }
            int count = metricNames.length;
            Native.pollMetrics(pRuleDB, metricValues);
            Metric[] metrics = new Metric[count];
            for(int i = 0; i < metricNames.length; ++i)
                metrics[i] = new Metric(metricNames[i], metricValues[i]);
            return metrics;
        }
    }

    /**
     * Closes the database and releases its memory. If your application outlives the database, you should call this to
     * prevent memory leaks.
     */
    public void close()
    {
        closeImpl(false);
    }

    /**
     * This is only here as a safeguard; you should call {@link #close()} if your application outlives the database.
     * @throws Throwable won't throw, but Java requires this.
     * @deprecated This is only here as a safeguard; you should call {@link #close()} if your application outlives the database.
     */
    @Override
    @SuppressWarnings("deprecation")
    @Deprecated
    protected void finalize() throws Throwable
    {
        closeImpl(true);
        super.finalize();
    }

    //==================================================================================================================

    private static final Logger logger;
    private long pRuleDB;
    private String[] metricNames;
    private double[] metricValues;
    private final Object lock = new Object(); // most things use the internal RW lock in native code, but for close and metrics, we keep a lock here

    private void closeImpl(boolean isFinalizing)
    {
        synchronized(lock)
        {
            if(pRuleDB != 0)
            {
                // for now, we can totally skip closing the database if the application is shutting down. since everything it
                // does is in memory, and it's not doing any external I/O for now, this should always be safe to do.
                if(isJvmClosing())
                    return;

                try
                {
                    if(isFinalizing)
                        logger.warn("RuleDB instance was not closed");
                    Native.deleteRuleDB(pRuleDB);
                }
                catch(Exception ex)
                {
                    logger.error("Error while closing RuleDB", ex);
                    if(!isFinalizing)
                        throw ex;
                }
                finally
                {
                    pRuleDB = 0;
                }
            }
        }
    }

    private void checkClosed()
    {
        if(pRuleDB == 0)
            throw new IllegalStateException("Attempt to use RuleDB object after it's been closed");
    }

    // https://stackoverflow.com/questions/6816858/detecting-that-jvm-is-shutting-down
    private static final Thread DUMMY_HOOK = new Thread();
    private static final Object hookLock = new Object();
    private static boolean jvmClosingCache;
    private static boolean isJvmClosing()
    {
        synchronized(hookLock)
        {
            // Cache this in case it's happening with many databases.
            if(jvmClosingCache)
                return true;
            try
            {
                // this isn't thread-safe, hence the lock object above
                Runtime.getRuntime().addShutdownHook(DUMMY_HOOK);
                Runtime.getRuntime().removeShutdownHook(DUMMY_HOOK);
            }
            catch(IllegalStateException ex)
            {
                jvmClosingCache = true;
                return true;
            }
            return false;
        }
    }

    private static String toMetaString(Object obj)
    {
        if(obj == null)
            return null; // native lib ignores nulls
        else if(obj instanceof Token)
            return obj.toString();
        else if(obj instanceof String)
        {
            String str = (String) obj;
            return str.length() == 0 ? null : Token.escapeLiteralNonEmpty(str);
        }
        else
            throw new IllegalArgumentException("Objects to match() must be of type String or Token (got " + obj.getClass().getCanonicalName() + ")");
    }

    @SuppressWarnings("unused") // called by native code
    private static void log(int level, String message)
    {
        if(level == 1)
            logger.info(message);
        else if(level == 2)
            logger.warn(message);
        else
            logger.error(message);
    }

    @SuppressWarnings("unused") // called by native code
    private static Throwable newException(String message)
    {
        return new SetmatchException(message);
    }

    static
    {
        // JNI_OnLoad() will call back to this class's logger. Therefore, we need to ensure the logger has
        // been initialized before loading the library. I could eliminated the circular dependency easily
        // enough, but I'd rather have just a single logger, which follows the slf4j conventions.
        logger = LoggerFactory.getLogger(RuleDB.class);
        LibraryLoader.load();
    }
}
