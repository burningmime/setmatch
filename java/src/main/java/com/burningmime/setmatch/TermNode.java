/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

/**
 * A RuleNode representing a single term. It can be used as-is, or further refined to represent numeric ranges.
 *
 * This is not meant to be implemented outside setmatch. To obtain an instance, please use the
 * {@link RuleNode#term(String)} method.
 */
public interface TermNode extends RuleNode
{
    /**
     * Creates a RuleNode matching the term that is tagged with a number equal to the value.
     * @param value the value which it must equal. Note that if you need to match floating point numbers
     *     instead of integers, use a small range around the target value to account for imprecision.
     * @return new RuleNode for the range.
     */
    public RuleNode equalTo(int value);

    /**
     * Creates a RuleNode matching the term that is tagged with a number not equal to the value.
     * @param value the value which it must equal. Note that if you need to match floating point numbers
     *     instead of integers, use a small range around the target value to account for imprecision.
     * @return new RuleNode for the range.
     */
    public RuleNode notEqualTo(int value);

    /**
     * Creates a RuleNode matching the term that is tagged with a number strictly less than the value.
     * @param value the value which it must be less than.
     * @return new RuleNode for the range.
     */
    public RuleNode lessThan(double value);

    /**
     * Creates a RuleNode matching the term that is tagged with a number less than or equal to the value.
     * @param value the value which it must be less than or equal to.
     * @return new RuleNode for the range.
     */
    public RuleNode lessThanOrEqualTo(double value);

    /**
     * Creates a RuleNode matching the term that is tagged with a number strictly greater than the value.
     * @param value the value which it must be greater than.
     * @return new RuleNode for the range.
     */
    public RuleNode greaterThan(double value);

    /**
     * Creates a RuleNode matching the term that is tagged with a number greater than or equal to the value.
     * @param value the value which it must be greater than or equal to.
     * @return new RuleNode for the range.
     */
    public RuleNode greaterThanOrEqualTo(double value);

    /**
     * Creates a RuleNode matching a term that is a tagged with a number within a specified range of values.
     * @param min The minimum value of the range.
     * @param max The maximum value of the range.
     * @param edgeBehaviour type of mathematical interval; determines what to do when given a value equal to min or equal to max.
     * @return new RuleNode for the range.
     */
    public RuleNode between(double min, double max, Interval edgeBehaviour);
}
