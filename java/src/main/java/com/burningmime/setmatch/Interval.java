/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

/**
 * The type of mathematical interval determines what to do when the value is equal to the minimum or maximum value.
 */
public enum Interval
{
    /**
     * The interval [a, b] matches values x for which a &lt;= x &lt;= b. For example, [3, 5] matches 3, 4, and 5.
     * Also called a "closed" interval.
     */
    INCLUSIVE,

    /**
     * The interval [a, b) matches values x for which a &lt;= x &lt; b. For example, [3, 5) matches 3 and 4, but not 5.
     * Also called a "right open" interval.
     */
    MIN_INCLUSIVE,

    /**
     * The interval (a, b] matches values x for which a &lt; x &lt;= b. For example, (3, 5] matches 4 and 5, but not 3.
     * Also called a "left open" interval.
     */
    MAX_INCLUSIVE,

    /**
     * The interval (a, b) matches values x for which a &lt; x &lt; b. For example, (3, 5) matches 4, but not 3 or 5.
     * Also called an "open" interval, even though it makes no sense that a closed interval lets in more numbers than
     * an open interval.
     */
    EXCLUSIVE
}
