/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * A node in a boolean expression tree.
 *
 * This is not meant to be implemented outside setmatch. To obtain instances, please use static methods inside this
 * class, such as {@link RuleNode#and(Collection)}.
 */
public interface RuleNode
{
    /**
     * Creates a RuleNode representing the logical conjunction of its children.
     * @param children the child nodes -- can either be Strings or other RuleNodes (or a combination thereof).
     * @return new RuleNode containing the children.
     */
    public static RuleNode and(Object... children)
    {
        return new NodeImpl.AndNode(NodeImpl.toNodeArray(children));
    }

    /**
     * Creates a RuleNode representing the logical conjunction of its children.
     * @param children the child nodes -- can either be Strings or other RuleNodes (or a combination thereof).
     * @return new RuleNode containing the children.
     */
    public static RuleNode and(Collection<?> children)
    {
        return  new NodeImpl.AndNode(NodeImpl.toNodeArray(children));
    }

    /**
     * Creates a RuleNode representing the logical conjunction of its children.
     * @param children the child nodes -- can either be Strings or other RuleNodes (or a combination thereof).
     * @return new RuleNode containing the children.
     */
    public static RuleNode and(Stream<?> children)
    {
        return new NodeImpl.AndNode(NodeImpl.toNodeArray(children));
    }

    /**
     * Creates a RuleNode representing the logical disjunction of its children.
     * @param children the child nodes -- can either be Strings or other RuleNodes (or a combination thereof).
     * @return new RuleNode containing the children.
     */
    public static RuleNode or(Object... children)
    {
        return new NodeImpl.OrNode(NodeImpl.toNodeArray(children));
    }

    /**
     * Creates a RuleNode representing the logical disjunction of its children.
     * @param children the child nodes -- can either be Strings or other RuleNodes (or a combination thereof).
     * @return new RuleNode containing the children.
     */
    public static RuleNode or(Collection<?> children)
    {
        return new NodeImpl.OrNode(NodeImpl.toNodeArray(children));
    }

    /**
     * Creates a RuleNode representing the logical disjunction of its children.
     * @param children the child nodes -- can either be Strings or other RuleNodes (or a combination thereof).
     * @return new RuleNode containing the children.
     */
    public static RuleNode or(Stream<?> children)
    {
        return new NodeImpl.OrNode(NodeImpl.toNodeArray(children));
    }

    /**
     * Creates a RuleNode representing the logical negation of its child.
     * @param child the child term
     * @return new RuleNode containing the child.
     */
    public static RuleNode not(String child)
    {
        return new NodeImpl.NotNode(term(child));
    }

    /**
     * Creates a RuleNode representing the logical negation of its child.
     * @param child the child node
     * @return new RuleNode containing the child.
     */
    public static RuleNode not(RuleNode child)
    {
        if(child == null)
            throw new IllegalArgumentException("Objects passed to RuleNode methods must not be null");
        return new NodeImpl.NotNode(child);
    }

    /**
     * Creates a RuleNode representing a single term. You can add numeric conditions to this if you so desire.
     * @param term the term to match
     * @return new RuleNode containing the term.
     */
    public static TermNode term(String term)
    {
        if(term == null || term.length() == 0)
            throw new IllegalArgumentException("Strings passed to RuleNode methods must not be null or empty");
        return new NodeImpl.TermImpl(term);
    }

    /**
     * Creates a RuleNode for matching a sequence of terms in order. This is useful for NLP when matching N-Grams. For
     * example, if you wanted to match "hello world" but not "world hello" or "hello cruel world", you would use
     * <pre>phrase("hello", "world")</pre> instead of <pre>and("hello", "world")</pre>. Remember that all matching is
     * done on exact strings, so you should normalize (stem or lemmatize) all terms and remove stop-words if doing NLP.
     * @param terms the ordered list of normalized tokens to match.
     * @return new RuleNode containing the terms.
     */
    public static RuleNode phrase(String... terms)
    {
        if(terms == null)
            throw new IllegalArgumentException("Terms array must not be null");
        if(terms.length == 0)
            throw new IllegalArgumentException("Phrase nodes must contain at least one term");
        for(String term : terms)
            if(term == null || term.length() == 0)
                throw new IllegalArgumentException("Strings passed to RuleNode methods must not be null or empty");
        return new NodeImpl.PhraseNode(terms);
    }

    /**
     * Gets the JSON representation of a rule with this node as its root.
     * @return the JSON representation of a rule with this node as its root.
     */
    public default String toJsonAsRoot()
    {
        return "{\"rule\":" + toJsonAsNode() + "}";
    }

    /**
     * Gets the JSON representation of this node.
     * @return the JSON representation of this node.
     */
    public String toJsonAsNode();
}
