/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

/**
 * A statistical data point for gathering information about a RuleDB instance.
 */
public class Metric
{
    private final String name;
    private final double value;
    /* package */ Metric(String name, double value) { this.name = name; this.value = value; }

    /**
     * Implementation-defined name of the metric.
     * @return Implementation-defined name of the metric.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Value of the metric. Depending on the type of metric, this could represent an exact count, a weighted mean
     * of counts, or a weighted mean of times.
     * @return value of the metric as a double
     */
    public double getValue()
    {
        return value;
    }

    /**
     * Returns a string representation of the metric, for example "{db.averageBlockCount.current = 30590.0}"
     * @return A string representation of the metric.
     */
    @Override
    public String toString()
    {
        return "{" + name + " = " + value + "}";
    }
}
