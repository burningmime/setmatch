/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

/* package */ class LibraryLoader
{
    private static final Logger logger = LoggerFactory.getLogger(LibraryLoader.class);
    private static final Object lock = new Object();
    private static boolean loaded = false;

    /* package */ static void load()
    {
        synchronized(lock)
        {
            if(!loaded)
            {
                try
                {
                    loaded = true; // mark right away to prevent nested call, because this references a class that loads this statically
                    OS os = getOs();
                    String extension = os == OS.WINDOWS ? ".dll" : os == OS.DARWIN ? ".dylib" : ".so";
                    Path tempFile = Files.createTempFile("libsetmatch", extension).toAbsolutePath();
                    String resourceName = "com/burningmime/setmatch/bin/" + os.toString().toLowerCase() + "/libsetmatch" + extension;
                    try(InputStream in = LibraryLoader.class.getClassLoader().getResourceAsStream(resourceName))
                    {
                        if(in == null)
                            throw new IOException("Could not find resource " + resourceName);
                        try(OutputStream out = Files.newOutputStream(tempFile))
                            { copyStream(in, out); }
                    }
                    tempFile.toFile().deleteOnExit();
                    String libPath = tempFile.toString();
                    logger.info("Loading JNI library: " + libPath);
                    System.load(libPath);
                }
                catch(IOException ex)
                {
                    loaded = false;
                    throw new UncheckedIOException(ex);
                }
                catch(Throwable any)
                {
                    loaded = false;
                    throw any;
                }
            }
        }
    }

    private enum OS { WINDOWS, DARWIN, LINUX }
    private static OS getOs()
    {
        String osArch = System.getProperty("os.arch");
        String vmArch = System.getProperty("sun.arch.data.model");
        if(osArch == null || !(osArch.equals("amd64") || osArch.equals("x86_64")) || vmArch == null || !vmArch.equals("64"))
        {
            throw new UnsupportedOperationException("setmatch is only supported on AMD64 architectures running 64-bit JVMs " +
                    "(os.arch=" + osArch + ", sun.arch.data.model=" + vmArch + ")");
        }

        String osName = System.getProperty("os.name").toLowerCase();
        if(osName.contains("mac") || osName.contains("darwin"))
            return OS.DARWIN;
        else if(osName.contains("windows"))
            return OS.WINDOWS;
        else if(osName.contains("nux"))
            return OS.LINUX;
        else
            throw new UnsupportedOperationException("Unsupported OS: " + osName);
    }

    private static void copyStream(InputStream in, OutputStream out) throws IOException
    {
        final int BUF_SIZE = 16384;
        byte[] buf = new byte[BUF_SIZE];
        while(true)
        {
            int count = in.read(buf);
            if(count == -1)
                return;
            if(count > 0)
                out.write(buf, 0, count);
        }
    }
}
