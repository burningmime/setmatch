/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

/* package */ class NodeImpl
{
    /* package */ static abstract class AbstractNode implements RuleNode
    {
        private static final Gson gson = new Gson(); // default settings work perfectly
        @Override public String toJsonAsNode() { return gson.toJson(this); }
        @Override public String toString() { return toJsonAsNode(); }
    }

    /* package */ static final class AndNode extends AbstractNode
    {
        private final RuleNode[] and;
        /* package */ AndNode(RuleNode[] and) { this.and = and; }
        @Override public boolean equals(Object obj) { return this == obj || (obj instanceof AndNode && Arrays.equals(and, ((AndNode) obj).and)); }
        @Override public int hashCode() { return AndNode.class.hashCode() + (31 * Arrays.hashCode(and)); }
    }

    /* package */ static final class OrNode extends AbstractNode
    {
        private final RuleNode[] or;
        /* package */ OrNode(RuleNode[] or) { this.or = or; }
        @Override public int hashCode() { return OrNode.class.hashCode() + (31 * Arrays.hashCode(or)); }
        @Override public boolean equals(Object obj) { return this == obj || (obj instanceof OrNode && Arrays.equals(or, ((OrNode) obj).or)); }
    }

    /* package */ static final class NotNode extends AbstractNode
    {
        private final RuleNode not;
        /* package */ NotNode(RuleNode not) { this.not = not; }
        @Override public int hashCode() { return NotNode.class.hashCode() + (31 * not.hashCode()); }
        @Override public boolean equals(Object obj) { return this == obj || (obj instanceof NotNode && not.equals(((NotNode) obj).not)); }
    }

    private static final class RangeData { private String term, kind; private Double min, max; }
    private static final class RangeNode extends AbstractNode
    {
        private final RangeData range;
        private RangeNode(RangeData range) { this.range = range; }
        @Override public int hashCode() { return RangeNode.class.hashCode() + (31 * Objects.hash(range.term, range.kind, range.min, range.max)); }
        @Override public boolean equals(Object obj)
        {
            if(this == obj)
                return true;
            if(!(obj instanceof RangeNode))
                return false;
            RangeData other = ((RangeNode) obj).range;
            return Objects.equals(range.term, other.term) && Objects.equals(range.kind, other.kind) &&
                    Objects.equals(range.min, other.min) && Objects.equals(range.max, other.max);
        }
    }

    /* package */ static final class PhraseNode extends AbstractNode
    {
        private final String[] phrase;
        /* package */ PhraseNode(String[] phrase) { this.phrase = phrase; }
        @Override public int hashCode() { return PhraseNode.class.hashCode() + (31 * Arrays.hashCode(phrase)); }
        @Override public boolean equals(Object obj) { return this == obj || (obj instanceof PhraseNode && Arrays.equals(phrase, ((PhraseNode) obj).phrase)); }
    }

    /* package */ static final class TermImpl extends AbstractNode implements TermNode
    {
        private final String[] phrase; // store it like this so gson serializes it as a phrase if it's not extended to a range
        /* package */ TermImpl(String term) { this.phrase = new String[] { term }; }
        @Override public int hashCode() { return TermImpl.class.hashCode() + (31 * Arrays.hashCode(phrase)); }
        @Override public boolean equals(Object obj) { return this == obj || (obj instanceof TermImpl && Arrays.equals(phrase, ((TermImpl) obj).phrase)); }

        // phrases with a single term can be used to construct ranges using a "fluent" interface
        @Override public RuleNode equalTo(int value) { return makeRange(phrase[0], value, value, Interval.INCLUSIVE); }
        @Override public RuleNode notEqualTo(int value) { return RuleNode.not(equalTo(value)); }
        @Override public RuleNode lessThan(double value) { return makeRange(phrase[0], Double.NEGATIVE_INFINITY, value, Interval.EXCLUSIVE); }
        @Override public RuleNode lessThanOrEqualTo(double value) { return makeRange(phrase[0], Double.NEGATIVE_INFINITY, value, Interval.INCLUSIVE); }
        @Override public RuleNode greaterThan(double value) { return makeRange(phrase[0], value, Double.POSITIVE_INFINITY, Interval.EXCLUSIVE); }
        @Override public RuleNode greaterThanOrEqualTo(double value) { return makeRange(phrase[0], value, Double.POSITIVE_INFINITY, Interval.INCLUSIVE); }
        @Override public RuleNode between(double min, double max, Interval edgeBehaviour) { return makeRange(phrase[0], min, max, edgeBehaviour); }

        private static RuleNode makeRange(String term, double min, double max, Interval kind)
        {
            if(term == null || term.length() == 0)
                throw new IllegalArgumentException("Missing or null term for range");
            if(Double.isNaN(min))
                throw new IllegalArgumentException("For range " + term + ", min value is NaN");
            if(Double.isNaN(max))
                throw new IllegalArgumentException("For range " + term + ", max value is NaN");
            boolean minFin = Double.isFinite(min);
            boolean maxFin = Double.isFinite(max);
            if(!minFin && !maxFin)
                throw new IllegalArgumentException("For range " + term + ", min and max are both infinite");
            if(min > max)
                throw new IllegalArgumentException("For range " + term + ", min value " + min + " is larger than max value " + max);
            if(kind == null)
                throw new IllegalArgumentException("For range " + term + ", interval kind is null");
            RangeData data = new RangeData();
            data.term = term;
            data.kind = kind.toString();
            if(minFin)
                data.min = min;
            if(maxFin)
                data.max = max;
            return new RangeNode(data);
        }
    }

    private static RuleNode toNode(Object obj)
    {
        if(obj == null)
            throw new IllegalArgumentException("Objects passed to RuleNode methods must not be null");
        else if(obj instanceof RuleNode)
            return (RuleNode) obj;
        else if(obj instanceof String)
            return RuleNode.term((String) obj);
        else
            throw new IllegalArgumentException("Objects passed to RuleNode methods must either be RuleNodes or Strings");
    }

    /* package */ static RuleNode[] toNodeArray(Object[] objs)
    {
        if(objs == null)
            throw new IllegalArgumentException("Array passed to AND or OR nodes may not be null");
        if(objs.length == 0)
            throw new IllegalArgumentException("Array passed to AND or OR nodes may not be empty");
        RuleNode[] result = new RuleNode[objs.length];
        for(int i = 0; i < result.length; ++i)
            result[i] = toNode(objs[i]);
        return result;
    }

    /* package */ static RuleNode[] toNodeArray(Collection<?> objs)
    {
        if(objs == null)
            throw new IllegalArgumentException("Array passed to AND or OR nodes may not be null");
        if(objs.isEmpty())
            throw new IllegalArgumentException("Array passed to AND or OR nodes may not be empty");
        RuleNode[] result = new RuleNode[objs.size()];
        int i = 0;
        for(Object obj : objs)
            result[i++] = toNode(obj);
        return result;
    }

    /* package */ static RuleNode[] toNodeArray(Stream<?> data)
    {
        return data.map(NodeImpl::toNode).toArray(RuleNode[]::new);
    }
}
