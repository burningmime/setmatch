/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

/* package */ class Native
{
    // Note this is in a separate file instead of private methods in RuleDB because you
    // must pass all dependencies to javac when generating headers, and it turns into a
    // nightmare getting slf4j, gson, etc without maven. This is the easiest way.
    static native byte[] compileRuleJson(String rule);
    static native long newRuleDB();
    static native boolean addRuleJson(long pRuleDB, long id, String json);
    static native boolean addRuleCompiled(long pRuleDB, long id, byte[] compiledRule);
    static native boolean removeRule(long pRuleDB, long id);
    static native boolean containsRule(long pRuleDB, long id);
    static native long[] match(long pRuleDB, String[] tokens, int length);
    static native String[] getMetricNames(long pRuleDB);
    static native void pollMetrics(long pRuleDB, double[] values);
    static native String debugGetRuleAsString(long pRuleDB, long id);
    static native void deleteRuleDB(long pRuleDB);
}
