/*
 * Copyright 2019-2020 Robert William Fraser IV.
 *
 * This file is part of setmatch.
 *
 * setmatch is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * setmatch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with setmatch.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.burningmime.setmatch;

/**
 * Tokens passed to {@link RuleDB#match} functions. These represent the actual values to match against the rules.
 */
public final class Token
{
    /**
     * Creates a literal token.
     * @param s the token string
     * @return a token to be used in {@link RuleDB#match}.
     */
    public static Token of(String s)
    {
        if(s == null || s.length() == 0)
            throw new IllegalArgumentException("Token name cannot be null or empty");
        return new Token(escapeLiteralNonEmpty(s));
    }

    /**
     * Creates a token with a number value.
     * @param s the term string (name of the field).
     * @param value the numeric value. Cannot be NaN or infinity.
     * @return a token to be used in {@link RuleDB#match}.
     */
    public static Token of(String s, double value)
    {
        if(s == null || s.length() == 0)
            throw new IllegalArgumentException("Token name cannot be null or empty");
        if(Double.isInfinite(value) || Double.isNaN(value))
            throw new IllegalArgumentException("Value passed for token " + s + " was NaN or infinity");
        StringBuilder sb = new StringBuilder();
        sb.append("{n=");
        sb.append(value);
        sb.append('}');
        if(s.charAt(0) == '{')
            sb.append('{');
        sb.append(s);
        return new Token(sb.toString());
    }

    /**
     * Gets a string representation of the token.
     * @return a string representation of the token.
     */
    @Override
    public String toString()
    {
        return value;
    }

    //==================================================================================================================

    private final String value;
    private Token(String value) { this.value = value; }

    /* package */ static String escapeLiteralNonEmpty(String s)
    {
        if(s.charAt(0) == '{')
            return "{" + s;
        else
            return s;
    }
}
