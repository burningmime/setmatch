#!/usr/bin/env bash

# Copyright 2019-2020 Robert William Fraser IV.
#
# This file is part of setmatch.
#
# setmatch is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# setmatch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with setmatch.  If not, see <https://www.gnu.org/licenses/>.

set -eo pipefail

printUsage() {
    cat <<- EOF
USAGE: build.sh build_type [options ...]

Build types:
    debug        = Build debug library and run unit tests
    release      = Build release library and run unit tests
    java         = Build java wrapper (requires you build release lib first)
    docs         = Build documentation
    clean        = Remove all build artifacts
    full         = Full build

Options for debug/release builds:
    -no-tests    = Skip unit tests
    -no-valgrind = Skip valgrind tests (implied by -no-tests or release mode)
    -test-chaos  = Include chaos test
    -test=*      = For manual specification of test cases (see boost docs)
    -mingw       = Cross-compile for Windows using MinGW (release builds only)
    -osxcross    = Cross-compile for Mac OSX using osxcross (release builds only)
    -coverage    = Generate code coverage (requires lcov, only working on gcc7)
    --           = All subsequent arguments passed to cmake

Options for Java builds:
    -no-tests    = Skip unit tests
    --           = All subsequent arguments passed to maven
EOF
}

#ANSI color sequences
colorRed=$(echo -ne "\033[0;31m")
colorGreen=$(echo -ne "\033[0;32m")
colorDfl=$(echo -ne "\033[0m")
echoerr() { (>&2 echo "${colorRed}$*${colorDfl}"); }
echogood() { (>&1 echo "${colorGreen}$*${colorDfl}") }
sayThenDo() { echogood "$@"; "$@"; }

# Get directory script is in so that you can invoke the script from any directory
scriptFile=$(readlink -f "${BASH_SOURCE[0]}")
scriptDir=$(cd -P "$(dirname "$scriptFile")" >/dev/null && pwd)
cd "$scriptDir"

# really hacky way to get JAVA_HOME: https://stackoverflow.com/a/20653441/125601
if [[ -z "$JAVA_HOME" ]]; then
    export JAVA_HOME=$(dirname $(dirname $(readlink -f $(which javac))))
fi

# Check if running inside WSL: https://stackoverflow.com/questions/38086185
if grep -qE "(Microsoft|WSL)" /proc/version &>/dev/null; then
    isWsl="1"
fi

# first arg is build type
if [[ "$#" == 0 ]]; then
    printUsage
    exit 1
fi
buildType="$1"
shift

# these need defaults
extraArgs=""
cmakeFlags=""
sharedLibExt=".so"
executableExt=""
testParams=""
chaosParams=""
targetDir="$scriptDir/bin/$buildType"
javaResourceRoot="$scriptDir/java/src/main/resources"
javaResourceBin="$javaResourceRoot/com/burningmime/setmatch/bin"
targetOs="linux"

# parse options
while [[ "$#" -gt 0 ]]; do
    case "$1" in
        -mingw)
            toolchain="mingw"
            sharedLibExt=".dll"
            executableExt=".exe"
            targetDir="$targetDir-windows"
            targetOs="windows"
            if [[ -z "$isWsl" ]]; then skipTests="1"; fi
            skipValgrind="1"
            shift;;
        -osxcross)
            toolchain="osxcross"
            sharedLibExt=".dylib"
            targetDir="$targetDir-darwin"
            targetOs="darwin"
            skipTests="1"
            shift;;
        -coverage)
            # TODO cmake doesn't seem to rebuild when this is turned off; find out why
            cmakeFlags="${cmakeFlags} -DSETMATCH_COVERAGE=ON"
            doCoverage="1"
            shift;;
        -no-valgrind)
            skipValgrind="1"
            shift;;
        -no-tests)
            skipTests="1"
            shift;;
        -test-chaos)
            chaosParams="--run_test=+ChaosTest"
            shift;;
        -test=*)
            testParams="${testParams} --run_test=${1#*=}"
            shift;;
        --)
            shift
            while [[ "$#" -gt 0 ]]; do
                extraArgs="${extraArgs} \"$1\""
                shift
            done;;
        *)
            echoerr "Unknown argument $1"
            printUsage
            exit 1;;
    esac
done

if [[ "$buildType" == "debug" || "$buildType" == "release" ]]; then

    if [[ "$buildType" == "debug" ]]; then
        cmakeFlags="-DCMAKE_BUILD_TYPE=Debug $cmakeFlags"
    else
        cmakeFlags="-DCMAKE_BUILD_TYPE=Release $cmakeFlags"
    fi

    # add toolchain file if cross-compiling
    if [[ "$toolchain" != "" ]]; then
        if [[ "$buildType" == "debug" ]]; then
            echoerr "Debug builds don't work on MinGW or osxcross."
            exit 1
        fi
        cmakeFlags="-DCMAKE_TOOLCHAIN_FILE=$scriptDir/etc/$toolchain.txt $cmakeFlags"
    fi

    # do the build itself
    mkdir -p "$targetDir"
    cd "$targetDir"
    sayThenDo cmake $cmakeFlags $extraArgs "$scriptDir"
    make -j$(nproc)

    # Strip the shared libs -- I tried doing this as a cmake postbuild step and it worked fine in windows/linux,
    # but it's a multi-step process for osx, which gets ugly. We do this mostly to reduce the size of the JAR file,
    # which would otherwise be like 5MB
    if [[ "$buildType" == "release" ]]; then
        if [[ "$toolchain" != "osxcross" ]]; then
            strip --strip-unneeded "$targetDir/libsetmatch$sharedLibExt"
        else
            x86_64-apple-darwin15-strip -u "$targetDir/libsetmatch$sharedLibExt" -o "$targetDir/libsetmatch$sharedLibExt.strip"
            rm "$targetDir/libsetmatch$sharedLibExt"
            mv "$targetDir/libsetmatch$sharedLibExt.strip" "$targetDir/libsetmatch$sharedLibExt"
        fi
    fi

    # unit tests w/ code coverage
    if [[ "$doCoverage" == "1" ]]; then
        export GCOV_PREFIX="$targetDir/CMakeFiles"
        export GCOV_PREFIX_STRIP=$(echo "$GCOV_PREFIX" | grep -o "/" | wc -l)
        lcov -z -d "$GCOV_PREFIX"
        rm -rf "$scriptDir/coverage" "$targetDir/coverage"
        sayThenDo "$targetDir/setmatch-tests$executableExt" $testParams $chaosParams
        lcov -c -o "$targetDir/lcov_all.info" -d "$GCOV_PREFIX"
        lcov -e "$targetDir/lcov_all.info" -o "$targetDir/lcov_sm.info" "$scriptDir/src/main/"*
        genhtml -o "$targetDir/coverage" "$targetDir/lcov_sm.info"
        mv "$targetDir/coverage" "$scriptDir/coverage"

    # plain unit tests
    elif [[ -z "$skipTests" ]]; then

        # unit tests w/ valgrind -- do this first
        # TODO less repitition of test cases. just segregate into "with valgrind" and "no valgrind" groups
        if [[ -z "$skipValgrind" && "$buildType" == "debug" ]]; then
            sayThenDo valgrind --track-origins=yes --leak-check=full --error-exitcode=1 --suppressions="$scriptDir/etc/valgrind.supp" \
                "$targetDir/setmatch-tests$executableExt" --run_test=!@novalgrind $testParams
        fi

        # tests without valgrind (mainly the chaos test)
        sayThenDo "$targetDir/setmatch-tests$executableExt" $testParams $chaosParams

#        # When something segfaults in Windows, it just crashes silently. So all this extra stuff is here to make
#        # sure that if it has a nonzero exit code, we actually print an error
#        echogood "$targetDir/cpp-api-example$executableExt"
#        "$targetDir/cpp-api-example$executableExt" || exampleExitCode=$? && true
#        if [[ ! -z "$exampleExitCode" && "$exampleExitCode" != "0" ]]; then
#            echoerr "C++ example failed with exit code $exampleExitCode"
#            exit "$exampleExitCode"
#        fi
#        echogood "$targetDir/cpp-api-example$executableExt"
#        "$targetDir/c-api-example$executableExt" || exampleExitCode=$? && true
#        if [[ ! -z "$exampleExitCode" && "$exampleExitCode" != "0" ]]; then
#            echoerr "C example failed with exit code $exampleExitCode"
#            exit "$exampleExitCode"
#        fi

    else
        echoerr "Skipping unit tests"
    fi

    # Linux has some versioning issues, so display here the highest referenced glibc
    if [[ "$targetOs" == "linux" ]]; then
      echoerr "Beware the versions (top 5 shown):"
      echoerr "=================================="
      echoerr "$(objdump -T "$targetDir/libsetmatch.so" | grep -Po 'GLIBC_[0-9.]*.*$' | sort -V | tail -n5)"
      echoerr "=================================="
    fi

    # copy JNI lib to Java resource dir
    # only do this in release mode for now, mostly because it's too slow to run the unit tests w/ debug lib
    # really the better solution is just to not run that test if lib is debug
    if [[ "$buildType" == "release" ]]; then
        mkdir -p "$javaResourceBin/$targetOs"
        echogood "Copying $targetDir/libsetmatch$sharedLibExt to $javaResourceBin/$targetOs/libsetmatch$sharedLibExt"
        cp "$targetDir/libsetmatch$sharedLibExt" "$javaResourceBin/$targetOs/libsetmatch$sharedLibExt"
    fi

# Java build requires jni lib to have been built first, and basically lets Maven take care of most of it
elif [[ "$buildType" == "java" ]]; then

    winLib="$javaResourceBin/windows/libsetmatch.dll"
    linLib="$javaResourceBin/linux/libsetmatch.so"
    macLib="$javaResourceBin/darwin/libsetmatch.dylib"

    if [[ -f "$winLib" ]]; then winSupport=1; fi
    if [[ -f "$linLib" ]]; then linSupport=1; fi
    if [[ -f "$macLib" ]]; then macSupport=1; fi

    if [[ -z "$winSupport" && -z "$linSupport" && -z "$macSupport" ]]; then
        echoerr "Did not find compiled JNI library. Looked for:"
        echoerr "    $linLib"
        echoerr "    $winLib"
        echoerr "    $macLib"
        echoerr "Compile the JNI library with \".\build.sh release\" first before compiling Java"
        exit 1
    fi
    if [[ -z "$linSupport" ]]; then
        skipTests=1
        echoerr "Did not find $linLib"
        echoerr "Compiled JAR will not work on Linux"
        echoerr "Skipping JUnit tests because of missing Linux library"
    fi
    if [[ -z "$winSupport" ]]; then
        echoerr "Did not find $winLib"
        echoerr "Compiled JAR will not work on Windows"
    fi
    if [[ -z "$macSupport" ]]; then
        echoerr "Did not find $macLib"
        echoerr "Compiled JAR will not work on OSX"
    fi

    # construct maven arguments
    mvnArgs="clean package"
    if [[ ! -z "$skipTests" ]]; then
        mvnArgs="${mvnArgs} -DskipTests"
    fi

    # actually run it (needs to be done from the pom directory)
    cd "$scriptDir/java"
    sayThenDo mvn $mvnArgs $extraArgs

# Delete all artifacts (including cmake artifacts)
elif [[ "$buildType" == "clean" ]]; then
    rm -rf "$scriptDir/artifacts"
    rm -rf "$scriptDir/bin"
    rm -rf "$scriptDir/apidocs"
    rm -rf "$scriptDir/java/target"
    rm -rf "$scriptDir/doc/pages"
    rm -rf "$scriptDir/doc/m.math.cache"
    rm -rf "$scriptDir/etc/libsetmatch.def"
    rm -rf "$javaResourceRoot"
    rm -rf "$scriptDir/coverage"

elif [[ "$buildType" == "docs" ]]; then

    rm -rf "$scriptDir/artifacts/site"

    # C++ docs with doxygen
    rm -rf "$scriptDir/apidocs"
    cd "$scriptDir/etc"
    python3 "$scriptDir/doc/m.css/doxygen/dox2html5.py" "$scriptDir/etc/Doxyfile"
    mkdir -p "$scriptDir/artifacts/site/doxygen"
    cp -ar "$scriptDir/apidocs/html/"* "$scriptDir/artifacts/site/doxygen/"
    rm -rf "$scriptDir/apidocs"

    # Javadocs with maven
    cd "$scriptDir/java"
    mvn javadoc:javadoc -P release
    mkdir -p "$scriptDir/artifacts/site/javadoc"
    cp -ar "$scriptDir/java/target/site/apidocs/"* "$scriptDir/artifacts/site/javadoc/"

    # Website with Pelican (has its own build script file)
    cd "$scriptDir"
    chmod u+x "$scriptDir/doc/build.sh"
    "$scriptDir/doc/build.sh"
    cp -ar "$scriptDir/doc/pages/"* "$scriptDir/artifacts/site/"
    rm -rf "$scriptDir/doc/pages/"

# "Official" build in docker
elif [[ "$buildType" == "full" ]]; then
    # Make sure we have correct permissions on this file
    chmod u+x "$scriptFile"

    # First, a full clean
    "$scriptFile" clean

    # Then run debug build because if something breaks, we get a friendly stack trace
    # Also do chaos test because why not it should run on every build anyway
    "$scriptFile" debug -test-chaos

    # Do real build
    "$scriptFile" release
    "$scriptFile" release -mingw
    "$scriptFile" release -osxcross
    "$scriptFile" java -- -P release
    "$scriptFile" docs

    # Copy artifacts to local bin and include directories for upload to gitlab
    cd "$scriptDir"
    mkdir -p artifacts/bin
    cp "bin/release/libsetmatch.so" "artifacts/bin/"
    cp "bin/release-windows/libsetmatch.dll" "artifacts/bin/"
    cp "etc/libsetmatch.lib" "artifacts/bin/"
    cp "bin/release-darwin/libsetmatch.dylib" "artifacts/bin/"
    cp "java/target/$(ls java/target | grep -E 'setmatch-[0-9.]+(-SNAPSHOT)?\.jar')" "artifacts/bin/"
    mkdir -p artifacts/include
    cp "$scriptDir/src/public/setmatch.h" "$scriptDir/artifacts/include/setmatch.h"
    cp "$scriptDir/src/public/setmatch.hpp" "$scriptDir/artifacts/include/setmatch.hpp"
    cp "$scriptDir/LICENSE" "$scriptDir/artifacts/"

else
    echoerr "Unknown build type: $buildType" >&2
    printUsage
    exit 1
fi
